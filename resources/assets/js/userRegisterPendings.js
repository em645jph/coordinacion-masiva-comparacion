/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var loaded = false;
var gkey;
$(document).ready(function () {
  drawGrid();
  getTaxIdTypeCombo();
  getTaxGroupsCombo();

  $("#tbUsersPendings tbody").on("click", "tr", function () {
    $(this).toggleClass("selected");
  });

  $("#btnSendReject").click(function () {
    denyRequest();
  });

  $("#btnApprove").click(function () {
    approveRequest();
  });
});

function initKnockout() {
  viewModel = {
    pendingUsers: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function drawGrid() {
  processingModal();

  $.ajax({
    type: "GET",
    url: url_application + "/User/getUsersPending",
    success: function (data, statusCode) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadPendingUsersCb(data);
      Swal.close();
    },
    fail: function (data, statusCode) {
      showModal("", "Status code:" + statusCode, "error");
    },
  });
}

function loadPendingUsersCb(data) {
  //viewModel.createdMenu.removeAll();
  deleteDataTables();
  viewModel.pendingUsers(data);
  newDataTableMultiple("tbUsersPendings", null, true, true, false, "single");
}

function getTaxGroupsCombo() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getTaxGroupsCombo",
    success: function (data) {
      $("#selectTaxGroup").append(data);
    },
    fail: function (data) {
      showModal("", data, "error");
    },
  });
}

function getTaxIdTypeCombo() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getTaxIdTypeCombo",
    success: function (data) {
      $("#selectDocumentType").append(data);
    },
    fail: function (data) {
      showModal("", data, "error");
    },
  });
}

function manageRequest() {
  processingModal();
  $.ajax({
    type: "PUT",
    url: url_application + "/User/applyRegister",
    data: JSON.stringify(inputsform),
    success: function (data) {
      showModal("", data, "success");
      drawGrid();
    },
    fail: function (data) {
      showModal("", data, "error");
    },
  });
}

function getFormData() {
  var taxGroup = $("#").val();
  var taxType = $("#").val();
  var isResidentCaba = $("#").val();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function modalApprove(input) {
  $("#modalApprove").modal({
    backdrop: "static",
  });
  completeDataFromRequest(input.getAttribute("rowid"));
}

function modalDocuments() {
  $("#frameDoc").attr("src", "");
  $("#modalDocuments").modal({
    backdrop: "static",
  });
}

function modalReject(input) {
  $("#modalReject").modal({
    backdrop: "static",
  });
  gkey = input.getAttribute("reqGkey");
}

function showConfirmModal(title, text, type, callback) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callback;
    }
  });
}

function denyRequest(input) {
  //showConfirmModal("","Esta seguro de rechazar la solicitud?","warning", url_application + "/");
  processingModal();

  var data = new Object();
  data.action = "REJECT";
  data.requestGkey = gkey;

  if ($("#reasonText").val() == null || $("#reasonText").val() == "") {
    showModal("", "Por favor, complete un motivo.", "warning");
    return;
  }

  data.reason = $("#reasonText").val();

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Customer/manageUserRequest",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalCallback("", message, "success", window.location.href);
  });

  request.fail(function (message) {
    showModal(
      "",
      "Se rechazo correctamente, pero no se pudo enviar el Correo de información.",
      "error",
    );
  });
}

function getDocumentsByRegisterId(input) {
  processingModal();
  $.ajax({
    type: "GET",
    url:
      url_application +
      "/Document/getDocumentsByRegisterId/" +
      input.getAttribute("reggkey"),
    success: function (data) {
      Swal.close();
      if (data == null || data == "") {
        Swal.fire("", "Sin resultados", "info");
        return;
      } else {
        $("#listDocuments").html("").append(data);
        modalDocuments();
      }
    },
    fail: function (data) {
      showModal("", data, "error");
    },
  });
}

function loadDocumentFrame(id) {
  processingModal();
  $("#frameDoc").attr(
    "src",
    url_application + "/Document/getServerDocumentByKey/" + id,
  );
  Swal.close();
}

function completeDataFromRequest(id) {
  var rowData = viewModel.pendingUsers()[id];
  var d = rowData.documentNbr;
  console.log(JSON.stringify(rowData));
  $("#documentNbrTitle").text(
    "CUIT: " +
      d.substring(0, 2) +
      "-" +
      d.substring(2, 10) +
      "-" +
      d.substring(10, 11),
  );
  $("#gkey").val("").val(rowData.reqGkey);
  $("#formBusinessType").val("").val(rowData.businessType);
  $("#formCity").val("").val(rowData.city);
  $("#formEmailComex").val("").val(rowData.emailComex);
  $("#formEmailAddress").val("").val(rowData.emailAddress);
  $("#formNote").val("").val(rowData.notes);
  $("#formFullname").val("").val(rowData.fullname);
  $("#formAddress").val("").val(rowData.address);
  $("#formEmailBilling").val("").val(rowData.emailManagement);
}

function approveRequest() {
  //showConfirmModal("","Esta seguro de rechazar la solicitud?","warning", url_application + "/");
  processingModal();

  var data = new Object();
  data.gkey = $("#gkey").val();
  data.full_name = $("#formFullname").val();
  data.requestType = "CREATE_CUSTOMER";
  data.billingIsCabaResident = $("#selectIsCabaResident").val();
  data.billingTaxGroupId = $("#selectTaxGroup").val();
  data.billingTaxIdType = $("#selectDocumentType").val();
  data.businessType = $("#formBusinessType").val();
  data.city = $("#formCity").val();
  data.comexEmailAddress = $("#formEmailComex").val();
  data.managementEmailAddress = $("#formEmailBilling").val();
  data.notes = $("#selectTaxGroup").val();
  console.log(JSON.stringify(data));
  var request = $.ajax({
    type: "POST",
    url: url_application + "/Customer/approveRequest",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalCallback("", message, "success", window.location.href);
  });

  request.fail(function (message) {
    showModal("", message.responseText, "error");
  });
}
