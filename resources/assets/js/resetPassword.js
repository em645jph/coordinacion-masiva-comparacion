/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  $("#btnResetPassword").click(function (e) {
    e.preventDefault();
    resetPasswordByEmail();
  });
});

function resetPasswordByEmail() {
  var obj = new Object();
  obj.login = $("#login").val();
  if (obj.login == null || obj.login == "" || obj.login == undefined) {
    showModal(
      "Atención!",
      "Por favor, completar con un CUIT/CUIL correcto.",
      "warning",
    );
  } else {
    processingModal();
    var request = $.ajax({
      url: url_application + "/User/resetPasswordByEmail",
      type: "POST",
      data: JSON.stringify(obj),
      contentType: "application/json",
    });

    request.done(function (message) {
      showModalCallback("Atencion!", message, "success", url_application);
    });

    request.fail(function (jqXHR, textStatus) {
      showModal("Atencion!", jqXHR.responseText, "error");
    });
  }
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
