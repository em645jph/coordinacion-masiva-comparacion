var loaded = false;
var table;
$(document).ready(function () {
  Swal.fire(
    "",
    "Recuerde que si ha coordinado servicios adicionales como verificación, scanner o pesada; deberá generar el presupuesto una vez finalizada la operativa coordinada. De lo contrario quedarán gastos impagos.",
    "info",
  );

  $("#btnGenerate").hide();
  $("#btnFindEntity").click(function () {
    $("#btnGenerate").hide();
    findEntityById();
    $('[data-toggle="tooltip"]').tooltip();
  });

  $("#tbUnitsByBl tbody").on("click", "tr", function () {
    if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
      $("#btnGenerate").show();
    } else {
      $("#btnGenerate").hide();
    }
  });

  $("#btnGenerate").click(function () {
    generateDraftByUnitsIds();
  });
});

function toggleSpinner(input) {
  //Reemplazamos el boton de descarga por el spinner
  if ($("#btnDownload").is(":visible")) {
    $("#btnDownload").hide();
    $("#divSpinner").show();
    getPdfByDraftId(input.getAttribute("invNbr"));
  } else {
    $("#btnDownload").show();
    $("#divSpinner").hide();
  }
}

function initKnockout() {
  viewModel = {
    UnitsByBl: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  viewModel.UnitsByBl.removeAll();
  deleteDataTables();
  viewModel.UnitsByBl(data);
  table = $("#tbUnitsByBl").DataTable({
    select: "multi",
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
    } /*
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],*/,
    bDestroy: true,
    bFilter: true,
    bSort: true,
    sScrollY: "30vh",
  });
  table
    .on("select", function (e, dt, type, indexes) {
      if (type === "row") {
        var rows = table.rows(indexes).nodes().to$();
        //var ignore = node.hasClass( 'ignoreme' );
        $.each(rows, function () {
          if ($(this).hasClass("ignoreme")) table.row($(this)).deselect();
        });
      }
      if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    })
    .on("deselect", function (e, dt, type, indexes) {
      if ($("#tbUnitsByBl").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    });
}

function findEntityById() {
  processingModal();

  var type = $("#selectType").children("option:selected").val();
  var value = $("#valueNbr").val();
  if (value == "" || type == null || type == "") {
    showModal("", "Por favor, complete los campos requeridos.", "error");
    return;
  }

  var request = $.ajax({
    url: url_application + "/Billing/findUnitsByType/" + type + "/" + value,
    type: "GET",
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnitsByBl").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function generateDraftByUnitsIds() {
  console.log("Armamos el string para enviar al backend..");
  processingModal();

  var dataArr = [];
  var apptTimeArr = [];
  var rows = $("tr.selected");
  var rowData = $("#tbUnitsByBl").DataTable().rows(rows).data();
  $.each($(rowData), function (key, value) {
    dataArr.push(value["1"]); //"name" being the value of your first column.
    apptTimeArr.push(value["8"]); //"name" being the value of your first column.
  });
  console.log(dataArr);

  if ($("#tbUnitsByBl").DataTable().rows(rows).count() < 1) {
    showModal(
      "",
      "Por favor, seleccione al menos un contenedor para facturar.",
      "error",
    );
    return;
  }

  var obj = new Object();
  obj.formUnits = dataArr;
  obj.formCategory = "IMPRT";
  obj.formApptTime = apptTimeArr;

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/createInvoiceDraftByUnits",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    if (data.status == "OK") {
      var url = url_application + "/Payment/paymentSelector/" + data.msg;
      showModalHtml(
        "",
        "<div id='divPdf'><div style='display: none;' id='divSpinner' class='spinner-border text-primary m-2' role='status'><span class='sr-only'>Loading...</span>" +
          "</div><button type='button' onclick='toggleSpinner(this);' id='btnDownload' invNbr='" +
          data.msg +
          "' class='btn btn-info'>DESCARGAR PDF - " +
          data.msg +
          "</button></div><hr>",
        "success",
        url,
      );
      //showTimerModal("",data.msg,2000,reloadGrid);
    } else {
      Swal.fire("", data, "error");
    }
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function showModalHtml(title, text, type, callbackUrl) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    html: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, pagar!",
    cancelButtonText: "No, gracias.",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callbackUrl;
    } else {
      window.location.reload();
    }
  });
}

function reloadGrid() {
  $("#btnFindEntity").click();
}

function getPdfByDraftId(draft) {
  //processingModal();

  if (draft == null || draft == "" || draft == undefined) {
    Swal.fire("", "Error al obtener documento. Reintente nuevamente.", "error");
    return;
  }

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + draft,
    timeout: 600000,
  });

  request.done(function (data) {
    $("#btnDownload").show();
    $("#divSpinner").hide();
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showModal("", "No se pudo abrir el PDF", "error");
      }
    }
    //Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}
