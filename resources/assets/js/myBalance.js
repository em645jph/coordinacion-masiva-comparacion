/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {});

function getBalanceAccountByTaxId() {
  processingModal();
  var data = new Object();
  data.type = "SLDF";

  var request = $.ajax({
    url: url_application + "/Customer/getBalanceAccountByTaxId",
    type: "POST",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {});

  request.fail(function (jqXHR, textStatus) {
    showModal("", "Error al ejecutar el Pago. Reintente mas tarde", "error");
  });
}
