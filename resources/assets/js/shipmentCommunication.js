/**
 * Author: Dionel Delgado
 *
 */

const APPROVE_CUSTOMS_LOAD = "APPROVE_CUSTOMS_LOAD";
const BLOCK_CUSTOMS_LOAD = "BLOCK_CUSTOMS_LOAD";
const UNBLOCK_CUSTOMS_LOAD = "UNBLOCK_CUSTOMS_LOAD";
var table = null;
$(document).ready(function () {
  initKnockout();
  cleanSearchParams();
  loadInitCombos();
  initHtmlElements();
});

function initHtmlElements() {
  initCombos();
  initButtons();
  initToggleColumns();
  initApptModal();
}

function initApptModal() {
  $("#shipmentCommunicationLockModel").on("shown.bs.modal", function () {
    clearStyle();
    $("#noteLock").val("");
    $("#typeLock").val("");
    $("#cbResult").val("");
    supportUnitViewModel.selectedTypeLock(null);
    supportUnitViewModel.selectedTypeResult(null);
  });

  $("#shipmentCommunicationLockModel").on("hidden.bs.modal", function () {
    clearStyle();
    $("#noteLock").val("");
    $("#typeLock").val("");
    $("#cbResult").val("");
    supportUnitViewModel.permisionAction(false);
    supportUnitViewModel.selectedTypeLock(null);
    supportUnitViewModel.selectedTypeResult(null);
    adjustDataTableColumns();
  });

  $("#shipmentCommunicationApproveModel").on("shown.bs.modal", function () {
    clearStyle();
    $("#noteApprove").val("");
  });

  $("#shipmentCommunicationApproveModel").on("hidden.bs.modal", function () {
    clearStyle();
    $("#noteApprove").val("");
    supportUnitViewModel.permisionAction(false);
    adjustDataTableColumns();
  });

  $("#shipmentCommunicationUnLockModel").on("shown.bs.modal", function () {
    clearStyle();
    $("#noteUnlock").val("");
  });

  $("#shipmentCommunicationUnLockModel").on("hidden.bs.modal", function () {
    clearStyle();
    $("#noteUnlock").val("");
    supportUnitViewModel.permisionAction(false);
    supportUnitViewModel.holdGkeyModal(null);
    supportUnitViewModel.holdReferenceIdModal(null);
    adjustDataTableColumns();
  });

  $("#shipmentCommunicationListEventLockModel").on(
    "shown.bs.modal",
    function () {
      clearStyle();
      adjustDataTableColumns();
    },
  );

  $("#shipmentCommunicationListEventLockModel").on(
    "hidden.bs.modal",
    function () {
      clearStyle();
      supportUnitViewModel.unitModal(null);
      supportUnitViewModel.permissionModal(null);
      showEventTable([]);
      adjustDataTableColumns();
    },
  );

  $("#shipmentCommunicationHistoryModel").on("shown.bs.modal", function () {
    clearStyle();
    adjustDataTableColumns();
  });

  $("#shipmentCommunicationHistoryModel").on("hidden.bs.modal", function () {
    clearStyle();
    deleteHistoryEventDataTable();
    supportUnitViewModel.historyEventTitle(null);
    adjustDataTableColumns();
  });

  $("#shipmentCommunicationActionByPermModal").on(
    "hidden.bs.modal",
    function () {
      clearStyle();
      clearActionByPermModalInput();
      adjustDataTableColumns();
    },
  );
}

function initCombos() {
  loadInitCombos();
}

function initButtons() {
  $("#btnSearch").click(function () {
    showProcessingModal();
    findUnit(true, 1, null);
  });

  $("#formSearchUnits").submit(function (e) {
    e.preventDefault();
    true, 1;
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#searchStatus").multiselect({
    enableHTML: false,
    buttonWidth: "100%",
    includeSelectAllOption: false,
    selectAllText: " Seleccionar todo",
    buttonClass: "form-control-custom",
    nonSelectedText: "Sin seleccionar",
    allSelectedText: "Todos los estados seleccionados",
  });

  $("#btnApplyActionByPerm").click(function () {
    applyActionByPermission();
  });
}

function initKnockout() {
  supportUnitViewModel = {
    searchFilters: ko.observableArray([]),
    selectedFilter: ko.observable(),
    selectedTypeLock: ko.observable(),
    selectedTypeResult: ko.observable(),
    permisionAction: ko.observable(false),
    viewBottomSearh: ko.observable(false),
    searchVessels: ko.observableArray([]),
    searchScStatus: ko.observableArray([]),
    searchScTypesLock: ko.observableArray([]),
    searchScTypesResult: ko.observableArray([]),
    searchScActionType: ko.observableArray([]),
    yesNoCb: ko.observableArray([]),
    unitTable: ko.observableArray([]),
    eventTable: ko.observableArray([]),
    historyTable: ko.observableArray([]),
    unitServiceTable: ko.observableArray([]),
    unitModal: ko.observable(),
    obVisitIdModal: ko.observable(),
    obVesselModal: ko.observable(),
    permissionModal: ko.observable(),
    statusModal: ko.observable(),
    gkeyModal: ko.observable(),
    client_perm_gkeyModal: ko.observable(),
    holdGkeyModal: ko.observable(),
    holdReferenceIdModal: ko.observable(),
    noteModal: ko.observable(),
    historyEventTitle: ko.observable(),
    searchVisitId: ko.observable(),
    searchVisitInfo: ko.observable(),
    searchPermission: ko.observable(),
    actionVisitId: ko.observable(),
    viewApproveModal: function (obj) {
      viewModelApprove(
        obj.gkey,
        obj.id,
        obj.client_perm_gkey,
        obj.client_perm_ref_id,
        obj.unit_status,
        obj.ob_visit_id,
        obj.obVesselInfo,
      );
    },
    viewLockModal: function (obj) {
      viewModelLock(
        obj.gkey,
        obj.id,
        obj.client_perm_gkey,
        obj.client_perm_ref_id,
        obj.unit_status,
        obj.ob_visit_id,
        obj.obVesselInfo,
      );
    },
    viewUnLockModal: function (obj) {
      viewModelUnLock(obj.gkey, obj.reference_id);
    },
    viewHistoryModal: function (unitPermissionToView) {
      viewHistoryEventsIntern(unitPermissionToView);
    },
    actionLock: function (obj) {
      updateShipmentCommunicationStatus(true, BLOCK_CUSTOMS_LOAD);
    },
    actionUnLock: function (obj) {
      updateShipmentCommunicationStatus(true, UNBLOCK_CUSTOMS_LOAD);
    },
    actionApprove: function (obj) {
      updateShipmentCommunicationStatus(true, APPROVE_CUSTOMS_LOAD);
    },
    actiongetListEventsLock: function (obj) {
      viewListEventLockModel(
        obj.gkey,
        obj.id,
        obj.client_perm_gkey,
        obj.client_perm_ref_id,
        obj.unit_status,
        obj.ob_visit_id,
        obj.obVesselInfo,
      );
    },
    actionExport: function (obj) {
      exportTable();
    },
    actionDownloadPDF: function () {
      getReportTotalByVessels();
    },
    actionByPermission: function () {
      validateActionByPermission();
    },
  };
  ko.applyBindings(supportUnitViewModel);
}

function loadInitCombos() {
  showProcessingModal();

  $.ajax({
    type: "GET",
    url: url_application + "/ShipmentCommunicationManagement/loadInitCombos/",
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }

      loadOptionFilterShipmentCommunication(data);
      loadVesselShipmentCommunication(data);
      loadStatusShipmentCommunication(data);
      loadlockTypesShipmentCommunication(data);
      loadResultTypesShipmentCommunication(data);
      loadActionTypesShipmentCommunication(data);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function loadEventsLock(gkey, referenceId) {
  var searchParam = {
    gkey: gkey,
    referenceId: referenceId,
  };
  //console.log("searchParam:" + JSON.stringify(searchParam));
  $.ajax({
    type: "POST",
    url:
      url_application +
      "/ShipmentCommunicationManagement/lisEventByPermisions/",
    data: JSON.stringify(searchParam),
    contentType: "application/json",
    success: function (data, statusCode, xhr) {
      //console.log("Result :" + JSON.stringify(data));
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      } else {
        showdivTableEvent();
        showEventTable(data);
      }
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function knowDateDocCutOff(visitId) {
  var param = {
    visitId: visitId,
  };
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/ShipmentCommunicationManagement/knowCutOff/",
    data: param,
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      } else {
        supportUnitViewModel.permisionAction(data);
      }
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
    },
  });
}

function loadOptionFilterShipmentCommunication(data) {
  supportUnitViewModel.searchFilters(
    data["optionFilterShipmentCommunicationList"],
  );
}

function loadVesselShipmentCommunication(data) {
  supportUnitViewModel.searchVessels(data["vesselList"]);
}

function loadStatusShipmentCommunication(data) {
  var statusData = [];
  data["statusShipmentCommunicationList"].forEach((element) =>
    statusData.push({ label: element.key, value: element.description }),
  );
  $("#searchStatus").multiselect("dataprovider", statusData);
}

function loadlockTypesShipmentCommunication(data) {
  supportUnitViewModel.searchScTypesLock(
    data["lockTypesShipmentCommunicationList"],
  );
}

function loadResultTypesShipmentCommunication(data) {
  supportUnitViewModel.searchScTypesResult(
    data["resultTypesShipmentCommunicationList"],
  );
}

function loadActionTypesShipmentCommunication(data) {
  supportUnitViewModel.searchScActionType(
    data["actionTypeShipmentCommunicationList"],
  );
}

function findUnit(showProcessing, btn, msgText) {
  //console.log("findUnit");
  var errorMsg = "";
  if (errorMsg == "") {
    var searchParam = {
      vesselId: $("#searchVessel option:selected").val(),
      unitId: $("#searchContainer").val(),
      customersBoardingPermissionId: $("#searchBoardingPermission").val(),
      shipmentCommunicationStatus: $("#searchStatus").val().join(),
    };
    supportUnitViewModel.searchVisitId(
      $("#searchVessel option:selected").val(),
    );
    supportUnitViewModel.searchVisitInfo(
      $("#searchVessel option:selected").text(),
    );
    supportUnitViewModel.searchPermission($("#searchBoardingPermission").val());
    console.log("searchParam:" + JSON.stringify(searchParam));
    let isNotNullVessel =
      searchParam.vesselId == "" || searchParam.vesselId == undefined
        ? true
        : false;
    let isNotNullUnit =
      searchParam.unitId == "" || searchParam.unitId == undefined
        ? true
        : false;
    let isNotNullPermissionId =
      searchParam.customersBoardingPermissionId == "" ? true : false;
    let isNotNullStatus =
      searchParam.shipmentCommunicationStatus == "" ? true : false;

    if (
      isNotNullVessel &&
      isNotNullUnit &&
      isNotNullPermissionId &&
      isNotNullStatus
    ) {
      console.log(
        isNotNullVessel +
          " / " +
          isNotNullUnit +
          " / " +
          isNotNullPermissionId +
          " / " +
          isNotNullStatus,
      );
      showUnitTable([], null, null);
      closeProcessingModal();
    } else {
      $.ajax({
        type: "POST",
        url:
          url_application +
          "/ShipmentCommunicationManagement/findUnitsShipmentCommunication/",
        data: JSON.stringify(searchParam),
        contentType: "application/json",
        datatype: "json",
        success: function (data, statusCode, xhr) {
          //console.log("estado del llamado =" +statusCode + " / " +  data);
          var ct = xhr.getResponseHeader("content-type") || "";
          if (ct.indexOf("html") > -1) {
            showModalSessionClose(
              "Session Expirada",
              "Disculpe, por seguridad la session ha sido cerrada.",
              "info",
            );
          } else if (typeof msg != "undefined" && !resultIsOk(data)) {
            showErrorHtmlMsg(msg);
          } else {
            if (showProcessing) {
              showDivTable();
              if (btn == null && msgText != null) {
                showUnitTable(
                  data,
                  table.page.info().page,
                  $(".dataTables_scrollBody").scrollTop(),
                );
                closeProcessingModal();
                if (msgText.includes("Bloqueo")) {
                  $("#shipmentCommunicationLockModel").modal("hide");
                }
                if (msgText.includes("Desbloqueo")) {
                  $("#shipmentCommunicationUnLockModel").modal("hide");
                }
                if (msgText.includes("Aprobación")) {
                  $("#shipmentCommunicationApproveModel").modal("hide");
                }
                if (msgText.includes("Contenedores actualizados"))
                  showSuccessHtmlMsg("Listo", msgText);
                else $.notify(msgText, "success");
              } else {
                showUnitTable(data, null, null);
                closeProcessingModal();
              }
            }
          }
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    }
  } else showErrorMsg(errorMsg);
}

function updateShipmentCommunicationStatus(showProcessing, action) {
  //console.log("updateShipmentCommunicationStatus");
  var errorMsg = "";
  let msgTitle = "";
  let msgText = "";
  let typeError = $("#typeLock option:selected").val();
  //console.log("Tipo de error = "+ typeError);
  if (errorMsg == "" && typeError != null) {
    if (showProcessing) showProcessingModal();
    var searchParam = {
      gkey: supportUnitViewModel.gkeyModal(),
      id: supportUnitViewModel.unitModal(),
      flagGkey: supportUnitViewModel.client_perm_gkeyModal(),
      referenceId: supportUnitViewModel.permissionModal(),
      notes:
        $("#noteLock").val() != ""
          ? $("#noteLock").val()
          : $("#noteUnlock").val() != ""
            ? $("#noteUnlock").val()
            : $("#noteApprove").val(),
      action: action,
      reason: supportUnitViewModel.selectedTypeLock(),
      result: supportUnitViewModel.selectedTypeResult(),
      obVessel: supportUnitViewModel.obVesselModal(),
      holdGkey: supportUnitViewModel.holdGkeyModal(),
      holdReferenceId: supportUnitViewModel.holdReferenceIdModal(),
    };
    //console.log("searchParam:" + JSON.stringify(searchParam));
    $.ajax({
      type: "POST",
      url:
        url_application +
        "/ShipmentCommunicationManagement/updateStatusUnitShipmentComunication/",
      data: JSON.stringify(searchParam),
      contentType: "application/json",
      success: function (data, statusCode, xhr) {
        var ct = xhr.getResponseHeader("content-type") || "";
        if (ct.indexOf("html") > -1) {
          showModalSessionClose(
            "Session Expirada",
            "Disculpe, por seguridad la session ha sido cerrada.",
            "info",
          );
        }
        if (data != "SUCCESS") {
          $("#shipmentCommunicationLockModel").modal("hide");
          $("#shipmentCommunicationUnLockModel").modal("hide");
          $("#shipmentCommunicationApproveModel").modal("hide");
          if (statusCode == 409) {
            $.notify(data, "error");
          } else {
            showErrorHtmlMsg(data);
          }
        } else {
          if (action == BLOCK_CUSTOMS_LOAD) {
            msgText =
              "Bloqueo del contendor " +
              supportUnitViewModel.unitModal() +
              " Realizado exitosamente!";
            $("#Note").val("");
          }
          if (action == UNBLOCK_CUSTOMS_LOAD) {
            msgText =
              "Desbloqueo del contendor " +
              supportUnitViewModel.unitModal() +
              " Realizado exitosamente!";
            loadEventsLock(
              supportUnitViewModel.gkeyModal(),
              supportUnitViewModel.permissionModal(),
            );
          }
          if (action == APPROVE_CUSTOMS_LOAD) {
            msgText =
              "Aprobación del contendor " +
              supportUnitViewModel.unitModal() +
              " Realizado exitosamente!";
          }

          findUnit(showProcessing, null, msgText);
          //$.notify(msgText, "success");
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function viewHistoryEventsIntern(unitPermissionToView) {
  supportUnitViewModel.historyEventTitle(
    "Historial de eventos " +
      unitPermissionToView.id +
      " - " +
      unitPermissionToView.client_perm_ref_id,
  );
  var param = {
    unitGkeyStr: unitPermissionToView.gkey,
    referenceId: unitPermissionToView.client_perm_ref_id,
  };
  $.ajax({
    type: "GET",
    url: url_application + "/ShipmentCommunicationManagement/getHistoryEvents/",
    data: param,
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      } else {
        showHistoryEventsTable(data);
        $("#shipmentCommunicationHistoryModel").modal();
      }
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
    },
  });
}

function hideDivTable() {
  $("#divTable").hide();
}

function showDivTable() {
  $("#divTable").show();
}

function hidedivTableEvent() {
  $("#divTableEvent").hide();
}

function showdivTableEvent() {
  $("#divTableEvent").show();
}

function cleanSearchParams() {
  $("#searchFilter").val("");
  $("#searchVessel").val("");
  $("#searchContainer").val("");
  $("#searchBoardingPermission").val("");
  $("#searchStatus").val("");
  showUnitTable([], null, null);
  supportUnitViewModel.viewBottomSearh(false);
  $("#searchStatus:selected").removeAttr("selected");
  supportUnitViewModel.selectedFilter(null);
}

function showUnitTable(data, pageNumber, scrollY) {
  supportUnitViewModel.unitTable.removeAll();
  deleteUnitDataTable();
  supportUnitViewModel.unitTable(data);
  table = $("#tbUnit")
    .DataTable({
      bDestroy: true,
      bFilter: true,
      deferRender: true,
      sScrollX: true,
      sScrollY: "60vh",
      scrollCollapse: true,
      paging: true,
      bPaginate: true,
      bSort: true,
      stateSave: true,
      drawCallback: function (settings) {
        //alert( 'DataTables has redrawn the table' );
        //console.log( api.rows( {page:'current'} ).data() );
      },
      oLanguage: {
        sEmptyTable: "No hay registros a mostrar",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sInfoFiltered: " - filtrado de _MAX_ registros",
        sLengthMenu: "Mostrar _MENU_ registros",
        oPaginate: {
          sPrevious: "Anterior",
          sNext: "Siguiente",
        },
        sSearch: "Buscar",
      },
      pageLength: 100,
      aoColumnDefs: [
        {
          aTargets: [2],
          bVisible: false,
        },
        {
          aTargets: [3],
          bVisible: false,
        },
        {
          aTargets: [4],
          bVisible: false,
        },
      ],
    })
    .search("")
    .draw();

  if (pageNumber != null && scrollY != null) {
    table.page(pageNumber).draw("page");
    $(".dataTables_scrollBody").scrollTop(scrollY);
  }
}

function showEventTable(data) {
  supportUnitViewModel.eventTable.removeAll();
  deleteEventDataTable();
  supportUnitViewModel.eventTable(data);
  var table = $("#tbevents").DataTable({
    bDestroy: true,
    bFilter: false,
    deferRender: false,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "_START_ - _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sInfoFiltered: " - filtrado de _MAX_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
    pageLength: 10,
  });
}

function showHistoryEventsTable(data) {
  supportUnitViewModel.historyTable.removeAll();
  supportUnitViewModel.historyTable(data);
  $("#unitPermissionHistoryTable").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sInfoFiltered: " - filtrado de _MAX_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
  });
}

function initToggleColumns() {
  $("a.toggle-vis").on("click", function (e) {
    e.preventDefault();
    var table = $("#tbUnit").DataTable();
    var column = table.column($(this).attr("data-column"));
    column.visible(!column.visible());
  });
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function deleteUnitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function deleteEventDataTable() {
  $("#tbevents").DataTable().clear();
  $("#tbevents").DataTable().destroy();
}

function deleteHistoryEventDataTable() {
  $("#unitPermissionHistoryTable").DataTable().clear();
  $("#unitPermissionHistoryTable").DataTable().destroy();
}

function viewModelLock(
  gkey,
  id,
  flagGkey,
  referenceId,
  status,
  ob_visit_id,
  ob_vessel_info,
) {
  knowDateDocCutOff(ob_visit_id);
  supportUnitViewModel.gkeyModal(gkey), supportUnitViewModel.unitModal(id);
  supportUnitViewModel.client_perm_gkeyModal(flagGkey),
    supportUnitViewModel.permissionModal(referenceId);
  supportUnitViewModel.statusModal(status);
  supportUnitViewModel.obVesselModal(ob_vessel_info);
  $("#shipmentCommunicationLockModel").modal();
}

function viewModelApprove(
  gkey,
  id,
  flagGkey,
  referenceId,
  status,
  ob_visit_id,
  ob_vessel_info,
) {
  knowDateDocCutOff(ob_visit_id);
  supportUnitViewModel.gkeyModal(gkey), supportUnitViewModel.unitModal(id);
  supportUnitViewModel.client_perm_gkeyModal(flagGkey),
    supportUnitViewModel.permissionModal(referenceId);
  supportUnitViewModel.statusModal(status);
  supportUnitViewModel.obVisitIdModal(ob_visit_id);
  supportUnitViewModel.obVesselModal(ob_vessel_info);
  $("#shipmentCommunicationApproveModel").modal();
}

function viewListEventLockModel(
  gkey,
  id,
  flagGkey,
  referenceId,
  status,
  ob_visit_id,
  ob_vessel_info,
) {
  supportUnitViewModel.gkeyModal(gkey);
  supportUnitViewModel.unitModal(id);
  supportUnitViewModel.permissionModal(referenceId);
  loadEventsLock(gkey, referenceId);
  supportUnitViewModel.statusModal(status);
  supportUnitViewModel.obVisitIdModal(ob_visit_id);
  supportUnitViewModel.obVesselModal(ob_vessel_info);
  $("#shipmentCommunicationListEventLockModel").modal();
}
function viewModelUnLock(holdGkey, holdReferenceId) {
  knowDateDocCutOff(supportUnitViewModel.obVisitIdModal());
  supportUnitViewModel.holdGkeyModal(holdGkey);
  supportUnitViewModel.holdReferenceIdModal(holdReferenceId);
  $("#shipmentCommunicationUnLockModel").modal();
}

function viewActionByPermissionModal() {
  supportUnitViewModel.actionVisitId(supportUnitViewModel.searchVisitId());
  $("#txtActionVessel").val(supportUnitViewModel.searchVisitInfo());
  $("#txtActionPermission").val(supportUnitViewModel.searchPermission());
  $("#shipmentCommunicationActionByPermModal").modal();
}

/*function exportTable(){
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "/" + month + "/" + year + " " + hour + ":" + mins;
    var a = document.createElement('a');
    var data_type = 'data:application/vnd.ms-excel;charset=utf-8';  
    var table_html = $('#tbUnit')[0].outerHTML;
    table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
    var css_html = '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
    a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' + table_html + '</body></html>');
    a.click();
}*/

function onChangeInputForm() {
  let unitId = $("#searchContainer").val() || "";
  let customersBoardingPermissionId =
    $("#searchBoardingPermission").val() || "";
  let shipmentCommunicationStatus = $("#searchStatus").val() || "";
  let vesselId = $("#searchVessel option:selected").val() || "";
  let lookBottom =
    unitId.trim() != ""
      ? true
      : vesselId.trim() != ""
        ? true
        : customersBoardingPermissionId.trim() != ""
          ? true
          : shipmentCommunicationStatus != ""
            ? true
            : false;
  supportUnitViewModel.viewBottomSearh(lookBottom);
}

function callfindUnit() {
  showProcessingModal();
  findUnit(true, null, null);
  onChangeInputForm();
}

function onChangeInputFilterForm(parameter) {
  if (parameter == "--" || parameter == "") {
    cleanSearchParams();
  }
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function clearActionByPermModalInput() {
  $("#txtActionVessel").val("");
  $("#txtActionPermission").val("");
  $("#cbActionType").val("");
  $("#cbActionReason").val("");
  $("#txtActionNotes").val("");
  supportUnitViewModel.actionVisitId("");
}

function exportTable() {
  var table = $("#tbUnit").DataTable();
  $("<table>")
    .append($(table.table().header()).clone())
    .append(table.$("tr").clone())
    .table2excel({
      exclude: ".noExl",
      name: "Worksheet Name",
      filename:
        "REPORTE COMUNICACIÓN DE EMBARQUE " +
        moment(new Date()).format("DD-MM-YYYY HH:MM:SS"),
      fileext: ".xls", // file extension
    });
}

function getReportTotalByVessels() {
  showProcessingModal();
  var request = $.ajax({
    type: "POST",
    url:
      url_application +
      "/ShipmentCommunicationManagement/shipmentCommunication/export/pdf",
    contentType: "application/json",
    data: $("#searchVessel option:selected").val(),
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }
      if (data == null || data == "") {
        showModalHtml("", "Sin resultados", "info");
        return;
      }
      //downloadPDF(data);
      openPDF(data);
      Swal.close();
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
    },
  });
}

function validateActionByPermission() {
  var permTemp = supportUnitViewModel.searchPermission();
  var unitArrayTemp = supportUnitViewModel.unitTable();
  var permArrayTemp = [];
  if (
    typeof permTemp != undefined &&
    permTemp != null &&
    permTemp != "" &&
    permTemp != "null"
  ) {
    for (i = 0; i < unitArrayTemp.length; i++) {
      var dataObj = unitArrayTemp[i];
      var dataPerm = dataObj["client_perm_ref_id"];
      if (
        typeof dataPerm != undefined &&
        dataPerm != null &&
        dataPerm != "" &&
        dataPerm != "null"
      ) {
        if (!permArrayTemp.includes(dataPerm)) permArrayTemp.push(dataPerm);
        if (dataPerm != permTemp) permTemp = dataPerm;
      }
    }
    if (permArrayTemp.length == 1)
      supportUnitViewModel.searchPermission(permTemp);
    //console.log("searchPermission:" + supportUnitViewModel.searchPermission());
    //console.log("permArrayTemp:" + permArrayTemp);
    //console.log("permArrayTemp length:" + permArrayTemp.length);

    if (permArrayTemp.length > 1) {
      var arrayLength = permArrayTemp.length;
      var questionMsg =
        "Hay " +
        arrayLength +
        " permisos de embarque como resultado de la búsqueda, la acción aplicará a todos los contenedores de los permisos";
      Swal.fire({
        allowOutsideClick: false,
        title: "Desea continuar?",
        text: questionMsg,
        type: "question",
        showCancelButton: true,
        confirmButtonColor: "#4fa7f3",
        cancelButtonColor: "#d57171",
        confirmButtonText: "Sí, continuar!",
      }).then((result) => {
        if (result.value) {
          viewActionByPermissionModal();
        }
      });
    } else viewActionByPermissionModal();
  } else viewActionByPermissionModal();
}

function downloadPDF(pdf) {
  var isFirefox = typeof InstallTrigger !== "undefined";
  if (isFirefox) {
    var win = window.open("data:application/pdf;base64," + pdf, "_blank");
  } else {
    const linkSource = "data:application/pdf;base64," + pdf;
    const downloadLink = document.createElement("a");
    const fileName = "REPORTE TOTAL_" + new Date().toISOString() + ".pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}

function openPDF(pdf) {
  var win = window.open();
  win.document.write(
    '<iframe src="' +
      "data:application/pdf;base64," +
      pdf +
      '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>',
  );
  return false;
}

function clearStyle() {
  var element = document.getElementById("body");
  element.style.removeProperty("padding-right");
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function applyActionByPermission(applyObject) {
  showProcessingModal();
  if (typeof applyObject == "undefined" || applyObject == null) {
    applyObject = new Object();
    applyObject.visitId = supportUnitViewModel.actionVisitId();
    applyObject.permission = $("#txtActionPermission").val().toUpperCase();
    applyObject.action = $("#cbActionType option:selected").val().toUpperCase();
    applyObject.reason = $("#cbActionReason option:selected")
      .val()
      .toUpperCase();
    applyObject.notes = $("#txtActionNotes").val().toUpperCase();
  }
  //console.log("apply object:" + JSON.stringify(applyObject))
  $.ajax({
    type: "POST",
    url:
      url_application +
      "/ShipmentCommunicationManagement/applyActionByPermission/",
    contentType: "application/json",
    data: JSON.stringify(applyObject),
    success: function (data, statusCode) {
      closeProcessingModal();
      var msg = data["msg"];
      if (typeof msg != "undefined" && resultIsError(data)) {
        showErrorHtmlMsg(msg);
      } else if (typeof msg != "undefined" && resultIsWarning(data)) {
        $("#btnCancelActionByPerm").click();
        showWarningMsg(msg);
      } else if (resultRequiresConfirm(data)) {
        var questionType = data["questionType"];
        Swal.fire({
          allowOutsideClick: false,
          title: "Desea continuar?",
          text: msg,
          type: "question",
          showCancelButton: true,
          confirmButtonColor: "#4fa7f3",
          cancelButtonColor: "#d57171",
          confirmButtonText: "Sí, aplicar acción!",
        }).then((result) => {
          if (result.value) {
            applyObject[questionType] = true;
            applyActionByPermission(applyObject);
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            delete applyObject[questionType];
          }
        });
      } else if (typeof msg != "undefined" && resultIsOk(data)) {
        $("#btnCancelActionByPerm").click();
        console.log("Acción por permiso realizada");
        showProcessingModal();
        findUnit(true, null, msg);
      } else showErrorHtmlMsg(msg);
    },
    fail: function (data, statusCode) {
      s;
      closeProcessingModal();
      showErrorMsg("Status code:" + statusCode);
    },
  });
}
