/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

function newDataTable(tableId, sScrollY, canSort, canFilter, isResponsive) {
  var scrollY = sScrollY == null ? "40vh" : sScrollY;

  var table = $("#" + tableId).DataTable({
    responsive: isResponsive,
    dom: "Bfrtip",
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: canFilter,
    bSort: canSort,
    sScrollY: scrollY,
  });

  return table;
}

function newDataTable(
  tableId,
  sScrollY,
  canSort,
  canFilter,
  isResponsive,
  emptyMessage,
) {
  var scrollY = sScrollY == null ? "40vh" : sScrollY;
  var eMessage =
    emptyMessage == null ? "No hay registros a mostrar" : emptyMessage;

  var table = $("#" + tableId).DataTable({
    responsive: isResponsive,
    dom: "Bfrtip",
    buttons: ["excel", "pdf"],
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: eMessage,
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: canFilter,
    bSort: canSort,
    sScrollY: scrollY,
  });

  return table;
}

function newDataTableMultiple(
  tableId,
  sScrollY,
  canSort,
  canFilter,
  isResponsive,
  multiSelect,
) {
  var scrollY = sScrollY == null ? "40vh" : sScrollY;

  var table = $("#" + tableId).DataTable({
    responsive: isResponsive,
    dom: "Bfrtip",
    select: multiSelect,
    buttons: ["excel", "pdf"],
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: canFilter,
    bSort: canSort,
    sScrollY: scrollY,
  });

  return table;
}

function newDataTableCheckbox(
  tableId,
  sScrollY,
  canSort,
  canFilter,
  isResponsive,
  multiSelect,
  emptyMessage,
) {
  var scrollY = sScrollY == null ? "40vh" : sScrollY;
  var eMessage =
    emptyMessage == null ? "No hay registros a mostrar" : emptyMessage;

  var table = $("#" + tableId).DataTable({
    responsive: isResponsive,
    dom: "Bfrtip",
    buttons: ["excel", "pdf"],
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: eMessage,
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: canFilter,
    bSort: canSort,
    sScrollY: scrollY,
    columnDefs: [
      {
        orderable: false,
        className: "select-checkbox",
        targets: 0,
      },
    ],
    select: {
      style: "multi",
      selector: "td:not(:last-child)",
    },
    order: [[1, "asc"]],
  });

  return table;
}

function newDataTableWithTotals(
  tableId,
  sScrollY,
  canSort,
  canFilter,
  isResponsive,
  multiSelect,
  emptyMessage,
  columnNbrToSum,
) {
  var scrollY = sScrollY == null ? "40vh" : sScrollY;
  var eMessage =
    emptyMessage == null ? "No hay registros a mostrar" : emptyMessage;

  var table = $("#" + tableId).DataTable({
    responsive: isResponsive,
    dom: "Bfrtip",
    buttons: ["excel", "pdf"],
    oLanguage: {
      sLoadingRecords: "Cargando datos - por favor espere...",
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      sInfoFiltered: " - filtrando de _MAX_ resultados",
      sInfoEmpty: "Sin resultados",
      sProcessing: "Cargando datos...",
      oPaginate: {
        sFirst: "Primera",
        sLast: "Última",
        sNext: "Siguiente",
        sPrevious: "Anterior",
      },
      oAria: {
        sSortAscending: ": ordenamiento ascendente",
        sSortDescending: ": ordenamiento descendente",
      },
    },
    bDestroy: true,
    bFilter: canFilter,
    bSort: canSort,
    sScrollY: scrollY,
    footerCallback: function (row, data, start, end, display) {
      var api = this.api(),
        data;
      var currentPosition = api.colReorder.transpose(columnNbrToSum);

      // Remove the formatting to get integer data for summation
      var intVal = function (i) {
        return typeof i === "string"
          ? i.replace(/[\$,]/g, "") * 1
          : typeof i === "number"
            ? i
            : 0;
      };

      // Total over all pages
      total = api
        .column(currentPosition)
        .data()
        .reduce(function (a, b) {
          return intVal(a) + intVal(b);
        }, 0);

      // Total over this page
      pageTotal = api
        .column(currentPosition, { page: "current" })
        .data()
        .reduce(function (a, b) {
          return intVal(a) + intVal(b);
        }, 0);

      // Update footer
      $(api.column(currentPosition).footer()).html(
        "$ " + parseMoneyValue(total),
      );
    },
  });

  return table;
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}
