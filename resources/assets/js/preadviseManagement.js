var table;
var loaded = false;
var op;
var permind = 0;
var editando = false;
var changed = false;
var ugkey = 0;
$(document).ready(function () {
  $("input").attr("autocomplete", "nope");
  loadLineOpCb();

  $("#btnFindEntity").click(function () {
    findUnitsByBooking();
  });

  $("#formUnitWeight").change(function () {
    validateWeight();
  });

  $("#btnClean").click(function () {
    $("#bkgnbr").val("").focus();
    $("#selectLineOp").val("");
  });

  $("#tbUnitsByBkg tbody").on("click", "tr", function () {
    $(this).toggleClass("selected");
  });

  $("#inlineRadioExterno").click(function () {
    Swal.fire(
      "",
      "Estimado cliente, usted eligió presentar VGM externo. Recuerde que es obligatorio dicha presentación previo al ingreso por el Gate.",
      "info",
    );
  });

  $("#formUnitNbr").change(function () {
    if ($(this).val().length != 11) {
      Swal.fire(
        "",
        "Por favor, complete el campo Contenedor con un valor válido.",
        "warning",
      );
      $(this).val("");
      return;
    }
    for (var i = 0; i < $("#tbUnitsByBkg").DataTable().rows().count(); i++) {
      if (
        $("#tbUnitsByBkg").DataTable().rows().data()[i][1] ===
          $("#formUnitNbr").val() &&
        !editando
      ) {
        Swal.fire(
          "",
          "Por favor, ingrese un contenedor que no haya sido preavisado con anterioridad.",
          "warning",
        );
        $(this).val("").focus();
        return;
      }
    }
  });

  $("#modalToggle").click(function () {
    changed = false;
    editando = false;
    $("#formUnitNbr").attr("disabled", false);
    $('a[href="#stepUnit"]').click();
    $("#inlineRadioTerminal").prop("checked", true);
    $("#formUnitSize").val(-1);
    $("#formUnitNbr").attr("ugkey", 0);
    $("#formPreadviseUnit input").each(function () {
      $(this).val("");
    });
    $("#tbodyPermissions").empty();
    op = "PREADVISE_UNIT";
    $("#titleForUnitId").text("Datos del contenedor");
    $("#modal").modal({
      backdrop: "static",
    });
  });

  $("#btnStepUnit").click(function () {
    $('#myTab a[href="#stepPermission"]').tab("show");
  });

  $("#btnStepPermission").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepOog"]').tab("show");
  });

  $("#btnStepOog").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepSeals"]').tab("show");
  });

  $("#btnStepSeals").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepConfirm"]').tab("show");
  });

  $("#btnStepConfirm").click(function (e) {
    e.preventDefault();
    validateForm();
  });

  $("#btnAddPerms").click(function () {
    addPermissionToTable();
  });
});

function updateDataBySize() {
  var option = $("#formUnitSize :selected");
  $("#formUnitTemp").val(option.attr("temp"));
  $("#formUnitTare").val(option.attr("tare"));
  $("#formUnitLine").val(option.attr("lineop"));
}

function validateForm() {
  if (!changed) {
    showModal(
      "",
      "Por favor, algun campo para realizar la actualización.",
      "error",
    );
    error = true;
  }
  var error = false;
  $("#formPreadviseUnit input.required").each(function () {
    if (this.value == "" || this.value == undefined) {
      showModal(
        "",
        "Por favor, complete todos los campos requeridos.",
        "error",
      );
      error = true;
    }
  });
  if (
    $("#formUnitSize").val() == null ||
    $("#formUnitSize").val() == undefined
  ) {
    showModal("", "Por favor, complete todos los campos requeridos.", "error");
    error = true;
  }

  if (!error) {
    confirmModalPreadvise();
  }
}

function deletePreadviseUnit(id) {
  var d = table.row(id).data();
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html: "Esta seguro de despreavisar el contenedor " + d[1] + "?",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      cancelPreadvise(id);
    }
  });
}

function validateWeight() {
  if (
    parseFloat(
      $("#formUnitWeight").val() < 0 ||
        parseFloat($("#formUnitTare").val()) < 0,
    )
  ) {
    Swal.fire(
      "",
      "Los valores de Peso no pueden ser negativos. Verifique.",
      "warning",
    );
    $("#formUnitWeight").val("");
    return;
  }
}

function confirmCloseModalPreadvise() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html: "Si cierra este formulario, perderá la información cargada. Esta seguro?",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      // Tenemos que limpiar el formulario primero
      $("#modal").modal("hide");
    }
  });
}

function cancelPreadvise(id) {
  processingModal();

  var obj = new Object();
  obj.formUnitGkey = parseInt($("#" + id).attr("ugkey"));
  obj.formUnitNbr = $("#" + id).attr("uid");
  obj.formBookingNbr = $("#" + id).attr("bknbr");

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Appointment/cancelPreadviseUnit",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (msg) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    if (msg == "SUCCESS") {
      showTimerModal(
        "",
        "Contenedor despreavisado correctamente",
        2000,
        reloadGrid,
      );
      $("#btnFindEntity").click();
    } else {
      Swal.fire("", msg, "error");
      return;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("", jqXHR.responseText, "error");
  });
}

function reloadGrid() {
  $("#btnFindEntity").click();
}

function showPermissionByUnit(id) {
  var gkey = parseInt($("#" + id).attr("ugkey"));
  if (gkey == undefined || gkey == "") {
    Swal.fire(
      "",
      "No se puede consultar los permisos de este contenedor. Intente mas tarde",
      "warning",
    );
    return;
  }
  processingModal();

  var request = $.ajax({
    url: url_application + "/Appointment/findPermissionByUnitGkey/" + gkey,
    type: "GET",
  });

  request.done(function (data) {
    Swal.close();
    if (data == null || data == "") {
      Swal.fire("", "No hay permisos asociados al contenedor", "info");
      return;
    }
    $("#permissionsBody").empty();
    for (var i = 0; i < data.length; i++) {
      $("#permissionsBody").append("<tr><td>" + data[i] + "</td></tr>");
    }
    $("#modalPermission").modal({
      backdrop: "static",
    });
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("", jqXHR.responseText, "error");
  });
}

function editPreadviseUnit(id) {
  // primero debemos traer la información de N4
  // Condicion VGM + Precinto + Permisos + OOG
  // Si Appt == null : Se puede modificar Tara y Peso
  // Si Appt != null : No se puede modificar Tara y Peso
  processingModal();
  var bkgkey = $(id).attr("bkgkey");
  var bknbr = $(id).attr("bknbr");
  var unitId = $(id).attr("unitId");
  editando = true;
  var request = $.ajax({
    url:
      url_application +
      "/Appointment/findPreadviseUnit/" +
      bkgkey +
      "/" +
      bknbr +
      "/" +
      unitId,
    type: "GET",
  });

  $("#formPreadviseUnit input").each(function () {
    $(this).val("");
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire(
        "",
        "No se encontró datos de preaviso para el contenedor.",
        "info",
      );
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    //viewModel.UnitPreadvise(data);
    completeUnitPreadviseEdit(data);
    op = "UPDATE_PREADVISED_UNIT";
    $("#tbodyPermissions").empty();
    if (data.permissions != null) {
      for (var i = 0; i < data.permissions.length; i++) {
        $("#tbodyPermissions").append(
          "<tr id='editPerm_" +
            i +
            "'><td>" +
            data.permissions[i] +
            "</td><td>" +
            "<i id='" +
            i +
            "' onclick='javascript:removePerms(this.id)' class='ti-trash'></i></td></tr>",
        );
      }
    }
    Swal.close();
    $('a[href="#stepUnit"]').click();
    $("#modal").modal("show");
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    Swal.fire("", jqXHR.responseText, "error");
  });
}

function getDataPreadviseForm() {
  var obj = new Object();
  $("#formPreadviseUnit :input").each(function () {
    obj[this.name] = this.value.toUpperCase();
  });
  obj.formBookingGkey = parseInt($("#formUnitSize :selected").attr("bkgkey"));
  obj.formBookingNbr = $("#bkgnbr").val().toUpperCase();
  obj.formBookingItemGkey = parseInt(
    $("#formUnitSize :selected").attr("itemgkey"),
  );
  obj.formUnitGkey = $("#formUnitNbr").attr("ugkey");
  if (obj.formUnitGkey == "" && ugkey == 0) {
    obj.formUnitGkey = 0;
  } else {
    obj.formUnitGkey = ugkey.toString();
  }

  obj.permsADD = "";
  obj.permsREMOVE = "";
  if ($("#tablePermissions tbody tr").length > 0) {
    $("#tablePermissions tbody tr.ADD").each(function () {
      obj.permsADD += $(this).find("td:first-child").html() + ";";
    });
    $("#tablePermissions tbody tr.REMOVE").each(function () {
      obj.permsREMOVE += $(this).find("td:first-child").html() + ";";
    });
  }
  obj.formVgmCondition = "VGMT4";
  if ($("#inlineRadioExterno").prop("checked")) {
    obj.formVgmCondition = "VGMEXT";
  }
  obj.type = op.toUpperCase();
  return obj;
}

function completeUnitPreadviseEdit(data) {
  $("#formUnitNbr")
    .val(data.unitId)
    .attr("uGkey", data.uGkey)
    .attr("disabled", true);
  ugkey = data.uGkey;
  $("#formUnitSize").val(data.size);
  $("#formUnitLine").val(data.lineOp);
  $("#formUnitTemp").val(data.temperature);
  $("#formUnitTare").val(data.tare);
  $("#formUnitWeight").val(data.weight - data.tare);
  if (data.haveAppt) {
    $("#formUnitTare,#formUnitWeight").attr("disabled", true);
  }
  $("#formUnitVgmEmail").val(data.vgmEmail);
  $("#formUnitOogTop").val(data.oogTop);
  $("#formUnitOogBack").val(data.oogBack);
  $("#formUnitOogRight").val(data.oogRight);
  $("#formUnitOogFront").val(data.oogFront);
  $("#formUnitOogLeft").val(data.oogLeft);
  $("#formUnitSealCustoms").val(data.seal2);
  if (data.vgmCondition == "VGMT4") {
    $("#inlineRadioTerminal").prop("checked", "checked");
  } else {
    $("#inlineRadioExterno").prop("checked", "checked");
  }
}

function removePerms(id) {
  $("#editPerm_" + id)
    .attr("style", "display: none")
    .attr("class", "REMOVE");
}

function initKnockout() {
  viewModel = {
    UnitsByBkg: ko.observableArray([]),
    Units: ko.observableArray([]),
    Items: ko.observableArray([]),
    UnitPreadvise: ko.observableArray([]),
    lineOpCb: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  deleteDataTables();
  viewModel.UnitsByBkg(data);
  viewModel.Units(data.units);
  viewModel.Items(data.items);
  table = newDataTable("tbUnitsByBkg", null, true, true, true);
  /*table = $("#tbUnitsByBkg").DataTable({
		select: 'single',
		"oLanguage": {
            "sEmptyTable": "No hay registros a mostrar",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sSearch": "Buscar",
            "sLengthMenu": "Mostrar _MENU_ resultados"
        },
        "bDestroy": true,
		"bFilter" : true,
		"bSort": true,
		"sScrollY": "30vh"
	});*/
  Swal.close();
}

function findUnitsByBooking() {
  processingModal();
  $("#formPreadviseUnit input").each(function () {
    $(this).val("");
  });
  var lineOp = $("#selectLineOp").children("option:selected").val();
  var bkg = $("#bkgnbr").val();
  if (bkg == "" || lineOp == null || lineOp == "") {
    Swal.fire("", "Por favor, complete los campos requeridos.", "error");
    return;
  }

  var request = $.ajax({
    url: url_application + "/Appointment/findBooking/" + bkg + "/" + lineOp,
    type: "GET",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire(
        "",
        "No se encontró un Booking activo asociado a la Linea que seleccionó. Corrobore la información.",
        "info",
      );
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    validateQuantity(data);
    $("#tbUnitsByBkg,#tableBookingDetails").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function validateQuantity(data) {
  var total = 0;
  for (var i = 0; i < data.items.length; i++) {
    total += data.items[i].quantity;
  }
  if (total > data.units.length) {
    $("#modalToggle").show();
  } else {
    $("#modalToggle").hide();
  }
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function confirmModalPreadvise() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html:
      "Esta seguro de preavisar éste contenedor? <br> Asegurese de haber completado la información correctamente. <br> Recuerde que el permiso de embarque" +
      " y el precinto puede modificarlo hasta antes del pago.",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      preadviseUnit();
    }
  });
}

function preadviseUnit() {
  processingModal();
  var data = getDataPreadviseForm();

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Appointment/preadviseUnit",
    data: JSON.stringify(data),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (msg) {
    if (data == null || data == "") {
      showModal("", "Sin resultados", "info");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    if (msg == "SUCCESS") {
      showTimerModal(
        "",
        "Contenedor preavisado correctamente",
        2000,
        reloadGrid,
      );
      // Cerramos el modal de preaviso
      $("#modal").modal("hide");
      $("#btnFindEntity").click();
    } else {
      Swal.fire("", msg, "error");
      return;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function loadSearchLineOpCb(data) {
  viewModel.lineOpCb(data);
}

function loadLineOpCb() {
  $.ajax({
    type: "GET",
    url: url_application + "/AppointmentManagement/getLineOpList/",
    success: function (data, statusCode) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadSearchLineOpCb(data);
    },
    fail: function (data, statusCode) {
      showModal("", "Status code:" + statusCode, "error");
    },
  });
}

function refreshTempBySize() {
  $("#formUnitTemp").val(0);
}

function addPermissionToTable() {
  // 19008EG02000385S
  var perm = $("#inputPerms").val().trim();
  if (perm.length != 16) {
    Swal.fire(
      "Atención",
      "Por favor, complete con un Permiso de Embarque válido.",
      "warning",
    );
    return;
  }
  permind++;
  $("#tablePermissions").append(
    "<tr class='ADD' id='editPerm_" +
      permind +
      "'><td>" +
      perm.toUpperCase() +
      "</td><td><i id='" +
      permind +
      "' onclick='javascript:removePerms(this.id)' class='ti-trash'></i></td></tr>",
  );
  $("#inputPerms").val("");
}

function showTimerModal(title, text, timerMillis, callback) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onClose: () => {
      callback();
    },
  });
}
