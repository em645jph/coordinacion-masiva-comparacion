/**
 * AUTHOR: NAHUEL FACCINETTI
 */

const BLOCK_CUSTOMS_LOAD = "BLOCK_CUSTOMS_LOAD";

$(document).ready(function () {
  initKnockout();
  loadInitCombos();
  initButton();
  initModals();
});

function initButton() {
  $("#btnSearch").click(function () {
    searchUnits(true);
  });

  $("#btnClean").click(function () {
    $("#txtSearchUnit").val("");
  });

  $("#formSearchUnits").submit(function (e) {
    e.preventDefault();
    searchUnits(true);
  });
}

function initModals() {
  $("#scannerLockModal").on("shown.bs.modal", function () {
    $("#noteLock").val("");
    $("#typeLock").val("");
    $("#cbResult").val("");
    viewModel.selectedTypeLock(null);
  });

  $("#scannerLockModal").on("hidden.bs.modal", function () {
    $("#noteLock").val("");
    $("#typeLock").val("");
    $("#cbResult").val("");
    viewModel.selectedTypeLock(null);
  });
}

function searchUnits(showProcessingModal) {
  var valUnit = $("#txtSearchUnit").val().trim();
  var param = {
    unitId: valUnit,
  };

  if (valUnit.trim() == "" || valUnit.trim() == null) {
    Swal.fire("", "Por favor, Ingrese un contenedor a buscar", "info");
  } else {
    if (valUnit.trim().length != 11) {
      Swal.fire(
        "",
        "Por favor, complete el campo Contenedor con un valor válido.",
        "error",
      );
    } else {
      if (showProcessingModal) {
        processingModal();
      }
      $.ajax({
        type: "GET",
        url: url_application + "/ScannerManagement/findUnitBlockScanner/",
        data: param,
        success: function (data, statusCode, xhr) {
          var ct = xhr.getResponseHeader("content-type") || "";
          if (ct.indexOf("html") > -1) {
            showModalSessionClose(
              "Session Expirada",
              "Disculpe, por seguridad la session ha sido cerrada.",
              "info",
            );
          } else {
            console.log(data);
            deleteVisitDataTable();
            loadUnitTable(data);
            Swal.close();
          }
        },
        fail: function (data, statusCode) {
          showErrorMsg(data);
        },
      });
    }
  }
}

function loadInitCombos() {
  showProcessingModal();

  $.ajax({
    type: "GET",
    url: url_application + "/ScannerManagement/loadTypeBlock/",
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      }

      loadlockTypesShipmentCommunication(data);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function initKnockout() {
  viewModel = {
    unitBlockScanner: ko.observableArray([]),
    searchScTypesLock: ko.observableArray([]),
    selectedTypeLock: ko.observable(),
    permisionAction: ko.observable(false),
    unitModal: ko.observable(),
    permissionModal: ko.observable(),
    statusModal: ko.observable(),
    gkeyModal: ko.observable(),
    client_perm_gkeyModal: ko.observable(),
    obVesselModal: ko.observable(),
    actionLock: function (obj) {
      customsBlockScanner(true, BLOCK_CUSTOMS_LOAD);
    },
    viewLockModal: function (obj) {
      viewModelLock(
        obj.gkey,
        obj.id,
        obj.client_perm_gkey,
        obj.client_perm_ref_id,
        obj.unit_status,
        obj.ob_visit_id,
        obj.obVesselInfo,
      );
    },
  };
  ko.applyBindings(viewModel);
}

function viewModelLock(
  gkey,
  id,
  flagGkey,
  referenceId,
  status,
  ob_visit_id,
  ob_vessel_info,
) {
  knowDateDocCutOff(ob_visit_id);
  viewModel.gkeyModal(gkey), viewModel.unitModal(id);
  viewModel.client_perm_gkeyModal(flagGkey),
    viewModel.permissionModal(referenceId);
  viewModel.statusModal(status);
  viewModel.obVesselModal(ob_vessel_info);
  $("#scannerLockModal").modal();
}

function loadlockTypesShipmentCommunication(data) {
  viewModel.searchScTypesLock(
    data["lockTypesShipmentCommunicationList"].find((m) => m.key === "SCANNER"),
  );
}

function knowDateDocCutOff(visitId) {
  showProcessingModal();
  var param = {
    visitId: visitId,
    currentDate: Date.now(),
  };
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: url_application + "/ShipmentCommunicationManagement/knowCutOff/",
    data: param,
    success: function (data, statusCode, xhr) {
      var ct = xhr.getResponseHeader("content-type") || "";
      if (ct.indexOf("html") > -1) {
        showModalSessionClose(
          "Session Expirada",
          "Disculpe, por seguridad la session ha sido cerrada.",
          "info",
        );
      } else {
        viewModel.permisionAction(data);
        closeProcessingModal();
      }
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
    },
  });
}

function customsBlockScanner(showProcessing, action) {
  var errorMsg = "";
  let msgTitle = "";
  let msgText = "";
  let typeError = $("#typeLock option:selected").val();
  if (errorMsg == "" && typeError != null) {
    if (showProcessing) showProcessingModal();
    var searchParam = {
      gkey: viewModel.gkeyModal(),
      id: viewModel.unitModal(),
      flagGkey: viewModel.client_perm_gkeyModal(),
      referenceId: viewModel.permissionModal(),
      notes: $("#noteLock").val() != "" ? $("#noteLock").val() : "",
      action: action,
      reason: viewModel.selectedTypeLock(),
      obVessel: viewModel.obVesselModal(),
    };
    $.ajax({
      type: "POST",
      url: url_application + "/ScannerManagement/customsBlockScanner/",
      data: JSON.stringify(searchParam),
      contentType: "application/json",
      success: function (data, statusCode, xhr) {
        var ct = xhr.getResponseHeader("content-type") || "";
        if (ct.indexOf("html") > -1) {
          showModalSessionClose(
            "Session Expirada",
            "Disculpe, por seguridad la session ha sido cerrada.",
            "info",
          );
        }
        if (data != "SUCCESS") {
          $("#scannerLockModal").modal("hide");
          if (statusCode == 409) {
            //showErrorHtmlMsg(data);
            $.notify(data, "error");
          } else {
            showErrorHtmlMsg(data);
          }
        } else {
          msgText =
            "Bloqueo del contendor " +
            viewModel.unitModal() +
            " Realizado exitosamente!";
          $("#Note").val("");
          $("#scannerLockModal").modal("hide");
          if (showProcessing) closeProcessingModal();
          searchUnits(false);
          $.notify(msgText, "success");
          //showSuccessMsg(msgTitle, msgText);
        }
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function deleteVisitDataTable() {
  $("#tbUnit").DataTable().clear();
  $("#tbUnit").DataTable().destroy();
}

function loadUnitTable(data) {
  viewModel.unitBlockScanner.removeAll();
  viewModel.unitBlockScanner(data.unitBlockScanner);
  var table = $("#tbUnit").DataTable({
    bDestroy: true,
    deferRender: true,
    sScrollX: true,
    bFilter: false,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
    order: [[5, "desc"]],
    pageLength: 50,
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
