var loaded = false;
$(document).ready(function () {
  $("input").attr("autocomplete", "nope");

  $("#btnFindEntity").click(function () {
    findUnitsByBooking();
  });

  $("#tbUnitsByBl tbody").on("click", "tr", function () {
    $(this).toggleClass("selected");
  });

  $("#vgmRadioExternal").click(function () {
    Swal.fire(
      "",
      "Estimado cliente, usted eligió presentar VGM externo. Recuerde que es obligatorio dicha presentación previo al ingreso por el Gate.",
      "info",
    );
  });

  $("#modalToggle").click(function () {
    $("#titleForUnitId").text("Datos del contenedor");
    $("#modal").modal({
      backdrop: "static",
    });
  });

  $("#btnStepUnit").click(function () {
    $('#myTab a[href="#stepPermission"]').tab("show");
  });

  $("#btnStepPermission").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepOog"]').tab("show");
  });

  $("#btnStepOog").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepSeals"]').tab("show");
  });

  $("#btnStepSeals").click(function (e) {
    e.preventDefault();
    $('#myTab a[href="#stepConfirm"]').tab("show");
  });

  $("#btnStepConfirm").click(function (e) {
    e.preventDefault();
    confirmModalPreadvise();
  });

  $("#datetimepicker3").datetimepicker({
    inline: true,
    appendText: "(yyyy-mm-dd)",
    minDate: "2019/9/29",
  });

  $("#btnCoordinate").click(function () {
    if (
      $("#datetimepicker3").val() == "" ||
      $("#datetimepicker3").val() == undefined
    ) {
      Swal.fire(
        "Atención",
        "Por favor, seleccione una fecha de coordinación para su contenedor",
        "error",
      );
      return;
    } else {
      preventApptUnknown();
    }
  });

  $("#btnAddPerms").click(function () {
    addPermissionToTable();
  });
});

function preventApptUnknown() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    text:
      "Esta seguro de coordinar éste contenedor para " +
      $("#datetimepicker3").val(),
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      //Cerramos el modal de preaviso
      $("#modalAppt").modal("hide");
      Swal.fire(
        "",
        "Contenedor coordinado para " + $("#datetimepicker3").val(),
        "info",
      );
    }
  });
}

function refreshLabelDateTimeAppt() {
  var current = $("#datetimepicker3").val();
  $("#labelDatetimeAppt").text("La fecha y hora seleccionada es " + current);
}

function deletePreadviseUnit(id) {
  var d = table.row(id).data();
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html: "Esta seguro de despreavisar el contenedor " + d[1] + "?",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      cancelPreadvise(id);
    }
  });
}

function confirmCloseModalPreadvise() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html: "Si cierra este formulario, perderá la información cargada. Esta seguro?",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      //Tenemos que limpiar el formulario primero
      $("#modal").modal("hide");
    }
  });
}

function cancelPreadvise(id) {
  processingModal();

  var obj = new Object();
  obj.formUnitGkey = parseInt($("#" + id).attr("ugkey"));
  obj.formUnitNbr = $("#" + id).attr("uid");
  obj.formBookingNbr = $("#" + id).attr("bknbr");

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Appointment/cancelPreadviseUnit",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    Swal.fire(JSON.parse(data.message));
    $("#btnFindEntity").click();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function showPermissionByUnit(id) {
  var gkey = parseInt($("#" + id).attr("ugkey"));
  if (gkey == undefined || gkey == "") {
    Swal.fire(
      "Atencion",
      "No se puede consultar los permisos de este contenedor. Intente mas tarde",
      "warning",
    );
    return;
  }
  processingModal();

  var request = $.ajax({
    url: url_application + "/Appointment/findPermissionByUnitGkey/" + gkey,
    type: "GET",
  });

  request.done(function (data) {
    Swal.close();
    if (data == null || data == "") {
      Swal.fire("", "No hay permisos asociados al contenedor", "info");
      return;
    }
    $("#permissionsBody").empty();
    for (var i = 0; i < data.length; i++) {
      $("#permissionsBody").append("<tr><td>" + data[i] + "</td></tr>");
    }
    $("#modalPermission").modal({
      backdrop: "static",
    });
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function editPreadviseUnit(id) {
  //primero debemos traer la información de N4
  //Condicion VGM + Precinto + Permisos + OOG
  // Si Appt == null : Se puede modificar Tara y Peso
  // Si Appt != null : No se puede modificar Tara y Peso
  processingModal();
  var ugkey = $("#" + id).attr("ugkey");

  var request = $.ajax({
    url: url_application + "/Appointment/editPreadviseUnit/" + ugkey,
    type: "GET",
  });

  request.done(function (data) {
    Swal.close();
    if (data == null || data == "") {
      Swal.fire(
        "",
        "No se encontró datos de preaviso para el contenedor.",
        "info",
      );
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    viewModel.UnitPreadvise(data);
    $("#modalEditPreadvise").modal("show");
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function initKnockout() {
  viewModel = {
    UnitsByBl: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  /*viewModel.UnitsByBkg.removeAll();
	viewModel.Units.removeAll();
	viewModel.Items.removeAll();*/
  deleteDataTables();
  viewModel.UnitsByBl(data);
  $("#tbUnitsByBl").DataTable({
    //		select: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sLengthMenu: "Mostrar _MENU_ resultados",
      /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
    },
    bDestroy: true,
    bFilter: true,
    bSort: true,
    sScrollY: "30vh",
  });
}

function findUnitsByBooking() {
  processingModal();

  var lineOp = $("#selectLineOp").children("option:selected").val();
  var bkg = $("#bkgnbr").val();
  if (bkg == "" || lineOp == null || lineOp == "") {
    Swal.fire(
      "Request failed",
      "Por favor, complete los campos requeridos.",
      "error",
    );
    return;
  }
  initKnockout();
  var request = $.ajax({
    url: url_application + "/Billing/findUnitsByBooking/" + bkg + "/" + lineOp,
    type: "GET",
  });

  Swal.close();

  request.done(function (data) {
    if (data == null || data == "") {
      Swal.fire(
        "",
        "No se encontró un Booking activo asociado a la Linea que seleccionó. Corrobore la información.",
        "info",
      );
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnitsByBkg,#modalToggle,#tableBookingDetails").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function confirmModalPreadvise() {
  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html: "Esta seguro de preavisar éste contenedor? <br> Asegurese de haber cargado la información correctamente.",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      preadviseUnit();
    }
  });
}

function preadviseUnit() {
  processingModal();

  var data = getDataPreadviseForm();

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Appointment/preadviseUnit",
    data: JSON.stringify(data),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModal("Sin resultados");
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    Swal.close();
    //Limpiamos todo el modal
    showModal("OK");
    //Cerramos el modal de preaviso
    $("#modal").modal("hide");
    //Actualizamos la tabla de contenedores preavisados
    $("#btnFindEntity").click();
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    showModal("Request failed", jqXHR.responseText, "error");
  });
}

function getDataPreadviseForm() {
  var obj = new Object();
  $("#formPreadviseUnit :input").each(function () {
    obj[this.name] = this.value;
  });
  obj.formBookingGkey = parseInt($("#formUnitLine").attr("bkgkey"));
  obj.formBookingNbr = $("#bkgnbr").val();
  obj.formBookingItemGkey = parseInt(
    $("#formUnitSize :selected").attr("itemgkey"),
  );
  obj.perms = "";
  $("#tablePermissions tbody tr").each(function () {
    obj.perms += $(this).find("td:first-child").html() + ";";
  });
  return obj;
}

function getIcon() {
  return '<i class="ti-alert"></i>';
}

function unitCoordination(id) {
  var d = table.row(id).data();
  //console.log( d );
  cleanModalCoordination();
  $("#divForUnit").append("Usted va a coordinar el Contenedor " + d[1]);
  $("#modalAppt").modal("show");
}

function cleanModalCoordination() {
  //$('#datetimepicker3').datetimepicker({value:'2019/9/29 07:00'});
  $("#divForUnit").html("");
}

function findLineOperators() {
  processingModal();

  var request = $.ajax({
    url: url_application + "/Line/buildSelectLineOp/",
    type: "GET",
  });

  request.done(function (data) {
    Swal.close();
    $("#selectLineOp").append(data);
  });

  request.fail(function (jqXHR, textStatus) {
    Swal.close();
    Swal.fire("Request failed", jqXHR.responseText, "error");
  });
}

function refreshTempBySize(value) {
  $("#formUnitTemp").val($("#formUnitSize option:selected").text());
}

function addPermissionToTable() {
  //19008EG02000385S
  var perm = $("#inputPerms").val().trim();
  if (perm.length != 16) {
    Swal.fire(
      "Atención",
      "Por favor, complete con un Permiso de Embarque válido.",
      "warning",
    );
    return;
  }
  $("#tablePermissions").append(
    "<tr><td>" + perm + "</td><td><i class='ti-trash'></i></td></tr>",
  );
  $("#inputPerms").val("");
}
