var loaded = false;
var reportNbr = 0;
$(document).ready(function () {
  processingModal();
  getMyCustomers();

  $("#btnFindCustomerInformation").click(function () {
    processingModal();
    getCustomerReports($("#selectCustomer").children("option:selected").val());
  });

  $("#deleteMyReportBtn").click(function () {
    showConfirmModal(
      "Atención",
      "Esta seguro de eliminar esta presentación?",
      "info",
    );
  });

  $("#htmlToPdfCreator").click(function () {
    printPDF();
  });
});

function getMyCustomers() {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCustomers",
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomersCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function loadCustomersCb(data) {
  viewModel.customers(data);
}

function getCustomerReports(customerCuit) {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getCustomerReports/" + customerCuit,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomerReportCb(data);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function loadCustomerReportById(docs, items, files) {
  //viewModel.customerReportDocuments.removeAll();
  //viewModel.customerReportItems.removeAll();
  viewModel.customerReportDocuments(docs);
  viewModel.customerReportItems(items);
  viewModel.customerReportFiles(files);
}

function loadAdditionalDataFromRow(data, event) {
  reportNbr = data.reportNumber.toString();
  $("#titleInformationReport,#titleInformationReport2").text(
    "Reporte #" + reportNumberLeftPad(reportNbr),
  );
  loadCustomerReportById(data.docs, data.items, data.files);
  $("#modalInformation").modal();
}

function deleteMyReport(data, event) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Atención!",
    text: "Esta seguro de cancelar su presentación?",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      processingModal();
      var request = $.ajax({
        type: "POST",
        url: url_application + "/balance/deleteMyReport/" + data.reportNumber,
        timeout: 600000,
      });

      request.done(function (data) {
        showModal("", data, "success");
        getCustomerReports(
          $("#selectCustomer").children("option:selected").val(),
        );
      });

      request.fail(function (jqXHR, textStatus) {
        showModal("", jqXHR.responseText, "error");
      });
    }
  });
}

function reportNumberLeftPad(value) {
  return value.toString().padStart(5, "0");
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}

function inputMoneyValue(input) {
  $("#" + input.id).val(
    parseFloat(input.value, 10)
      .toFixed(2)
      .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
      .toString(),
  );
}

function getDateTime(value) {
  return new Date(value).toLocaleDateString();
}

function initKnockout() {
  viewModel = {
    customerReport: ko.observableArray([]),
    customerReportDocuments: ko.observableArray([]),
    customerReportItems: ko.observableArray([]),
    customerReportFiles: ko.observableArray([]),
    customers: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadCustomerReportCb(data) {
  showCustomerReportGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showCustomerReportGrid(data) {
  viewModel.customerReport.removeAll();
  deleteDataTables();
  for (var int = 0; int < data.length; int++) {
    var customerFullname = data[int].customerData.customerName;
    var customerCuit = data[int].customerData.customerCuit;
    data[int].reportData.customerKey =
      customerFullname + "(" + customerCuit + ")";
    viewModel.customerReport.push(data[int].reportData);
  }
  //showModalHtml('Informacion',ko.toJSON(data),'info');
  //$("#tableBox").show();
  $("#tableBox").show();
  newDataTable("tbCustomerReport", null, true, true, true);
  //$("#tbBalance").show();
  closeProcessingModal();
}

function downloadFile(data, event) {
  const linkSource = "data:application/pdf;base64," + data.filePath;
  const downloadLink = document.createElement("a");
  const fileName =
    "ReportNro" + data.reportNumber + "_" + makeid(10).toUpperCase() + ".pdf";

  downloadLink.href = linkSource;
  downloadLink.download = fileName;
  downloadLink.click();
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function printPDF() {
  processingModal();
  $("#titleInformationReport2").show();
  $("#htmlToPdfCreator").hide();

  var HTML_Width = $("#modalBodyReportData").width();
  var HTML_Height = $("#modalBodyReportData").height();
  var top_left_margin = 15;
  var PDF_Width = HTML_Width + top_left_margin * 2;
  var PDF_Height = PDF_Width * 1.5 + top_left_margin * 2;
  var canvas_image_width = HTML_Width;
  var canvas_image_height = HTML_Height;

  var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

  html2canvas($("#modalBodyReportData")[0], { allowTaint: true }).then(
    function (canvas) {
      canvas.getContext("3d");
      dpi: 300, // Set to 300 DPI
        console.log(canvas.height + "  " + canvas.width);

      var imgData = canvas.toDataURL("application/pdf", 1.0);
      var pdf = new jsPDF("p", "pt", [PDF_Width, PDF_Height]);
      pdf.addImage(
        imgData,
        "PDF",
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height,
      );

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(
          imgData,
          "PDF",
          top_left_margin,
          -(PDF_Height * i) + top_left_margin * 4,
          canvas_image_width,
          canvas_image_height,
        );
      }

      pdf.save("APMT_ReporteNro" + reportNumberLeftPad(reportNbr) + ".pdf");
    },
  );
  $("#titleInformationReport2").hide();
  $("#htmlToPdfCreator").show();
  closeProcessingModal();
}
