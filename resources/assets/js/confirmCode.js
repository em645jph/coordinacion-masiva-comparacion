/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  $("#btnConfirmCode").click(function (e) {
    e.preventDefault();
    confirmCode();
  });

  $(".small").keyup(function () {
    $(this).next().focus();
  });
});

function getFormData($form) {
  var unindexed_array = $form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function (n, i) {
    indexed_array[n["name"]] = n["value"];
  });

  return indexed_array;
}

function confirmCode() {
  var $form = $("#formCodes");
  var data = getFormData($form);

  if (data == null || data == "")
    showModal(
      "Atención!",
      "Por favor, completar con un valor correcto.",
      "warning",
    );

  processingModal();
  var request = $.ajax({
    url: url_application + "/User/confirmCode",
    type: "POST",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalCallback("Atencion!", message, "success", url_application);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("Atencion!", jqXHR.responseText, "error");
  });
}

function validateDocumentNumber(value) {
  var error = false;
  (value.length == 0 || value == "") == true ? (error = true) : error;
  value.match(cuilRegex) == false ? (error = true) : error;
  value.match(cuilLengthRegex) == null ? (error = true) : error;
  return error;
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
