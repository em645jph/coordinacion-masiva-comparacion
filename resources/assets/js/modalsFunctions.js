/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

function showConfirmModal(
  title,
  text,
  type,
  showCancelButton,
  titleCallback,
  textCallback,
  callback,
) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = url_logout;
    }
  });
}

function showConfirmModal(title, text, type, showCancelButton, callback) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callback;
    }
  });
}

function showBasicModal(textMessage) {
  Swal.fire({ allowOutsideClick: false, text: textMessage });
}

function showModal(title, text, type) {
  Swal.fire({
    type: type,
    title: title,
    text: text,
  });
}

function showModalCallback(title, text, type, callbackUrl) {
  Swal.fire({
    allowOutsideClick: false,
    type: type,
    title: title,
    text: text,
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callbackUrl;
    }
  });
}

function showModalWithFunction(entityFinder) {
  var entityResp;
  Swal.fire({
    allowOutsideClick: false,
    title: "Buscando " + entityFinder, // Buscando CUIT
    html: "Por favor aguarde un instante",
    onBeforeOpen: () => {
      Swal.showLoading();
      var result = 1;
      if (result == 1) {
        entityResp = $("#documentNbr").val();
        Swal.close();
      }
    },
  });
  return entityResp;
}

function showTimerModal(title, text, timerMillis, callback) {
  Swal.fire({
    title: title,
    html: text,
    timer: timerMillis,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onClose: () => {
      callback;
    },
  });
}

function showErrorMsg(errorMsg) {
  Swal.fire({
    type: "error",
    title: "Error",
    text: errorMsg,
  });
}

function showErrorHtmlMsg(errorMsg) {
  Swal.fire({
    type: "error",
    title: "Error",
    html: errorMsg,
  });
}

function showSuccessMsg(msgTitle, msgText) {
  Swal.fire({
    type: "success",
    title: msgTitle,
    text: msgText,
  });
}

function showSuccessHtmlMsg(msgTitle, msgText) {
  Swal.fire({
    type: "success",
    title: msgTitle,
    html: msgText,
  });
}

function showWarningMsg(msgTitle, msgText) {
  Swal.fire({
    type: "warning",
    title: msgTitle,
    text: msgText,
  });
}

function showWarningMsg(msgText) {
  Swal.fire({
    type: "warning",
    title: "Alerta",
    text: msgText,
  });
}

function showWarningHtmlMsg(htmlMsg) {
  Swal.fire({
    type: "warning",
    title: "Alerta",
    html: htmlMsg,
  });
}

function showInfoMsg(msgText) {
  Swal.fire({
    type: "info",
    title: "",
    text: msgText,
  });
}

function showInfoHtmlMsg(htmlMsg) {
  Swal.fire({
    type: "info",
    title: "",
    html: htmlMsg,
  });
}

function showProcessingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function showTemporaryMsg(msgTitle, msgText, timerMillis) {
  Swal.fire({
    title: msgTitle,
    text: msgText,
    timer: timerMillis,
  });
}

function closeProcessingModal() {
  Swal.close();
}

function showModalHtml(title, text, type) {
  Swal.fire({
    type: type,
    title: title,
    html: text,
  });
}

function showModalHtmlCallback(title, text, type, url) {
  Swal.fire({
    type: type,
    title: title,
    html: text,
    allowOutsideClick: false,
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = url;
    }
  });
}

function showModalSessionClose(title, text, type) {
  Swal.fire({
    type: type,
    title: title,
    html: text,
    allowOutsideClick: false,
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.reload();
    }
  });
}

function showModalHtmlLarge(title, text, type) {
  Swal.fire({
    type: type,
    title: title,
    html: text,
    customClass: "swal-large",
    showConfirmButton: false,
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
