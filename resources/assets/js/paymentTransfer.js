/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

$(document).ready(function () {
  //42708.6
  $("#amount").maskMoney({
    thousands: ",",
    symbol: "$ ",
    symbolStay: false,
    precision: 2,
  });

  $("#btnAddTransfer").click(function () {
    addTransferToTable();
  });

  $("#datetransfer").datepicker({ dateFormat: "yy-mm-dd", maxDate: "0" });

  $("#btnPay").click(function () {
    buildTransferData();
  });

  $("#btnBack").click(function () {
    confirmExit();
  });
});

function validateAmountsTransfers() {
  var arr = html2json();
  var amounts = 0;
  var draftAmount = parseFloat(
    parseFloat($("#invAmount").text().replace(",", "")).toFixed(2),
  );
  for (var index = 1; index <= $("#transferBody tr").length - 1; index++) {
    //amounts += parseFloat(arr["T"+index]["2"].replace("$",""));
    amounts += parseFloat(
      parseFloat(arr["T" + index]["2"].replace(/,/g, "")).toFixed(2),
    );
  }
  if (amounts < draftAmount) {
    showModal(
      "",
      "El total de las transferencias ($" +
        amounts.toFixed(2) +
        ") no supera el monto de la factura ($" +
        draftAmount.toFixed(2) +
        "). Agrege una transferencia más.",
      "warning",
    );
    return false;
  }
  return true;
}

function buildTransferData() {
  if (!validateAmountsTransfers()) return;

  processingModal();

  var request = $.ajax({
    url: url_application + "/Transfer/generateTransferPay",
    type: "POST",
    data: JSON.stringify(html2json()),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalHtmlCallback(
      "",
      message,
      "info",
      url_application + "/Invoice/drafts",
    );
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "Error al ejecutar el Pago. Reintente mas tarde", "error");
  });
}

function calculateTotalPartial() {
  var arr = html2json();
  var amounts = 0;
  for (var index = 1; index <= $("#transferBody tr").length - 1; index++) {
    //amounts += parseFloat(arr["T"+index]["2"].replace("$",""));
    amounts += parseFloat(
      parseFloat(arr["T" + index]["2"].replace(/,/g, "")).toFixed(2),
    );
  }
  $("#footTotal").text("$" + amounts.toFixed(2));
}

function addTransferToTable() {
  var error = false;
  var i = $("#transferBody tr").length - 1;
  if (i == 5) {
    showModal(
      "",
      "Usted agrego la cantidad máxima de transferencias posibles.",
      "error",
    );
    return;
  }

  var bank;
  if ($("#selectBank :selected").val() == "-1") {
    error = true;
  } else {
    bank = $("#selectBank :selected").val();
  }
  var transfer;
  if ($("#referenceNbr").val() == "" || $("#referenceNbr").val() == undefined) {
    error = true;
  } else {
    transfer = $("#referenceNbr").val();
  }
  var amount;
  if ($("#amount").val() == "" || $("#amount").val() == undefined) {
    error = true;
  } else {
    //amount = $("#amount").val().replace("$","").replace(",","").trim();
    amount = $("#amount").val();
  }
  var date;
  if ($("#datetransfer").val() == "" || $("#datetransfer").val() == undefined) {
    error = true;
  } else {
    date = $("#datetransfer").val();
  }

  if (error) {
    showModal("", "Por favor, complete los campos requeridos.", "error");
    return;
  }
  var invNbr = $("#invNbr").text();
  var tr =
    "<tr id='trfrow_" +
    i +
    "'><td>" +
    bank +
    "</td><td>" +
    transfer +
    "</td><td>$" +
    amount +
    "</td><td>" +
    date +
    "</td>" +
    "<td><i id='" +
    i +
    "' onclick='deleteRow(this.id)' class='ti-trash'></i></td><td style='display: none;'>" +
    invNbr +
    "</td></tr>";
  $("#transferBody").append(tr);
  cleanInputs();
  calculateTotalPartial();
  if ($("#transferBody tr").length - 1 > 0) {
    $("#btnPay").attr("disabled", false).show();
  }
}

function cleanInputs() {
  $("input").val("");
  $("#selectBank").val(-1);
}

function deleteRow(id) {
  $("#trfrow_" + id).remove();
  if ($("#transferBody tr").length - 1 == 0) {
    $("#btnPay").attr("disabled", true).hide();
    $("#footTotal").text("$00.00");
  }
  calculateTotalPartial();
}

function html2json() {
  var json = "{";
  var otArr = [];
  var tbl2 = $("#transfertable tr").each(function (i) {
    if (i == 0) {
      return true;
    }
    x = $(this).children();
    var itArr = [];
    x.each(function () {
      itArr.push('"' + $(this).text().replace("$", "") + '"');
    });
    otArr.push('"' + "T" + i + '": [' + itArr.join(",") + "]");
  });
  json += otArr.join(",") + "}";

  return JSON.parse(json);
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function confirmExit() {
  Swal.fire({
    allowOutsideClick: false,
    title: "",
    text: "Esta seguro de salir del metodo de pago Transferencia?",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      history.back();
    }
  });
}

function showTimerMsg(html, timer) {
  Swal.fire({
    title: "",
    html: html,
    timer: timer,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
  });
}
