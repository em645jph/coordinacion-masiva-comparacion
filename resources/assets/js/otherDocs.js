var loaded = false;
var table;
$(document).ready(function () {
  $("#btnFindEntity").click(function () {
    findEntityById();
  });

  $("#tbUnits tbody").on("click", "tr", function () {
    if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
      $("#btnGenerate").show();
    } else {
      $("#btnGenerate").hide();
    }
  });

  $("#btnGenerate").click(function () {
    getGatepassByUnits();
  });
});

function getGatepassByUnits() {
  processingModal();

  var error = false;
  var dataArr = [];
  var rows = $("tr.selected");
  var rowData = $("#tbUnits").DataTable().rows(rows).data();
  $.each($(rowData), function (key, value) {
    if ($("#" + value[0]).attr("canBeDownloaded") == "0") {
      showModalHtml(
        "",
        "Uno o alguno de los contenedores seleccionados no cuenta con los requerimientos para obtener el gate-pass. " +
          "<br> Verifique que los contenedores estén coordinados y pagados y vuelva a intentarlo.",
        "error",
      );
      error = true;
      return;
    } else {
      dataArr.push(value["0"]); //"name" being the value of your first column.
    }
  });

  if (error) return;

  if ($("#tbUnits").DataTable().rows(rows).count() < 1) {
    showModalHtml(
      "",
      "Por favor, seleccione al menos un contenedor para generar el documento.",
      "error",
    );
    return;
  }

  var obj = new Object();
  obj.formUnits = dataArr;
  obj.formCategory = rowData[0][1];

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Document/getGatepassByType",
    contentType: "application/json",
    data: JSON.stringify(obj),
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModalHtml("", "Sin resultados", "info");
      return;
    }
    downloadPDF(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModalHtml("", "No se pudo obtener el GatePass", "error");
  });
}

/**
 * Creates an anchor element `<a></a>` with
 * the base64 pdf source and a filename with the
 * HTML5 `download` attribute then clicks on it.
 * @param  {string} pdf
 * @return {void}
 */
function downloadPDF(pdf) {
  const linkSource = "data:application/pdf;base64," + pdf;
  const downloadLink = document.createElement("a");
  const fileName = "GatePass_" + makeid(10).toUpperCase() + ".pdf";

  downloadLink.href = linkSource;
  downloadLink.download = fileName;
  downloadLink.click();
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function initKnockout() {
  viewModel = {
    Units: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data) {
  showUnitsGrid(data);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data) {
  viewModel.Units.removeAll();
  deleteDataTables();
  viewModel.Units(data);

  table = $("#tbUnits")
    .DataTable({
      select: "multiple",
      oLanguage: {
        sEmptyTable: "No hay registros a mostrar",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sSearch: "Buscar",
        sLengthMenu: "Mostrar _MENU_ resultados",
        /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
      },
      /*columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],*/
      bDestroy: true,
      bFilter: true,
      bSort: true,
      sScrollY: "30vh",
    })
    .on("select", function (e, dt, type, indexes) {
      if (type === "row") {
        var rows = table.rows(indexes).nodes().to$();
        //var ignore = node.hasClass( 'ignoreme' );
        $.each(rows, function () {
          if ($(this).hasClass("ignoreme")) table.row($(this)).deselect();
        });
      }
      if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    })
    .on("deselect", function (e, dt, type, indexes) {
      if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    });
}

function findEntityById() {
  processingModal();

  type = $("#selectType").children("option:selected").val();
  value = $("#valueNbr").val();
  if (value == "" || type == null || type == "") {
    showModal("", "Por favor, complete los campos requeridos.", "error");
    return;
  }

  var request = $.ajax({
    url: url_application + "/Document/findUnits/" + type + "/" + value,
    type: "POST",
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModalHtml(
        "",
        "Sin resultados. <br> Verifique que el contenedor, B/L o Booking buscado sea correcto.",
        "info",
      );
      return;
    }
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    $("#tbUnits").show();
    loadUnitsCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModalHtml("", "No se pudo obtener algun resultado.", "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
