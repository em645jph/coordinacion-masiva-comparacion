/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

var regards = [
  "Salut",
  "Hallo",
  "Ciao",
  "Ahoj",
  "Czesc",
  "Hola",
  "Shalom",
  "OlÃ¡",
  "Merhaba",
  "Kon'nichiwa",
  "NÃ­ hao",
];

$(document).ready(function () {
  showRandomMessage();
  showEmailModal();
});

function showEmailModal() {
  var full_name = $("#fullname").val();
  var phoneNumber = $("#phoneNumber").val();
  var inpMail = $("#email").val();
  if (inpMail == 0) {
    (async () => {
      const { value: email } = await Swal.fire({
        title: "Ops! Parece que no tenemos registrado su correo electrónico.",
        text: "Por seguridad, te pedimos que insertes un correo en donde puedas recibir todas tus novedades operativas de los contenedores coordinados, claves y muchas otras notificaciones",
        input: "email",
        type: "warning",
        inputLabel: "Your email address",
        inputPlaceholder: "Ingrese su correo electrónico",
        confirmButtonText: "Guardar",
        closeOnConfirm: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });
      var data = new Object();
      data.contactEmail = email;
      data.fullname = full_name;
      data.telephone = phoneNumber;
      data.source = "updateEmailModal";
      if (email) {
        fetch(url_application + "/User/updateUserData", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        })
          .then((response) => {
            console.log("Success:", response);
            Swal.fire(
              "Su correo ha sido actualizado correctamente",
              "",
              "success",
            );
          })
          .catch((error) => {
            Swal.fire("Invalid Request", "", "error");
          });
      }
    })();
  }
}

function showRandomMessage() {
  var index = Math.floor(Math.random() * 10);
  $("#welcomeMessageRandom").prepend(regards[index] + ", ");
}
