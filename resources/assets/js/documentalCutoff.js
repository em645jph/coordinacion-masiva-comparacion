/**
 * Author: Nahuel Faccinetti
 */

var currentYear = new Date().getFullYear();
var loaded = false;
var visitIdGlobal;
var vesselN;
var dateTime;
const validTime = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

$(document).ready(function () {
  $("#modalEditCutoff").modal("hide");
  $("#modalHistoryCutoff").modal("hide");
  $("#detailVisits").hide();

  findLoadVisit();

  $("#btnConfirm").click(function () {
    addOrUpdateCutoff();
  });

  $("#dateCutoff").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: 0,
    firstDay: 1,
    dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    monthNamesShort: [
      "Ene",
      "Feb",
      "Mar",
      "Abr",
      "May",
      "Jun",
      "Jul",
      "Ago",
      "Sep",
      "Oct",
      "Nov",
      "Dic",
    ],
    yearRange: currentYear + ":" + (currentYear + 1),
  });

  $("#iconCalendar").click(function () {
    $("#dateCutoff").focus();
  });

  $("#dateCutoff").attr("readonly", "readonly");
  $("#timeCutoff").focus(function () {
    $("#timeCutoff").attr("type", "time");
  });

  $("#timeCutoff").keypress(function (e) {
    if (e.which == 13) {
      addOrUpdateCutoff();
    }
  });

  initModals();
});

function initModals() {
  $("#modalHistoryCutoff").on("shown.bs.modal", function () {
    adjustDataTableColumns();
  });

  $("#modalHistoryCutoff").on("hidden.bs.modal", function () {
    deleteHistoryDataTable();
  });
}

function editCutoff(id) {
  visitIdGlobal = $(id).attr("visitId");
  vesselN =
    $(id).attr("vessel") +
    "/" +
    $(id).attr("ibVyg") +
    "/" +
    $(id).attr("obVyg");
  $("#dateCutoff").val("");
  $("#timeCutoff").val("");
  viewModel.vesselName(vesselN);
  $("#modalEditCutoff").modal("show");
}

function findLoadVisit() {
  var parameter = {
    param: "",
  };
  processingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/DocumentalCutoff/findLoadVisitByParam/",
    data: parameter,
    success: function (data, statusCode) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      deleteVisitDataTable();
      loadVeselVisitTable(data);
      validateQuantity(data);
      Swal.close();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function viewHistoryEventsIntern(visitToView) {
  var param = {
    visitId: visitToView.visitId,
  };
  $.ajax({
    type: "GET",
    url: url_application + "/DocumentalCutoff/getHistoryEvents/",
    data: param,
    success: function (data, statusCode) {
      showDocCutoffHistoryTable(data);
      $("#modalHistoryCutoff").modal();
    },
    fail: function (data, statusCode) {
      showErrorMsg(data);
    },
  });
}

function deleteVisitDataTable() {
  $("#dataTableVisits").DataTable().clear();
  $("#dataTableVisits").DataTable().destroy();
}

function deleteHistoryDataTable() {
  $("#docCutoffHistoryTable").DataTable().clear();
  $("#docCutoffHistoryTable").DataTable().destroy();
}

function initKnockout() {
  viewModel = {
    LoadVisit: ko.observableArray([]),
    vesselName: ko.observableArray(),
    historyEventList: ko.observableArray([]),
    viewHistoryEvents: function (visitToView) {
      viewHistoryEventsIntern(visitToView);
    },
  };
  ko.applyBindings(viewModel);
}

function loadVeselVisitTable(data) {
  viewModel.LoadVisit.removeAll();
  viewModel.LoadVisit(data.vesselVisitList);
  var table = $("#dataTableVisits").DataTable({
    bDestroy: true,
    bFilter: true,
    deferRender: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sInfoFiltered: " - filtrado de _MAX_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
    order: [[5, "desc"]],
    pageLength: 50,
  });
  if (data.hideEdit) $(table.column(6).visible(false));
}

function showDocCutoffHistoryTable(data) {
  viewModel.historyEventList.removeAll();
  viewModel.historyEventList(data);
  $("#docCutoffHistoryTable").DataTable({
    bDestroy: true,
    bFilter: true,
    deferRender: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
      sInfoFiltered: " - filtrado de _MAX_ registros",
      sLengthMenu: "Mostrar _MENU_ registros",
      oPaginate: {
        sPrevious: "Anterior",
        sNext: "Siguiente",
      },
    },
  });
}

function validateQuantity(data) {
  if (data.vesselVisitList.length > 0) {
    $("#detailVisits").show();
  } else {
    $("#detailVisits").show();
    $("tbody").addClass("tableEmpty");
    $("tbody").html(
      "No hay datos para mostrar. Pruebe refrescando la página o vuelva a intentarlo más tarde",
    );
  }
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function addOrUpdateCutoff() {
  var date = $("#dateCutoff").val().toString();
  var time = $("#timeCutoff").val().toString();
  dateTime = date + " " + time + ":00";
  if (date == null || date == "" || time == null || time == "") {
    Swal.fire("", "Por favor, complete los campos requeridos.", "info");
    return;
  } else if (!validTime.test(time)) {
    Swal.fire(
      "",
      "Por favor ingrese una fecha válida (formato HH:MM (24hs)",
      "info",
    );
    return;
  }

  Swal.fire({
    allowOutsideClick: false,
    type: "info",
    title: "Atención",
    html:
      "Se actualizará la fecha de Cutoff al " +
      date +
      " a las " +
      time +
      "hs para el buque " +
      vesselN,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Confirmar",
    cancelButtonText: "Descartar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      callToServiceAddOrUpdate();
    }
  });
}

function callToServiceAddOrUpdate() {
  var updateParam = {
    visitId: visitIdGlobal,
    cutoffDate: dateTime,
  };
  $.ajax({
    type: "POST",
    url: url_application + "/DocumentalCutoff/addOrUpdateCutoff/",
    data: updateParam,
    success: function (data, statusCode) {
      if (data.status == "ERROR") {
        Swal.fire("", data.msg, "error");
      } else {
        findLoadVisit();
        Swal.fire({
          type: "success",
          title: "Cutoff Actualizado Correctamente",
          showConfirmButton: false,
          timer: 2000,
        });
        $("#modalEditCutoff").modal("hide");
      }
    },
    fail: function (data, statusCode) {
      Swal.fire("", "Error al actualizar la fecha de Cutoff", "error");
    },
  });
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}
