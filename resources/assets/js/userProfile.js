/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var loaded = false;
var regex =
  /^([20]{2}|[23]{2}|[24]{2}|[27]{2}|[30]{2}|[33]{2}|[34]{2})[0-9]{8}([0-9]{1})/gm;
var cuilLengthRegex = /^([0-9]{11})$/g;
var delink;

$(document).ready(function () {
  $("#navLinkedAccounts").click(function () {
    reloadGrids();
  });

  $("#submitLinkUser").click(function () {
    findLinkUser($("#typeOfEntity").val());
  });

  $("#addLinkUserModal").on("hidden.bs.modal", function () {
    closeModalCleanInputs();
  });

  $("#navBalance").click(function () {
    drawBalances();
  });

  $("#btnUpdate").click(function () {
    updateMyData();
  });

  $("#navLinkedAccountsCompost").click(function () {
    loadLinkedUsersCompositionState();
  });

  $("#addLinkUser").click(function () {
    $("#typeOfEntity").val("BILLING");
  });

  $("#addLinkUserCompost").click(function () {
    $("#typeOfEntity").val("COMPOST");
  });
});

function loadLinkedUsersCompositionState() {
  findCompositionStateMyParents();
  findCompositionStateMyChildrens();
}

function findCompositionStateMyParents() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getMyParentsUsersToComposition",
    success: function (data) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadParentUsersCompositionBalanceCb(data);
    },
    fail: function (message) {
      showModal("", message, "error");
    },
  });
}

function findCompositionStateMyChildrens() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getMyChildrenUsersToComposition",
    success: function (data) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadChildUsersCompositionBalanceCb(data);
    },
    fail: function (message) {
      showModal("", message, "error");
    },
  });
}

function loadParentUsersCompositionBalanceCb(data) {
  showParentCompositionBalanceGrid(data);
}

function loadChildUsersCompositionBalanceCb(data) {
  showChildCompositionBalanceGrid(data);
}

function showParentCompositionBalanceGrid(data) {
  viewModel.parentUsers.removeAll();
  viewModel.compositionParentUsers(data);
  newDataTable("tbParentUsersCompost", null, true, true, false);
}

function showChildCompositionBalanceGrid(data) {
  viewModel.childUsers.removeAll();
  viewModel.compositionChildUsers(data);
  newDataTable("tbChildUsersCompost", null, true, true, false);
}

function reloadGrids() {
  deleteDataTables();
  drawMyParents();
  drawMyChildrens();
  findCompositionStateMyParents();
  findCompositionStateMyChildrens();
}

function initKnockout() {
  viewModel = {
    parentUsers: ko.observableArray([]),
    childUsers: ko.observableArray([]),
    balances: ko.observableArray([]),
    compositionParentUsers: ko.observableArray([]),
    compositionChildUsers: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showParentGrid(data) {
  viewModel.parentUsers.removeAll();
  viewModel.parentUsers(data);
  newDataTable("tbParentUsers", null, true, true, false);
}

function showChildGrid(data) {
  viewModel.childUsers.removeAll();
  viewModel.childUsers(data);
  newDataTable("tbChildUsers", null, true, true, false);
}

function showBalancesGrid(data) {
  viewModel.balances.removeAll();
  viewModel.balances(data);
  newDataTable("tbBalances", null, true, true, false);
}

function drawMyParents() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getMyParentsUsers",
    success: function (data) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadParentUsersCb(data);
    },
    fail: function (message) {
      showModal("", message, "error");
    },
  });
}

function drawMyChildrens() {
  $.ajax({
    type: "GET",
    url: url_application + "/User/getMyChildrenUsers",
    success: function (data) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadChildUsersCb(data);
    },
    fail: function (message) {
      showModal("", message, "error");
    },
  });
}

function loadParentUsersCb(data) {
  showParentGrid(data);
}

function loadChildUsersCb(data) {
  showChildGrid(data);
}

function loadBalancesCb(data) {
  showBalancesGrid(data);
}

function addLinkUser(type) {
  processingModal();
  var cuit = $("#cuitLink").val();
  if ($("#cuitLink").val() == "" || $("#cuitLink").val() == null) {
    showModal("", "Por favor, complete el CUIT/CUIL", "warning");
    return;
  }
  if (validateDocumentNumber(cuit)) {
    showModal(
      "",
      "El número de CUIL/CUIL sólo puede ser numérico y de 11 caracteres.",
      "warning",
    );
    return;
  }

  var data = new Object();
  data.cuitLink = cuit;
  data.type = type;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Customer/addLinkUser",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModal("", message, "success");
    //window.location.reload();
    closeModalCleanInputs();
    reloadGrids();
  });

  request.fail(function (message) {
    showModal("", message, "error");
  });
}

function closeModalCleanInputs() {
  $("#addLinkUserModal input").each(function (id) {
    $(this).val("");
  });
  $("#addLinkUserModal").modal("hide");
}

function findLinkUser(type) {
  processingModal();
  type = type;
  cuit = $("#cuitLink").val();

  if (cuit == "" || cuit == null || cuit == undefined) {
    showModal("", "Por favor, complete el CUIT/CUIL", "warning");
    return;
  }

  if (validateDocumentNumber(cuit)) {
    showModal(
      "",
      "El número de CUIL/CUIL sólo puede ser numérico y de 11 caracteres.",
      "warning",
    );
    return;
  }

  var data = new Object();
  data.cuitLink = cuit;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Customer/findLinkUser",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (usr) {
    if (usr == null) {
      showModal("", "Usuario no existe", "warning");
      return;
    } else {
      confirmModalHtml(
        "",
        "Usted va a conectar su CUIT/CUIL con la Razón Social: <strong>" +
          usr.fullName +
          "</strong><br> Está seguro?",
        "info",
        addLinkUser(type),
      );
      /*if(action != undefined && action == "ADD"){
				confirmModalHtml("","Usted va a conectar su CUIT/CUIL con la Razón Social: " + usr.fullName + "<br> Está seguro?","info", addLinkUser );
			} else if(action != undefined && action == "DELETE"){
				delink = cuit;
				confirmModalHtml("","Usted va a quitar la Razón Social: " + usr.fullName + " de sus usuarios linkeados. <br> Está seguro?","info", removeLinkUser );
			}*/
    }
  });

  request.fail(function (message) {
    showModal("", message.responseText, "warning");
  });
}

function confirmModalHtml(title, text, type, callback) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    html: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, estoy seguro!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      callback();
    }
  });
}

function validateDocumentNumber(value) {
  var error = false;
  (value.length == 0 || value == "") == true ? (error = true) : error;
  value.match(regex) == null ? (error = true) : error;
  value.match(cuilLengthRegex) == null ? (error = true) : error;
  return error;
}

function removeLinkUser(gkey, type) {
  processingModal();

  var data = new Object();
  data.gkey = gkey;
  data.type = type;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Customer/removeLinkUser",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModal("", message, "success");
    $("#navLinkedAccounts").click();
    closeModalCleanInputs();
    reloadGrids();
  });

  request.fail(function (message) {
    showModal("", message, "error");
  });
}

function drawBalances() {
  $.ajax({
    type: "GET",
    url: url_application + "/Customer/getBalanceAccount",
    success: function (data) {
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      loadBalancesCb(data);
    },
    fail: function (message) {
      showModal("", message, "error");
    },
  });
}

function updateMyData() {
  var data = new Object();
  data.fullname = $("#editFullname").val();
  data.contactEmail = $("#editContactEmail").val();
  data.telephone = $("#editTelephone").val();

  var request = $.ajax({
    type: "POST",
    url: url_application + "/User/updateUserData",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModal("", message, "info");
  });

  request.fail(function (message) {
    showModal("", message, "error");
  });
}
