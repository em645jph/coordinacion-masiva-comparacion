$(document).ready(function () {
  $("#btnRegister").click(function () {
    registerUser();
  });
  $('input[type="file"]').change(function () {
    checkextension(this.id);
  });
});

function saveUserForm() {
  var errorMsg = validateForm();
  if (errorMsg == "") {
    showProcessingModal();
    $.ajax({
      type: "POST",
      url: url_application + "/User/registerUser",
      data: tipServiceParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          cleanTipService();
          initLoad(false);
          showSuccessMsg("Listo!", msg);
        } else showErrorHtmlMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(statusCode + "Se produjo un error");
      },
    });
  } else {
    showErrorMsg(errorMsg);
  }
}

function registerUser() {
  var errorMsg = validateForm();
  if (errorMsg == "") {
    processingModal();
    var form = $("#formRegisterUser")[0];
    data = new FormData(form);

    var request = $.ajax({
      type: "POST",
      enctype: "multipart/form-data",
      url: url_application + "/User/registerUser",
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
    });

    request.done(function (message) {
      showModalCallback(
        "",
        message,
        "success",
        url_application + "/Invoice/invoicing",
      );
    });

    request.fail(function (message) {
      //alert("El archivo no es pdf");
      //showModal("",message,"error");
      showModal(
        "",
        "Por favor, adjunte la documentación solicitada.",
        "warning",
      );
    });
  } else {
    showErrorMsg(errorMsg);
  }
}

function validateForm() {
  var emailAddress = $("#emailAddress").val();
  var emailManagement = $("#emailManagement").val();
  var emailComex = $("#emailComex").val();
  var errorMsg = "";

  if (emailAddress == null || emailAddress == "") {
    errorMsg = "Ingresar email de contacto";
  } else if (emailManagement == null || emailManagement == "") {
    errorMsg = "Ingresar un mail de facturación";
  } else if (emailComex == null || emailComex == "") {
    errorMsg = "Ingresar un mail de comex";
  } else if (
    !isValidStr(emailAddress) ||
    !isValidStr(emailManagement) ||
    !isValidStr(emailComex)
  ) {
    errorMsg = "Ingresá un email válido";
  }

  return errorMsg;
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
