/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var loaded = false;
$(document).ready(function () {
  $("#btnSearch").click(function () {
    getData();
  });

  $("#btnClean").click(function () {
    $("#txtSearchFromDate,#txtSearchToDate").val("");
  });

  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchFromDate")
    .datepicker({ dateFormat: "yy-mm-dd" })
    .bind("change", function () {
      var minValue = $(this).val();
      minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
      minValue.setDate(minValue.getDate() + 1);
      $("#txtSearchToDate").datepicker("option", "minDate", minValue);
    });
});

function getData() {
  processingModal();

  var obj = new Object();
  obj.formStartDate = $("#txtSearchFromDate").val();
  obj.formEndDate = $("#txtSearchToDate").val();

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/logs/getAuditData",
    data: JSON.stringify(obj),
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadUsersCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function showUsersGrid(data) {
  viewModel.users.removeAll();
  deleteDataTables();
  viewModel.users(data);
  newDataTable("tbAudit", "60vh", true, true, false);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function initKnockout() {
  viewModel = {
    users: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadUsersCb(data) {
  showUsersGrid(data);
}

function millisecondsToDate(lastLogin) {
  if (lastLogin == null) {
    return "-";
  }
  var d = new Date(lastLogin);
  return d.toLocaleDateString() + " " + d.toLocaleTimeString();
}
