/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */
var amount = 0;

$(document).ready(function () {
  $("#btnValidateCode").click(function () {
    validateCodeIntern();
  });

  $("#resendCode").click(function () {
    resendValidCode();
  });
});

function generatePayment(code) {
  processingModal();
  var data = new Object();
  data.type = "SLDF";
  data.amount = amount;
  data.code = code;

  var request = $.ajax({
    url: url_application + "/Payment/addPayment",
    type: "POST",
    data: JSON.stringify(data),
    contentType: "application/json",
  });

  request.done(function (message) {
    showModalHtml("", message, "info");
    setTimeout(function () {
      window.location.href = url_application + "/Invoice/drafts";
    }, 2000);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "Error al ejecutar el Pago. Reintente mas tarde", "error");
  });
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function showModalConfirmation(text, val) {
  Swal.fire({
    allowOutsideClick: false,
    title: "",
    text: text,
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;",
    cancelButtonText: "No",
    footer: "<a href>Por qu&eacute; aparece este mensaje?</a>",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      amount = val;
      sendValidCode();
    }
  });
}

function sendValidCode() {
  processingModal();

  var request = $.ajax({
    url: url_application + "/Payment/sendCode",
    type: "POST",
  });

  request.done(function (message) {
    if (message == "EXISTS" || message == "SUCCESS") {
      getInputCode();
      return;
    } else {
      showModal(
        "",
        "Error al solicitar un codigo. Reintente mas tarde.",
        "error",
      );
      return;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "Error al solicitar un codigo. Reintente mas tarde", "error");
  });
}

function validateCode(code) {
  processingModal();

  var data = new Object();
  data.codeInputed = code;

  var request = $.ajax({
    url: url_application + "/Payment/validateCode",
    data: JSON.stringify(data),
    contentType: "application/json",
    type: "POST",
  });

  request.done(function (message) {
    if (message == "SUCCESS") {
      generatePayment(code);
    } else {
      showModal("", "Codigo inválido, reintente.", "warning");
      return;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "No se pudo validar el codigo ingresado.", "error");
  });
}

function resendValidCode() {
  processingModal();

  var request = $.ajax({
    url: url_application + "/Payment/resendCode",
    type: "POST",
  });

  request.done(function (message) {
    if (message == "EXISTS" || message == "SUCCESS") {
      getInputCode();
      return;
    } else {
      showModal(
        "",
        "Error al solicitar un codigo. Reintente mas tarde.",
        "error",
      );
      return;
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", "Error al solicitar un codigo. Reintente mas tarde", "error");
  });
}

function getInputCode() {
  swal.close();
  $("#modalCodeInputed").modal({
    backdrop: "static",
    keyboard: false,
  });
}

function validateCodeIntern() {
  var code = $("#txtCode").val();

  if (code == undefined || code == "" || code == null) {
    $("#labelMsg").text("Por favor, complete un código").show();
    return;
  }

  validateCode(code);
}
