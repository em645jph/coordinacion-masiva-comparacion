/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initKnockout();
  initHtmlElements();
  var tempUser = {};
});

function initKnockout() {
  vipViewModel = {
    searchTranTypeCb: ko.observableArray([]),
    categoryCb: ko.observableArray([]),
    dayCb: ko.observableArray([]),
    ruleTypeCb: ko.observableArray([]),
    apptList: ko.observableArray([]),
    customerList: ko.observableArray([]),
    customerEventList: ko.observableArray([]),
    apptEventList: ko.observableArray([]),
    apptQuotaList: ko.observableArray([]),
    eventList: ko.observableArray([]),
    activeCustomerList: ko.observableArray([]),
    deleteAppt: function (apptToDelete) {
      askToDeletAppt(apptToDelete);
    },
    deleteCustomer: function (customerToDelete) {
      deleteCustomerIntern(customerToDelete);
    },
    deleteEvent: function (eventToDelete) {
      deleteEventIntern(eventToDelete);
    },
    recoverCustomer: function (customerToRecover) {
      recoverCustomerIntern(customerToRecover);
    },
    recoverEvent: function (eventToRecover) {
      recoverEventIntern(eventToRecover);
    },
    viewCustomerEvent: function (customerKo) {
      viewCustomerEventIntern(customerKo);
    },
    viewApptEvent: function (apptKo) {
      viewApptEventIntern(apptKo);
    },
    viewApptQuota: function (apptKo) {
      viewApptQuotaIntern(apptKo);
    },
  };

  ko.applyBindings(vipViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initDateTimePickers();
  initCombos();
  initModal();
  initCheckBoxs();
  initLoad();
}

function initLoad() {
  initLoad();
}

function initInputs() {
  $("#txtSearchCustomer").focus();

  $("input[type=number]").keypress(function () {
    console.log("keypress");
    var key = event.keyCode ? event.keyCode : event.which;
    if ((key < 48 || key > 57) && key != 95) return false;
    return true;
  });

  toggleDayCb(true);
  toggleDateInput(true);
}

function initButtons() {
  $("#btnSearchVipAppt").click(function () {
    searchVipAppt(true);
  });

  $("#btnSearchVipAppt").click(function () {
    searchVipAppt(true);
  });

  $("#btnCleanSearchVipAppt").click(function () {
    cleanSearchVipAppt();
  });

  $("#btnSaveVipAppt").click(function () {
    addVipAppt();
  });

  $("#btnSearchUser").click(function () {
    searchUser();
  });

  $("#btnSaveVipCustomer").click(function () {
    addVipCustomer();
  });

  $("#btnSaveVipEvent").click(function () {
    addVipEvent();
  });

  $("#btnSaveVipApptEvent").click(function () {
    updateVipApptEvent();
  });

  $("#btnSaveApptQuota").click(function () {
    updateVipApptQuota();
  });

  $("#btnSaveVipCustomerEvent").click(function () {
    updateVipCustomerEvent();
  });

  $("#btnRefreshVipCustomer").click(function () {
    getAllVipCustomer(true);
  });

  $("#btnRefreshVipEvent").click(function () {
    getAllVipEvent(true);
  });
}

function initDateTimePickers() {
  $("#txtDate").datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
}

function initCombos() {
  $("#cbRuleType").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "WEEKDAY") {
      toggleDayCb(false);
      toggleDateInput(true);
    } else if (selectedValue == "DATE") {
      toggleDayCb(true);
      toggleDateInput(false);
      $("#txtDate").focus();
    } else {
      toggleDayCb(true);
      toggleDateInput(true);
    }
  });
}

function initModal() {
  $("#addVipApptModal").on("shown.bs.modal", function () {
    getActiveVipCustomer();
  });

  $("#addVipApptModal").on("hide.bs.modal", function () {
    cleanAddVipApptModal();
  });

  $("#addVipCustomerModal").on("shown.bs.modal", function () {
    $("#txtCustomer").focus();
  });

  $("#addVipCustomerModal").on("hide.bs.modal", function () {
    cleanAddVipCustomerModal();
  });

  $("#addVipEventModal").on("shown.bs.modal", function () {
    $("#txtVipEventId").focus();
  });

  $("#addVipEventModal").on("hide.bs.modal", function () {
    cleanAddVipEventModal();
  });

  $("#editVipApptEventModal").on("shown.bs.modal", function () {
    showApptEventTable();
    adjustDataTableColumns();
  });

  $("#editVipApptQuotaModal").on("shown.bs.modal", function () {
    showApptQuotaTable();
    adjustDataTableColumns();
  });

  $("#editVipQuotaEventModal").on("hide.bs.modal", function () {
    deleteApptQuotaDataTable();
  });

  $("#editVipCustomerEventModal").on("shown.bs.modal", function () {
    showCustomerEventTable();
    adjustDataTableColumns();
  });

  $("#editVipCustomerEventModal").on("hide.bs.modal", function () {
    $("#ckCheckAllEvent").prop("checked", false);
    deleteCustomerEventDataTable();
  });

  $("#editVipCustomerEventModal").on("hide.bs.modal", function () {
    $("#ckCheckAllApptEvent").prop("checked", false);
    deleteApptEventDataTable();
  });

  $(".nav-tabs a").on("shown.bs.tab", function () {
    adjustDataTableColumns();
  });
}

function initCheckBoxs() {
  $("#ckCheckAllEvent").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectableEvent:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });

  $("#ckCheckAllApptEvent").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectableApptEvent:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function initLoad() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/initLoad/",
    success: function (data, statusCode) {
      loadCategoryCb(data["categoryList"]);
      loadDayCb(data["dayList"]);
      loadRuleTypeCb(data["ruleTypeList"]);
      showVipApptTable(data["vipApptList"]);
      showVipCustomerTable(data["vipCustomerList"]);
      showVipEventTable(data["vipEventList"]);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function searchVipAppt(showLoading) {
  var errorMsg = validateCustomerWildcardField();
  if (errorMsg == "") {
    if (showLoading) showProcessingModal();
    var searchParam = {
      prCustomerWildCard: $("#txtSearchCustomer").val(),
      prCategory: $("#cbSearchCategory option:selected").val(),
      prDayStr: $("#cbSearchDayOfWeek option:selected").val(),
    };
    $.ajax({
      type: "GET",
      data: searchParam,
      url: url_application + "/VipManagement/findVipAppt/",
      success: function (result, statusCode) {
        if (showLoading) closeProcessingModal();
        var msg = result["msg"];
        if (typeof msg != "undefined" && !resultIsOk(result)) {
          showErrorHtmlMsg(msg);
        } else {
          showVipApptTable(result);
        }
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  }
}

function searchUser() {
  var errorMsg = validateUserWildcardField();
  if (errorMsg == "") {
    showProcessingModal();
    var searchParam = { prWildcard: $("#txtCustomer").val() };
    $.ajax({
      type: "GET",
      data: searchParam,
      url: url_application + "/VipManagement/findUser/",
      success: function (result, statusCode) {
        closeProcessingModal();
        var msg = result["msg"];
        if (typeof msg != "undefined" && !resultIsOk(result)) {
          showErrorHtmlMsg(msg);
        } else {
          tempUser = result;
          fillCustomerModalInput(result);
        }
      },
      fail: function (data, statusCode) {
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  }
}

function getAllVipAppt(showLoading) {
  if (showLoading) showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getActiveVipAppt/",
    success: function (data, statusCode) {
      showVipApptTable(data);
      if (showLoading) closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function getAllVipCustomer(showLoading) {
  if (showLoading) showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getAllVipCustomer/",
    success: function (data, statusCode) {
      showVipCustomerTable(data);
      if (showLoading) closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function getActiveVipCustomer() {
  showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getActiveVipCustomer/",
    success: function (data, statusCode) {
      loadVipCustomerCb(data);
      closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function getAllVipEvent(showLoading) {
  if (showLoading) showProcessingModal();
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getAllVipEvent/",
    success: function (data, statusCode) {
      showVipEventTable(data);
      if (showLoading) closeProcessingModal();
    },
    fail: function (data, statusCode) {
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function addVipAppt() {
  var vipApptParam = {
    vipCustomerGkey: $("#cbVipCustomer option:selected").val(),
    category: $("#cbCategory option:selected").val(),
    ruleType: $("#cbRuleType option:selected").val(),
    dayStr: $("#cbDayOfWeek option:selected").val(),
    dateStr: $("#txtDate").val(),
    applyCustomerEvent: $("#ckApplyCustomerEvent").prop("checked"),
  };
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/addVipAppt/",
    data: JSON.stringify(vipApptParam),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        $("#btnCancelSaveVipAppt").click();
        showSuccessMsg("Listo!", msg);
        searchVipAppt(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          "Se produjo un error guardando el turno vip. Intentá nuevamente",
      );
    },
  });
}

function addVipCustomer() {
  if (typeof tempUser == "undefined")
    showErrorMsg("Ingresá la información de un cliente.");
  else {
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/VipManagement/addVipCustomer/",
      data: JSON.stringify(tempUser),
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          $("#btnCancelSaveVipCustomer").click();
          showSuccessMsg("Listo!", msg);
          getAllVipCustomer(false);
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el cliente vip. Por favor intente nuevamente",
        );
      },
    });
  }
}

function addVipEvent() {
  var errorMsg = validateEventIdField();
  if (errorMsg == "") {
    var eventParam = {
      prEventId: $("#txtVipEventId").val(),
    };
    console.log("eventParam:" + JSON.stringify(eventParam));
    $.ajax({
      type: "POST",
      url: url_application + "/VipManagement/addVipEvent/",
      data: eventParam,
      success: function (result, statusCode) {
        var msg = result["msg"];
        if (resultIsOk(result)) {
          $("#btnCancelSaveVipEvent").click();
          showSuccessMsg("Listo!", msg);
          getAllVipEvent(false);
        } else showErrorMsg(msg);
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            "Se produjo un error guardando el evento. Intentá nuevamente",
        );
      },
    });
  } else {
    showErrorMsg(errorMsg);
    $("#txtVipEventId").focus();
  }
}

function viewApptEventIntern(apptKo) {
  showProcessingModal();
  var searchParam = {
    prVipApptGkeyStr: apptKo["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getVipApptEvents/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        closeProcessingModal();
        fillApptEventTable(data);
        $("#editVipApptEventModal").modal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function viewApptQuotaIntern(apptKo) {
  showProcessingModal();
  var searchParam = {
    prVipApptGkeyStr: apptKo["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getVipApptQuota/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        closeProcessingModal();
        fillApptQuotaTable(data);
        $("#editVipApptQuotaModal").modal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function viewCustomerEventIntern(customerKo) {
  showProcessingModal();
  var searchParam = {
    prVipCustomerGkeyStr: customerKo["gkey"],
  };
  $.ajax({
    type: "GET",
    url: url_application + "/VipManagement/getVipCustomerEvents/",
    data: searchParam,
    success: function (data, statusCode) {
      var msg = data["msg"];
      if (typeof msg != "undefined" && !resultIsOk(data)) {
        showErrorHtmlMsg(msg);
      } else {
        closeProcessingModal();
        fillCustomerEventTable(data);
        $("#editVipCustomerEventModal").modal();
      }
    },
    fail: function (data, statusCode) {
      s;
      showErrorMsg("Status code:" + statusCode);
    },
  });
}

function updateVipApptEvent() {
  showProcessingModal();
  var param = getApptEventToUpdate();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/updateVipApptEvent/",
    data: JSON.stringify(param),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        closeProcessingModal();
        getAllVipAppt(true);
        $("#btnCancelSaveVipApptEvent").click();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error actualizando los eventos del turno vip. Intentá nuevamente",
      );
    },
  });
}

function updateVipApptQuota() {
  showProcessingModal();
  var param = getApptQuotaToUpdate();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/updateVipApptQuota/",
    data: JSON.stringify(param),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        closeProcessingModal();
        getAllVipAppt(true);
        $("#btnCancelSaveVipApptQuota").click();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error actualizando las reservas del turno vip. Intentá nuevamente",
      );
    },
  });
}

function updateVipCustomerEvent() {
  showProcessingModal();
  var param = getCustomerEventToUpdate();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/updateVipCustomerEvent/",
    data: JSON.stringify(param),
    success: function (result, statusCode) {
      var msg = result["msg"];
      if (resultIsOk(result)) {
        closeProcessingModal();
        getAllVipCustomer(true);
        $("#btnCancelSaveVipCustomerEvent").click();
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error actualizando los eventos del cliente vip. Intentá nuevamente",
      );
    },
  });
}

function askToDeletAppt(apptToDelete) {
  Swal.fire({
    allowOutsideClick: false,
    title: "Desea continuar?",
    text: "No podrá recuperar el registro",
    type: "question",
    showCancelButton: true,
    confirmButtonColor: "#4fa7f3",
    cancelButtonColor: "#d57171",
    confirmButtonText: "Sí, eliminar!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.value) {
      deleteApptIntern(apptToDelete);
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      adjustDataTableColumns();
    }
  });
}

function deleteApptIntern(apptToDelete) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/deleteVipAppt/",
    data: JSON.stringify(apptToDelete),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        searchVipAppt(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el turno vip. Intentá nuevamente",
      );
    },
  });
}

function deleteCustomerIntern(cutomerToDelete) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/deleteVipCustomer/",
    data: JSON.stringify(cutomerToDelete),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        closeProcessingModal();
        showSuccessMsg("Listo!", msg);
        getAllVipCustomer(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el cliente vip. Intentá nuevamente",
      );
    },
  });
}

function deleteEventIntern(eventToDelete) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/deleteVipEvent/",
    data: JSON.stringify(eventToDelete),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        getAllVipEvent(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error eliminando el evento. Intentá nuevamente",
      );
    },
  });
}

function recoverCustomerIntern(customerToRecover) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/recoverVipCustomer/",
    data: JSON.stringify(customerToRecover),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        getAllVipCustomer(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error activando el cliente vip. Intentá nuevamente",
      );
    },
  });
}

function recoverEventIntern(eventToRecover) {
  showProcessingModal();
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/VipManagement/recoverVipEvent/",
    data: JSON.stringify(eventToRecover),
    success: function (result, statusCode) {
      closeProcessingModal();
      var msg = result["msg"];
      if (resultIsOk(result)) {
        showSuccessMsg("Listo!", msg);
        getAllVipEvent(false);
      } else showErrorMsg(msg);
    },
    fail: function (result, statusCode) {
      showErrorMsg(
        statusCode +
          " Se produjo un error activando el evento. Intentá nuevamente",
      );
    },
  });
}

function showVipApptTable(data) {
  vipViewModel.apptList.removeAll();
  deleteVipApptDataTable();
  vipViewModel.apptList(data);
  var table = $("#tbVipAppt").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function showVipCustomerTable(data) {
  vipViewModel.customerList.removeAll();
  deleteVipCustomerDataTable();
  vipViewModel.customerList(data);
  var table = $("#tbVipCustomer").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function showVipEventTable(data) {
  vipViewModel.eventList.removeAll();
  deleteVipEventDataTable();
  vipViewModel.eventList(data);
  var table = $("#tbVipEvent").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "60vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function showApptEventTable() {
  var table = $("#tbApptEvent").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: false,
    sScrollY: "40vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function showApptQuotaTable() {
  var table = $("#tbApptQuota").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: false,
    sScrollY: "40vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function showCustomerEventTable() {
  var table = $("#tbCustomerEvent").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: false,
    sScrollY: "40vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sSearch: "Buscar",
    },
  });
}

function loadCategoryCb(data) {
  vipViewModel.categoryCb.removeAll();
  vipViewModel.categoryCb(data);
}

function loadDayCb(data) {
  vipViewModel.dayCb.removeAll();
  vipViewModel.dayCb(data);
}

function loadRuleTypeCb(data) {
  vipViewModel.ruleTypeCb.removeAll();
  vipViewModel.ruleTypeCb(data);
}

function loadVipCustomerCb(data) {
  vipViewModel.activeCustomerList.removeAll();
  vipViewModel.activeCustomerList(data);
}

function refreshFilterSearchData() {
  $("#btnSearchFilter").click();
}

function refreshRuleSearchData() {
  $("#btnSearchRule").click();
}

function validateCustomerWildcardField() {
  var wildcard = $("#txtSearchCustomer").val();
  var errorMsg = "";
  if (!isValidStr(wildcard))
    errorMsg = "El campo no puede contener caracteres especiales";
  return errorMsg;
}

function validateUserWildcardField() {
  var wildcard = $("#txtCustomer").val();
  var errorMsg = "";
  if (!isValidStr(wildcard))
    errorMsg = "El campo no puede contener caracteres especiales";
  else if (wildcard == "") errorMsg = "Ingresá el nombre o CUIT del cliente";
  return errorMsg;
}

function validateEventIdField() {
  var id = $("#txtVipEventId").val();
  var errorMsg = "";
  if (!isValidStr(id))
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "") errorMsg = "El campo evento es obligatorio";
  return errorMsg;
}

function fillApptEventTable(data) {
  vipViewModel.apptEventList.removeAll();
  deleteApptEventDataTable();
  vipViewModel.apptEventList(data);
}

function fillApptQuotaTable(data) {
  vipViewModel.apptQuotaList.removeAll();
  deleteApptEventDataTable();
  vipViewModel.apptQuotaList(data);
}

function fillApptEventTable(data) {
  vipViewModel.apptEventList.removeAll();
  deleteApptEventDataTable();
  vipViewModel.apptEventList(data);
}

function fillApptQuotaTable(data) {
  vipViewModel.apptQuotaList.removeAll();
  deleteApptEventDataTable();
  vipViewModel.apptQuotaList(data);
}

function fillCustomerModalInput(user) {
  $("#txtCustomerName").val(user["full_name"]);
  $("#txtCustomerDocNbr").val(user["document_nbr"]);
}

function fillCustomerEventTable(data) {
  vipViewModel.customerEventList.removeAll();
  deleteCustomerEventDataTable();
  vipViewModel.customerEventList(data);
}

function getApptEventToUpdate() {
  var selectedEvents = $(".selectableApptEvent:checked");
  var eventGkeyArray = [];
  var apptGkey = "";
  var paramObject = {};
  if (selectedEvents != null) {
    selectedEvents.each(function () {
      var selectedEvent = $(this);
      var gkey = selectedEvent.attr("gkey");
      if (gkey != null && gkey != "") {
        eventGkeyArray.push(gkey);
      }
      apptGkey = selectedEvent.attr("vipApptGkey");
    });
  }
  if (apptGkey == "") {
    var notSelectedEvents = $(".selectableApptEvent:not(:checked)");
    notSelectedEvents.each(function () {
      var notSelectedEvent = $(this);
      apptGkey = notSelectedEvent.attr("vipApptGkey");
    });
  }
  paramObject["apptGkeyStr"] = apptGkey;
  paramObject["eventGkeyArray"] = eventGkeyArray;
  console.log("paramObject" + JSON.stringify(paramObject));
  return paramObject;
}

function getApptQuotaToUpdate() {
  var inputQuotas = $(".inputable:enabled");
  var paramObject = [];
  if (inputQuotas != null) {
    inputQuotas.each(function () {
      var paramQuota = {};
      var inputQuota = $(this);
      var gkey = inputQuota.attr("gkey");
      var apptGkey = inputQuota.attr("vipApptGkey");
      var columnName = inputQuota.attr("columnName");
      var pdGkey = inputQuota.attr("pdGkey");
      var value = inputQuota.val();
      paramQuota["quotaGkey"] = gkey;
      paramQuota["vipApptGkey"] = apptGkey;
      paramQuota["columnName"] = columnName;
      paramQuota["pdGkey"] = pdGkey;
      paramQuota["value"] = value;
      paramObject.push(paramQuota);
    });
  }
  console.log("paramObject" + JSON.stringify(paramObject));
  return paramObject;
}

function getCustomerEventToUpdate() {
  var selectedEvents = $(".selectableEvent:checked");
  var eventGkeyArray = [];
  var customerGkey = "";
  var paramObject = {};
  if (selectedEvents != null) {
    selectedEvents.each(function () {
      var selectedEvent = $(this);
      var gkey = selectedEvent.attr("gkey");
      if (gkey != null && gkey != "") {
        eventGkeyArray.push(gkey);
      }
      customerGkey = selectedEvent.attr("vipCustomerGkey");
    });
  }
  if (customerGkey == "") {
    var notSelectedEvents = $(".selectableEvent:not(:checked)");
    notSelectedEvents.each(function () {
      var notSelectedEvent = $(this);
      customerGkey = notSelectedEvent.attr("vipCustomerGkey");
    });
  }
  paramObject["customerGkeyStr"] = customerGkey;
  paramObject["eventGkeyArray"] = eventGkeyArray;
  console.log("paramObject" + JSON.stringify(paramObject));
  return paramObject;
}

function cleanSearchVipAppt() {
  $("#txtSearchCustomer").val("");
  $("#cbSearchCategory").val("");
  $("#cbSearchDayOfWeek").val("");
  $("#txtSearchCustomer").focus();
}

function cleanAddVipApptModal() {
  $("#cbVipCustomer").val("");
  $("#cbCategory").val("");
  $("#cbRuleType").val("");
  $("#cbDayOfWeek").val("");
  $("#txtDate").val("");
  $("ckApplyCustomerEvent").prop("checked", true);
}

function cleanAddVipEventModal() {
  $("#txtVipEventId").val("");
}

function cleanAddVipCustomerModal() {
  $("#txtCustomer").val("");
  $("#txtCustomerName").val("");
  $("#txtCustomerDocNbr").val("");
  tempUser = {};
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteVipApptDataTable() {
  $("#tbVipAppt").DataTable().clear();
  $("#tbVipAppt").DataTable().destroy();
}

function deleteVipCustomerDataTable() {
  $("#tbVipCustomer").DataTable().clear();
  $("#tbVipCustomer").DataTable().destroy();
}

function deleteVipEventDataTable() {
  $("#tbVipEvent").DataTable().clear();
  $("#tbVipEvent").DataTable().destroy();
}

function deleteCustomerEventDataTable() {
  $("#tbCustomerEvent").DataTable().clear();
  $("#tbCustomerEvent").DataTable().destroy();
}

function deleteApptEventDataTable() {
  $("#tbApptEvent").DataTable().clear();
  $("#tbApptEvent").DataTable().destroy();
}

function deleteApptEventDataTable() {
  $("#tbApptQuota").DataTable().clear();
  $("#tbApptQuota").DataTable().destroy();
}

function toggleDayCb(hide) {
  if (hide) {
    $("#cbDayOfWeek").val("");
    $("#divDayOfWeek").hide();
  } else $("#divDayOfWeek").show();
}

function toggleDateInput(hide) {
  if (hide) {
    $("#txtDate").val("");
    $("#divDate").hide();
  } else $("#divDate").show();
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
