var loaded = false;
var totalBalance = 0;
var totalDelayed = 0;
/* Global var for counter */
var giCount = 1;
Dropzone.autoDiscover = false;

$(document).ready(function () {
  $("#inputAmount").maskMoney({
    thousands: ",",
    symbol: "$ ",
    symbolStay: false,
    precision: 2,
    allowNegative: true,
  });

  processingModal();
  getMyCustomers();

  $("#btnFindCustomerInformation").click(function () {
    giCount = 1;
    var cuitSelected = $("#selectCustomer").children("option:selected").val();
    if (
      cuitSelected == "" ||
      cuitSelected == undefined ||
      cuitSelected == null
    ) {
      showModal("Atención!", "Seleccione un CUIT a gestionar", "warning");
      return;
    } else {
      prepareCustomerData(
        $("#selectCustomer").children("option:selected").val(),
      );
    }
  });

  $("#inputDate").datepicker({ dateFormat: "yy-mm-dd", maxDate: new Date() });

  $("#tbBalance tbody").on("click", "tr", function () {
    setTimeout(function () {
      calculateSelectedRows();
    }, 100);
  });

  $("#addItem").click(function () {
    addItemsToTable();
  });

  $("#selectItemType").change(function () {
    var type = parseInt($("#selectItemType").children("option:selected").val());
    if (type === 1) {
      $("#inputDate").datepicker(
        "option",
        "maxDate",
        new Date(new Date(new Date().setDate(new Date().getDate() + 15))),
      );
      prepareTransferInformation();
    } else if (type == 1 || type == 2 || type == 3 || type == 4) {
      $("#inputDate").datepicker("option", "maxDate", new Date());
      prepareRettInfo();
    } else if (type == 5) {
      prepareDiffInfo();
    } else {
      prepareInputsDefault();
    }
  });

  $("#myDropzone").dropzone({
    url: url_application + "/balance/createPresentationByCustomer",
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 5,
    maxFiles: 5,
    maxFilesize: 1,
    addRemoveLinks: true,
    init: function () {
      dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

      // for Dropzone to process the queue (instead of default form behavior):
      document
        .getElementById("btnSendPresent")
        .addEventListener("click", function (e) {
          if (dzClosure.files.length < 1) {
            showModal(
              "Atención!",
              "Debe adjuntar al menos 1 comprobante. Por favor, verifique y vuelva a intentar.",
              "warning",
            );
            return;
          }

          processingModal();

          var amountSelected = 0;
          var amountInputs = 0;
          var selectedData = $("#tbCustomerReportDocuments")
            .DataTable()
            .rows()
            .data()
            .toArray();
          var inputData = $("#tbItems").DataTable().rows().data().toArray();
          for (var i = 0; i < selectedData.length; i++) {
            amountSelected += parseFloat(selectedData[i][2].replace(/\,/g, ""));
          }
          for (var i = 0; i < inputData.length; i++) {
            if (inputData[i][4] < 0) {
              amountInputs += parseFloat(inputData[i][4]);
            } else {
              amountInputs += parseFloat(inputData[i][4].replace(/\,/g, ""));
            }
          }
          if (amountSelected < 0) {
            showModal(
              "Atención!",
              "El monto total seleccionado para presentar no puede ser negativo. Elija comprobantes para un total mayor a 0. Por favor, verifique y vuelva a intentar.",
              "warning",
            );
            return;
          }
          var amountDiff = amountSelected - amountInputs;
          if (amountDiff > 10) {
            showModal(
              "Atención!",
              "El monto ingresado [$" +
                parseMoneyValue(amountInputs) +
                "] y el monto seleccionado[$" +
                parseMoneyValue(amountSelected) +
                "] no coinciden en su totalidad[Diferencia = $" +
                parseMoneyValue(amountDiff) +
                "]. Por favor, verifique y vuelva a intentar.",
              "warning",
            );
            return;
          }

          // Make sure that the form isn't actually being sent.
          e.preventDefault();
          e.stopPropagation();
          dzClosure.processQueue();
        });

      dzClosure.on("sendingmultiple", function (data, xhr, formData) {
        var myObj, x;
        var docs = collectDocs();
        var items = collectItems();
        myObj = {
          customer: $("#selectCustomer").children("option:selected").val(),
          //"acctnbr":
          docs: docs,
          items: items,
        };

        // this will get sent
        formData.append("jsonObject", JSON.stringify(myObj));
      });

      dzClosure.on("complete", function (file) {
        if (
          this.getUploadingFiles().length === 0 &&
          this.getQueuedFiles().length === 0
        ) {
          showModal("", "Presentación enviada correctamente.", "success");
          $("#modalFiles").modal("hide");
        }
      });
    },
  });

  $("#selectBank").change(function () {
    $("#inputConcept").val($("#selectBank").children("option:selected").val());
  });

  /* Add a click handler to the rows - this could be used as a callback */
  $("#tbItems tbody").click(function (event) {
    $($("#tbItems").dataTable().fnSettings().aoData).each(function () {
      $(this.nTr).removeClass("row_selected");
    });
    $(event.target.parentNode).addClass("row_selected");
  });

  /* Add a click handler for the delete row */
  $("#removeItem").click(function () {
    var oTable = $("#tbItems").dataTable();
    var anSelected = fnGetSelected(oTable);
    oTable.fnDeleteRow(anSelected[0]);
  });
});

$(document).keyup(function (e) {
  if (e.key === "Escape") {
    // escape key maps to keycode `27`
    $("#tbBalance").DataTable().rows().deselect();
    calculateSelectedRows();
  }
});

$("#inputAmount").on("cut copy paste", function (e) {
  e.preventDefault();
  console.log("Copiar y pegar no habilitado");
});

function prepareInputsDefault() {
  $("#selectBank,#selectItemType").val(-1);
  $("#lblInputConcept").text("Concepto");
  $("#divFormConcept").show();
  $("#divSelectTransfer").hide();
  $("#inputDate,#inputReference,#inputAmount,#inputConcept").val("");
  $("#lblDiffCambio").hide();
}

function prepareRettInfo() {
  $("#inputConcept,#inputReference").val("");
  $("#divFormConcept,#divFormReference").show();
  $("#divSelectTransfer").hide();
  $("#lblInputConcept").text("Ret. Certificado Nro:");
  $("#lblDiffCambio").hide();
}

function prepareDiffInfo() {
  $("#divFormConcept").show();
  $("#divFormConcept,#divFormReference,#divSelectTransfer").hide();
  $("#inputConcept,#inputReference").val("DIFERENCIA CAMBIO");
  $("#lblInputConcept").text("Concepto");
  $("#lblDiffCambio").show();
}

function prepareTransferInformation() {
  $("#divSelectTransfer").show();
  $("#divFormConcept").hide();
  $("#inputReference").val("");
  $("#divFormReference").show();
  $("#lblDiffCambio").hide();
}

function showModalInputsData() {
  if ($("#tbBalance").DataTable().rows({ selected: true }).count() > 0) {
    Dropzone.forElement("#myDropzone").removeAllFiles(true);
    //clearDataTablesModal();
    prepareInputsDefault();
    $("#modalFiles").modal();
    $("#tbItems").DataTable().clear().draw();
    setTimeout(function () {
      newDataTableWithTotals(
        "tbItems",
        "15vh",
        true,
        true,
        false,
        false,
        null,
        4,
      );
      var dataTable = newDataTableWithTotals(
        "tbCustomerReportDocuments",
        "15vh",
        true,
        true,
        true,
        false,
        null,
        2,
      );
      completeSelectedData(dataTable);
    }, 1500);
  } else {
    showModal(
      "",
      "Por favor, seleccione al menos 1 item de la lista para presentar",
      "error",
    );
  }
}

function Value(id, type, value) {
  var self = this;
  this.type = ko.observable(id);
  this.type = ko.observable(type);
  this.value = ko.observable(value);
}

function Register(id, date, document, amount) {
  var self = this;
  this.id = ko.observable(id);
  this.documentDate = ko.observable(date);
  this.documentNumber = ko.observable(document);
  this.documentAmount = ko.observable(amount);
}

function Item(id, type, concept, date, reference, amount) {
  var self = this;
  this.id = ko.observable(id);
  this.type = ko.observable(type);
  this.concept = ko.observable(concept);
  this.date = ko.observable(date);
  this.reference = ko.observable(reference);
  this.amount = ko.observable(amount);
}

function completeSelectedData(dataTable) {
  /*setTimeout(function(){
		var dataTable = newDataTableWithTotals('tbCustomerReportDocuments',"15vh",true,true,true,false,null,2);
	}, 1500);*/
  var data = $("#tbBalance")
    .DataTable()
    .rows({ selected: true })
    .data()
    .toArray();
  var array = [];
  dataTable.clear();
  for (var i = 0; i < data.length; i++) {
    array = [];
    array.push(data[i][1], data[i][2], parseMoneyValue(data[i][4]));
    dataTable.row.add(array);
  }
  dataTable.draw();

  var amounts = 0;
  amounts = $("#tbCustomerReportDocuments").DataTable().column(2).data();
  var total = 0;
  for (var i = 0; i < amounts.length; i++) {
    total += parseFloat(amounts[i].replace(",", ""));
  }
  $("#selectedRegsValues").text("Total: $" + parseMoneyValue(total));
  //$('#divModalBody').prepend("<button class='btn btn-primary' id='selectedRegsValues' style='float:right;'>Monto Seleccionado: <strong>"+total.toFixed(2)+"</strong></button><br><br>");
}

function clearDataTablesModal() {
  $("#tbItems,#tbCustomerReportDocuments").DataTable().clear().draw();
}

function compareDataSelectedToInputs() {
  var amountSelected = 0;
  var amountInputs = 0;
  var selectedData = null;
  var inputData = null;
  selectedData = $("#tbCustomerReportDocuments")
    .DataTable()
    .rows()
    .data()
    .toArray();
  inputData = $("#tbItems").DataTable().rows().data().toArray();
  for (var i = 0; i < selectedData.length; i++) {
    amountSelected += parseFloat(selectedData[i][2].replace(/\,/g, ""));
  }
  for (var i = 0; i < inputData.length; i++) {
    amountInputs += parseFloat(inputData[i][4].replace(/\,/g, ""));
  }
  if (amountSelected < 0) {
    showModal(
      "Atención!",
      "El monto total seleccionado para presentar no puede ser negativo. Elija comprobantes para un total mayor a 0. Por favor, verifique y vuelva a intentar.",
      "warning",
    );
    return false;
  }
  var amountDiff = 0;
  amountDiff = amountSelected - amountInputs;
  if (amountDiff < -10) {
    showModal(
      "Atención!",
      "El monto ingresado [$" +
        parseMoneyValue(amountInputs) +
        "] y el " +
        "monto seleccionado[$" +
        parseMoneyValue(amountSelected) +
        "] no coinciden en su totalidad[Diferencia = $" +
        parseMoneyValue(amountDiff) +
        "].  " +
        "Por favor, verifique y vuelva a intentar.",
      "warning",
    );
    return false;
  }
  return true;
}

function addItemsToTable() {
  var isOk = $("#selectItemType").children("option:selected").val() == "-1";
  var type = $("#selectItemType").children("option:selected").text();
  var date = $("#inputDate").val();
  var concept = $("#inputConcept").val();
  var reference = $("#inputReference").val();
  var amount = $("#inputAmount").val();
  if (reference === "DIFERENCIA CAMBIO") {
    if (amount > 0) {
      showModal(
        "",
        "Recuerde que los valores de Diferencia de Cambio deben ingresarse con valores negativos.",
        "error",
      );
      return;
    }
  }

  if (
    !isOk &&
    !isNullOrEmpty(date) &&
    !isNullOrEmpty(concept) &&
    !isNullOrEmpty(amount)
  ) {
    $("#tbItems")
      .dataTable()
      .fnAddData([type, concept, date, reference, amount]);

    giCount++;
    prepareInputsDefault();
  } else {
    showModal(
      "",
      "Por favor, complete los datos antes de agregarlos a la lista.",
      "error",
    );
    return;
  }
}

function isNullOrEmpty(value) {
  return value == null || value == "" || value == undefined;
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
  var aReturn = new Array();
  var aTrs = oTableLocal.fnGetNodes();

  for (var i = 0; i < aTrs.length; i++) {
    if ($(aTrs[i]).hasClass("row_selected")) {
      aReturn.push(aTrs[i]);
    }
  }
  return aReturn;
}

function getValuesAssociatedByDocumentNbr(data, event) {
  var amount = data.amount;
  processingModal();
  var paramObject = {};
  paramObject["documentNbr"] = data.documentNbr;
  paramObject["documentDate"] = data.documentDate;
  $("#titleInformationData").text(
    "Información adicional de " + data.documentNbr,
  );

  var request = $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/balance/getValuesAssociatedByDocumentNbr",
    data: JSON.stringify(paramObject),
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    data.usdAmount = amount / data.usdAmount;
    loadInformationCb(data);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function collectDocs() {
  var data = $("#tbCustomerReportDocuments")
    .DataTable()
    .rows()
    .data()
    .toArray();
  var array = [];
  var register = "";
  for (var i = 0; i < data.length; i++) {
    register = {
      documentDate: data[i][0],
      documentNumber: data[i][1],
      documentAmount: data[i][2],
    };
    array.push(register);
  }
  return array;
}

function collectItems() {
  var data = $("#tbItems").DataTable().rows().data().toArray();
  var array = [];
  var item = "";
  for (var i = 0; i < data.length; i++) {
    //		array.push(new Item(i, type, concept, date, reference, amount)
    item = {
      type: data[i][0],
      concept: data[i][1],
      date: data[i][2],
      reference: data[i][3],
      amount: data[i][4],
    };
    array.push(item);
  }
  return array;
}

function getCustomerInformationById(cuitNbr) {
  var acctype;
  var paramObject = {};
  paramObject["documentNbr"] = cuitNbr;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getCustomerInformationById",
    contentType: "application/json",
    data: JSON.stringify(paramObject),
    async: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomerBalanceCb(data);
    acctype = data[0].customerCreditStatus;
    if (acctype === "CASH") {
      hiddenInformationToCash(true);
      $("#divBoxCashSuggest,#divBoxCashSuggest2").show();
      $("#divBoxCashStatus").show();
    } else {
      hiddenInformationToCash(false);
      $("#divBoxCashSuggest,#divBoxCashSuggest2,#divBoxCashStatus").hide();
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });

  return acctype;
}

function hiddenInformationToCash(hide) {
  if (hide) {
    $(
      "#divBoxBalance,#divBoxLimit,#divBoxInform,#divBoxSelectedItems,#divBoxVencido,#divBoxNoVencido",
    ).hide();
    $("#labelVencido").html("Saldo deudor");
    $("#labelNoVencido").html("Saldo a favor");
  } else {
    $(
      "#divBoxBalance,#divBoxLimit,#divBoxInform,#divBoxSelectedItems,#divBoxVencido,#divBoxNoVencido",
    ).show();
    $("#labelVencido").html("Total vencido");
    $("#labelNoVencido").html("Total no vencido");
  }
}

function showCompositionBalance(cuitNbr, accType) {
  //$('#rowBoxHeaders').hide();
  var paramObject = {};
  paramObject["documentNbr"] = cuitNbr;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCompositionBalance",
    contentType: "application/json",
    data: JSON.stringify(paramObject),
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadBalanceCb(data, accType);
    showHeaderInformation(data, accType);
    //calculateTotalBalanceByCashAcct();
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function updateStatus(reportNumber, stage) {
  var request = $.ajax({
    type: "POST",
    url:
      url_application +
      "/balance/updateReportStatus/" +
      reportNumber +
      "/" +
      stage,
    timeout: 600000,
  });

  request.done(function (data) {
    showModal("", data, "error");
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function prepareCustomerData(nroCuit) {
  //resetTotals();
  processingModal();
  var accType = getCustomerInformationById(nroCuit);
  //Carga la tabla de información
  showCompositionBalance(nroCuit, accType);
}

function showHeaderInformation(data, accType) {
  totalBalance = 0;
  if (data.length > 0) {
    getTotalsHeaders(data, accType);
  } else {
    //		$('#rowBoxHeaders').hide();
  }
  $("#rowBoxHeaders").show();
}

function getMyCustomers() {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCustomers",
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomersCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function calculateTotalBalanceByCashAcct(totalB) {
  //var totalResult = totalBalance - totalDelayed;
  if (totalB > 0) {
    $("#totalCashStatus").text("$" + parseMoneyValue(totalB));
    $("#cardBoxCashStatus").removeClass("bg-success").addClass("bg-danger");
    $("#labelCashStatus").text("SALDO DEUDOR");
  } else {
    $("#totalCashStatus").text("$" + parseMoneyValue(totalB * -1));
    $("#cardBoxCashStatus").removeClass("bg-danger").addClass("bg-success");
    $("#labelCashStatus").text("SALDO A FAVOR");
  }
}

function resetTotals() {
  totalBalance = 0;
  totalNotDelayed = 0;
  totalDelayed = 0;
}

function calcularTotalCash(data) {
  var totalSaldo = 0;
  for (var i = 0; i < data.length; i++) {
    totalSaldo += data[i].balance;
  }
  //Sumar todo lo de la columna SALDO == 1392750.28
  if (totalSaldo > 0) {
    //SI me da positivo SALDO DEUDOR
    $("#totalCashStatus").text("$" + parseMoneyValue(totalSaldo));
    $("#cardBoxCashStatus").removeClass("bg-success").addClass("bg-danger");
    $("#labelCashStatus").text("SALDO DEUDOR");
  } else {
    //SI me da NEGATIVO SALDO A FAVOR * -1 PARA QUE APAREZCA POSITIVO
    $("#totalCashStatus").text("$" + parseMoneyValue(totalSaldo * -1));
    $("#cardBoxCashStatus").removeClass("bg-danger").addClass("bg-success");
    $("#labelCashStatus").text("SALDO A FAVOR");
  }
}

function getTotalsHeaders(data, accType) {
  if (accType == "CASH") {
    calcularTotalCash(data);
  } else {
    //Sumar todo lo de la columna SALDO
    var totalVencido = 0;
    var totalNoVencido = 0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].qtyDelayDays > 0) {
        totalVencido += data[i].balance;
      } else {
        totalNoVencido += data[i].balance;
      }
    }
    //Pero vencido == > mora mayor 0
    //No Vencido == < mora menor a 0

    $("#totalBalanceLbl").text("$" + parseMoneyValue(totalNoVencido));
    $("#totalDelayedBalanceLbl").text("$" + parseMoneyValue(totalVencido));
  }
  /*var customerName = data[0]["customerName"];
	$('#headerLbl').text(customerName);*/
  /*totalBalance = 0;
	totalNotDelayed = 0;
	totalDelayed = 0;
	for(var i = 0; i < data.length; i++){
		//totalBalance += data[i].balance;
		if(data[i].qtyDelayDays > 0){
			totalDelayed += data[i].balance;
		} else {
			totalNotDelayed += data[i].balance;
		}
	}
	totalBalance = totalNotDelayed - totalDelayed;
	calculateTotalBalanceByCashAcct(totalBalance);
	$('#totalBalanceLbl').text('$' + parseMoneyValue(totalNotDelayed));
	var percentDelayed = ( (totalDelayed*100)/totalBalance ).toFixed(2);
	$('#totalDelayedBalanceLbl').text('$' + parseMoneyValue(totalDelayed));
	$('#barDelayed').attr('style','width: '+percentDelayed+'%;');
	$('#barTotal').attr('style','width: 100%;');
	$('#totalDelayedBalanceLbl').text('$' + parseMoneyValue(totalDelayed));*/
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}

function inputMoneyValue(input) {
  var reg = new RegExp("^[0-9]+$");
  if (!reg.test(input.value.trim())) {
    showModal("", "Monto ingresado es incorrecto..", "error");
    $("#" + input.id)
      .val(0)
      .change();
    return;
  }
  var value = input.value.trim() == "" || input.value == null ? 0 : input.value;
  $("#" + input.id).val(
    parseFloat(value, 10)
      .toFixed(2)
      .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
      .toString(),
  );
}

function initKnockout() {
  viewModel = {
    balance: ko.observableArray([]),
    dataInform: ko.observableArray([]),
    customers: ko.observableArray([]),
    customerBalance: ko.observableArray([]),
    customerReportDocuments: ko.observableArray([]),
    entity: ko.observableArray([]),
    values: ko.observableArray([]),
    usdData: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadCustomerBalanceCb(data) {
  viewModel.customerBalance(data);
}

function loadCustomersCb(data) {
  viewModel.customers(data);
}

function loadBalanceCb(data, accType) {
  showBalanceGrid(data, accType);
}

function loadInformationCb(data) {
  if (data == "" || data == null || data.invType == null) {
    showModal("", "Sin información", "warning");
  } else {
    showInformationGrid(data);
    closeProcessingModal();
  }
}

function getDateTime(value) {
  return (
    new Date(value).toLocaleDateString() +
    " " +
    new Date(value).toLocaleTimeString()
  );
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showInformationGrid(data) {
  //viewModel.dataInform.removeAll();
  //deleteDataTables();
  viewModel.dataInform(data);
  switch (data.invType) {
    case "IMPORT":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityImport();
      }
      break;
    case "EXPORT":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityExport();
      }
      break;
    case "MANIFEST":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityManifest();
      }
      break;
    default:
      showNotesData();
      break;
  }
  if (data.entity != null) {
    if (data.entity.length > 0) {
      viewModel.entity(data.entity);
      $("#tbEntityData").show();
    } else {
      emptyEntity();
    }
  } else {
    emptyEntity();
  }
  viewModel.values(data.units);
  viewModel.usdData(data.usdAmount);
  $("#modalDataAssociate").modal();
  //showModalHtml('Informacion',ko.toJSON(data),'info');
  //$("#tableBox").show();
  //newDataTable("tbInformation", null, true, true, true);
  //$("#tbBalance").show();
}

function showEntityManifest() {
  $("#theadValues").text("Manifiesto(s)");
}

function showEntityImport() {
  $("#theadValues").text("Contenedor(es)");
  $("#theadEntity").text("BL(s)");
}

function showEntityExport() {
  $("#theadValues").text("Contenedor(es)");
  $("#theadEntity").text("Booking(s)");
}

function showNotesData() {
  $("#theadEntity").text("Notas");
  $("#theadValues").text("Datos adicionales");
}

function emptyEntity() {
  $("#tbEntityData").hide();
}

function showBalanceGrid(data, accType) {
  viewModel.balance.removeAll();
  deleteDataTables();
  viewModel.balance(data);
  $("#tableBox").show();
  var emptyMsg = "Su cuenta no presenta movimientos";
  if (accType === "OAC") {
    newDataTableCheckbox("tbBalance", null, true, true, false, true, emptyMsg);
  } else {
    newDataTable("tbBalance", null, true, true, true, emptyMsg);
  }
  $("#tbBalance").show();
}

function calculateSelectedRows() {
  var amountSelected = 0;
  var dt = $("#tbBalance").DataTable().rows({ selected: true }).data();
  for (var i = 0; i < dt.length; i++) {
    amountSelected += parseFloat(dt[i][4]);
  }
  console.log(amountSelected);
  $("#totalSelectedBalanceLbl").text(
    "$" +
      parseFloat(amountSelected, 10)
        .toFixed(2)
        .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
        .toString(),
  );
  var percentSelected = ((amountSelected * 100) / totalBalance).toFixed(2);
  $("#barSelected").attr("style", "width: " + percentSelected + "%;");
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
