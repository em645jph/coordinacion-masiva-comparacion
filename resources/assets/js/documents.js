var loaded = false;
var table;
$(document).ready(function () {
  $("#btnFindEntity").click(function () {
    findEntityById();
  });

  $("#tbUnits tbody").on("click", "tr", function () {
    if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
      $("#btnGenerate").show();
    } else {
      $("#btnGenerate").hide();
    }
  });

  $("#btnGenerate").click(function () {
    getGatepassByUnits();
  });

  initCombos();
  initButtons();
  initModal();
});

function initButtons() {
  $("#btnSaveApptTruckingDetails").click(function () {
    saveTruckingDetails();
  });
}

function initCombos() {
  $("#selectType").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "STRGE") {
      $("#valueNbr").prop("maxLength", 50);
      var ph = "CONTENEDOR1,CONTENEDOR2,CONTENEDOR3";
      $("#valueNbr").attr("placeholder", ph);
    } else {
      $("#valueNbr").prop("maxLength", 100);
      $("#valueNbr").attr("placeholder", "");
    }
    $("#valueNbr").focus();
  });
}

function initModal() {
  $("#addApptTruckingDetailsModal").on("shown.bs.modal", function () {
    $("#txtTruckLicense").focus();
  });

  $("#addApptTruckingDetailsModal").on("hidden.bs.modal", function () {
    cleanModalInputs();
  });
}

function getGatepassByUnits() {
  processingModal();

  var error = false;
  var dataArr = [];
  var apptArr = [];
  var rows = $("tr.selected");
  var rowData = $("#tbUnits").DataTable().rows(rows).data();
  $.each($(rowData), function (key, value) {
    if ($("#" + value[0]).attr("canBeDownloaded") == "0") {
      var category = $("#" + value[0]).attr("category");
      var entityLabel = category == "APPT" ? "turnos" : "contenedores";
      var msg =
        "Uno o alguno de los " +
        entityLabel +
        " seleccionados no cuenta con los requerimientos para obtener el gate-pass. " +
        "<br> Verifique que los " +
        entityLabel +
        " estén coordinados y pagados y vuelva a intentarlo.";
      if (category == "IMPRT") {
        msg += "<br><br>Recuerde abonar los cargos del contenedor y manifiesto";
      }
      showModalHtml("", msg, "error");
      error = true;
      return;
    } else {
      dataArr.push(value["0"]); //"name" being the value of your first column.
      apptArr.push($("#" + value[0]).attr("apptNbr"));
    }
  });

  if (error) return;

  if ($("#tbUnits").DataTable().rows(rows).count() < 1) {
    showModalHtml(
      "",
      "Por favor, seleccione al menos un registro para generar el documento.",
      "error",
    );
    return;
  }

  var obj = new Object();
  obj.formUnits = dataArr;
  obj.formAppt = apptArr;
  obj.formCategory = rowData[0][1];

  var request = $.ajax({
    type: "POST",
    url: url_application + "/Document/getGatepassByType",
    contentType: "application/json",
    data: JSON.stringify(obj),
  });

  request.done(function (data) {
    if (data == null || data == "") {
      showModalHtml("", "Sin resultados", "info");
      return;
    }
    downloadPDF(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModalHtml("", "No se pudo obtener el GatePass", "error");
  });
}

/**
 * Creates an anchor element `<a></a>` with
 * the base64 pdf source and a filename with the
 * HTML5 `download` attribute then clicks on it.
 * @param  {string} pdf
 * @return {void}
 */
function downloadPDF(pdf) {
  var isFirefox = typeof InstallTrigger !== "undefined";
  if (isFirefox) {
    var win = window.open("data:application/pdf;base64," + pdf, "_blank");
  } else {
    const linkSource = "data:application/pdf;base64," + pdf;
    const downloadLink = document.createElement("a");
    const fileName = "GatePass_" + makeid(10).toUpperCase() + ".pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function initKnockout() {
  viewModel = {
    Units: ko.observableArray([]),
    updateTruckingDetails: function (apptToUpdate) {
      updateTruckingDetailsIntern(apptToUpdate);
    },
  };
  ko.applyBindings(viewModel);
}

function loadUnitsCb(data, type) {
  showUnitsGrid(data, type);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showUnitsGrid(data, type) {
  viewModel.Units.removeAll();
  deleteDataTables();
  viewModel.Units(data);

  table = $("#tbUnits")
    .DataTable({
      select: "multiple",
      oLanguage: {
        sEmptyTable: "No hay registros a mostrar",
        sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        sSearch: "Buscar",
        sLengthMenu: "Mostrar _MENU_ resultados",
        /*"oPaginate": {
            	"sFirst": "Primera pagina", // This is the link to the first page
            	"sPrevious": "Pagina anterior", // This is the link to the previous page
            	"sNext": "Proxima pagina", // This is the link to the next page
            	"sLast": "Ultima pagina" // This is the link to the last page
        	}*/
      },
      /*columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],*/
      bDestroy: true,
      bFilter: true,
      bSort: true,
      sScrollY: "30vh",
    })
    .on("select", function (e, dt, type, indexes) {
      if (type === "row") {
        var rows = table.rows(indexes).nodes().to$();
        //var ignore = node.hasClass( 'ignoreme' );
        $.each(rows, function () {
          if ($(this).hasClass("ignoreme")) table.row($(this)).deselect();
        });
      }
      if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    })
    .on("deselect", function (e, dt, type, indexes) {
      if ($("#tbUnits").DataTable().rows(".selected").count() > 0) {
        $("#btnGenerate").show();
      } else {
        $("#btnGenerate").hide();
      }
    });

  var entityColumnName = "APPT" == type ? "Turno" : "Contenedor";
  $(table.column(0).header()).text(entityColumnName);
}

function findEntityById() {
  processingModal();

  type = $("#selectType").children("option:selected").val();
  value = $("#valueNbr").val();
  if (value == "" || type == null || type == "") {
    showModal("", "Por favor, complete los campos requeridos.", "error");
    return;
  }
  var errorMsg = type == "STRGE" ? validateSearchFields(type) : "";

  if (errorMsg != "") {
    showErrorMsg(errorMsg);
  } else {
    var request = $.ajax({
      url: url_application + "/Document/findUnits/" + type + "/" + value,
      type: "POST",
    });

    request.done(function (data) {
      if (data == null || data == "") {
        showModalHtml(
          "",
          "Sin resultados. <br> Verifique que el contenedor, B/L, Booking o EDO buscado sea correcto.",
          "info",
        );
        return;
      }
      if (!loaded) {
        initKnockout();
        loaded = true;
      }
      console.log("data:" + JSON.stringify(data));
      $("#tbUnits").show();
      loadUnitsCb(data["unitList"], type);
      Swal.close();
      var tempMsg = data["tempMsg"];
      if (typeof tempMsg != "undefined" && tempMsg != null && tempMsg != "") {
        showWarningHtmlMsg(tempMsg);
      }
    });

    request.fail(function (jqXHR, textStatus) {
      console.log("jqXHR:" + JSON.stringify(jqXHR));
      console.log("textStatus:" + textStatus);
      showModalHtml("", "No se pudo obtener algun resultado.", "error");
    });
  }
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}

function validateSearchFields(type) {
  var searchValue = $("#valueNbr").val();
  var errorMsg = "";
  if (!isValidStr(searchValue)) {
    var typeLabel = "contenedor";
    errorMsg =
      "La búsqueda sólo permite número de " +
      typeLabel +
      " separado por comas. Por favor eliminá los caracteres especiales";
  }
  return errorMsg;
}

function saveTruckingDetails() {
  console.log("saveTruckingDetails");
  var errorMsg = validateModalFields();
  if (errorMsg == "") {
    showProcessingModal();
    var updateParam = {
      prApptNbrStr: $("#txtApptNbr").val(),
      prTruckLicense: $("#txtTruckLicense").val(),
      prDriverGkeyStr: $("#txtDriverGkey").val(),
      prDriverName: $("#txtDriverName").val(),
      prDriverLicense: $("#txtDriverLicense").val(),
    };
    console.log("updateParam:" + JSON.stringify(updateParam));
    $.ajax({
      type: "POST",
      url: url_application + "/Appointment/updateApptTruckingDetails/",
      data: updateParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        closeProcessingModal();
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorHtmlMsg(msg);
        } else {
          $("#btnCancelSaveApptTruckingDetails").click();
          showSuccessMsg("Listo!", msg);
          $("#btnFindEntity").click();
        }
      },
      fail: function (data, statusCode) {
        s;
        closeProcessingModal();
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else {
    showErrorMsg(errorMsg);
  }
}

function updateTruckingDetailsIntern(apptToUpdate) {
  fillModalInputs(apptToUpdate);
  $("#addApptTruckingDetailsModal").modal();
}

function fillModalInputs(apptToUpdate) {
  $("#txtApptNbr").val(apptToUpdate["apptNbr"]);
  $("#txtDriverGkey").val(apptToUpdate["driverGkey"]);
  $("#txtTruckLicense").val(apptToUpdate["truckLicense"]);
  $("#txtDriverName").val(apptToUpdate["driverName"]);
  $("#txtDriverLicense").val(apptToUpdate["driverLicense"]);
  $("#txtTruckLicense").focus();
}

function cleanModalInputs() {
  $("#txtApptNbr").val("");
  $("#txtDriverGkey").val("");
  $("#txtTruckLicense").val("");
  $("#txtDriverName").val("");
  $("#txtDriverLicense").val("");
}

function validateModalFields() {
  var errorMsg = "";
  var truckLicense = $("#txtTruckLicense").val();
  var driverName = $("#txtDriverName").val();
  var driverLicense = $("#txtDriverLicense").val();
  if (
    !isValidStr(truckLicense) ||
    !isValidStr(driverName) ||
    !isValidStr(driverLicense)
  ) {
    errorMsg =
      "Los campos sólo permiten números y letras. Eliminá los caracteres especiales";
  }
  return errorMsg;
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}
