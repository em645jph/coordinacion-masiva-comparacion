var loaded = false;
var totalBalance = 0;
var totalDelayed = 0;
var reportNbr = 0;

$(document).ready(function () {
  processingModal();
  getAllCustomers();

  $(".basic-single-customers").select2();

  $("#btnFindCustomerInformation").click(function () {
    giCount = 1;
    prepareCustomerData($("#selectCustomer").children("option:selected").val());
  });
});

$(document).keyup(function (e) {
  if (e.key === "Escape") {
    // escape key maps to keycode `27`
    $("#tbBalance").DataTable().rows().deselect();
    //calculateSelectedRows();
  }
});

function getAllCustomers() {
  var request = $.ajax({
    type: "GET",
    url: url_application + "/Customer/getAllCustomers",
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    viewModel.users(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function isNullOrEmpty(value) {
  return value == null || value == "" || value == undefined;
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
  var aReturn = new Array();
  var aTrs = oTableLocal.fnGetNodes();

  for (var i = 0; i < aTrs.length; i++) {
    if ($(aTrs[i]).hasClass("row_selected")) {
      aReturn.push(aTrs[i]);
    }
  }
  return aReturn;
}

function getValuesAssociatedByDocumentNbr(data, event) {
  var amount = data.amount;
  processingModal();
  var paramObject = {};
  paramObject["documentNbr"] = data.documentNbr;
  paramObject["documentDate"] = data.documentDate;
  $("#titleInformationData").text(
    "Información adicional de " + data.documentNbr,
  );

  var request = $.ajax({
    type: "POST",
    contentType: "application/json",
    url: url_application + "/balance/getValuesAssociatedByDocumentNbr",
    data: JSON.stringify(paramObject),
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    data.usdAmount = amount / data.usdAmount;
    loadInformationCb(data);
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function getCustomerInformationById(cuitNbr) {
  var acctype;
  var paramObject = {};
  paramObject["documentNbr"] = cuitNbr;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getCustomerInformationById",
    contentType: "application/json",
    data: JSON.stringify(paramObject),
    async: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomerBalanceCb(data);
    acctype = data[0].customerCreditStatus;
    if (acctype === "CASH") {
      hiddenInformationToCash(true);
      $("#divBoxCashSuggest,#divBoxCashSuggest2").show();
      $("#divBoxCashStatus").show();
    } else {
      hiddenInformationToCash(false);
      $("#divBoxCashSuggest,#divBoxCashSuggest2,#divBoxCashStatus").hide();
    }
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });

  return acctype;
}

function hiddenInformationToCash(hide) {
  $("#divBoxSelectedItems").hide();
  if (hide) {
    $(
      "#divBoxBalance,#divBoxLimit,#divBoxInform,#divBoxVencido,#divBoxNoVencido",
    ).hide();
    $("#labelVencido").html("Saldo deudor");
    $("#labelNoVencido").html("Saldo a favor");
  } else {
    $(
      "#divBoxBalance,#divBoxLimit,#divBoxInform,#divBoxVencido,#divBoxNoVencido",
    ).show();
    $("#labelVencido").html("Total vencido");
    $("#labelNoVencido").html("Total no vencido");
  }
}

function showCompositionBalance(cuitNbr, accType) {
  var paramObject = {};
  paramObject["documentNbr"] = cuitNbr;

  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCompositionBalance",
    contentType: "application/json",
    data: JSON.stringify(paramObject),
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadBalanceCb(data, accType);
    showHeaderInformation(data, accType);
    //calculateTotalBalanceByCashAcct();
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function prepareCustomerData(nroCuit) {
  processingModal();
  var accType = getCustomerInformationById(nroCuit);
  //Carga la tabla de información
  showCompositionBalance(nroCuit, accType);
}

function showHeaderInformation(data, accType) {
  totalBalance = 0;
  if (data.length > 0) {
    getTotalsHeaders(data, accType);
    $("#rowBoxHeaders").show();
  }
}

function getMyCustomers() {
  var request = $.ajax({
    type: "POST",
    url: url_application + "/balance/getMyCustomers",
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
  });

  request.done(function (data) {
    if (!loaded) {
      initKnockout();
      loaded = true;
    }
    loadCustomersCb(data);
    Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function calcularTotalCash(data) {
  var totalSaldo = 0;
  for (var i = 0; i < data.length; i++) {
    totalSaldo += data[i].balance;
  }
  //Sumar todo lo de la columna SALDO == 1392750.28
  if (totalSaldo > 0) {
    //SI me da positivo SALDO DEUDOR
    $("#totalCashStatus").text("$" + parseMoneyValue(totalSaldo));
    $("#cardBoxCashStatus").removeClass("bg-success").addClass("bg-danger");
    $("#labelCashStatus").text("SALDO DEUDOR");
  } else {
    //SI me da NEGATIVO SALDO A FAVOR * -1 PARA QUE APAREZCA POSITIVO
    $("#totalCashStatus").text("$" + parseMoneyValue(totalSaldo * -1));
    $("#cardBoxCashStatus").removeClass("bg-danger").addClass("bg-success");
    $("#labelCashStatus").text("SALDO A FAVOR");
  }
}

function calcularTotalOAC(data) {
  //Sumar todo lo de la columna SALDO
  var totalVencido = 0;
  var totalNoVencido = 0;
  for (var i = 0; i < data.length; i++) {
    if (data[i].qtyDelayDays > 0) {
      totalVencido += data[i].balance;
    } else {
      totalNoVencido += data[i].balance;
    }
  }
  //Pero vencido == > mora mayor 0
  //No Vencido == < mora menor a 0
}

function calculateTotalBalanceByCashAcct(totalB) {
  //var totalResult = totalBalance - totalDelayed;
  /*if(totalB > 0){
		$('#totalCashStatus').text('$' + parseMoneyValue(totalB));
		$('#cardBoxCashStatus').removeClass('bg-success').addClass('bg-danger');
		$('#labelCashStatus').text('SALDO DEUDOR');
	} else {
		$('#totalCashStatus').text('$' + parseMoneyValue(totalB*-1));
		$('#cardBoxCashStatus').removeClass('bg-danger').addClass('bg-success');
		$('#labelCashStatus').text('SALDO A FAVOR');
	}*/
}

function resetTotals() {
  totalBalance = 0;
  totalNotDelayed = 0;
  totalDelayed = 0;
}

function getTotalsHeaders(data, accType) {
  if (accType == "CASH") {
    calcularTotalCash(data);
  } else {
    //Sumar todo lo de la columna SALDO
    var totalVencido = 0;
    var totalNoVencido = 0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].qtyDelayDays > 0) {
        totalVencido += data[i].balance;
      } else {
        totalNoVencido += data[i].balance;
      }
    }
    //Pero vencido == > mora mayor 0
    //No Vencido == < mora menor a 0

    $("#totalBalanceLbl").text("$" + parseMoneyValue(totalNoVencido));
    $("#totalDelayedBalanceLbl").text("$" + parseMoneyValue(totalVencido));
  }
  /*var customerName = data[0]["customerName"];
	$('#headerLbl').text(customerName);*/
  /*totalBalance = 0;
	totalNotDelayed = 0;
	totalDelayed = 0;
	for(var i = 0; i < data.length; i++){
		//totalBalance += data[i].balance;
		if(data[i].qtyDelayDays > 0){
			totalDelayed += data[i].balance;
		} else {
			totalNotDelayed += data[i].balance;
		}
	}
	totalBalance = totalNotDelayed - totalDelayed;
	calculateTotalBalanceByCashAcct(totalBalance);
	$('#totalBalanceLbl').text('$' + parseMoneyValue(totalNotDelayed));
	var percentDelayed = ( (totalDelayed*100)/totalBalance ).toFixed(2);
	$('#totalDelayedBalanceLbl').text('$' + parseMoneyValue(totalDelayed));
	$('#barDelayed').attr('style','width: '+percentDelayed+'%;');
	$('#barTotal').attr('style','width: 100%;');
	$('#totalDelayedBalanceLbl').text('$' + parseMoneyValue(totalDelayed));*/
}

function parseMoneyValue(value) {
  return parseFloat(value, 10)
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
    .toString();
}

function initKnockout() {
  viewModel = {
    balance: ko.observableArray([]),
    dataInform: ko.observableArray([]),
    customers: ko.observableArray([]),
    customerBalance: ko.observableArray([]),
    customerReportDocuments: ko.observableArray([]),
    entity: ko.observableArray([]),
    values: ko.observableArray([]),
    usdData: ko.observableArray([]),
    users: ko.observableArray([]),
  };
  ko.applyBindings(viewModel);
}

function loadCustomerBalanceCb(data) {
  viewModel.customerBalance(data);
}

function loadCustomersCb(data) {
  viewModel.customers(data);
}

function loadBalanceCb(data, accType) {
  showBalanceGrid(data, accType);
}

function loadInformationCb(data) {
  if (data == "" || data == null || data.invType == null) {
    showModal("", "Sin información", "warning");
  } else {
    showInformationGrid(data);
    closeProcessingModal();
  }
}

function getDateTime(value) {
  return (
    new Date(value).toLocaleDateString() +
    " " +
    new Date(value).toLocaleTimeString()
  );
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function showInformationGrid(data) {
  //viewModel.dataInform.removeAll();
  //deleteDataTables();
  viewModel.dataInform(data);
  switch (data.invType) {
    case "IMPORT":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityImport();
      }
      break;
    case "EXPORT":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityExport();
      }
      break;
    case "MANIFEST":
      if (data.entity.includes("NOTAS")) {
        showNotesData();
      } else {
        showEntityManifest();
      }
      break;
    default:
      showNotesData();
      break;
  }
  if (data.entity != null) {
    if (data.entity.length > 0) {
      viewModel.entity(data.entity);
      $("#tbEntityData,#tbValuesData").show();
    } else {
      emptyEntity();
    }
  } else {
    emptyEntity();
  }
  if (data.units != null) {
    if (data.units.length > 0) {
      $("#tbValuesData").show();
    } else {
      emptyValues();
    }
  } else {
    emptyValues();
  }
  viewModel.values(data.units);
  viewModel.usdData(data.usdAmount);
  $("#modalDataAssociate").modal();
  //showModalHtml('Informacion',ko.toJSON(data),'info');
  //$("#tableBox").show();
  //newDataTable("tbInformation", null, true, true, true);
  //$("#tbBalance").show();
}

function showEntityManifest() {
  $("#theadValues").text("Manifiesto(s)");
}

function showEntityImport() {
  $("#theadValues").text("Contenedor(es)");
  $("#theadEntity").text("BL(s)");
}

function showEntityExport() {
  $("#theadValues").text("Contenedor(es)");
  $("#theadEntity").text("Booking(s)");
}

function showNotesData() {
  $("#theadEntity").text("Notas");
  $("#theadValues").text("Datos adicionales");
}

function emptyEntity() {
  $("#tbEntityData").hide();
}

function emptyValues() {
  $("#tbValuesData").hide();
}

function showBalanceGrid(data, accType) {
  viewModel.balance.removeAll();
  deleteDataTables();
  viewModel.balance(data);
  $("#tableBox").show();
  var emptyMsg = "Su cuenta no presenta movimientos";
  newDataTable("tbBalance", null, true, true, true);
  $("#tbBalance").show();
}

function processingModal() {
  Swal.fire({
    title: "Procesando...",
    text: "Aguarde un instante por favor",
    imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
    showConfirmButton: false,
    allowOutsideClick: false,
  });
}
