/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  var unitDto = {};
  var storageUnitMap = {};
});

function initKnockout() {
  draftViewModel = {
    manifestCueTable: ko.observableArray([]),
  };
  ko.applyBindings(draftViewModel);
}

function initHtmlElements() {
  initInputs();
  initButtons();
  initCheckBoxs();
  //initCombos();
  //initApptModal();
}

function initInputs() {
  $("#txtSearchUnitId").focus();
}

function initButtons() {
  $("#btnSearch").click(function () {
    findManifest(true);
  });

  $("#btnGenerateDraft").click(function () {
    generateDraft();
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function findManifest(showProcessing) {
  $("#ckCheckAll").prop("checked", false);
  var errorMsg = validateSearchFields();
  if (errorMsg == "") {
    if (showProcessing) showProcessingModal();
    deleteManifestCueDataTable();
    var searchParam = {
      prUnitId: $("#txtSearchUnitId").val(),
    };
    $.ajax({
      type: "GET",
      url: url_application + "/Billing/findManifestList/",
      data: searchParam,
      success: function (data, statusCode) {
        var msg = data["msg"];
        var notFoundManifest = data["notFoundManifest"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else {
          showManifestCueTable(data);
          if (showProcessing) closeProcessingModal();
          if (typeof notFoundManifest != "undefined") {
            showWarningHtmlMsg(notFoundManifest);
          }
        }
        adjustDataTableColumns();
      },
      fail: function (data, statusCode) {
        s;
        showErrorMsg("Status code:" + statusCode);
      },
    });
  } else showErrorMsg(errorMsg);
}

function generateDraft() {
  var draftParam = buildDraftParam();
  console.log("draftParam:" + JSON.stringify(draftParam));
  if (draftParam != null && !jQuery.isEmptyObject(draftParam)) {
    showProcessingModal();
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/Invoice/generateManifestDraft/",
      data: JSON.stringify(draftParam),
      success: function (data) {
        closeProcessingModal();
        var msg = data["msg"];
        if (typeof msg != "undefined" && !resultIsOk(data)) {
          showErrorMsg(msg);
        } else if (data.status == "OK") {
          var url = url_application + "/Payment/paymentSelector/" + data.msg;
          showModalHtml(
            "",
            "<div id='divPdf'><div style='display: none;' id='divSpinner' class='spinner-border text-primary m-2' role='status'><span class='sr-only'>Loading...</span>" +
              "</div><button type='button' onclick='toggleSpinner(this);' id='btnDownload' invNbr='" +
              data.msg +
              "' class='btn btn-info'>DESCARGAR PDF - " +
              data.msg +
              "</button></div><hr>",
            "success",
            url,
          );
        } else {
          Swal.fire("", data, "error");
        }
      },
      fail: function (data) {
        closeProcessingModal();
        Swal.close();
        showModal("Request failed", jqXHR.responseText, "error");
      },
    });
  }
}

function showManifestCueTable(data) {
  draftViewModel.manifestCueTable.removeAll();
  deleteManifestCueDataTable();
  draftViewModel.manifestCueTable(data["manifestCueList"]);
  var table = $("#tbManifestCue").DataTable({
    bDestroy: true,
    bFilter: true,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: true,
    bPaginate: true,
    bSort: true,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
    },
  });
}

function refreshSearchData() {
  findManifest(false);
}

function validateSearchFields() {
  var searchUnitId = $("#txtSearchUnitId").val();
  var errorMsg = "";
  if (searchUnitId == null || searchUnitId == "") {
    errorMsg = "Por favor ingrese el campo obligatorio (*) para la búsqueda";
  } else if (!isValidStr(searchUnitId))
    errorMsg =
      "La búsqueda sólo permite número de manifiesto separado por comas. Por favor eliminá los caracteres especiales";
  return errorMsg;
}

function cleanSearchParams() {
  $("#txtSearchUnitId").val("");
  deleteDataTables();
  $("#txtSearchUnitId").focus();
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function deleteManifestCueDataTable() {
  $("#tbManifestCue").DataTable().clear();
  $("#tbManifestCue").DataTable().destroy();
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function resultIsError(result) {
  var status = result["status"];
  return status == "ERROR";
}

function resultRequiresConfirm(result) {
  var status = result["status"];
  return status == "QUESTION";
}

function resultIsWarning(result) {
  var status = result["status"];
  return status == "WARNING";
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function buildDraftParam() {
  var draftParam = {};
  var selectedManifestNbrArray = getSelectedManifestNbrArray();
  if (selectedManifestNbrArray != null && selectedManifestNbrArray.length > 0) {
    draftParam["manifestNbr"] = selectedManifestNbrArray;
  }
  return draftParam;
}

function getSelectedManifestNbrArray() {
  var selectedManifests = $(".selectable:checked");
  if (selectedManifests.length == 0) {
    showWarningMsg("Debe seleccionar al menos un registro");
    return "";
  } else return getManifestNbrArray(selectedManifests);
}

function getManifestNbrArray(selectedManifests) {
  var nbrArray = [];
  if (selectedManifests != null) {
    selectedManifests.each(function () {
      var selectedManifest = $(this);
      var manifestNbr = selectedManifest.attr("manifestNbr");
      if (manifestNbr != null && manifestNbr != "") nbrArray.push(manifestNbr);
    });
  }
  return nbrArray;
}

function adjustDataTableColumns() {
  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';/{}|\\":<>\?]/g.test(str);
}

function toggleSpinner(input) {
  //Reemplazamos el boton de descarga por el spinner
  if ($("#btnDownload").is(":visible")) {
    $("#btnDownload").hide();
    $("#divSpinner").show();
    getPdfByDraftId(input.getAttribute("invNbr"));
  } else {
    $("#btnDownload").show();
    $("#divSpinner").hide();
  }
}

function getPdfByDraftId(draft) {
  //processingModal();

  if (draft == null || draft == "" || draft == undefined) {
    Swal.fire("", "Error al obtener documento. Reintente nuevamente.", "error");
    return;
  }

  var request = $.ajax({
    type: "POST",
    enctype: "multipart/form-data",
    url: url_application + "/Invoice/getPdfByDraftId/" + draft,
    timeout: 600000,
  });

  request.done(function (data) {
    $("#btnDownload").show();
    $("#divSpinner").hide();
    if (data == null || data == "") {
      Swal.fire("", "Sin resultados", "info");
      return;
    } else {
      data = data.replace(
        "http://10.54.1.93:11080",
        "https://apps.apmterminals.com.ar",
      );
      var win = window.open(data, "_blank");
      if (win) {
        //Browser has allowed it to be opened
        win.focus();
      } else {
        //Browser has blocked it
        showModal("", "No se pudo abrir el PDF", "error");
      }
    }
    //Swal.close();
  });

  request.fail(function (jqXHR, textStatus) {
    showModal("", jqXHR.responseText, "error");
  });
}

function showModalHtml(title, text, type, callbackUrl) {
  Swal.fire({
    allowOutsideClick: false,
    title: title,
    html: text,
    type: type,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "S&iacute;, pagar!",
    cancelButtonText: "No, gracias.",
  }).then((result) => {
    if (result.dismiss != "cancel") {
      window.location.href = callbackUrl;
    } else {
      window.location.reload();
    }
  });
}
