/**
 * Author: Jimena Chavez
 *
 */

$(document).ready(function () {
  initHtmlElements();
  initKnockout();
  getMenuParentList("INIT");
});

function initKnockout() {
  menuViewModel = {
    parentMenu: ko.observableArray([]),
    createdMenu: ko.observableArray([]),
    modalParentMenu: ko.observableArray([]),
    editMenu: function (menuToEdit) {
      editMenuIntern(menuToEdit);
    },
    deleteMenu: function (menuToDelete) {
      deleteMenuIntern(menuToDelete);
    },
    recoverMenu: function (menuTorecover) {
      recoverMenuIntern(menuTorecover);
    },
  };
  ko.applyBindings(menuViewModel);
}

function initHtmlElements() {
  $("#txtSearchMenuId").focus();
  initInputs();
  initButtons();
  initDateTimePickers();
  initCombos();
  initModal();
  initCheckBoxs();
}

function initInputs() {
  $("#txtSearchMenuId").keypress(function () {
    var key = event.keyCode ? event.keyCode : event.which;
    if ((key < 65 || key > 90) && (key < 97 || key > 122) && key != 95)
      return false;
    return true;
  });

  $("#txtMenuId").keypress(function () {
    var key = event.keyCode ? event.keyCode : event.which;
    if ((key < 65 || key > 90) && (key < 97 || key > 122) && key != 95)
      return false;
    return true;
  });
}

function initButtons() {
  $("#btnSearch").click(function () {
    $("#ckCheckAll").prop("checked", false);
    var errorMsg = validateSearchFields();
    if (errorMsg == "") {
      var searchParam = {
        prId: $("#txtSearchMenuId").val().toUpperCase(),
        prLabel: $("#txtSearchMenuLabel").val(),
        prParentGkeyStr: $("#cbSearchParentMenu option:selected").val(),
        prFromDateStr: $("#txtSearchFromDate").val(),
        prToDateStr: $("#txtSearchToDate").val(),
        prLifeCycleState: $("#cbSearchMenuStatus option:selected").val(),
      };
      $.ajax({
        type: "GET",
        url: url_application + "/MenuManagement/findMenuByParam/",
        data: searchParam,
        success: function (data, statusCode) {
          showCreatedMenu(data);
        },
        fail: function (data, statusCode) {
          showErrorMsg("Status code:" + statusCode);
        },
      });
    } else showErrorMsg(errorMsg);
  });

  $("#btnAddMenu").click(function () {
    $("#modalTitle").text("Agregar Menú");
    getMenuParentList("NEW");
  });

  $("#btnClean").click(function () {
    cleanSearchParams();
  });

  $("#btnSaveMenu").click(function () {
    addOrUpdateMenu();
  });

  $("#btnDeleteSelectedMenu").click(function () {
    deleteSelectedMenu();
  });

  $("#btnRecoverSelectedMenu").click(function () {
    recoverSelectedMenu();
  });
}

function initDateTimePickers() {
  $("#txtSearchFromDate").datepicker({ dateFormat: "yy-mm-dd" });
  $("#txtSearchToDate").datepicker({ dateFormat: "yy-mm-dd" });
}

function initCombos() {
  $("#cbMenuParent").change(function () {
    var selectedValue = $(this).val();
    if (selectedValue == "") {
      enableParentMenuInputs();
      $("#txtMenuIcon").focus();
    } else {
      enableSubMenuInputs();
      $("#txtMenuController").focus();
    }
  });
}

function initModal() {
  $("#addEditMenuModal").on("shown.bs.modal", function () {
    $("#txtMenuId").focus();
  });

  $("#addEditMenuModal").on("hidden.bs.modal", function () {
    cleanAddEditFormParams();
  });
}

function initCheckBoxs() {
  $("#ckCheckAll").change(function () {
    var check = $(this).is(":checked");
    var canBeSelected = $(".selectable:enabled");
    canBeSelected.each(function () {
      if (check) $(this).prop("checked", true);
      else $(this).prop("checked", false);
    });
  });
}

function addOrUpdateMenu() {
  var isEditingItem = isEditingMenu();
  if ((!isEditingItem && !canAddItem) || (isEditingItem && !canEditItem))
    showErrorMsg("No tiene permisos para realizar esta acción");
  else {
    var errorMsg = validateFormFields();
    if (errorMsg == "") {
      var menuData = getMenuDataFromModal();
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/MenuManagement/addOrUpdateMenu/",
        data: JSON.stringify(menuData),
        success: function (result, statusCode) {
          var msg = result["msg"];
          if (resultIsOk(result)) {
            showSuccessMsg("Listo!", msg);
            $("#btnCancelSaveMenu").click();
            refreshSearchData();
          } else showErrorMsg(msg);
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              "Se produjo un error guardando el menú. Por favor intente nuevamente",
          );
        },
      });
    } else {
      showErrorMsg(errorMsg);
      $("#txtMenuId").focus();
    }
  }
}

function editMenuIntern(menuToEdit) {
  if (canEditItem) {
    var param = {
      prMenuGkey: menuToEdit.gkey,
    };
    getMenuParentList("UPDATE");
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: url_application + "/MenuManagement/getMenu/",
      data: param,
      success: function (data, statusCode) {
        fillMenuInputOnModal(data);
      },
      fail: function (data, statusCode) {
        showErrorMsg(data);
        $("#btnCancelSaveMenu").click();
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function deleteMenuIntern(menuToDelete) {
  if (canDeleteItem) {
    menuToDelete = deleteDtoAttributes(menuToDelete);
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/MenuManagement/deleteMenu/",
      data: JSON.stringify(menuToDelete),
      success: function (result, statusCode) {
        showSuccessMsg("Listo!", result);
        refreshSearchData();
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error eliminando el menú. Por favor intente nuevamente",
        );
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function recoverMenuIntern(menuTorecover) {
  if (canRecoverItem) {
    menuTorecover = deleteDtoAttributes(menuTorecover);
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: url_application + "/MenuManagement/recoverMenu/",
      data: JSON.stringify(menuTorecover),
      success: function (result, statusCode) {
        showSuccessMsg("Listo!", result);
        refreshSearchData();
      },
      fail: function (result, statusCode) {
        showErrorMsg(
          statusCode +
            " Se produjo un error activando el menú. Por favor intente nuevamente",
        );
      },
    });
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function deleteSelectedMenu() {
  if (canDeleteItem) {
    var selectedMenuJson = getSelectedMenuJson();
    if (selectedMenuJson != null && selectedMenuJson != "") {
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/MenuManagement/deleteSelectedMenu/",
        data: selectedMenuJson,
        success: function (result, statusCode) {
          showSuccessMsg("Listo!", result);
          refreshSearchData();
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error eliminando los menús. Por favor intente nuevamente",
          );
        },
      });
    }
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function recoverSelectedMenu() {
  if (canRecoverItem) {
    var selectedMenuJson = getSelectedMenuJson();
    if (selectedMenuJson != null && selectedMenuJson != "") {
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url_application + "/MenuManagement/recoverSelectedMenu/",
        data: selectedMenuJson,
        success: function (result, statusCode) {
          showSuccessMsg("Listo!", result);
          refreshSearchData();
        },
        fail: function (result, statusCode) {
          showErrorMsg(
            statusCode +
              " Se produjo un error activando los menús. Por favor intente nuevamente",
          );
        },
      });
    }
  } else showErrorMsg("No tiene permisos para realizar esta acción");
}

function getMenuParentList(action) {
  $.ajax({
    type: "GET",
    url: url_application + "/MenuManagement/getParentMenu/",
    success: function (data, statusCode) {
      if (action == "NEW" || action == "UPDATE") loadModalMenuParentCb(data);
      else loadSearchMenuParentCb(data);
    },
    fail: function (data, statusCode) {
      showModal("Error", "Status code:" + statusCode, "error");
    },
  });
}

function showCreatedMenu(data) {
  menuViewModel.createdMenu.removeAll();
  deleteDataTables();
  menuViewModel.createdMenu(data);
  $("#tbCreatedMenu").DataTable({
    bDestroy: true,
    bFilter: false,
    sScrollX: true,
    sScrollY: "30vh",
    scrollCollapse: true,
    paging: false,
    bPaginate: false,
    bFilter: false,
    bSort: false,
    oLanguage: {
      sEmptyTable: "No hay registros a mostrar",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sInfoEmpty: "Sin registros",
    },
  });
}

function refreshSearchData() {
  $("#btnSearch").click();
}

function loadSearchMenuParentCb(data) {
  menuViewModel.parentMenu(data);
}

function loadModalMenuParentCb(data) {
  menuViewModel.modalParentMenu(data);
}

function getMenuDataFromModal() {
  var menuData = {};
  menuData["id"] = $("#txtMenuId").val().toUpperCase();
  menuData["label"] = $("#txtMenuLabel").val();
  menuData["description"] = $("#txtMenuDesc").val();
  menuData["controller"] = $("#txtMenuController").val();
  menuData["action"] = $("#txtMenuAction").val();
  menuData["icon"] = $("#txtMenuIcon").val();
  menuData["parentGkey"] = $("#cbMenuParent option:selected").val();
  var menuGkey = $("#txtMenuGkey").val();
  if (menuGkey != null && menuGkey != "") menuData["gkey"] = menuGkey;
  return menuData;
}

function isEditingMenu() {
  var menuGkey = $("#txtMenuGkey").val();
  return menuGkey != null && menuGkey != "";
}

function fillMenuInputOnModal(data) {
  $("#modalTitle").text("Editar Menú");
  $("#txtMenuGkey").val(data["gkey"]);
  $("#txtMenuId").val(data["id"]);
  $("#txtMenuLabel").val(data["label"]);
  $("#txtMenuDesc").val(data["description"]);
  var menuParentGkey = data["parentGkey"];
  if (menuParentGkey != null) {
    $("#cbMenuParent").val(menuParentGkey).change();
    $("#txtMenuController").val(data["controller"]);
    $("#txtMenuAction").val(data["action"]);
  } else {
    enableParentMenuInputs();
    $("#txtMenuIcon").val(data["icon"]);
  }
  $("#addEditMenuModal").modal();
}

function deleteDtoAttributes(menuDto) {
  delete menuDto["createdStr"];
  delete menuDto["changedStr"];
  delete menuDto["parentLabel"];
  delete menuDto["isActive"];
  return menuDto;
}

function validateSearchFields() {
  var id = $("#txtSearchMenuId").val();
  var label = $("#txtSearchMenuLabel").val();
  var errorMsg = "";
  if (!isValidStr(id) || !isValidStr(label))
    errorMsg = "Por favor elimine los caracteres especiales de la búsqueda";
  return errorMsg;
}

function validateFormFields() {
  var id = $("#txtMenuId").val();
  var label = $("#txtMenuLabel").val();
  var description = $("#txtMenuDesc").val();
  var controller = $("#txtMenuController").val();
  var action = $("#txtMenuAction").val();
  var icon = $("#txtMenuIcon").val();
  var parentGkey = $("#cbMenuParent option:selected").val();
  var errorMsg = "";
  if (
    !isValidStr(id) ||
    !isValidStr(label) ||
    !isValidStr(description) ||
    !isValidStr(controller) ||
    !isValidStr(action) ||
    !isValidStr(icon)
  )
    errorMsg = "Los campos no pueden contener caracteres especiales";
  else if (id == "" || id.length < 3)
    errorMsg =
      "El campo Id es obligatorio y debe tener como mínimo 2 caracteres de longitud";
  else if (label == "" || label.length < 3)
    errorMsg =
      "El campo Nombre es obligatorio y debe tener como mínimo 2 caracteres de longitud";
  else if (parentGkey == "" && icon == "")
    errorMsg = "Por favor ingrese Ícono para el menú";
  else if (parentGkey != "" && (controller == "" || action == ""))
    errorMsg = "Por favor ingrese Controlador y Acción para el sub menú";
  return errorMsg;
}

function getSelectedMenuJson() {
  var selectedMenus = $(".selectable:checked");
  if (selectedMenus.length == 0) {
    showWarningMsg("Alerta", "Debe seleccionar al menos un registro");
    return "";
  } else return JSON.stringify(getMenuGkeyArray(selectedMenus));
}

function getMenuGkeyArray(selectedMenus) {
  var gkeyArray = [];
  if (selectedMenus != null) {
    selectedMenus.each(function () {
      var selectedMenu = $(this);
      var gkey = selectedMenu.attr("gkey");
      if (gkey != null && gkey != "") gkeyArray.push(gkey);
    });
  }
  return gkeyArray;
}

function cleanSearchParams() {
  $("#txtSearchMenuId").val("");
  $("#txtSearchMenuLabel").val("");
  $("#cbSearchParentMenu").val("");
  $("#txtSearchFromDate").val("");
  $("#txtSearchToDate").val("");
  $("#cbSearchMenuStatus").val("");
  $("#txtSearchMenuId").focus();
}

function cleanAddEditFormParams() {
  $("#txtMenuId").val("");
  $("#txtMenuLabel").val("");
  $("#txtMenuDesc").val("");
  $("#cbMenuParent").val("");
  $("#txtMenuController").val("");
  $("#txtMenuAction").val("");
  $("#txtMenuIcon").val("");
  $("#txtMenuGkey").val("");
  $("#txtMenuController").prop("disabled", true);
  $("#txtMenuAction").prop("disabled", true);
}

function deleteDataTables() {
  var tables = $.fn.dataTable.fnTables(true);
  $(tables).each(function () {
    $(this).dataTable().fnClearTable();
    $(this).dataTable().fnDestroy();
  });
}

function enableParentMenuInputs() {
  $("#txtMenuController").val("");
  $("#txtMenuAction").val("");
  $("#txtMenuController").prop("disabled", true);
  $("#txtMenuAction").prop("disabled", true);
  $("#txtMenuIcon").prop("disabled", false);
}

function enableSubMenuInputs() {
  $("#txtMenuIcon").val("");
  $("#txtMenuIcon").prop("disabled", true);
  $("#txtMenuController").prop("disabled", false);
  $("#txtMenuAction").prop("disabled", false);
}

function resultIsOk(result) {
  var status = result["status"];
  return status == "OK";
}

function isValidStr(str) {
  return !/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
