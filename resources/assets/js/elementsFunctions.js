/**
 * Author: Lucas Juarez
 * APM Terminals Buenos Aires
 */

function disableChildInputs(parent, type) {
  $("#" + parent + " :input[type=" + type + "]").attr("disabled", true);
}
function enableChildInputs(parent) {
  $("#" + parent + " :input").attr("disabled", false);
}

function cleanAllInputsById(parentId) {
  $("#" + parentId + " :input[type=text]").val("");
  $("#" + parentId + " :input[type=checkbox]").attr("checked", false);
  enableChildInputs(parentId);
}
