<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="editVipCustomerEventModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Editar Eventos - Cliente Vip</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="table-responsive" >
                                <table class="table table-hover datatable" id="tbCustomerEvent" width="100%">
	                                <thead>
										<tr>
										    <th> <input type="checkbox" id="ckCheckAllEvent"></th>
										    <th>Nombre</th>
										    <th>Entidad</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: vipViewModel.customerEventList">
										<tr>
											<td><input id="ckCheckOneEvent" class="selectableEvent" type="checkbox" data-bind="attr: {gkey:gkey, vipCustomerGkey: vip_customer_gkey}, checked: included"></td>
										    <td><label data-bind="text: event_type_id"></label></td>
										    <td><label data-bind="text: applies_to"></label></td>
										</tr>
									</tbody>		                                
                              </table>
                            </div>							
						</div>
					</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${customerEventModalButtons}
				<button type="button" class="btn btn-dark" id="btnCancelSaveVipCustomerEvent" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	