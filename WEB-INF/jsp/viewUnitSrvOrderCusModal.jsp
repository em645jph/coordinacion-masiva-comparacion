<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUnitSrvOrderCusModal" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Servicios</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<div class="col-12">
						<div class="table-responsive">
							<table class="table table-hover datatable" id="tbUnitServiceOrder" width="100%">
								<thead>
									<tr>
										<th>Contenedor</th>
										<th>Servicio</th>
										<th>Fecha Servicio</th>
										<th>Estado Servicio</th>
										<th>Anular</th>
									</tr>
								</thead>
								<tbody  data-bind="foreach: supportUnitViewModel.unitServiceTable">
									<tr>
										<td><label data-bind="text: unitId"></label></td>
										 <!-- ko {if: (srvOrderTypeDescription != null && srvOrderTypeDescription != "")} -->
											    <td><label data-bind="text: srvOrderTypeDescription"></label></td>
										 <!-- /ko -->
										<!-- ko {ifnot: (srvOrderTypeDescription != null && srvOrderTypeDescription != "")} -->
											    <td><label data-bind="text: srvOrderNbr"></label></td>
										 <!-- /ko -->
										<td><label data-bind="text: srvOrderStartDateStr"></label></td>
										<td><label data-bind="text: srvOrderStatus"></label></td>
										<td>
											<button class="btn btn-icon btn-danger btn-sm" data-bind="click: $parent.cancelSo, enable: allowToCancelService"> 
												<i class="ti-close"></i>
											</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark"
					id="btnCancelViewUnitSrvOrder" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>
