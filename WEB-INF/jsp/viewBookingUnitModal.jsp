<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewBookingUnitModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Contenedores preavisados</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<div class="col-12">
							<div class="table-responsive" >
                                <table class="table table-hover datatable" id="tbBookingUnit" width="100%">
                                	${bookingUnitDataTable}                                
                                </table>
                            </div>							
						</div>
				</div>				
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewBookingUnit" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	