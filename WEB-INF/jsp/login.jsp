<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/apmtba.css" />" />
<script>
$(document).ready(function(){
	
	//Bind the event handler to the "submit" JavaScript event
	$('#form_login').submit(function () {
		if($('#j_username').val() === "" ||
				$('#j_password').val() === ""){
			Swal.fire("","Por favor, complete los datos para iniciar sesi�n.","warning");
			return false;
		} else {
			Swal.fire({
				title: "Procesando...",
				text: "Aguarde un instante por favor",
				imageUrl: url_application + "/resources/assets/images/ajax_loading.gif",
				showConfirmButton: false,
				allowOutsideClick: false
			});
		}
	});
	
});
</script>
<body onload='document.f.j_username.focus()'>
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">

				<div class="wrapper-page">

					<div class="m-t-40 card-box">
						<div class="text-center">
							<h2 class="text-uppercase m-t-0 m-b-30">
								<a href="${pageContext.servletContext.contextPath}" class="text-success"> <span><img
										src="https://trademarks.justia.com/media/image.php?serial=79104823" alt="" height=70"></span>
								</a>
							</h2>
						</div>
						<div class="account-content">
							<form method='POST' class="form-horizontal" id="form_login" name='f' action="<c:url value='j_spring_security_check' />">

								<div class="form-group m-b-20">
									<div class="col-12">
										<label for="emailaddress">CUIT/CUIL</label> 
										<input class="form-control" tabindex="1" 
											placeholder='<spring:message code="login.user" />' type="text" id="j_username" name='j_username'>
									</div>
								</div>

								<div class="form-group m-b-20">
									<div class="col-12">
									<label for="password">CONTRASE�A</label> 
									<input class="form-control" name='j_password' id="j_password" 
										tabindex="2" placeholder='<spring:message code="login.password" />' type="password">
									</div>
								</div>

								<div class="form-group account-btn text-center m-t-10">
									<div class="col-12">
										<button id="btnLogin" class="btn btn-lg btn-block button button--primary" tabindex="2" type="submit">Ingresar</button>
										<br><br>
										<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
											<div class="alert alert-danger" role="alert">
												<i class="mdi mdi-block-helper"></i>
												<strong>${SPRING_SECURITY_LAST_EXCEPTION.message}</strong>
											</div>
										</c:if>
										<c:if test="${not empty msg}">
											<div  class="alert alert-success" role="alert">
												<i class="mdi mdi-check-all"></i>
												${msg}
											</div>
										</c:if>
									</div>
								</div>
								<a href="resetPassword" tabindex="5"
										class="text-muted pull-right font-14">Olvid&oacute; su contrase&ntilde;a?</a><br>
								<a href="reactivateAccount" tabindex="5"
											class="text-muted pull-right font-14">Reactivar cuenta</a>

							</form>

							<div class="clearfix"></div>

						</div>
					</div>
					<!-- end card-box-->


					<div class="row m-t-50">
						<div class="col-sm-12 text-center">
							<p class="text-muted">
								No tienes cuenta? <a href="userRegister"
									class="text-dark m-l-5"><strong>Registrarse</strong></a>
							</p>
						</div>
					</div>
					<br>
					<br>
					<div class="col-12">
						<form action="https://apps.apmterminals.com.ar/solicitudesonline">
						    <button type="submit" id="btnSolcs" class="btn btn-lg btn-block button button--success" tabindex="2">SOLICITUDES ONLINE</button>
						</form>
					</div>
					<br><br>
					<div class="col-12">
						<form action="https://apps.apmterminals.com.ar/GestionClientes/">
						    <button type="submit" id="btnCGC" class="btn btn-lg btn-block button button--success" tabindex="2">CENTRO DE GESTI�N DE CLIENTES</button>
						</form>
					</div>

				</div>
				<!-- end wrapper -->

			</div>
		</div>
	</div>
</section>
</body>