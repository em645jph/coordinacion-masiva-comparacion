<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade" id="shipmentCommunicationUnLockModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Desbloquear Contenedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		    <!-- ko {if: permisionAction() != false} -->
		       	<div  class="alert alert-danger ng-star-inserted" role="alert">
				    <strong _ngcontent-c10="">Cutoff documental vencido o inexistente!</strong> No es posible desbloquear el contenedor.
				</div>
			 <!-- /ko -->
        <form>
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="Note">Observación: </label>
		      <textarea class="form-control rounded-0"  rows="3" id="noteUnlock" maxlength="200" placeholder="Observaciones adicionales"></textarea>
		    </div>
		  </div>
		</form>
      </div>
      <div class="modal-footer"> 
        	<button type="button" class="btn btn-primary" data-bind="click: actionUnLock, style: { display: permisionAction() != true ? 'block' : 'none' }">Desbloquear</button> 
        	<button type="button" class="btn btn-dark" id="btnCancelUnblockPermission" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>