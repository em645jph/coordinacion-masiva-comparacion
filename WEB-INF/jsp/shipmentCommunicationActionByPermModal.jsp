<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade" id="shipmentCommunicationActionByPermModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="actionByPermModalTitle">Acci�n por familia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- ko {if: permisionAction() != false} -->
       	<div  class="alert alert-danger ng-star-inserted" role="alert">
		    <strong _ngcontent-c10="">Cutoff documental vencido o inexistente!</strong> No es posible bloquear los contenedores.
		</div>
	 <!-- /ko -->
        <form>
		  <div class="form-row" data-bind= "style:{display: searchVisitId() != null? 'block' : 'none'}">
		    <div class="form-group col-md-12">
		      <label for="txtActionVessel">Buque:</label>
		      <input type="text" class="form-control" id="txtActionVessel"  disabled>
		    </div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="txtActionPermission">Permiso: (*)</label>
		      <input type="text" class="form-control" id="txtActionPermission" data-bind="enable: searchPermission() == null || searchPermission() == ''" style="text-transform: uppercase;">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="cbActionType">Acci�n: (*)</label>
		    <select id="cbActionType" class="form-control" name="cbActionType" data-bind="options:supportUnitViewModel.searchScActionType, optionsText:'description' , optionsValue:'key', optionsCaption: '--'"></select>
		  </div>
		  <div class="form-group">
		    <label for="cbActionReason">Motivo: (*)</label>
		    <select id="cbActionReason" class="form-control" name="cbActionReason"   data-bind="options:supportUnitViewModel.searchScTypesLock, optionsText:'description' , optionsValue:'key', optionsCaption: '--'"></select>
		  </div>
		  <div class="form-group">
		    <label for="txtActionNotes">Observaci�n: (*)</label>
		    <textarea class="form-control rounded-0"  rows="3" id="txtActionNotes" placeholder="Razon de la acci�n" required="required" maxlength="170" style="text-transform: uppercase;"></textarea>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-primary" id="btnApplyActionByPerm">Aplicar acci�n</button>
			<button type="button" class="btn btn-dark" id="btnCancelActionByPerm" data-dismiss="modal">Cancelar</button>
	 </div>
    </div>
  </div>
</div>

