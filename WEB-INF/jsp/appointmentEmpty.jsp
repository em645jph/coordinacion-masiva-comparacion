<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/facturacion.js" />"
	charset="UTF-8"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/appointmentEmpty.js" />" charset="UTF-8"></script>	
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />

<style>
#tbUnitsByBl,#btnStartCoordinate {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Coordinaci�n Vac�os</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase">Ingres� n�mero de contenedor</h6>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="entityid" name="entityid" required>
					</span>
					<br>
					<button type="button" id="btnFindEntity" name="btnFindEntity" class="button button--primary">Buscar</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-striped datatable" id="tbUnitsByBl">
                    <thead>
                    <tr>
                        <th>Contenedor</th>
                        <th>Tama�o</th>
                        <th>Linea Operadora</th>
                        <th>Coordinado</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.UnitsByBl">
                    <tr>                                       
                        <td><label data-bind="text: unitId"></label></td>
                        <td>42G1</td>
                        <td>MAE</td>
                        <td><i class="ti-close" style="color: red;"></i></td>
                    </tr>
                    </tbody>
                </table>
			
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="button button--success" id="btnStartCoordinate">Coordinar</button>
			</div>
		</div>
		
	</div>
<!-- end container -->

<!-- Modal -->
<div class="modal fade" id="modalAppt" tabindex="-1" role="dialog" 
	aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Solicitud de turno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">�</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="divForUnit"></div>
        <input type="text" id="datetimepicker3" onchange="refreshLabelDateTimeAppt()"/>
        <label id="labelDatetimeAppt"></label>
      </div>
      <div class="modal-footer">
        <button type="button" class="button button--primary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="button button--success" id="btnCoordinate">Coordinar</button>
      </div>
    </div>
  </div>
</div>