<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="modalHistoryCutoff">
	<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Historial de Eventos</h4>
					<h3></h3>
					<button type="button" class="close" data-dismiss="modal">�</button>
				</div>
			<!-- Modal body -->
			<div class="row">
				<div class="col-sm-12">
					<div class="card-box">
						<div class="table-responsive">
							<table class="table datatable" width="100%" id="docCutoffHistoryTable">
								<thead>
								 	<tr>
									   <th>Fecha</th>				
									   <th>Usuario</th>				
									   <th>Cambios</th>				
						  			</tr>
						 		<thead>
							  	<tbody data-bind="foreach: viewModel.historyEventList">				  		
							  		<tr class="trVisits">
							  			<td data-bind="text: placedTimeStr"></td>
							  			<td data-bind="text: placedBy"></td>		  				
							  			<td data-bind="text: note"></td>		  				
							  		</tr>						 		
							</table>	
						</div>
					</div>	  	
				</div>
			</div>								 		
			<!-- Modal footer -->
				<div class="modal-footer justify-content-center">
					<button type="button" class="btn btn-dark" id="btnCloseModal" data-dismiss="modal">Cerrar</button>
				</div>			
			</div>		
	</div>
</div>	