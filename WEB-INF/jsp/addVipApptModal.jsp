<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addVipApptModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Agregar Turno Vip</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addVipApptForm">
					<div class="form-group">
						<label for="cbVipCustomer" class="col-sm-12 col-form-label">Cliente Vip(*):</label>
						<div class="col-sm-12">
							<select id="cbVipCustomer" class="form-control" name="cbVipCustomer"
								data-bind="options:vipViewModel.activeCustomerList, optionsText:'customer_label', optionsValue:'gkey', optionsCaption: '--'">
							</select>
						</div>
					</div>
					<div class="form-group">
							<label for="cbCategory" class="col-sm-12 col-form-label">Categor�a(*):</label>
							<div class="col-sm-12">
								<select id="cbCategory" class="form-control" name="cbCategory"  
										data-bind="options:vipViewModel.categoryCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
								</select>
							</div>
					</div>
					<div class="form-group">
						<label for="cbRuleType" class="col-sm-12 col-form-label">Tipo Filtro(*):</label>
						<div class="col-sm-12">								
								<select id="cbRuleType" class="form-control" name="cbRuleType"
									data-bind="options:vipViewModel.ruleTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
								</select>
						</div>
					</div>
					<div class="form-group" id="divDayOfWeek">
						<div class="col-sm-12">
							<label for="cbDayOfWeek">D�a:</label>										
							<select id="cbDayOfWeek" class="form-control" name="cbDayOfWeek"  
							        data-bind="options:vipViewModel.dayCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
							</select>
						</div>
					</div>
					<div class="form-group" id="divDate">
						<label for="txtDate" class="col-sm-12 col-form-label">Fecha espec�fica:</label>
						<div class="col-sm-12">
							<input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="nope" />
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox checkbox-primary col-sm-12">
                                <input id="ckApplyCustomerEvent" type="checkbox" checked>
                                <label for="ckApplyCustomerEvent">Aplicar  eventos del cliente vip por defecto</label>
                         </div>
                    </div>	
				</form>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${apptModalButtons}
				<button type="button" class="btn btn-dark" id="btnCancelSaveVipAppt" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	