<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/userManagement.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchUserDocumentNbr">CUIT</label>
										<input type="text" name="txtSearchUserDocumentNbr" id="txtSearchUserDocumentNbr" class="form-control" maxlength="250"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchRoleDesc">Nombre</label>
										<input type="text" name="txtSearchUserName" id="txtSearchUserName" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchRole">Rol</label>										
										<select id="cbSearchRole" class="form-control" name="cbSearchRole"  
										        data-bind="options:userViewModel.roleList, optionsText:'label', optionsValue:'gkey', optionsCaption: '--'">
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchFromDate">Fecha Inicio</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control" placeholder="YYYY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchToDate">Fecha Fin</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control" placeholder="YYYY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchUserStatus"> Estado</label>										
										<select width="40%" id="cbSearchUserStatus" class="form-control" name="cbSearchUserStatus">
											<option value="" >--</option>
											<option value="ACT">Activo</option>
											<option value="OBS">Obsoleto</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div id="actionButtons">
								${actionButtons}
							</div>
							<div class="col-sm-12">
								<br />
							</div>
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbCreatedUser" width="100%">
                                	${dataTable}                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="updateUserCredentialModal.jsp" %>
            </div> <!-- end container -->