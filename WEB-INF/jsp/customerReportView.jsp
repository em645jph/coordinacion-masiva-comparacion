<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/customerReport.js" />" charset="UTF-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.2/css/colReorder.dataTables.min.css">

<div class="wrapper">
	<div class="container-fluid">
	
			<div class="row">
				<div class="col-sm-12">
					<h2>Presentaciones</h2>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-12">
					<div class="card-box" id="tableBox">
						<table class="table table-striped datatable" id="tbCustomerReport">
		                    <thead>
			                    <tr>
			                    	<th>Presentación #</th>
			                        <th>Cliente</th>
			                        <th>Creador</th>
			                        <th>Creado</th>
			                        <th>Modificador</th>
			                        <th>Modificado</th>
			                        <th>Estado</th>
			                        <th>Notas</th>
			                        <th>Información</th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.customerReport">
			                    <tr data-bind="attr:{ 'id': $index }">                                       
			                        <td data-bind="text: reportNumberLeftPad(reportNumber)"></td>
			                        <td data-bind="text: customerKey"></td>
			                        <td data-bind="text: creator"></td>
			                        <td data-bind="text: getDateTime(created)"></td>
			                        <td data-bind="text: changer"></td>
			                        <td data-bind="text: getDateTime(changed)"></td>
			                        <td data-bind="text: status"></td>
			                        <td data-bind="text: notes"></td>
			                        <td data-bind="click: loadAdditionalDataFromRow"><i class="mdi mdi-information-outline" style="font-size: 20px;"></i></td>
			                    </tr>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
		
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modalInformation" tabindex="2" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titleInformationReport">Presentación #</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" id="modalBodyReportData">
	        <h5 class="modal-title" id="titleInformationReport2" style="display:none;">Presentación #</h5>
	      	<button type="button" id="htmlToPdfCreator" class="btn btn-primary" style="float:right;">DESCARGAR PRESENTACIÓN DE PAGO</button>
	      	<br>
	      	<h3>Documentos</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportDocuments">
                <thead>
                 <tr>
                     <th>Fecha</th>
                 	 <th>Documento</th>
                     <th>Monto</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportDocuments">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: documentDate"></td>
                     <td data-bind="text: documentNumber"></td>
                     <td data-bind="text: parseMoneyValue(documentAmount)"></td>
                 </tr>
                </tbody>
                <!-- <tfoot>
		            <tr>
						<th></th>
						<th>TOTAL</th>
						<th>Monto</th>
					</tr>
		        </tfoot> -->
            </table>
            <hr>
            <h3>Orden de Pago</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportItems">
                <thead>
                 <tr>
                 	 <th>Fecha</th>
                 	 <th>Item</th>
                 	 <th>Concepto</th>
                 	 <th>Referencia</th>
                     <th>Monto</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportItems">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: itemDate"></td>
                     <td data-bind="text: itemType"></td>
                     <td data-bind="text: itemString01"></td>
                     <td data-bind="text: itemReference"></td>
                     <td data-bind="text: parseMoneyValue(itemValue)"></td>
                 </tr>
                </tbody>
                <!-- <tfoot>
		            <tr>
						<th></th>
						<th></th>
						<th></th>
						<th>TOTAL</th>
						<th>Monto</th>
					</tr>
		        </tfoot> -->
            </table>
            <hr>
            <h3>Documentos relacionados</h3>
	        <table class="table table-striped datatable" id="tbCustomerReportFiles">
                <thead>
                 <tr>
                 	 <th>#</th>
                     <th>Archivo</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportFiles">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: ($index() + 1)"></td>
                     <td data-bind="click: downloadFile, attr:{'url': gkey}"><i class="mdi mdi-file" style="font-size: 20px;"></i></td>
                 </tr>
                </tbody>
            </table>
	      </div>
	      <div id="btnsAction">
		      <button type="button" class="btn btn-danger" id="btnReject" style="float: right;">RECHAZAR</button>
	      </div>
	      <br>
	      <div class="modal-footer">
	        <br>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
		id="reasonSmallModal" style="display: none;"
		aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Comentarios</h5>
				</div>
				<div class="modal-body">
				<label for="reasonLbl">Motivo:</label>
				<textarea id="reasonLbl" class="form-control" maxlength="50"></textarea>
				<br>
				<button type="button" class="btn btn-danger" id="btnReject2" style="float: right;">RECHAZAR</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<!-- end container -->
