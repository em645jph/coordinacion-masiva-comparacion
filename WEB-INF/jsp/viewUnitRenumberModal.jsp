<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUnitRenumberModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Corregir contenedor</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<label for="txtUnitRenumber"> Contenedor:</label> 
						<input type="text" name="txtUnitRenumber" id="txtUnitRenumber" class="form-control" />
					</div>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveUnitRenumber">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelUnitRenumber" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>	