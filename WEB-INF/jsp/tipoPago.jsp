<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<script src="<c:url value="/resources/assets/js/paymentSelector.js" />" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<style>
<!--
.btn-sq-lg {
  width: 150px !important;
  height: 150px !important;
}

.btn-sq {
  width: 100px !important;
  height: 100px !important;
  font-size: 10px;
}

.btn-sq-sm {
  width: 50px !important;
  height: 50px !important;
  font-size: 10px;
}

.btn-sq-xs {
  width: 25px !important;
  height: 25px !important;
  padding:2px;
}
.card-box > a.btn.btn-sq-lg.btn-primary {
    margin-left: 5%;
    padding: 20px;
}
button.btn.btn-sq-lg.btn-danger {
    margin-left: 5%;
    padding: 20px;
}
-->
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Pago</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h3 class="text-uppercase text-center">Seleccion� el medio de pago</h3>
					<br>
						<table class="table table-dark">
							<tr>
								<td>DRAFT: <strong>${invoice.invNbr}</strong></td>
								<td>PENDIENTE: $<strong><fmt:formatNumber value = "${invoice.invOwed}" type = "number" maxFractionDigits = "2"/></strong></td>
							</tr>
						</table>
						<script>
							if("${msg}" != ""){
								showModalConfirmation("${msg}","${invoice.cusPositiveBalance}");
							}
						</script>
					<hr>
					
					<c:choose>
					    <c:when test = "${showEPayment}">
							<a href="${pageContext.servletContext.contextPath}/Payment/paymentMethod/1" class="btn btn-sq-lg btn-primary"> 
							   <i class="fa fa-user fa-5x"></i> <br> Debin
							</a>
						</c:when>
					</c:choose>
					
					<c:choose>
					    <c:when test = "${interbankingIsAvailable}">
					        <a href="${pageContext.servletContext.contextPath}/Payment/paymentMethod/3" class="btn btn-sq-lg btn-primary"> 
								<i class="fa fa-user fa-5x"></i>
								<br> Interbanking
							</a>
					    </c:when>
					</c:choose>
					
					<c:choose>
					    <c:when test = "${invoice.cusPositiveBalance > 0 }">
					        <a href="${pageContext.servletContext.contextPath}/Payment/paymentMethod/4" class="btn btn-sq-lg btn-primary"> 
								<i class="fa fa-user fa-5x"></i>
								<br> Saldo a favor
							</a>
					    </c:when>    
					    <c:otherwise>
					       <button class="btn btn-sq-lg btn-danger" disabled> 
								<i class="fa fa-user fa-5x"></i>
								<br> Usted no posee saldo a favor <br>
							</button>
					    </c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>

	</div>
	
<!-- Modal -->
<div class="modal fade" id="modalCodeInputed" tabindex="-1" role="dialog" aria-labelledby="modalCodeInputed" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Validaci�n de c�digo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
		  <input type="text" class="form-control" id="txtCode" placeholder="Ingrese el c�digo..." aria-label="Ingrese el c�digo..." aria-describedby="btnValidateCode">
		  <div class="input-group-append">
		    <button class="button button--primary" type="button" id="btnValidateCode">Enviar</button>
		  </div>
		</div>
		<small>Si no le lleg� el c�digo, haga clic <strong><a id="resendCode" href="#" class="text-right">aqu�</a></strong></small>
      </div>
      <label id="labelMsg" style="color: red; display: none;"></label>
    </div>
  </div>
</div>