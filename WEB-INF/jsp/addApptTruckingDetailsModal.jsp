<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addApptTruckingDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Datos de Transporte</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addApptTruckingDetailsForm">
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtApptNbr" disabled>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-9">
							<input type="hidden" class="form-control" id="txtDriverGkey" disabled>
						</div>
					</div>
					<div class="form-group">
						<label for="txtTruckLicense" class="col-sm-12 col-form-label">Patente (*):</label>
						<div class="input-group col-sm-12">
							<input type="text" name="txtTruckLicense" id="txtTruckLicense" class="form-control" maxlength="7" placeholder="Ingres� la patente sin espacios" style="text-transform: uppercase;"/>
						</div>
					</div>
					<div class="form-group">
						<label for="txtDriverName" class="col-sm-12 col-form-label">Conductor (*):</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtDriverName" maxlength="60" placeholder="Ingres� el nombre completo" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="form-group">
						<label for="txtDriverLicense" class="col-sm-12 col-form-label">Licencia (*):</label>
						<div class="col-sm-12">
							<input type="text" class="form-control" id="txtDriverLicense" maxlength="8" placeholder="Ingres� el n�mero de licencia" style="text-transform: uppercase;">
						</div>
					</div>
					<div>
						<br>
					</div>
				</form>
				<div>
					<blockquote class="blockquote blockquote-reverse">
						<p class="text-justify" style="font-size:1.25rem">La informaci�n consignada precedentemente reviste car�cter de DECLARACION JURADA por lo que todo ocultamiento o falsa informaci�n ser� pasible de la sanci�n establecida en el Articulo 292 del C�digo Penal.</p>
					</blockquote>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveApptTruckingDetails">Guardar</button>&nbsp;&nbsp;
				<button type="button" class="btn btn-dark" id="btnCancelSaveApptTruckingDetails" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	