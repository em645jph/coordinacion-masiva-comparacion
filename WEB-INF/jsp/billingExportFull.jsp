<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/billingExportFull.js" />" charset="UTF-8"></script>	

<style>
#tbUnitsByBl,#btnGenerate {
	display: none;
}
</style>	
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n Exportaci�n Full</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<br>
						<table class="table table-dark">
							<tr>
								<td>RAZ�N SOCIAL: <strong>${customer.customerName} (${customer.customerId})</strong></td>
								<td>CUIT/CUIL: <strong>${customer.customerTaxId}</strong></td>
								<td>CONDICI�N PAGO: <strong>${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}</strong></td>
							</tr>
						</table>
					<hr>
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Ingres�
						n�mero de Booking y selecciona L�nea mar�tima</h6>
					<div class="row">
						<div class="col-sm-6">
							<label for="bkgnbr">Booking (*)</label> <input type="text"
								class="form-control" id="bkgnbr" name="bkgnbr" required>
						</div>
						<div class="col-sm-6">
							<label for="selectLineOp">Linea Operadora (*)</label> <select
								id="selectLineOp" class="form-control" name="selectLineOp"
								data-bind="options:viewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: 'Seleccione opci�n...'">
							</select>
						</div>
					</div>
					<div class="row optionalView">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary"
								id="btnFindEntity">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success"
								id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-striped datatable" id="tbUnitsByBl">
                    <thead>
                    <tr>
                    	<th></th>
                        <th>Booking</th>
                        <th>Contenedor</th>
                        <th>Coordinado</th>
                        <th>Turno</th>
                        <th>Cutoff</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.UnitsByBl">
                    
                    <!-- ko {if: apptNbr == 0 } -->
					<tr class="ignoreme" data-toggle="tooltip" data-placement="top" title="No disponible para facturaci�n">
                        <td></td>
                        <td data-bind="text: bookingNbr"></td>
                        <td data-bind="text: unitId"></td>
                        <!-- ko {if: apptNbr == 0} -->
                        <td><i class="ti-close"></i></td>
                        <!-- /ko -->
                        <!-- ko {ifnot: apptNbr == 0} -->
                        <td><i class="ti-check"></i></td>
                        <!-- /ko -->
                        <td data-bind="text: apptTimeStr"></td>
                        <td data-bind="text: cvCutoffStr"></td>
                        <!-- ko {if: drafted > 0} -->
                        <!-- <td><i class="ti-check"></i></td> -->
                        <!-- /ko -->
                        <!-- ko {ifnot: drafted > 0} -->
                        <!-- <td><i class="ti-close"></i></td> -->
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    <!-- ko {ifnot: apptNbr == 0 } -->
					<tr>
                        <td></td>
                        <td data-bind="text: bookingNbr"></td>
                        <td data-bind="text: unitId"></td>
                        <!-- ko {if: apptNbr == 0} -->
                        <td><i class="ti-close"></i></td>
                        <!-- /ko -->
                        <!-- ko {ifnot: apptNbr == 0} -->
                        <td><i class="ti-check"></i></td>
                        <!-- /ko -->
                        <td data-bind="text: apptTimeStr"></td>
                        <td data-bind="text: cvCutoffStr"></td>
                        
                        <!-- ko {if: drafted > 0} -->
                        <!-- <td><i class="ti-check"></i></td> -->
                        <!-- /ko -->
                        <!-- ko {ifnot: drafted > 0} -->
                        <!-- <td><i class="ti-close"></i></td> -->
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    </tbody>
                </table>
			
			</div>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="button button--primary" id="btnGenerate">Generar draft</button>
			</div>
		</div>
		
	</div>
<!-- end container -->