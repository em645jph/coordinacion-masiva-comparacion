<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/userRegisterPendings.js" />" charset="UTF-8"></script>

        <div class="wrapper">
            <div class="container-fluid">
            
                <div class="row">
					<div class="col-sm-12">
						<h2>Administración de nuevos usuarios</h2>
					</div>
				</div>
				<br>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table table-striped datatable">
                                <table class="table table-hover datatable" id="tbUsersPendings">
                                    <thead>
	                                    <tr>
	                                    	<th>CUIT</th>
	                                    	<th>Razon Social</th>
	                                        <th>Email contacto</th>
	                                        <th>Email comex</th>
	                                        <th>Email facturación</th>
	                                        <th>Documentación</th>
	                                        <th>Aprobar</th>
	                                        <th>Rechazar</th>
	                                    </tr>
                                    </thead>
                                    <tbody data-bind="foreach: viewModel.pendingUsers">
	                                    <tr data-bind="attr:{reqGkey: reqGkey}">
	                                    	<td data-bind="text: documentNbr"></td>
	                                    	<td data-bind="text: fullname"></td>                                       
	                                        <td data-bind="text: emailAddress"></td>
	                                        <td data-bind="text: emailComex"></td>
	                                        <td data-bind="text: emailManagement"></td>
	                                        <td><button type="button" data-bind="attr:{'regGkey': reqGkey}" onclick="getDocumentsByRegisterId(this);" class="btn btn-icon btn-success btn-sm" id="">VER</button></td>
	                                        <td>
	                                        	<button type="button" data-bind="attr:{'regGkey': reqGkey, 'rowid': $index}" data-action="approve" onclick="modalApprove(this);" class="btn btn-icon btn-success btn-sm" id="">Aprobar</button>
	                                        </td>
	                                        <td>
	                                        	<button type="button" data-bind="attr:{reqGkey: reqGkey}" data-action="reject" onclick="modalReject(this);" class="btn btn-icon btn-danger btn-sm" id="">Rechazar</button>
	                                        </td>
	                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end container -->
            
<!-- Modal -->
<div class="modal fade" id="modalApprove" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
     	<div class="row text-center">
			<div class="col-sm-12">
				<h2>Información adicional</h2>
				<br>
				<h4 id="documentNbrTitle"></h4>
			</div>
		</div>
		<br>
        <form>
        	<div class="col-md-12">
        		<div class="row">
        			<input type="hidden" id="gkey">
        			<div class="col-md-6">
        				<label>Tax Group</label>
        				<select id="selectTaxGroup" class="form-control selectpicker" data-live-search="true">
        					<option data-tokens="first" value="-1">Seleccione una opción</option>
        				</select>
        			</div>
        			<div class="col-md-6">
        				<label>Tipo Documento</label>
        				<select id="selectDocumentType" class="form-control selectpicker" data-live-search="true">
        					<option value="-1">Seleccione una opción</option>
        				</select>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-6">
        				<label>Residente CABA?</label>
        				<select id="selectIsCabaResident" class="form-control">
        					<option value="-1">Seleccione una opción</option>
        					<option value="1">SI</option>
        					<option value="2">NO</option>
        				</select>
        			</div>
        			<div class="col-md-6">
        				<label>Razon Social</label>
        				<input type="text" class="form-control required" id='formFullname' name="formFullname"></input>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-6">
        				<label>Ciudad</label>
        				<input type="text" class="form-control required" id='formCity' name="formCity"></input>
        			</div>
        			<div class="col-md-6">
        				<label>Dirección</label>
        				<input type="text" class="form-control required" id='formAddress' name="formAddress"></input>
        			</div>
        			
        		</div>
        		<div class="row">
        			<div class="col-md-6">
        				<label>Tipo de Actividad</label>
        				<input type="text" class="form-control required" id='formBusinessType' name="formBusinessType"></input>
        			</div>
        			<div class="col-md-6">
        				<label>Email de contacto</label>
        				<input type="text" class="form-control required" id='formEmailAddress' name="formEmailAddress"></input>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-6">
        				<label>Email COMEX</label>
        				<input type="text" class="form-control required" id='formEmailComex' name="formEmailComex"></input>
        			</div>
        			<div class="col-md-6">
        				<label>Email facturación</label>
        				<input type="text" class="form-control required" id='formEmailBilling' name="formEmailBilling"></input>
        			</div>        			        			
        		</div>
        		<div class="row">
        			<div class="col-md-6">
        				<label>Notas</label>
        				<input type="text" class="form-control required" id='formNote' name="formNote"></input>
        			</div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" data-dismiss="modal" id="btnApprove">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
    
  </div>
</div>


<!--Modal: Name-->
<div class="modal fade" id="modalDocuments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
		<div class="modal-body mb-0 p-0">
			<div class="row text-center">
				<div class="col-sm-12">
					<h2>Documentación</h2>
				</div>
			</div>
			<br>
			<div class="col-md-12">
			    <div class="row">
					<div class="col-md-12">
						<div class="" id="listDocuments">
				        </div>
			        </div>
			     	<div class="col-md-12">
						<div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
						  <br><br><br><br>
				          <iframe class="embed-responsive-item" src="" id="frameDoc"></iframe>
				        </div>
			        </div>
			    </div>
			</div>
		</div>
		<div class="modal-footer">
	        <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
	    </div>

    </div>
<!--/.Content-->

  </div>
</div>


<!--Modal: Name-->
<div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
		<div class="modal-body mb-0 p-0">
			<div class="row text-center">
				<div class="col-sm-12">
					<h2>Rechazo de solicitud</h2>
				</div>
			</div>
			<br>
			<div class="col-md-12">
			    <div class="row">
					<div class="col-md-12">
						<form class="" id="formReject">
							<label>Por favor, ingrese un motivo.</label>
							<br>
							<textarea rows="2" cols="2" class="form-control" id="reasonText"></textarea>
							<br>
							<small>Tenga en cuenta que el motivo sera enviado al cliente en una notificación.</small>
							<br>
				        </form>
			        </div>
			    </div>
			</div>
		</div>
		<div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
	        <button type="button" class="btn btn-info" data-dismiss="modal" id="btnSendReject">Enviar</button>
	    </div>

    </div>
	<!--/.Content-->
	</div>
</div>