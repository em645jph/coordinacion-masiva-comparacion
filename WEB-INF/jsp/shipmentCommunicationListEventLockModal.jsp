<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="shipmentCommunicationListEventLockModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eventos de Bloqueos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="unit">Contenedor</label>
		      <input type="text" class="form-control" id="unit"  disabled data-bind="textInput: unitModal">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="permission">Permiso</label>
		      <input type="text" class="form-control" id="permission" disabled data-bind="textInput: permissionModal">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="status">Estado actual</label>
		    <input type="text" class="form-control" id="status" disabled data-bind="textInput: statusModal">
		  </div>
		</form>
		<div class="row p-3 text-center">
		<div class="col-ms-12 col-md-12 text-center" data-bind="if: eventTable().length == 0 ">
						<div class="row spinner-border" role="status">
		  					 <span class="sr-only"></span>
						</div>
						<div>
		  					 <span> Cargado....</span>
						</div>
					</div>
		</div>
		<div class="row" id="divTableEvent">
                    <div class="col-sm-12" data-bind="if: eventTable().length != 0 ">				
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbevents" width="100%">
                                		<thead>
											<tr>
											    <th>Fecha bloqueo</th>
											    <th>Notas</th>
											    <th>Fecha desbloqueo</th>
											    <th>Notas</th>
											    <th>Desbloquear</th>
											</tr>
										</thead>
										<tbody data-bind="foreach: supportUnitViewModel.eventTable">
											<tr>
												<td><label  data-bind="text: placedTimeStr" ></label> </td>
												<td><label  data-bind="text: note" ></label> </td>
												<td><label  data-bind="text: vetoPlacedTimeStr" ></label> </td>
												<td><label  data-bind="text: veto_note" ></label></td>
												<td>
													 <!-- ko {if: veto_gkey == null} -->
													<button class="btn btn-icon  btn-light  btn-sm"  data-bind="click: $parent.viewUnLockModal"> <i class="ti-unlock"></i> </button>
													 <!-- /ko -->
												 	 <!-- ko {if: veto_gkey != null} -->
														<label></label>
													 <!-- /ko -->
												</td>
											</tr>
										</tbody>                                
                                </table>
                            </div>
                    </div>
                </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-dark" id="btnClosePemBlockList" data-dismiss="modal">Cerrar</button>
      </div> 
    </div>
  </div>
</div>