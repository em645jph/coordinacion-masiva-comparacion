<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/billingManifest.js" />" charset="UTF-8"></script>	

<style>
#tbUnitsByBl,#btnGenerate {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n Manifiestos</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
				    <br>
						<table class="table table-dark">
							<tr>
								<td>RAZ�N SOCIAL: <strong>${customer.customerName} (${customer.customerId})</strong></td>
								<td>CUIT/CUIL: <strong>${customer.customerTaxId}</strong></td>
								<td>CONDICI�N PAGO: <strong>${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}</strong></td>
							</tr>
						</table>
					<hr>
					<div class="row">
						<div class="col-sm-12">
							<label for="txtSearchKeyParameter">Manifiesto (*):</label> 
							<input type="text" name="txtSearchUnitId" id="txtSearchUnitId" class="form-control" rows="4" style="text-transform: uppercase;" placeholder="MANIFIESTO1, MANIFIESTO2, MANIFIESTO3"/> 
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary" id="btnSearch">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success" id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
				   <div id="actionButtons">
						<button type="button" class="btn btn-primary" id="btnGenerateDraft">Generar draft</button>
					</div>
					<div class="col-sm-12">
						<br />
					</div>
					<div class="table-responsive">
						<table class="table table-hover datatable" id="tbManifestCue" width="100%">
						    ${dataTable}
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
<!-- end container -->