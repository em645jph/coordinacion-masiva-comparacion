<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/vipManagement.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div>
							<ul class="nav nav-tabs tabs-bordered">
								<li class="nav-item"><a href="#appt" data-toggle="tab" aria-expanded="false" class="nav-link active"> Turnos VIP</a></li>
								<li class="nav-item"><a href="#customer" data-toggle="tab"  aria-expanded="false" class="nav-link"> Clientes VIP</a></li>
								<li class="nav-item"><a href="#event" data-toggle="tab" aria-expanded="true" class="nav-link"> Eventos </a></li>								
							</ul>
						</div>	
						<div>							
							<div class="tab-content">
								<div class="tab-pane fade show active" id="appt">
									<div class="row">
										<div class="col-sm-12">
											<div class="card-box">
												<div class="row">
													<div class="col-sm-4">
														<label for="txtSearchCustomer">Cliente:</label>
														<input type="text" name="txtSearchCustomer" id="txtSearchCustomer" class="form-control" maxlength="50" placeholder="CUIT o NOMBRE"/>
													</div>
													<div class="col-sm-4">
														<label for="cbSearchCategory">Categor�a:</label>										
														<select id="cbSearchCategory" class="form-control" name="cbSearchCategory"  
														        data-bind="options:vipViewModel.categoryCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
													<div class="col-sm-4">
														<label for="cbSearchDayOfWeek">D�a:</label>										
														<select id="cbSearchDayOfWeek" class="form-control" name="cbSearchDayOfWeek"  
														        data-bind="options:vipViewModel.dayCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<br />
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" align="center">
														<button type="button" class="button button--primary"
															id="btnSearchVipAppt">Buscar</button>
														&nbsp;&nbsp;
														<button type="button" class="button button--success"
															id="btnCleanSearchVipAppt">Limpiar</button>
													</div>
												</div>
											</div>										
										</div>										
									</div>
									<div class="row">
					                    <div class="col-sm-12">
					                        <div class="card-box">
					                            <div id="apptActionButtons">
													${apptActionButtons}
												</div>
												<div class="col-sm-12">
													<br />
												</div>
					                            <div class="table-responsive">
					                                <table class="table table-hover datatable" id="tbVipAppt" width="100%">
					                                	${apptDataTable}                                
					                                </table>
					                            </div>
					                        </div>
					                    </div>
					               </div>
								</div>
								<div class="tab-pane fade" id="customer">
									<div class="row">
					                    <div class="col-sm-12">
					                        <div class="card-box">
					                            <div id="customerActionButtons">
													${customerActionButtons}
													<button type="button" class="btn btn-success" id="btnRefreshVipCustomer">Refrescar</button>
												</div>
												<div class="col-sm-12">
													<br />
												</div>
					                            <div class="table-responsive">
					                                <table class="table table-hover datatable" id="tbVipCustomer" width="100%">
					                                	${customerDataTable}                                
					                                </table>
					                            </div>
					                        </div>
					                    </div>
					               </div>
								</div>
								<div class="tab-pane fade" id="event">
									<div class="row">
					                    <div class="col-sm-12">
					                        <div class="card-box">
					                            <div id="eventActionButtons">
													${eventActionButtons}
													<button type="button" class="btn btn-success" id="btnRefreshVipEvent">Refrescar</button>
												</div>
												<div class="col-sm-12">
													<br />
												</div>
					                            <div class="table-responsive">
					                                <table class="table table-hover datatable" id="tbVipEvent" width="100%">
					                                	${eventDataTable}                                
					                                </table>
					                            </div>
					                        </div>
					                    </div>
					               </div>
								</div>
							</div>
						</div>
					</div>
                </div>
                <!-- end row -->
                <%@include file="addVipEventModal.jsp"%>
                <%@include file="addVipCustomerModal.jsp"%>
                <%@include file="editVipCustomerEventModal.jsp"%>
                <%@include file="addVipApptModal.jsp"%>
                <%@include file="editVipApptEventModal.jsp"%>
                <%@include file="editVipApptQuotaModal.jsp"%>
            </div> <!-- end container -->