<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
<link rel="stylesheet" href="https://www.dropzonejs.com/css/dropzone.css?v=1595510599">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/balanceComposition.js" />" charset="UTF-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="<c:url value="/resources/assets/js/jquery.maskMoney.js" />" type="text/javascript"></script>
<script src="https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.2/css/colReorder.dataTables.min.css">
	
<style>
#tbDrafts,#tableBox {
	display: none;
}
.row_selected{
    background-color: #ff6161 !important
}
</style>
<div class="wrapper">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-sm-12">
				<h2>Mi estado de cuenta</h2>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-sm-4">
				<h6 class="text-muted font-13 mt-0 text-uppercase">Usuarios a gestionar</h6>
				<div class="input-group mb-3">
					<select id="selectCustomer" class="form-control" name="selectCustomer"
						data-bind="options:viewModel.customers, optionsText:'customerName' , optionsValue:'customerTaxId', optionsCaption: 'Seleccione opci�n...'">
					</select>
					<span class="input-group-append">
						<button class="btn btn-icon btn-primary" id="btnFindCustomerInformation">BUSCAR</button>
					</span>
				</div>
			</div>
		</div>
		<br>
		
		<div class="row">
                    <div class="col-sm-12">
						<div class="">
								<div class="row" style="display:none;" id="rowBoxHeaders">
									<div class="col-sm-2" id="divBoxLimit">
		                                <div class="card-box bg-primary" data-bind="foreach: viewModel.customerBalance">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">L�mite otorgado</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" style="font-size:25px;" data-bind="text: '$' + parseMoneyValue(customerCreditLimit)"></span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxCashStatus">
		                                <div class="card-box" id="cardBoxCashStatus">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase" id="labelCashStatus"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalCashStatus">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barDelayed" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxVencido">
		                                <div class="card-box bg-danger">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase" id="labelVencido"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalDelayedBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barDelayed" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
									<div class="col-sm-2" id="divBoxNoVencido">
		                                <div class="card-box">
		                                    <h6 class="text-muted font-13 mt-0 text-uppercase" id="labelNoVencido"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-success" id="barTotal" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxBalance">
		                                <div class="card-box bg-primary" data-bind="foreach: viewModel.customerBalance">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">Saldo</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" style="font-size:25px;" data-bind="text: '$' + parseMoneyValue(customerBalance)"></span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
									
		                            <div class="col-sm-2" id="divBoxSelectedItems">
		                                <div class="card-box bg-primary">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">Total seleccionado</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" id="totalSelectedBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            
		                            <div class="col-sm-3" id="divBoxCashSuggest" onclick="javascript:window.open('https://apps.apmterminals.com.ar/solicitudesonline')" style="display:none;cursor: pointer">
		                                <div class="card-box bg-warning">
		                                    <h6 class="text-white font-17 mt-0 text-uppercase text-center">Hac� click ac� para presentar el pago de tus deudas o realizar cualquier consulta, ingresar�s a Solicitudes Online.</h6>
		                                </div>
		                            </div>
		                            <div class="col-sm-3" id="divBoxCashSuggest2" style="display:none;cursor: pointer">
		                                <div class="card-box bg-info">
		                                    <a href="tel:080066612345">
		                                    	<h6 class="text-white mt-0 text-uppercase text-center font-20" style="font-size: 20px;">Si necesit�s asistencia telef�nica comunicate con nosotros al </h6>
		                                    	<h6 class="text-white mt-0 text-uppercase text-center font-20" style="font-size: 20px;"> <span><i class="ti-headphone-alt"></i></span><span> 0810-555-APMT(2768)</span> </h6>
		                                    </a>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxInform">
		                                <div class="card-box bg-success" data-bind="foreach: viewModel.customerBalance">
		                                    <a onclick="showModalInputsData()"><h2 class="text-white mt-0 text-uppercase text-center">Presentar pago</h2></a>
		                                </div>
		                            </div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
							</div>
                    </div>
                </div>
                
                
			<div class="row">
				<div class="col-md-12">
					<div class="card-box" id="tableBox">
						<table class="table table-striped datatable" id="tbBalance">
		                    <thead>
			                    <tr>
			                    	<th></th>
			                        <th>Fecha de Documento</th>
			                        <th>Documento #</th>
			                        <th>Monto</th>
			                        <th>Saldo</th>
			                        <th>Fecha Vencimiento</th>
			                        <th>Mora</th>
			                        <th>Informaci�n</th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.balance">
			                    <tr data-bind="attr:{ 'id': $index }, style: { 'background-color': qtyDelayDays > 0 ? 'lightcoral' : qtyDelayDays < 0 ? 'lightgreen' : 'white' }">                                       
			                        <td></td>
			                        <td data-bind="text: documentDate"></td>
			                        <td data-bind="text: documentNbr"></td>
			                        <td data-bind="text: amount.toFixed(2)"></td>
			                        <td data-bind="text: balance.toFixed(2)"></td>
			                        <td data-bind="text: documentDueDate"></td>
			                        <td data-bind="text: qtyDelayDays"></td>
			                        <td><i data-bind="click: getValuesAssociatedByDocumentNbr" class="mdi mdi-information-outline" style="font-size: 20px;"></i></td>
			                    </tr>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
		
	</div>
	
	<table id="tbInformation" class="table table-striped" style="display:none;">
		<thead>
         <tr>
             <th>Valor</th>
         </tr>
        </thead>
        <tbody data-bind="foreach: viewModel.dataInform">
        	<tr>
        		<td data-bind="text: $data"></td>
        	</tr>
        </tbody>
	</table>

	<!-- Modal -->
	<div class="modal fade" id="modalFiles" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titleInformationReport">Presentaci�n de Pago</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" id="divModalBody">
	      <table class="table table-striped datatable" id="tbCustomerReportDocuments">
                <thead>
                 <tr>
                     <th>Fecha</th>
                 	 <th>Documento</th>
                     <th>Monto</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.customerReportDocuments">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: documentDate"></td>
                     <td data-bind="text: documentNumber"></td>
                     <td data-bind="text: documentAmount"></td>
                 </tr>
                </tbody>
                <tfoot>
		            <tr>
		                <th></th>
	                 	<th>TOTAL</th>
	                    <th>Monto</th>
		            </tr>
		        </tfoot>
          </table>
	      <hr>
	      <h5><strong>Agregar pagos</strong></h5>
	      <form action="">
	      	  <!-- Inputs -->
	      	  	<div class="form-group">
				    <label for="lblInputType">Tipo</label>
				    <select class="form-control" id="selectItemType">
					    <option value="-1">Seleccione una opci�n...</option>
					    <option value="1">Transferencia</option>
					    <option value="2">Retenci�n Ganancias</option>
					    <option value="3">Retenci�n IIBB CABA</option>
					    <option value="4">Retenci�n SUSS</option>
					    <option value="5">Diferencia de Cambio</option>
					</select>
			    </div>
			    <div id="divSelectTransfer" style="display:none;">
			    	<label for="lblSelectBank">Banco</label>
			    	<select class="form-control" id="selectBank">
			    		<option value="-1">Seleccione una opci�n...</option>
			    		<option value="HSBC">HSBC</option>
			    		<option value="ICBC">ICBC</option>
			    	</select>
			    </div>
			    <div id="formDivGroup">
					<div class="form-group" id="divFormConcept">
					    <label for="inputConcept" id="lblInputConcept">Concepto</label>
					    <input type="text" class="form-control" id="inputConcept">
					</div>
				    <div class="form-group" id="divFormFecha">
					    <label for="inputDate">Fecha</label>
					    <input type="text" class="form-control" id="inputDate">
					</div>
					<div class="form-group" id="divFormReference">
					    <label for="inputReference">Referencia</label>
					    <input type="text" class="form-control" id="inputReference">
					</div>
					<div class="form-group" id="divFormAmount">
					    <label for="inputAmount">Monto</label>
					    <input type="text" class="form-control" id="inputAmount">
					    <small id="lblDiffCambio" style="display:none;">Recuerde que los valores de Diferencia de cambio deben ingresarse con el s�mbolo </small>
					</div>
					<button class="btn btn-primary" id="addItem" type="button">Agregar</button>
				</div>
	      	  <!-- End inputs -->
	      		<hr>
	      		<h5><strong>Pagos agregados</strong></h5>
	      		<table class="table table-striped datatable" id="tbItems">
	      			<thead>
						<tr>
							<th>Tipo</th>
							<th>Concepto</th>
							<th>Fecha</th>
							<th>Referencia</th>
							<th>Monto</th>
						</tr>	      			
	      			</thead>
	      			<tbody>     			
	      			</tbody>
	      			<tfoot>
			            <tr>
							<th></th>
							<th></th>
							<th></th>
							<th>TOTAL</th>
							<th>Monto</th>
						</tr>
			        </tfoot>
	      		</table>
	      		<button class="btn btn-danger" id="removeItem" type="button">Eliminar</button>
	      		<hr>
	      </form>
	      <h5><strong>Archivos adjuntos</strong></h5>
		      <!-- Dropzone -->
					<div id="myDropzone" class="dropzone"></div>
		      <!-- End dropzone  -->
	      </div>
	      <div class="modal-footer">
	        <button type="button" id="btnSendPresent" class="btn btn-success">PRESENTAR</button>
	      </div>
	      <hr>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modalDataAssociate" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titleInformationData">Informaci�n asociada</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      <table class="table table-striped datatable" id="tbDolarData">
                <thead>
                 <tr>
                     <th>USD</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.usdData">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: parseMoneyValue($data)"></td>
                 </tr>
                </tbody>
          </table>
	      <table class="table table-striped datatable" id="tbEntityData">
                <thead>
                 <tr>
                     <th id="theadEntity"></th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.entity">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: $data"></td>
                 </tr>
                </tbody>
          </table>
          <table class="table table-striped datatable" id="tbValuesData">
                <thead>
                 <tr>
                     <th>Contenedor(es)</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.values">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: $data"></td>
                 </tr>
                </tbody>
          </table>
	      <hr>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	
<!-- end container -->