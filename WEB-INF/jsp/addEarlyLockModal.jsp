<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="addEarlyLockModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="sfpModalTitle">Agregar Bloqueo anticipado</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">					
					<div class="col-sm-12">
						<label for="txtReferenceId"> Permiso de embarque: </label>										
						<input type="text" name="txtReferenceId" id="txtReferenceId" class="form-control" maxlength="16" placeholder="Permiso de embarque" style="text-transform: uppercase;"/>
					</div>
					<div class="col-sm-12">
						<label for="txtNote"> Notas: </label>										
						<textarea class="form-control rounded-0"  rows="2" id="txtNote" placeholder="Comentarios sobre el bloqueo anticipado" maxlength="100" style="text-transform: uppercase;"></textarea>
					</div>
					<div class="col-sm-12">
						<label for="txtReported"> Reportado:</label>
						<input type="text" name="txtReported" id="txtReported" class="form-control" placeholder="YYYY-MM-DD"/>
					</div>
				</div>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveEarlyLock">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSaveEarlyLock" data-dismiss="modal">Cancelar</button>
			</div>	
		</div>
	</div>
</div>	