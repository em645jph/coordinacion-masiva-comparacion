<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/scheduleManagement.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div>
							<ul class="nav nav-tabs tabs-bordered">
								<li class="nav-item"><a href="#filter" data-toggle="tab" aria-expanded="false" class="nav-link active"> Filtros </a></li>
								<li class="nav-item"><a href="#rules" data-toggle="tab"  aria-expanded="false" class="nav-link"> Reglas </a></li>
								<li class="nav-item"><a href="#businessHours" data-toggle="tab" aria-expanded="true" class="nav-link"> Horarios Inh�biles </a></li>								
							</ul>
						</div>	
						<div>							
							<div class="tab-content">
								<div class="tab-pane fade show active" id="filter">
									<div class="row">
										<div class="col-sm-12">
											<div class="card-box">
												<div class="row">
													<div class="col-sm-4">
														<label for="txtSearchSfdId">Id</label>
														<input type="text" name="txtSearchSfdId" id="txtSearchSfdId" class="form-control" maxlength="50" />
													</div>
													<div class="col-sm-4">
														<label for="txtSearchSfdDesc">Descripci�n</label> 
														<input type="text" name="txtSearchSfdDesc" id="txtSearchSfdDesc" class="form-control" maxlength="50" />
													</div>
													<div class="col-sm-4">
														<label for="cbSearchTranType">Tipo Transacci�n</label>										
														<select id="cbSearchTranType" class="form-control" name="cbSearchTranType"  
														        data-bind="options:scheduleViewModel.searchTranTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<br />
													</div>
												</div>
												<div class="row">
													<div class="col-sm-4">
														<label for="txtSearchSfdFromDate">V�lido Desde</label> 
														<input type="text" name="txtSearchSfdFromDate" id="txtSearchSfdFromDate" class="form-control" placeholder="YYYY-MM-DD" />
													</div>
													<div class="col-sm-4">
														<label for="txtSearchSfdToDate">V�lido Hasta</label> 
														<input type="text" name="txtSearchSfdToDate" id="txtSearchSfdToDate" class="form-control" placeholder="YYYY-MM-DD" />
													</div>
													<div class="col-sm-4">
														<label for="cbSearchAction">Acci�n</label> 
														<select id="cbSearchAction" class="form-control" name="cbSearchAction" 
														        data-bind="options:scheduleViewModel.searchActionCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<br />
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" align="center">
														<button type="button" class="button button--primary"
															id="btnSearchFilter">Buscar</button>
														&nbsp;&nbsp;
														<button type="button" class="button button--success"
															id="btnCleanSearchFilter">Limpiar</button>
													</div>
												</div>
											</div>										
										</div>										
									</div>
									<div class="row">
					                    <div class="col-sm-12">
					                        <div class="card-box">
					                            <div id="filterActionButtons">
													${filterActionButtons}
												</div>
												<div class="col-sm-12">
													<br />
												</div>
					                            <div class="table-responsive">
					                                <table class="table table-hover datatable" id="tbCreatedFilter" width="100%">
					                                	${filterDataTable}                                
					                                </table>
					                            </div>
					                        </div>
					                    </div>
					               </div>
								</div>
								<div class="tab-pane fade" id="rules">
									<div class="row">
										<div class="col-sm-12">
											<div class="card-box">
												<div class="row">
													<div class="col-sm-4">
														<label for="txtSearchRuleId">Id</label>
														<input type="text" name="txtSearchRuleId" id="txtSearchRuleId" class="form-control" maxlength="50" />
													</div>
													<div class="col-sm-4">
														<label for="txtSearchRuleDesc">Descripci�n</label> 
														<input type="text" name="txtSearchRuleDesc" id="txtSearchRuleDesc" class="form-control" maxlength="50" />
													</div>
													<div class="col-sm-4">
														<label for="cbSearchRuleTranType">Tipo Transacci�n</label>										
														<select id="cbSearchRuleTranType" class="form-control" name="cbSearchRuleTranType"  
														        data-bind="options:scheduleViewModel.searchTranTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<br />
													</div>
												</div>
												<div class="row">
													<div class="col-sm-4">
														<label for="txtSearchRuleFromDate">Creado Desde</label> 
														<input type="text" name="txtSearchRuleFromDate" id="txtSearchRuleFromDate" class="form-control" placeholder="YYYY-MM-DD" />
													</div>
													<div class="col-sm-4">
														<label for="txtSearchRuleToDate">Creado Hasta</label> 
														<input type="text" name="txtSearchRuleToDate" id="txtSearchRuleToDate" class="form-control" placeholder="YYYY-MM-DD" />
													</div>
													<div class="col-sm-4">
														<label for="cbSearchRuleAction">Acci�n</label> 
														<select id="cbSearchRuleAction" class="form-control" name="cbSearchRuleAction" 
														        data-bind="options:scheduleViewModel.searchRuleActionCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<br />
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12" align="center">
														<button type="button" class="button button--primary" id="btnSearchRule">Buscar</button> &nbsp;&nbsp;
														<button type="button" class="button button--success" id="btnCleanSearchRule">Limpiar</button>
													</div>
												</div>
											</div>										
										</div>										
									</div>
									<div class="row">
					                    <div class="col-sm-12">
					                        <div class="card-box">
					                            <div id="ruleActionButtons">
													${ruleActionButtons}
												</div>
												<div class="col-sm-12">
													<br />
												</div>
					                            <div class="table-responsive">
					                                <table class="table table-hover datatable" id="tbCreatedRule" width="100%">
					                                	${ruleDataTable}                                
					                                </table>
					                            </div>
					                        </div>
					                    </div>
					               </div>
								</div>
								<div class="tab-pane fade" id="businessHours">
									<div class="row">
										<div class="col-sm-12">
											<div class="card-box">
												 <p>COMMING SOON...</p>
                                                 <p>NO DESESPERES!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
                <!-- end row -->
                
                <%@include file="addEditSchFilterDefModal.jsp" %>
                <%@include file="addEditSchFilterParamModal.jsp" %>
                <%@include file="viewSchFilterDefCalendarModal.jsp" %>
                <%@include file="addEditSchRuleModal.jsp" %>
                <%@include file="viewSchFilterDefModal.jsp" %>
            </div> <!-- end container -->