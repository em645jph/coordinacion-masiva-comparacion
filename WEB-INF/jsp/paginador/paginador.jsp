
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${ paginador.cantidadDePaginas>1 }">
	<form:form id="frmPaginador" method="post" commandName="paginador" action="">
		<table class="contenedorNumeroDePaginas">
			<tr>
				<td colspan="${param.pColspan}">
					<c:set var="inicio" value="${paginador.viewPaginaInicio}" />
					<c:set var="fin" value="${paginador.viewPaginaFin}" />
					<table class="numeroDePaginas">
						<tr>
							<td class="numeroDePaginas"><spring:message code="field.paginas" /></td>
							<c:forEach var="i" begin="${inicio}" end="${fin}">
								<c:choose>
									<c:when test="${paginador.numeroDePagina==i}">
										<td align="center" style="width: 20px"><a class="numeroDePaginasActive"><c:out value="${i}"/></a></td>
									</c:when>
									<c:otherwise>
										<td align="center" style="width: 20px"><a class="numeroDePaginas" href="${pageContext.request.contextPath}/consultaArchivosPorNumeroDePagina.htm?numeroDeHojaSeleccionada=<c:out value="${i}"/>"><c:out value="${i}"/></a></td>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form:form>
</c:if>
