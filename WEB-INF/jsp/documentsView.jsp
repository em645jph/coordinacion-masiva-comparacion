<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/documents.js" />" charset="UTF-8"></script>	

<style>
#tbUnits,#btnGenerate {
	display: none;
}
</style>
	
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Descarga de GatePass</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">
					Ingres�	n�mero de BL/Booking/Contenedor/EDO</h6>
					<div class="row">
						<div class="col-sm-6">
							<label for="valueNbr">N�mero (*)</label> 
							<input type="text" class="form-control" id="valueNbr" name="valueNbr" required>
						</div>
						<div class="col-sm-6">
							<label for="selectType">Tipo (*)</label> 
								<select class="form-control" id="selectType" name="selectType" required>
								  <option value="-1">Seleccionar una opci�n...</option>
								  <option value="BK">Booking</option>
								  <option value="BL">Bill of Lading</option>
								  <option value="UNIT">Contenedor Full</option>
								  <option value="STRGE">Contenedor Vac�o</option>
								  <option value="APPT">Booking Retiro de Vac�o</option>
								</select>
						</div>
					</div>
					<div class="row optionalView">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary"
								id="btnFindEntity">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success"
								id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-striped datatable" id="tbUnits">
                    <thead>
                    <tr>
                        <th>Contenedor</th>
                        <th>Categoria</th>
                        <th>Pago Verificado</th>
                        <th>Coordinado</th>
                        <th>Datos Transporte</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.Units">
						<tr>
							<td data-bind="text: unitId, attr: { 'canBeDownloaded': haveHold && haveAppt && validTruckingDetails? 1 : 0, 'id' : unitId, 'category' : category, 'apptNbr' : apptNbr}"></td>
							<td data-bind="text: category"></td>
	                        <td>
	                        	<!-- ko {ifnot: haveHold} -->
	                        		<i class="ti-close"></i>
	                        	<!-- /ko -->
	                        	<!-- ko {if: haveHold} -->
	                        		<i class="ti-check"></i>
	                        	<!-- /ko -->
	                        </td>
	                        <td>
	                        	<!-- ko {ifnot: haveAppt} -->
	                        		<i class="ti-close"></i>
	                        	<!-- /ko -->
	                        	<!-- ko {if: haveAppt} -->
	                        		<i class="ti-check"></i>
	                        	<!-- /ko -->
	                        </td>
	                        <td>
	                        	<!-- ko {if: category == 'EXPRT' && hasTruckingDetails} -->
	                        		<a href="#" style="color:#4fc55b;"> <i class="ti-pencil-alt" data-bind="click: $parent.updateTruckingDetails"></i></a>
	                        	<!-- /ko -->
	                        	<!-- ko {if: category == 'EXPRT' && !hasTruckingDetails} -->
	                        		<a href="#" style="color:#f01313;"> <i class="ti-pencil-alt" data-bind="click: $parent.updateTruckingDetails"></i></a>
	                        	<!-- /ko -->
	                        	<!-- ko {if: category != 'EXPRT'} -->
	                        		<label>--</label>
	                        	<!-- /ko -->
	                        </td>
						</tr>
                    </tbody>
                </table>
			
			</div>
		</div>
		<br>
		<%@include file="addApptTruckingDetailsModal.jsp" %>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="button button--primary" id="btnGenerate">Descargar</button>
				<a id='dwnldLnk' download='GatePass.pdf' style="display:none;" /> 
			</div>
		</div>
		
	</div>
<!-- end container -->