<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/billingDeliverEmpty.js" />" charset="UTF-8"></script>	

<style>
#tbUnitsByBl,#btnGenerate {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n Retiro Vac�os</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
				    <br>
						<table class="table table-dark">
							<tr>
								<td>RAZ�N SOCIAL: <strong>${customer.customerName} (${customer.customerId})</strong></td>
								<td>CUIT/CUIL: <strong>${customer.customerTaxId}</strong></td>
								<td>CONDICI�N PAGO: <strong>${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}</strong></td>
							</tr>
						</table>
					<hr>
					<div class="row">
						<div class="col-sm-4">
							<label for="txtSearchOrderNbr">Booking/EDO:</label> 
							<input type="text" name="txtSearchOrderNbr" id="txtSearchOrderNbr" class="form-control" style="text-transform: uppercase;" placeholder="INGRESE RESERVA"/> 
						</div>
						<div class="col-sm-4 optionalView">
										<label for="cbSearchLineOp">Line Operator:</label>
										<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"  
										        data-bind="options:draftViewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
										</select>
						</div>
						<div class="col-sm-4">
							<label for="txtSearchApptNbr">Turno:</label> 
							<input type="text" name="txtSearchApptNbr" id="txtSearchApptNbr" class="form-control" style="text-transform: uppercase;" placeholder="TURNO1, TURNO2, TURNO3"/> 
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary" id="btnSearch">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success" id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
				   <div id="actionButtons">
						<button type="button" class="btn btn-primary" id="btnGenerateDraft">Generar draft</button>
					</div>
					<div class="col-sm-12">
						<br />
					</div>
					<div class="table-responsive">
						<table class="table table-hover datatable" id="tbPumAppointment" width="100%">
						    ${dataTable}
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
<!-- end container -->