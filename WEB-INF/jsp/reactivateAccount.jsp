<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script src="<c:url value="/resources/assets/js/reactivateAccount.js" />" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/apmtba.css" />" />
<section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="index.html" class="text-success">
                                            <span><img src="https://trademarks.justia.com/media/image.php?serial=79104823" alt="" height="50"></span>
                                        </a>
                                    </h2>
                                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                </div>
                                <div class="account-content">
                                    <div class="text-center m-b-20">
                                        <p class="text-muted m-b-0 line-h-24">
                                        	Ingrese su direcci&oacute;n de correo electr&oacute;nico y CUIT/CUIL para comenzar la reactivaci&oacute;n de su cuenta.  
                                        </p>
                                    </div>

                                    <form class="form-horizontal" action="#">

                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="emailaddress">CUIT/CUIL</label>
                                                <input class="form-control" type="text" id="documentNbr" required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="emailaddress">Correo electr&oacute;nico</label>
                                                <input class="form-control" type="email" id="emailaddress" required>
                                            </div>
                                        </div>

                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-12">
                                                <button id="btnReactivate" class="button button--primary" type="button">Comenzar</button>
                                            </div>
                                        </div>

                                    </form>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!-- end card-box-->


                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Volver al <a href="login" class="text-dark m-l-5">Login</a></p>
                                </div>
                            </div>

                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
        </section>