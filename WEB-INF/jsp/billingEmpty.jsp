<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/billingEmpty.js" />" charset="UTF-8"></script>	

<style>
#tbUnitsByBl,#btnGenerate {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n Vac�os</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase">Ingres� n�mero de contenedor</h6>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="entityid" name="entityid" required>
					</span>
					<br>
					<button type="button" id="btnFindEntity" name="btnFindEntity" class="button button--primary">Buscar</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="button button--primary" id="btnGenerate">Generar draft</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-striped datatable" id="tbUnitsByBl">
                    <thead>
                    <tr>
                        <th>Contenedor</th>
                        <th>Tama�o</th>
                        <th>Linea Operadora</th>
                        <th>Coordinaci�n</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.UnitsByBl">
                    <tr>                                       
                        <td><label data-bind="text: unitId"></label></td>
                        <td><label data-bind="text: unitId"></label></td>
                        <td><label data-bind="text: unitId"></label></td>
                        <td><label data-bind="text: unitId"></label></td>
                    </tr>
                    </tbody>
                </table>
			
			</div>
		</div>
		
	</div>
<!-- end container -->