<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewInvoicePaymentModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Pagos asociados</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
                    <div class="col-sm-12">
                    	<h5>Pagos</h5>
                    </div>
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbInvoicePayment" width="100%">
                                	<thead>
										<tr>
										    <th>Gkey</th>
										    <th>Tipo</th>
										    <th>Monto</th>
										    <th>Notas</th>
										    <th>Creado por</th>		
										    <th>Fecha creaci�n</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: supportInvoiceViewModel.paymentTable">
										<tr>
										    <td><label data-bind="text: gkey"></label></td>
										    <td><label data-bind="text: type"></label></td>
										    <td><label data-bind="text: amount"></label></td>
										    <td><label data-bind="text: remark"></label></td>
										    <td><label data-bind="text: creator"></label></td>
										    <td><label data-bind="text: payment_date_str"></label></td>
										</tr>
									</tbody>	                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                    	<h5>E-Payments</h5>
                    </div>
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbInvoiceEpayment" width="100%">
                                	<thead>
										<tr>
										    <th>Gkey</th>
										    <th>Tipo</th>
										    <th>Monto</th>
										    <th>Estado</th>
										    <th>Notas</th>		
										    <th>Fecha creaci�n</th>
										</tr>
									</thead>
									<tbody data-bind="foreach: supportInvoiceViewModel.epaymentTable">
										<tr>
										    <td><label data-bind="text: gkey"></label></td>
										    <td><label data-bind="text: type"></label></td>
										    <td><label data-bind="text: amount"></label></td>
										    <td><label data-bind="text: status"></label></td>
										    <td><label data-bind="text: notes"></label></td>
										    <td><label data-bind="text: created_str"></label></td>
										</tr>
									</tbody>	                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>				
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewInvoicePayment" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	