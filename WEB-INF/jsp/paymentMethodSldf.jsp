<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/facturacion.js" />"
	charset="UTF-8"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Pago mediante Saldo a Favor</h2>
			</div>
		</div>


		<div class="row">
			<div class="col-md-4 offset-md-4">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Configurá el pago con tu saldo a favor</h6>
					<br>
					<table class="table table-dark">
						<tr>
							<td>DRAFT: <strong>${draftgkey}</strong></td>
							<td>MONTO: $<strong>${draftgkey}</strong></td>
						</tr>
					</table>
					<hr>
					<label for="docTo">CUIT/CUIL</label> 
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="docTo" name="docTo" required>
					</span>
					<br>
					<button type="button" class="btn btn-lg btn-block button button--primary" id="btnPay" name="btnPay">Pagar</button>
				</div>
			</div>
		</div>
	</div>
