<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewSrvOrderCalendarModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Coordinar Servicio</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<label for="cbActiveUnit"> Contenedor: </label>										
						<select width="40%" id="cbActiveUnit" class="form-control" name="cbActiveUnit"
								data-bind="options:soViewModel.activeUnitList, optionsText:'unitId', optionsValue:'unitGkey', optionsCaption: '--'">
						</select>
					</div>
					<div class="col-sm-6">
						<label for="cbServiceType"> Servicios: </label>										
						<select width="40%" id="cbServiceType" class="form-control" name="cbServiceType"
								data-bind="options:soViewModel.serviceTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
						</select>
					</div>
				</div>
				<div class="row">
					<br>
				</div>
				<div class="row">
					<div id="calendar"></div>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id=btnCancelViewSrvOrderCalendar data-dismiss="modal">Cancelar</button>
			</div>		
		</div>
	</div>
</div>	