<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewUpdateUnitSealModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Actualizar Precinto Aduana</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<label for="txtUnitSeal2Update"> Precinto Aduana:</label> 
						<input type="text" name="txtUnitSeal2Update" id="txtUnitSeal2Update" class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12" align="justify">
						<br>
						<small><em>
							- Complete con el precinto de aduana alfanum�rico completo. Ejemplo: ABT012345 o DJ12345.<br>
							- En caso de contar con m�s de un precinto, coloque �nicamente el precinto de <strong>PUERTA del contenedor.</strong><br>
							- En caso de contar con <strong> m�s de un precinto en PUERTA</strong> del contenedor, coloque los n�meros subsiguientes de la siguiente forma, ejemplo: ABT012345/012346<br>
							- No coloque espacios ni guiones ni caracteres especiales al completarlo.<br>
							- En caso de haber una diferencia entre el precinto declarado y el f�sico al ingreso, no podr� ingresar el contenedor a la Terminal hasta solucionar la misma.<br>
						</em></small>
						
					</div>					
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSaveUnitSeal">Guardar</button>
				<button type="button" class="btn btn-dark" id="btnCancelUpdateUnitSeal" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>	