<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/facturacion.js" />"
	charset="UTF-8"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<style>
<!--
.btn-sq {
  width: 100px !important;
  height: 100px !important;
  font-size: 10px;
}

.btn-sq-sm {
  width: 50px !important;
  height: 50px !important;
  font-size: 10px;
}

.btn-sq-xs {
  width: 25px !important;
  height: 25px !important;
  padding:2px;
}
.card-box > a.btn.btn-sq-lg.btn-primary {
    margin-left: 10%;
    padding: 20px;
}
.btn-sq-lg {
    width: 200px !important;
    height: 150px !important;
}
-->
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h4 class="header-title m-t-0 m-b-20">Coordinación</h4>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box text-center">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Seleccioná el tipo de operación</h6>

					<a href="${pageContext.servletContext.contextPath}/Appointment/appointmentType/2" class="btn btn-sq-lg button button--primary"> 
						<br>
						<i class="fa fa-arrow-left fa-2x"></i>
						<i class="fa fa-truck fa-3x"></i>
						Importación Full
					</a>
					<a href="${pageContext.servletContext.contextPath}/Appointment/appointmentType/1" class="btn btn-sq-lg button button--primary"> 
						<br>
						<i class="fa fa-truck fa-3x fa-flip-horizontal"></i>
						<i class="fa fa-arrow-right fa-2x"></i> 
						Exportación Full
					</a>
					<a href="${pageContext.servletContext.contextPath}/Appointment/appointmentType/3" class="btn btn-sq-lg button button--primary"> 
						<br>
						<i class="fa fa-truck fa-3x"></i>
						<i class="fa fa-retweet fa-2x" aria-hidden="true"></i>
						Vacíos
					</a>
				</div>
			</div>
		</div>


	</div>
</div>
<!-- end container -->