<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="updateUserCredentialModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Actualizar credenciales</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form class="form-horizontal m-t-10" role="form" method="POST" enctype="multipart/form-data" id="addUpdateCredentialForm">
					<div class="form-group">
						<div class="checkbox checkbox-primary col-sm-9">
                                <input id="ckUpdateEmail" type="checkbox" checked>
                                <label for="ckUpdateEmail">Email Notificación:</label>
                         </div>	
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtCredentialEmail" maxlength="100">
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox checkbox-primary col-sm-9">
                                <input id="ckUpdateComexEmail" type="checkbox">
                                <label for="ckUpdateComexEmail">Email Comex:</label>
                         </div>	
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtComexEmail" maxlength="100" disabled>
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox checkbox-primary col-sm-9">
                                <input id="ckUpdateManagementEmail" type="checkbox">
                                <label for="ckUpdateManagementEmail">Email Facturación:</label>
                         </div>	
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtManagementEmail" maxlength="100" disabled>
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox checkbox-primary col-sm-9">
                                <input id="ckUpdatePassword" type="checkbox" checked>
                                <label for="ckUpdatePassword">
                                    Contraseña
                                </label>
                         </div>							
						<div class="col-sm-9">
							<input type="password" class="form-control" id="txtCredentialPassword" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						 <div class="checkbox checkbox-primary col-sm-9">
                                <input id="ckChangePassword" type="checkbox" checked>
                                <label for="ckChangePassword">
                                    Solicitar cambio de contraseña
                                </label>
                         </div>
					</div>
				</form>
            </div>
            <!-- Modal footer -->
			<div class="modal-footer">
				${credentialModalButtons}
			</div>		
		</div>
	</div>
</div>	