<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<script src="<c:url value="/resources/assets/js/myBalance.js" />" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />

<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>DATOS DE LA CUENTA</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
				
					<div id="myBalance">
						<div class="container">
							<div class="row">
						  		<div class="col-md-12">
						      		<div class="">
						          		<strong>RAZ�N SOCIAL</strong>
						          	</div>
						          	<div class="">
						          		<input type="text" class="form-control" value="${customer.customerName}" disabled>
						          	</div>
						      	</div>
						  	</div>
						  	<div class="row">
						  		<div class="col-md-12">
						      		<div class="">
						          		<strong>DIRECCI�N</strong>
						          	</div>
						          	<div class="">
						          		<input type="text" class="form-control" value="${customer.customerAddress}" disabled>
						          	</div>
						      	</div>
						  	</div>
						  	<div class="row">
						  		<div class="col-md-12">
						      		<div class="">
						          		<strong>CORREO FACTURACI�N</strong>
						          	</div>
						          	<div class="">
						          		<input type="text" class="form-control" value="${customer.customerBillingEmail}" disabled>
						          	</div>
						      	</div>
						  	</div>
						  	<div class="row">
						        <div class="col-md-6">
						              <div class="">
						                 <strong>CUIT</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerTaxId}" disabled>
						              </div>
						      	</div>
						      	<div class="col-md-6">
						              <div class="">
						                  <strong>TEL�FONO</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerTelephone}" disabled>
						              </div>
						      	</div>
						  	</div>
						  	<div class="row">
						        <div class="col-md-6">
						              <div class="">
						                  <strong>CORREO CONTACTO</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerContactEmail}" disabled>
						              </div>
						      	</div>
						      	<div class="col-md-6">
						              <div class="">
						                  <strong>CORREO RESP. COMERC. EXTERIOR</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerComexEmail}" disabled>
						              </div>
						      	</div>
						  	</div>
						  	<div class="row">
						        <div class="col-md-6">
						              <div class="">
						                  <strong>TIPO DE ACTIVIDAD</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerActivityType}" disabled>
						              </div>
						      	</div>
						      	<div class="col-md-6">
						              <div class="">
						                  <strong>UBICACI�N (ZONA DONDE RADICA LA COMPA�IA)</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerCity}" disabled>
						              </div>
						      	</div>
						  	</div>
						  	<div class="row">
						        <div class="col-md-6">
						              <div class="">
						                  <strong>CONDICI�N DE IVA</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerIvaCondition}" disabled>
						              </div>
						      	</div>
						      	<div class="col-md-6">
						              <div class="">
						                  <strong>CONDICI�N DE PAGO</strong>
						              </div>
						              <div class="">
						                  <input type="text" class="form-control" value="${customer.customerCreditStatus != 'OAC' ? 'Contado' : 'Cuenta Corriente'}" disabled>
						              </div>
						      	</div>
						  	</div>
						  	<br>
						  	<div class="row">
						        <div class="col-md-12">
						              <c:if test = "${customer.customerBalance >= 0}">
							              <div class="alert alert-success" role="alert">
							                <div class="col-md-12">
										         <p>SU CUENTA SE ENCUENTRA AL D�A:
										         <fmt:formatNumber value = "${customer.customerBalance}" 
										         	maxIntegerDigits="12" currencySymbol="$" type = "currency" maxFractionDigits = "2"/>  
												 <p>
							                </div>
							              </div>
						              </c:if>
						              <c:if test = "${customer.customerBalance < 0}">
							              <div class="alert alert-danger" role="alert">
							                <div class="col-md-12">
										         <p>USTED TIENE UNA DEUDA PENDIENTE:
										         <fmt:formatNumber value = "${customer.customerBalance}" pattern="$#,##0.00;-$#,##0.00" 
										         	maxIntegerDigits="12" currencySymbol="$" type = "currency" maxFractionDigits = "2"/>  
												 <p>
							                </div>
							              </div>
						              </c:if>
						      	</div>
						  	</div>
						  	 <div class="row p-0">
							  <div class="col-6 col-sm-6 col-md-6 text-right">
							  	<a href="${pageContext.servletContext.contextPath}/Invoice/invoicingSelector" class="button button--primary">Continuar</a>
							  </div>
							  <div class="col-6 col-sm-6 col-md-6 ">
							  	<a href="${pageContext.servletContext.contextPath}/Invoice/invoicing" class="button button--success ">Volver</a>
							  </div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>