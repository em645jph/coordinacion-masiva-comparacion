
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="col-sm-12">

    <div class="wrapper-page">

        <div class="m-t-40 card-box">
            <div class="account-content">
                <div class="text-center m-b-20">
                    <img src="https://www.portofgothenburg.com/globalassets/bilder/kryssning/unkown-vessel.png?preset=image-bank" title="" height="200" class="m-t-10">
                    <h3 class="expired-title mb-4 mt-2">P�gina no encontrada</h3>
                    <p class="text-muted m-t-20 line-h-24">
                    Usted estaba intentando abrir una p�gina que no se encuentra o no est� disponible. <br>
                    Por favor, corrobore la direcci�n a la que desea ingresar.</p>
                </div>

                <div class="row m-t-30">
                    <div class="col-md-8 offset-md-2">
                        <a href="${pageContext.servletContext.contextPath}/" class="btn btn-lg btn-block button button--primary">Volver al inicio</a>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
        <!-- end card-box-->

	</div>
