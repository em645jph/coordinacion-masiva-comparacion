<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/apmtba.css" />" />
<script src="<c:url value="/resources/assets/js/confirmCode.js" />" charset="UTF-8"></script>
<style>
input.form-control {
    margin: 0px 28px;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 18px;
	text-align: center;
}
</style>
<section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="index.html" class="text-success">
                                            <span><img src="https://trademarks.justia.com/media/image.php?serial=79104823" alt="" height="50"></span>
                                        </a>
                                    </h2>
                                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                </div>
                                <div class="account-content">
                                    <div class="text-center m-b-20">
                                        <p class="text-muted m-b-0 line-h-24">
                                        	Ingrese su direcci&oacute;n de correo electr&oacute;nico y codigo de reactivación.  
                                        </p>
                                    </div>

                                    <form class="form-horizontal" action="#" id="formCodes">
                                    	
                                    	<div class="form-group m-b-20">
                                            <div class="col-12" style="padding-left: 8%; padding-right: 8%;">
                                                <label for="emailaddress">Correo electr&oacute;nico de Contacto</label>
                                                <input class="form-control" type="email" name="email" required style="margin-left: 0 !important;">
                                            </div>
                                        </div>
                                    <label for="emailaddress" style="margin-left: 6.5%;">Codigo de reactivación</label>
									<div class="input-group">
											<input type="text" class="form-control small" name="char1" maxlength="1">
											<input type="text" class="form-control small" name="char2" maxlength="1">
											<input type="text" class="form-control small" name="char3" maxlength="1">
											<input type="text" class="form-control small" name="char4" maxlength="1">
											<input type="text" class="form-control small" name="char5" maxlength="1">
											<input type="text" class="form-control small" name="char6" maxlength="1"> 
											
										</div>
	</br>
										<div class="form-group account-btn text-center m-t-10">
	                                        <div class="col-12">
	                                            <button id="btnConfirmCode" class="button button--primary" type="button">Confirmar</button>
	                                        </div>
	                                    </div>
	                                    
									</form>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!-- end card-box-->


                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Volver al <a href="login" class="text-dark m-l-5">Login</a></p>
                                </div>
                            </div>

                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
        </section>