<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/allCustomerReports.js" />" charset="UTF-8"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	
<style>
#tbDrafts,#tableBox {
	display: none;
}
.row_selected{
    background-color: #ff6161 !important
}
body {
    padding-right: 0px !important;
}
</style>
<div class="wrapper">
	<div class="container-fluid">
	
		<div class="row">
			<div class="col-sm-12">
				<h2>Mi estado de cuenta</h2>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-sm-4">
				<h6 class="text-muted font-13 mt-0 text-uppercase">Usuarios a gestionar</h6>
				<div class="input-group mb-4">
					<select id="selectCustomer" class="form-control basic-single-customers" name="selectCustomer"
						data-bind="options:viewModel.users, optionsText:'customerName' , optionsValue:'customerId', optionsCaption: 'Seleccione opci�n...'">
					</select>
					<!-- <select id="selectCustomer" class="form-control basic-single-customers" name="selectCustomer"> -->
					</select>
					<span class="input-group-append">
						<button class="btn btn-icon btn-primary" id="btnFindCustomerInformation">BUSCAR</button>
					</span>
				</div>
			</div>
		</div>
		<br>
		
		<div class="row">
                    <div class="col-sm-12">
						<div class="">
								<div class="row" style="display:none;" id="rowBoxHeaders">
									<div class="col-sm-2" id="divBoxLimit">
		                                <div class="card-box bg-primary" data-bind="foreach: viewModel.customerBalance">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">L�mite otorgado</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" style="font-size:25px;" data-bind="text: '$' + parseMoneyValue(customerCreditLimit)"></span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-3" id="divBoxCashStatus">
		                                <div class="card-box" id="cardBoxCashStatus">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase" id="labelCashStatus"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalCashStatus">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barDelayed" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxVencido">
		                                <div class="card-box bg-danger">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase" id="labelVencido"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalDelayedBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barDelayed" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
									<div class="col-sm-2" id="divBoxNoVencido">
		                                <div class="card-box">
		                                    <h6 class="text-muted font-13 mt-0 text-uppercase" id="labelNoVencido"></h6>
		                                    <h2 class="mb-3 mt-2"><span id="totalBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-success" id="barTotal" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-sm-2" id="divBoxBalance">
		                                <div class="card-box bg-primary" data-bind="foreach: viewModel.customerBalance">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">Saldo</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" style="font-size:25px;" data-bind="text: '$' + parseMoneyValue(customerBalance)"></span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
									
		                            <div class="col-sm-2" id="divBoxSelectedItems" style="display:none;">
		                                <div class="card-box bg-primary">
		                                    <h6 class="text-white font-13 mt-0 text-uppercase">Total seleccionado</h6>
		                                    <h2 class="mb-3 mt-2"><span class="text-white" id="totalSelectedBalanceLbl">$00.00</span></h2>
		                                    <div class="progress progress-sm m-0">
		                                        <div class="progress-bar bg-warning" id="barSelected" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
		                                            <span class="sr-only"></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            
		                            <div class="col-sm-3" id="divBoxCashSuggest" onclick="javascript:window.open('https://apps.apmterminals.com.ar/solicitudesonline')" style="display:none;cursor: pointer">
		                                <div class="card-box bg-warning">
		                                    <h6 class="text-white font-17 mt-0 text-uppercase text-center">Hac� click ac� para presentar el pago de tus deudas o realizar cualquier consulta, ingresar�s a Solicitudes Online.</h6>
		                                </div>
		                            </div>
		                            <div class="col-sm-3" id="divBoxCashSuggest2" style="display:none;cursor: pointer">
		                                <div class="card-box bg-info">
		                                    <a href="tel:08105552768">
		                                    	<h6 class="text-white mt-0 text-uppercase text-center font-20" style="font-size: 20px;">Si necesit�s asistencia telef�nica comunicate con nosotros al </h6>
		                                    	<h6 class="text-white mt-0 text-uppercase text-center font-20" style="font-size: 20px;"> <span><i class="ti-headphone-alt"></i></span><span> 0810-555-APMT(2768)</span> </h6>
		                                    </a>
		                                </div>
		                            </div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
							</div>
                    </div>
                </div>
                
                
			<div class="row">
				<div class="col-md-12">
					<div class="card-box" id="tableBox">
						<table class="table table-striped datatable" id="tbBalance">
		                    <thead>
			                    <tr>
			                    	<th></th>
			                        <th>Fecha de Documento</th>
			                        <th>Documento</th>
			                        <th>Monto</th>
			                        <th>Saldo</th>
			                        <th>Fecha Vencimiento</th>
			                        <th>Mora</th>
			                        <th>Informaci�n</th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.balance">
			                    <tr data-bind="attr:{ 'id': $index }, style: { 'background-color': qtyDelayDays > 0 ? 'lightcoral' : qtyDelayDays < 0 ? 'lightgreen' : 'white' }">                                       
			                        <td></td>
			                        <td data-bind="text: documentDate"></td>
			                        <td data-bind="text: documentNbr"></td>
			                        <td data-bind="text: amount.toFixed(2)"></td>
			                        <td data-bind="text: balance.toFixed(2)"></td>
			                        <td data-bind="text: documentDueDate"></td>
			                        <td data-bind="text: qtyDelayDays"></td>
			                        <td data-bind="click: getValuesAssociatedByDocumentNbr"><i class="mdi mdi-information-outline" style="font-size: 20px;"></i></td>
			                    </tr>
		                    </tbody>
		                </table>
					</div>
				</div>
			</div>
		
	</div>
	
	<table id="tbInformation" class="table table-striped" style="display:none;">
		<thead>
         <tr>
             <th>Valor</th>
         </tr>
        </thead>
        <tbody data-bind="foreach: viewModel.dataInform">
        	<tr>
        		<td data-bind="text: $data"></td>
        	</tr>
        </tbody>
	</table>

	<!-- Modal -->
	<div class="modal fade" id="modalDataAssociate" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titleInformationData">Informaci�n asociada</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      <table class="table table-striped datatable" id="tbDolarData">
                <thead>
                 <tr>
                     <th>USD</th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.usdData">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: parseMoneyValue($data)"></td>
                 </tr>
                </tbody>
          </table>
	      <table class="table table-striped datatable" id="tbEntityData">
                <thead>
                 <tr>
                     <th id="theadEntity"></th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.entity">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: $data"></td>
                 </tr>
                </tbody>
          </table>
          <table class="table table-striped datatable" id="tbValuesData">
                <thead>
                 <tr>
                     <th id="theadValues"></th>
                 </tr>
                </thead>
                <tbody data-bind="foreach: viewModel.values">
                 <tr data-bind="attr:{ 'id': $index }">                                       
                     <td data-bind="text: $data"></td>
                 </tr>
                </tbody>
          </table>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	
<!-- end container -->