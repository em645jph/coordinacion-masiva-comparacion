<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script src="<c:url value="/resources/assets/js/changePassword.js" />" charset="UTF-8"></script>

<section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="index.html" class="text-success">
                                            <span><img src="https://trademarks.justia.com/media/image.php?serial=79104823" alt="" height="50"></span>
                                        </a>
                                    </h2>
                                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                </div>
                                <div class="account-content">
                                    <div class="text-center m-b-20">
                                        <p class="text-muted m-b-0 line-h-24">
                                        	Por su seguridad requerimos que cambie su contrase&ntilde;a.
                                        </p>
                                    </div>

                                    <form class="form-horizontal" action="#">

                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                            	<label for="emailaddress">Contrase&ntilde;a</label>
                                            	<div class="input-group">
                                    				<input type="password" id="newPassword" name="newPassword" class="form-control" required>
    													<span class="input-group-prepend">
                                    						<button id="btnShowNewPassword" type="button"
                                    						 onclick="showPassword(newPassword.id,iconNewPassword.id)" class="btn btn-default">
                                    						 <i id="iconNewPassword" class="mdi mdi-eye-off"></i></button>
                                    					</span>
                                				</div>
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                            	<label for="emailaddress">Confirmar contrase&ntilde;a</label>
                                            	<div class="input-group">
                                    				<input type="password" id="confirmPassword" name="confirmPassword" class="form-control" required>
    													<span class="input-group-prepend">
                                    						<button id="btnShowConfirmPassword" type="button" 
                                    						onclick="showPassword(confirmPassword.id,iconConfirmPassword.id)" class="btn btn-default">
                                    						<i id="iconConfirmPassword" class="mdi mdi-eye-off"></i></button>
                                    					</span>
                                				</div>
                                            </div>
                                        </div>

                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-12">
                                                <button id="btnChangePassword" class="btn btn-lg btn-primary btn-block" type="button">Cambiar contraseņa</button>
                                            </div>
                                        </div>

                                    </form>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!-- end card-box-->

                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
        </section>