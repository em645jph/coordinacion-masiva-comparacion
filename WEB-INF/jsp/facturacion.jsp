<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/facturacion.js" />" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />

<style>
#divRegisterBtn,#inputsBillingUser {
	display: none;
}
</style>

<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Facturaci�n</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<h4 class="text-muted font-14 m-t-0 text-uppercase">Ingres� el CUIT/CUIL a facturar:</h4>
								<span class="input-group-prepend"> <input type="text"
									class="form-control" id="docTo" name="docTo" required maxlength="11">
								</span>
						</div>
					</div>
					<div class="row mt-3">
					  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 offset-md-3 text-center">
					  <button id="btnFindUser" type="button" class="button button--primary">
							Buscar
					   </button>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<%@include file="creditNoteWarningModal.jsp" %>
	</div>
	
	<c:if test = "${msg != null}">
      <script>
      var msg = "${msg}";
      showModalHtml("", msg, "info");</script>
     </c:if>