
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table class="formTable">
	<tr>
		<td class="formTitulo"><spring:message code="field.atencion" /></td>
	</tr>
	<tr>
		<td class="formTd">
			<a class="letra1"><c:out value="${message}" /></a>
		</td>
	</tr>
</table>
