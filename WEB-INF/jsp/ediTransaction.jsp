<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/ediTransaction.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
		        </div> <!-- end row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-sm-3">
									<label for="cbSearchFilterBy">Tipo (*)</label> 
									<select id="cbSearchFilterBy" class="form-control" name="cbSearchFilterBy"
										data-bind="options:ediTransactionViewModel.filterByCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
									</select>
								</div>
								<div class="col-sm-3">
									<label for="cbSearchMsgType">Mensaje</label> 
									<select id="cbSearchMsgType" class="form-control" name="cbSearchMsgType"
										data-bind="options:ediTransactionViewModel.msgTypeCb, optionsText:'description', optionsValue:'key', optionsCaption: '--'">
									</select>
								</div>
								<div class="col-sm-3">
									<label for="txtSearchKeyword">Keyword</label> <input
										type="text" name="txtSearchKeyword" id="txtSearchKeyword" class="form-control" maxlength="100" style="text-transform: uppercase;"/>
								</div>
								<div class="col-sm-3">
									<label for="cbSearchLineOp">L�nea Operadora</label> 
									<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"
										data-bind="options:ediTransactionViewModel.lineOpCb, optionsText:'description', optionsValue:'id', optionsCaption: '--'">
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<br />
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-3">
									<label for="txtSearchVesselName">Nave</label> <input type="text" name="txtSearchVesselName" id="txtSearchVesselName"
										class="form-control" placeholder="Nombre completo o parcial de la nave" style="text-transform: uppercase;"/>
								</div>
							    <div class="col-sm-3">
									<label for="txtSearchVoyage">Viaje</label> <input
										type="text" name="txtSearchVoyage" id="txtSearchVoyage"
										class="form-control" placeholder="N�mero de viaje de entrada o salida" style="text-transform: uppercase;"/>
								</div>
								<div class="col-sm-3">
									<label for="txtSearchFromDate">Fecha Inicio</label> <input
										type="text" name="txtSearchFromDate" id="txtSearchFromDate"
										class="form-control" placeholder="YY-MM-DD" />
								</div>
								<div class="col-sm-3">
									<label for="txtSearchToDate">Fecha Fin</label> <input
										type="text" name="txtSearchToDate" id="txtSearchToDate"
										class="form-control" placeholder="YY-MM-DD" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<br />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" align="center">
									<button type="button" class="button button--primary"
										id="btnSearchEdiTran">Buscar</button>
									&nbsp;&nbsp;
									<button type="button" class="button button--success"
										id="btnCleanSearchEdiTran">Limpiar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
							<div class="table-responsive">
								<table class="table table-hover datatable" id="tbEdiTransaction" width="100%">
								    ${ediTransactionDataTable}
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- end row -->
				<%@include file="viewEdiFileSegmentModal.jsp" %>
            </div> <!-- end container -->