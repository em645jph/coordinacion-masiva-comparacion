<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="<c:url value="/resources/assets/js/userProfile.js" />" charset="UTF-8"></script>
<style>
#inputsBillingUser {
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

	<div class="row">
		<div class="col-sm-12">
			<h4 class="header-title m-t-0 m-b-20"></h4>
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">
			<div class="p-0 text-center">
				<div class="member-card">

					<div class="">
						<h5 class="m-b-5 mt-3">
							Este es tu perfil <strong>${fullname}</strong>
						</h5>
					</div>

				</div>

			</div>
			<!-- end card-box -->

		</div>
		<!-- end col -->
	</div>
	<!-- end row -->

	<div class="m-t-30">
		<ul class="nav nav-tabs tabs-bordered">
			<li class="nav-item">
				<a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active"> Mis datos </a>
			</li>
			<li class="nav-item"><a href="#profile" data-toggle="tab"
				aria-expanded="false" class="nav-link"> Editar datos </a></li>
				<li class="nav-item" id="navLinkedAccounts">
					<a href="#myLinkedAccounts" data-toggle="tab" aria-expanded="true" class="nav-link">
						Cuentas vinculadas Facturaci�n</a>
				</li>
				<li class="nav-item" id="navLinkedAccountsCompost" style="display:none;">
					<a href="#myLinkedAccountsStatus" data-toggle="tab" aria-expanded="true" class="nav-link">
						Cuentas vinculadas Estado de Cuenta</a>
				</li>
			<li class="nav-item">
				<a href="#balance" id="navBalance" data-toggle="tab" aria-expanded="false" class="nav-link"> Saldos </a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="home">
				<!-- Personal-Information -->
				<div class="panel panel-default panel-fill">
					<div class="panel-heading">
						<h3 class="panel-title">B&aacute;sicos</h3>
					</div>
					<div id="contenedor" class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div id="userProfileBody">
									<div class="panel-body">
										<div class="m-b-20">
											<strong>Mi CUIT/CUIL</strong> <br>
											<p class="text-muted">${user.login}</p>
										</div>
										<div class="m-b-20">
											<strong>Raz�n Social / Nombre</strong> <br>
											<p class="text-muted">${user.fullName}</p>
										</div>
										<div class="m-b-20">
											<strong>Tel�fono</strong> <br>
											<p class="text-muted">${user.phoneNumber}</p>
										</div>
										<div class="m-b-20">
											<strong>Correo Electr�nico de Contacto</strong> <br>
											<p class="text-muted">${user.emailAddress}</p>
										</div>
										<div class="m-b-20">
											<strong>Direcci�n F�sica</strong> <br>
											<p class="text-muted">${user.address}(${user.city})</p>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- Personal-Information -->
			</div>

			<div class="tab-pane" id="profile">
				<!-- Personal-Information -->
				<div class="panel panel-default panel-fill">
					<div class="panel-heading">
						<h3 class="panel-title">Editar mis datos</h3>
					</div>
					<div class="panel-body">
						<form role="form">
							<div class="form-group">
								<label for="">Mi CUIT/CUIL</label>: ${user.documentNbr} 
							</div>
							<div class="form-group" id="">
								<label for="editFullname">Razon Social / Nombre</label> 
								<input type="email"	id="editFullname" name="editFullname" class="form-control" value="${user.fullName}">
							</div>
							<div class="form-group" id="">
								<label for="editContactEmail">Correo electr�nico de Contacto</label> 
								<input type="email"	id="editContactEmail" name="editContactEmail" class="form-control" value="${user.emailAddress}">
							</div>
							<div class="form-group" id="">
								<label for="editTelephone">Telefono</label> 
								<input type="text"	value="${user.phoneNumber}" id="editTelephone" name="editTelephone" class="form-control">
							</div>
							<button class="button button--primary" type="button" id="btnUpdate">Actualizar</button>
						</form>

					</div>
				</div>
				<!-- Personal-Information -->
			</div>
				<div class="tab-pane" id="myLinkedAccounts">
					<div class="panel panel-default panel-fill">
						<div class="panel-heading">
							<h3 class="panel-title">Facturaci�n</h3>
						</div>
						<div id="contenedor" class="panel-body">
							<div class="row">
							    <div class="col-md-6">
							        <button type="button" data-toggle="modal" data-target="#addLinkUserModal" data-backdrop="static" data-keyboard="false"
							        			class="button button--success" id="addLinkUser" name="addLinkUser">Delegar CUIT facturaci�n</button>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="card-box">
										<div class="panel-heading">
											<h3 class="panel-title">Usuarios que me autorizaron a facturar en su nombre</h3>
										</div>
										<table class="table table-striped datatable" id="tbParentUsers">
		                                    <thead>
		                                    <tr>
		                                        <th>Id</th>
		                                        <th>Nombre</th>
		                                    </tr>
		                                    </thead>
		                                    <tbody data-bind="foreach: viewModel.parentUsers">
		                                    <tr data-bind="attr:{usrGkey: userGkey}">                                       
		                                        <td><label data-bind="text: documentNbr"></label></td>
		                                        <td><label data-bind="text: fullname"></label></td>
		                                    </tr>
		                                    </tbody>
		                                </table>
	                               	</div>
								</div>
								<div class="col-md-6">
									<div class="card-box">
										<div class="panel-heading">
											<h3 class="panel-title">Usuarios que autoric� a facturar en mi nombre</h3>
										</div>
										<table class="table table-striped datatable" id="tbChildUsers">
		                                    <thead>
		                                    <tr>
		                                        <th>Id</th>
		                                        <th>Nombre</th>
		                                        <th>Eliminar</th>
		                                    </tr>
		                                    </thead>
		                                    <tbody data-bind="foreach: viewModel.childUsers">
		                                    <tr data-bind="attr:{usrGkey: userGkey}">                                       
		                                        <td data-bind="text: documentNbr"></td>
		                                        <td data-bind="text: fullname"></td>
		                                        <td>
		                                        	<button type="button" id="" name="" data-bind="click: function (data) { removeLinkUser(userGkey, 'BILLING') }" class="btn btn-danger">
		                                        		<i class="ti-close"></i>
		                                        	</button>
		                                        </td>
		                                    </tr>
		                                    </tbody>
		                                </table>
	                                </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="myLinkedAccountsStatus">
					<div class="panel panel-default panel-fill">
						<div class="panel-heading">
							<h3 class="panel-title">Estado de Cuenta</h3>
						</div>
						<div id="contenedor" class="panel-body">
							<div class="row">
							    <div class="col-md-6">
							        <button type="button" data-toggle="modal" data-target="#addLinkUserModal" data-backdrop="static" data-keyboard="false"
							        			class="button button--success" id="addLinkUserCompost" name="addLinkUser">Delegar CUIT Est.Cuenta</button>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="card-box">
										<div class="panel-heading">
											<h3 class="panel-title">Usuarios que me autorizaron a administrar su Est.Cuenta</h3>
										</div>
										<table class="table table-striped datatable" id="tbParentUsersCompost">
		                                    <thead>
		                                    <tr>
		                                        <th>Id</th>
		                                        <th>Nombre</th>
		                                    </tr>
		                                    </thead>
		                                    <tbody data-bind="foreach: viewModel.compositionParentUsers">
		                                    <tr data-bind="attr:{usrGkey: userGkey}">                                       
		                                        <td><label data-bind="text: documentNbr"></label></td>
		                                        <td><label data-bind="text: fullname"></label></td>
		                                    </tr>
		                                    </tbody>
		                                </table>
	                               	</div>
								</div>
								<div class="col-md-6">
									<div class="card-box">
										<div class="panel-heading">
											<h3 class="panel-title">Usuarios que autoric� a administrar mi Est.Cuenta</h3>
										</div>
										<table class="table table-striped datatable" id="tbChildUsersCompost">
		                                    <thead>
		                                    <tr>
		                                        <th>Id</th>
		                                        <th>Nombre</th>
		                                        <th>Eliminar</th>
		                                    </tr>
		                                    </thead>
		                                    <tbody data-bind="foreach: viewModel.compositionChildUsers">
		                                    <tr data-bind="attr:{usrGkey: userGkey}">                                       
		                                        <td data-bind="text: documentNbr"></td>
		                                        <td data-bind="text: fullname"></td>
		                                        <td>
		                                        	<button type="button" id="" name="" data-bind="click: function (data) { removeLinkUser(userGkey,'COMPOST') }" class="btn btn-danger">
		                                        		<i class="ti-close"></i>
		                                        	</button>
		                                        </td>
		                                    </tr>
		                                    </tbody>
		                                </table>
	                                </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<div class="tab-pane" id="balance">
				<!-- Personal-Information -->
				<div class="panel panel-default panel-fill">
					<div class="panel-heading">
						<h3 class="panel-title">Saldo</h3>
					</div>
					
					<div class="panel-body">
					
						<table class="table table-striped datatable" id="tbBalances">
		                    <thead>
			                    <tr>
			                        <th>CUIT</th>
			                        <th>Cliente</th>
			                        <th>Tipo de Cuenta</th>
			                        <th>Limite de Credito</th>
			                        <th>Balance</th>
			                    </tr>
		                    </thead>
		                    <tbody data-bind="foreach: viewModel.balances">
			                    <tr data-bind="attr:{ 'id': $index }">                                       
			                        <td data-bind="text: customerTaxId"></td>
			                        <td data-bind="text: customerName"></td>
			                        <td data-bind="text: customerCreditStatus"></td>
			                        <td data-bind="text: customerCreditLimit"></td>
			                        <td data-bind="text: customerBalance"></td>
			                    </tr>
		                    </tbody>
		                </table>

					</div>
					
				</div>
				<!-- Personal-Information -->
			</div>
		</div>
	</div>

</div>
<!-- end container -->


<!-- Modal -->
<div class="modal fade" id="addLinkUserModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
      	<label>Ingrese el CUIT/CUIL para delegar permisos</label>
      	<div class="input-group mb-3">
		  <input type="text" id="cuitLink" name="cuitLink" class="form-control"/>
		  <div class="input-group-append">
		  	<input type="hidden" id="typeOfEntity"/>
		    <button type="button" class="btn btn-primary" id="submitLinkUser" name="submitLinkUser">Enviar</button>
		  </div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
    
  </div>
</div>