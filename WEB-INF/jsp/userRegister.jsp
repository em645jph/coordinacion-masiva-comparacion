<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script src="<c:url value="/resources/assets/js/userRegister.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/fileValidator.js" />" charset="UTF-8"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/apmtba.css" />" />
<style>
#userAdditionalInformation,#divRegisterBtn,#inputsBillingUser {
	display: none;
}
button#btnFindUser {
    padding: 5px 15px !important;
}
</style>
<div class="wrapper">
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">

                    <div class="m-t-40 card-box">
                        <div class="text-center">
                            <h2 class="text-uppercase m-t-0 m-b-30">
                                <a href="index.html" class="text-success">
                                    <span><img src="https://trademarks.justia.com/media/image.php?serial=79104823" alt="" height="50"></span>
                                </a>
                            </h2>
                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                        </div>
                        <div class="account-content">
                        	<div class="text-center m-b-20">
                                <h3 class="text-muted m-b-0 line-h-24">
                                	Nuevo usuario  
                                </h3>
                            </div>
                            
                            <form class="form-horizontal" id="formRegisterUser">

                                <div class="form-group m-b-20">
                                    <div class="col-12">
                                        <label for="documentNbr">CUIT/CUIL</label>
                                        <div class="input-group">
                            				<input type="text" id="documentNbr" name="documentNbr" class="form-control" required 
                            				placeholder="Ingrese su n�mero de CUIT o CUIL sin GUIONES" maxlength="11">
												<span class="input-group-prepend">
                            						<button id="btnFindUser" type="button" class="button button--primary"><i class="fa fa-search"></i></button>
                            					</span>
                        				</div>
                                    </div>
                                </div>
							     <div id="userAdditionalInformation">
		                                <div class="form-group m-b-20">
		                                    <div class="col-12">
		                                        <label for="fullname">Razon Social/Nombre</label>
		                                        <input class="form-control" type="text" id="fullname" name="fullname" required="" placeholder="...">
		                                    </div>
		                                </div>
		
		                                <div class="form-group m-b-20">
		                                    <div class="col-12">
		                                        <label for="address">Ubicaci&oacute;n f&iacute;sica</label>
		                                        <input class="form-control" type="text" required id="address" name="address" placeholder="...">
		                                    </div>
		                                </div>
		                                
		                                <div class="form-group m-b-20">
		                                    <div class="col-12">
		                                        <label for="emailAddress">Correo electr&oacute;nico de Contacto</label>
		                                        <input class="form-control" type="email" required id="emailAddress" name="emailAddress" placeholder="...">
		                                    </div>
		                                </div>
		                                
		                                <div class="form-group m-b-20">
		                                    <div class="col-12">
		                                        <label for="telephone">Tel&eacute;fono</label>
		                                        <input class="form-control" type="text" required id="telephone" name="telephone" placeholder="...">
		                                    </div>
		                                </div>
		                                
		                                <div class="form-group m-b-30" id="divToBeBilling">
                                            <div class="col-12">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="beBillingUser" name="beBillingUser" type="checkbox">
                                                    <label for="beBillingUser">
                                                        Facturar con CUIT propio <a id="userRegWhatThis" href="#">�Qu&eacute; es esto?</a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
		
								 </div>
								 
								 <div id="inputsBillingUser">
                                        
                                       	<div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="commercialEmailAddress">Correo Responsable ComEx.</label>
                                                <input class="form-control" type="email" required name="emailComex" id="emailComex" placeholder="...">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="emailManagement">Correo de Gesti�n de Cuenta</label>
                                                <input class="form-control" type="email" required name="emailManagement" id="emailManagement" placeholder="...">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="city">Ciudad</label>
                                                <input class="form-control" type="text" required name="city" id="city" placeholder="...">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="businessType">Tipo de Actividad</label>
                                                <input class="form-control" type="text" required name="businessType" id="businessType" placeholder="...">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-20">
                                            <div class="col-12">
                                                <label for="membCardFile">Carta Membretada</label>
                                                <input class="form-control" head="Carta Membretada" type="file" required id="membCardFile" name="membCardFile" placeholder="...">
                                                	<small>Para descargar un ejemplo de Carta Membretada, haga clic <a style="color:#fd7e14;" href="https://www.apmterminals.com/en/buenos-aires/practical-information/-/media/americas/Buenos-Aires/practical-information/carta-membretada-puerto-digital" target="_blank" download="">aqu�</a></small>
                                                <label for="constanciaFile">Constancia CUIT/CUIL</label>
                                                <input class="form-control" type="file" head="Constancia CUIT/CUIL" required id="constanciaFile" name="constanciaFile" placeholder="...">
                                                <label for="iibbFile">Ingresos Brutos</label>
                                                <input class="form-control" type="file" head="Ingresos Brutos" required id="iibbFile" name="iibbFile" placeholder="...">
                                            </div>
                                        </div>
                                       
                                       
                                  </div>
                                
								<div class="form-group account-btn text-center m-t-10" id="divRegisterBtn">
									<div class="col-12">
										<button id="btnRegister" class="btn btn-lg btn-primary btn-block" type="button">Registrar</button>
									</div>
								</div>
								
                            </form>

                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <!-- end card-box-->
                    <div class="row m-t-50">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Ya tienes cuenta?  <a href="login" class="text-dark m-l-5">Inicia sesi&oacute;n</a></p>
                        </div>
                    </div>
                </div>
                <!-- end wrapper -->
            </div>
        </div>
    </div>
</section>