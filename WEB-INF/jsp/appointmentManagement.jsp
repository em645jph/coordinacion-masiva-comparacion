<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/appointmentManagement.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="cbSearchApptSubType"> Tipo de operación (*)</label>										
										<select width="40%" id="cbSearchApptSubType" class="form-control" name="cbSearchApptSubType"
											data-bind="options:apptViewModel.apptSubTypeCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchKeyParameter">Contenedor (*)</label>
										<input type="text" name="txtSearchKeyParameter" id="txtSearchKeyParameter" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4 optionalView">
										<label for="cbSearchLineOp">Line Op</label>
										<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"  
										        data-bind="options:apptViewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbUnitAppointment" width="100%">
                                	${dataTable}                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="viewApptCalendarModal.jsp" %>
                <%@include file="selectApptTimeModal.jsp" %>
                <%@include file="viewApptUnitSealModal.jsp" %>
            </div> <!-- end container -->