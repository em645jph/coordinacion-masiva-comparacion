<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/supportUnit.js" />"></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-2">
										<label for="txtSearchContainerNbr">Contenedor(es):</label>
										<input type="text" name="txtSearchContainerNbr" id="txtSearchContainerNbr" class="form-control" maxlength="100" style="text-transform: uppercase;" placeholder="UNIT1, UNIT2"/>
									</div>								
									<div class="col-sm-2">
										<label for="cbSearchCategory">Categor�a:</label>										
										<select width="40%" id="cbSearchCategory" class="form-control" name="cbSearchCategory"
											data-bind="options:supportUnitViewModel.categoryCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-2">
										<label for="cbSearchTransitState">Estado:</label>										
										<select width="40%" id="cbSearchTransitState" class="form-control" name="cbSearchTransitState"
											data-bind="options:supportUnitViewModel.transitStateCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-2">
										<label for="cbSearchLineOp">L�nea Op:</label>
										<select id="cbSearchLineOp" class="form-control" name="cbSearchLineOp"  
										        data-bind="options:supportUnitViewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchIbVisit">I/B Visit:</label>
										<input type="text" name="txtSearchIbVisit" id="txtSearchIbVisit" class="form-control" maxlength="20" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchObVisit">O/B Visit:</label>
										<input type="text" name="txtSearchObVisit" id="txtSearchObVisit" class="form-control" maxlength="20" style="text-transform: uppercase;"/>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-2">
										<label for="txtSearchDocumentNbr">Documento (BL/Booking):</label>
										<input type="text" name="txtSearchDocumentNbr" id="txtSearchDocumentNbr" class="form-control" maxlength="50" style="text-transform: uppercase;"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchApptNbr">Turno:</label>
										<input type="number" name="txtSearchApptNbr" id="txtSearchApptNbr" class="form-control" min="1" max="999999"/>
									</div>									
									<div class="col-sm-2">
										<label for="cbSearchReefer">Reefer:</label>										
										<select width="40%" id="cbSearchReefer" class="form-control" name="cbSearchReefer"
											data-bind="options:supportUnitViewModel.yesNoCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-2">
										<label for="cbSearchHazardous">Hazardous:</label>										
										<select width="40%" id="cbSearchHazardous" class="form-control" name="cbSearchHazardous"
											data-bind="options:supportUnitViewModel.yesNoCb, optionsText:'description' , optionsValue:'key', optionsCaption: '--'">
										</select>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchEventType">Evento(s):</label>
										<input type="text" name="txtSearchEventType" id="txtSearchEventType" class="form-control" maxlength="200" style="text-transform: uppercase;" placeholder="EVENTO1, EVENTO2"/>
									</div>
									<div class="col-sm-2">
										<label for="txtSearchHoldId">Hold(s):</label>
										<input type="text" name="txtSearchHoldId" id="txtSearchHoldId" class="form-control" maxlength="200" style="text-transform: uppercase;" placeholder="HOLD1, HOLD2"/>
									</div>
								</div>			
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row" id="divTable">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div id="actionButtons">
								<button type="button" class="btn btn-primary" data-toggle="modal" id="btnAddPumAppt">Coordinar Retiro Vac�o</button>
							</div>
							<div id="columnButtons">
								Ver/ocultar: 
								<a class="toggle-vis" style="color:#17a2b8" data-column="5">Consolidado</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="7">Booking</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="8">BL</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="9">LDE</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="10">Vencimiento LDE</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="11">Devoluci�n Vac�o</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="16">POL</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="17">POD</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="18">Seal 1</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="19">Seal 2</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="20">Seal 3</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="21">Seal 4</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="24">Time In</a>  -  
								<a class="toggle-vis" style="color:#17a2b8" data-column="25">Time Out</a>
							</div>
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbUnit" width="100%">
                                		<thead>
											<tr>
											    <th>Unit Nbr</th>
											    <th>Category</th>
											    <th>Iso Type</th>
											    <th>Freight Kind</th>
											    <th>Transit State</th>
											    <th>Consolidado</th>
											    <th>Line Op</th>
											    <th>Booking Nbr</th>
											    <th>BL Nbr</th>
											    <th>LDE</th>
											    <th>Vencimiento LDE</th>
											    <th>Devoluci�n Vac�o</th>
											    <th>Appt Nbr</th>
											    <th>Appt Time</th>
											    <th>I/B Visit</th>
											    <th>O/B Visit</th>
											    <th>POL</th>
											    <th>POD</th>
											    <th>Seal 1</th>
											    <th>Seal 2</th>
											    <th>Seal 3</th>
											    <th>Seal 4</th>											    
											    <th>Gross Weight</th>											    
											    <th>VGM Weight</th>	
											    <th>Time In</th>
											    <th>Time Out</th>										    
											    <th>Reefer</th>
											    <th>Hazardous</th>
											    <th>Coordinar</th>
											    <th>Holds</th>
											    <th>CUE</th>
											    <th>SO</th>
											</tr>
										</thead>
										<tbody data-bind="foreach: supportUnitViewModel.unitTable">
											<tr>
											    <td><label data-bind="text: unitId"></label></td>
											    <td><label data-bind="text: unitCategory"></label></td>
											    <td><label data-bind="text: unitIsoTypeId"></label></td>
											    <td><label data-bind="text: unitFreightKind"></label></td>
											    <td><label data-bind="text: ufvTransitStateDesc"></label></td>
											    <td><label data-bind="text: stuffedStr"></label></td>
											    <td><label data-bind="text: unitLineOpId"></label></td>
											    <td><label data-bind="text: bookingNbr"></label></td>
											    <td><label data-bind="text: blNbr"></label></td>
											    <td><label data-bind="text: unitFreeDebtStatus"></label></td>
											    <td><label data-bind="text: unitFreeDebtExpireDate"></label></td>
											    <td><label data-bind="text: emptyReturnDateStr"></label></td>
											    <td><label data-bind="text: apptNbr"></label></td>
											    <td><label data-bind="text: apptRequestedDateStr"></label></td>
											    <td><label data-bind="text: ibVisitId"></label></td>
											    <td><label data-bind="text: obVisitId"></label></td>
											    <td><label data-bind="text: unitPod"></label></td>
											    <td><label data-bind="text: unitPol"></label></td>
											    <td><label data-bind="text: unitSealNbr1"></label></td>
											    <td><label data-bind="text: unitSealNbr2"></label></td>
											    <td><label data-bind="text: unitSealNbr3"></label></td>
											    <td><label data-bind="text: unitSealNbr4"></label></td>
											    <td><label data-bind="text: unitGrossWeight"></label></td>
											    <td><label data-bind="text: unitVgmWeight"></label></td>
											    <td><label data-bind="text: timeInStr"></label></td>
											    <td><label data-bind="text: timeOutStr"></label></td>
											    <!-- ko {if: unitRequiresPower && unitRequiredTempC == null} -->
											    <td><label style="color:blue"> <i class="ti-plug"></i></label></td>
											    <!-- /ko -->
											    <!-- ko {if: unitRequiresPower && unitRequiredTempC != null} -->
											    <td><label style="color:blue" data-bind="text: unitRequiredTempC"></label></td>
											    <!-- /ko -->
											    <!-- ko {if: (!unitRequiresPower)} -->
											    <td><label></label></td>
											    <!-- /ko -->
											    <!-- ko {if: (unitIsHazardous)} -->
											    <td><button class="btn btn-icon btn-warning btn-sm" data-bind="click: $parent.viewHazardous, enable: unitIsHazardous"> <i class="ti-alert"></i> </button></td>
											    <!-- /ko -->
											    <!-- ko {if: (!unitIsHazardous)} -->
											    <td><label></label></td>
											    <!-- /ko -->
											    <!-- ko {if: ((apptNbr == null && apptRequestedDate == null) || importApptNotExpired)} -->
											    <td><button class="btn btn-icon btn-primary btn-sm" data-bind="click: $parent.coordinateUnit, enable: allowToCreateApptCus"> <i class="ti-calendar"></i> </button></td>
											    <!-- /ko -->
											    <!-- ko {ifnot: ((apptNbr == null && apptRequestedDate == null) || importApptNotExpired)} -->
											    <td><button class="btn btn-icon btn-danger btn-sm" data-bind="click: $parent.cancelAppt, enable: allowToCancelAppt"> <i class="ti-close"></i> </button></td>
											    <!-- /ko -->
											    <!-- ko {if: (unitReleasedHoldCount < unitHoldCount)} -->
											    <td><a href="#" style="color:#F23C3C; font-weight:bold;" data-bind="text: unitHoldCount, click: $parent.viewUnitHold"></a></td>
											    <!-- /ko -->
											    <!-- ko {ifnot: (unitReleasedHoldCount < unitHoldCount)} -->
											    <td><a href="#" style="color:#4fc55b;" data-bind="text: unitHoldCount, click: $parent.viewUnitHold"></a></td>
											    <!-- /ko -->
											    <td><a href="#" style="color:#fd7e14;" data-bind="text: unitCueCount, click: $parent.viewUnitCue"></a></td>
											    <td><a href="#" style="color:#fd7e14;" data-bind="text: unitServiceCount, click: $parent.viewUnitServices"></a></td>
											</tr>
										</tbody>                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="viewUnitCalendarCusModal.jsp" %>
                <%@include file="selectCoordinationTimeCusModal.jsp" %>
                <%@include file="viewUnitSrvOrderCusModal.jsp" %>
            </div> <!-- end container -->