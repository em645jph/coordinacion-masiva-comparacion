<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/icons.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/jquery.datetimepicker.min.css" />" />
<script src="<c:url value="/resources/assets/js/preadviseManagement.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/modalsFunctions.js" />" charset="UTF-8"></script>
<style>
#tbUnitsByBkg,#modalToggle,#tableBookingDetails {
	display: none;
}
input {
	text-transform: uppercase; 
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Preaviso Exportación</h2>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Ingresá
						número de Booking y selecciona Línea marítima</h6>
					<div class="row">
						<div class="col-sm-6">
							<label for="bkgnbr">Booking (*)</label> <input type="text"
								class="form-control" id="bkgnbr" name="bkgnbr" required>
						</div>
						<div class="col-sm-6">
							<label for="selectLineOp">Linea Operadora (*)</label> <select
								id="selectLineOp" class="form-control" name="selectLineOp"
								data-bind="options:viewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: 'Seleccione opción...'">
							</select>
						</div>
					</div>
					<div class="row optionalView">
						<div class="col-sm-12">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" align="center">
							<button type="button" class="button button--primary"
								id="btnFindEntity">Buscar</button>
							&nbsp;&nbsp;
							<button type="button" class="button button--success"
								id="btnClean">Limpiar</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row" id="tableBookingDetails">
			<div class="col-md-10 offset-md-1">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Detalles del Booking</h6>
					<table class="table datatable" id="dataTableBkg">
						  	<tr>
						    	<th>Buque</th>
								<th>Viaje</th>
								<th>Fecha de Salida</th>
								<th>Linea</th>
								<th>POD</th>
								<th>POL</th>
								<th>Refrigerado</th>
								<th>IMO</th>
								<th>Shipper</th>
						  	</tr>
						 <tbody id="tbodyBkg" data-bind="foreach: viewModel.UnitsByBkg">
						  	<tr>
						    	<td data-bind="text: vessel.vesselName"></td>
								<td data-bind="text: vessel.vesselId"></td>
								<td data-bind="text: atd"></td>
								<td data-bind="text: lineOp"></td>
								<td data-bind="text: pod"></td>
								<td data-bind="text: pol"></td>
								<!-- ko {if: reefer} -->
								<td>
									<i class="ti-plug" style="color: blue;"></i>
								</td>
								<!-- /ko -->
								<!-- ko {ifnot: reefer} -->
								<td>-</td>
								<!-- /ko -->
								<!-- ko {if: hazardous} -->
								<td>
									<i class="ti-alert" style="color: red;"></i>
								</td>
								<!-- /ko -->
								<!-- ko {ifnot: hazardous} -->
								<td>-</td>
								<!-- /ko -->
								<td data-bind="text: shipper"></td>
						  	</tr>
						</tbody>
					</table>
			</div>
			<div class="col-md-10 offset-md-1">
					<h6 class="text-muted font-13 m-t-0 text-uppercase text-center">Equipamiento</h6>
					<table class="table datatable" id="dataTableEq">
						  	<tr>
		                        <th>Tamaño</th>
		                        <th>Cantidad</th>
		                        <th>Recibidos</th>
		                        <th>Mercadería</th>
		                        <th>Temperatura</th>
		                        <th>Ventilación</th>
		                        <th>Humedad</th>
		                        <th>CO2</th>
		                        <th>O2</th>
		                    </tr>
		               	<tbody id="tbodyEqs" data-bind="foreach: viewModel.Items">
						  	<tr>
						    	<td data-bind="text: size"></td>
								<td data-bind="text: quantity"></td>
								<td data-bind="text: tallyIn"></td>
								<td data-bind="text: commodity"></td>
								<td data-bind="text: temperature"></td>
								<td data-bind="text: ventilation"></td>
								<td data-bind="text: humidity"></td>
								<td data-bind="text: co2"></td>
								<td data-bind="text: o2"></td>
						  	</tr>
						</tbody>
					</table>
				</div>
			</div>
		
    	
    	<div class="row">
   			<div class="col-md-4">
				<button class="button button--success" id="modalToggle">Agregar contenedor</button>
		    </div>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-responsive datatable" id="tbUnitsByBkg">
                    <thead>
                    <tr>
                        <th>Desvincular</th>
                        <th>Contenedor</th>
                        <th>Tamaño</th>
                        <th>Ubicación</th>
                        <th>Línea</th>
                        <th>Peso Carga</th>
                        <th>Precinto Linea</th>
                        <th>Fuera de Medida</th>
                        <th>VGM</th>
                        <th>Editar</th>
                        <th>Permisos</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: viewModel.Units">
                    	<tr>
                    		<td>
                    		<!-- ko {if: location.location == 'S20_INBOUND' && !haveDraft} -->
                    			<i data-bind="attr: { 'id': $index, 'uid': unitId, 'ugkey': gkey, 'bknbr': bkNrb }" onclick="deletePreadviseUnit(this.id)" class="ti-close" 
                    				style="background-color: red; color: white;"></i>
                    		<!-- /ko -->
                    		</td>
                    		<td data-bind="text: unitId, attr: { 'ugkey': gkey }" ></td>
                    		<td data-bind="text: size"></td>
                    		<td data-bind="text: location.description"></td>
                    		<td data-bind="text: lineOp"></td>
                    		<td data-bind="text: weight"></td>
                    		<td data-bind="text: seal2"></td>
                    		<td data-bind="text: oog ? 'SI' : 'NO'"></td>
                    		<td data-bind="text: vgmWeight == 0 ? 'NO' : vgmWeight "></td>
                    		<td>
                    		<!-- ko {if: location.location == 'S20_INBOUND' && !haveDraft} -->
                    			<i data-bind="attr: { 'id': $index , 'bkgkey' : bkGkey , 'bknbr': bkNrb, 'ugkey': gkey, 'unitId': unitId }" 
                    				class="ti-pencil-alt" style="color: blue;" onclick="javascript:editPreadviseUnit(this)"></i>
                    		<!-- /ko -->
                    		</td>
                    		<td><a data-bind="attr: { 'id': $index, 'ugkey': gkey }" onclick="showPermissionByUnit(this.id)" style="color: red;">VER</a></td>
                    	</tr>
                    </tbody>
                </table>
			
			</div>
		</div>
	</div>	
<!-- end container -->

<!-- container wizard -->

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" 
	aria-labelledby="myLargeModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h2>Preavisar contenedor</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#stepUnit" role="tab" id="tabStepUnit">Contenedor</a>
          <li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#stepPermission" role="tab">Permisos</a>
          <li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#stepOog" role="tab">Sobremedidas</a>
          <li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#stepSeals" role="tab">Precintos</a>
          <li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#stepConfirm" role="tab">Confirmar</a>
          <li>
        </ul>
        
        <div class="tab-content mt-2" id="formPreadviseUnit">
          <div class="tab-pane fade show active" id="stepUnit" role="tabpanel">
            <h4 id="titleForUnitId"></h4>
            <div class="col-md-12">
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitNbr">Número</label>
		              <input type="text" class="form-control required" id='formUnitNbr' name="formUnitNbr" ugkey="0"></input>
			      	</div>
			      	<div class="col-md-6" data-bind="foreach: viewModel.UnitsByBkg"> 
		              <label for="formUnitLine">Línea</label>
		              <input data-bind="attr: {'value': lineOp}" type="text" 
		              			class="form-control required" id='formUnitLine' name="formUnitLine" disabled></input>
			      	</div>
				</div>
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitSize">Tamaño</label>
		              <select onchange="updateDataBySize()" id="formUnitSize" name="formUnitSize"
		               class="form-control required" data-bind="foreach: viewModel.Items">
		              	<option data-bind="attr: { 'value': size, 'bkgkey': bkGkey, 'temp': temperature, 
		              									'tare': tare, 'itemgkey': itemGkey, 'lineOp': lineOp }, text: size"></option>
		              </select>
			      	</div>
			      	<div class="col-md-6">
		              <label for="formUnitTemp">Temperatura</label>
		              <input type="text" class="form-control required" id='formUnitTemp' name="formUnitTemp" disabled></input>
			      	</div>
				</div>
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitTare">Tara (Kgs)</label>
		              <input type="text" class="form-control required" id='formUnitTare' name="formUnitTare"></input>
			      	</div>
			      	<div class="col-md-6">
		              <label for="formUnitWeight">Peso Mercadería (Kgs)</label>
		              <input type="text" class="form-control required" id='formUnitWeight' name="formUnitWeight"></input>
			      	</div>
				</div>
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitTare">Realiza VGM</label>
		              <div>
                        <div class="radio radio-info form-check-inline">
                                <input type="radio" id="inlineRadioTerminal" value="IN_TERMINAL" name="radioInline" checked="">
                                <label for="inlineRadio1"> Terminal </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                                <input type="radio" id="inlineRadioExterno" value="EXTERNAL" name="radioInline" checked="">
                                <label for="inlineRadio2"> Externo </label>
                        </div>
                      </div>
			      	</div>
			      	<div class="col-md-6">
		              <label for="formUnitVgmEmail">Email Notificación</label>
		              <input type="email" class="form-control required" id='formUnitVgmEmail' name="formUnitVgmEmail"></input>
			      	</div>
				</div>
			</div>
			<hr>
            <button class="btn btn-block button button--primary" id="btnStepUnit">Continuar</button>
          </div>
          <div class="tab-pane fade" id="stepPermission" role="tabpanel">
            <h4>Permiso de Embarque</h4>
            <div class="col-md-12">
				<div class="row">
			  		<div class="input-group mb-3">
					  <input type="text" class="form-control" id="inputPerms" placeholder="Ingrese un permiso de embarque..." aria-label="Ingrese un permiso de embarque..." 
					  	aria-describedby="basic-addon2">
					  <div class="input-group-append">
					    <button class="btn btn-block button button--success" type="button" id="btnAddPerms">AGREGAR</button>
					  </div>
					</div>
				</div>
				<div class="row">
			  		<div class="col-md-12">
			  			<table class="table table-hover" id="tablePermissions">
			  				<thead class="thead-dark">
						  		<tr>
								    <th>Permiso</th>
								    <th>Eliminar</th>
						  		</tr>
						  	</thead>
						  	<tbody id="tbodyPermissions">
						  	</tbody>
						</table>
			      	</div>
				</div>
			</div>
			<hr>
            <button class="btn btn-block button button--primary" id="btnStepPermission">Continuar</button>
          </div>
          <div class="tab-pane fade" id="stepOog" role="tabpanel">
            <h4>Fuera de Medida</h4>
            <div class="col-md-12">
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitOogTop">Arriba (cm)</label>
		              <input type="text" class="form-control" id='formUnitOogTop' name="formUnitOogTop"></input>
			      	</div>
			      	<div class="col-md-6">
		              <label for="formUnitOogFront">Frente (cm)</label>
		              <input type="text" class="form-control" id='formUnitOogFront' name="formUnitOogFront"></input>
			      	</div>
				</div>
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitOogBack">Atrás (cm)</label>
		              <input type="text" class="form-control" id='formUnitOogBack' name="formUnitOogBack"></input>
			      	</div>
			      	<div class="col-md-6">
		              <label for="formUnitOogLeft">Izquierda (cm)</label>
		              <input type="text" class="form-control" id='formUnitOogLeft' name="formUnitOogLeft"></input>
			      	</div>
				</div>
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitOogRight">Derecha (cm)</label>
		              <input type="text" class="form-control" id='formUnitOogRight' name="formUnitOogRight"></input>
			      	</div>
				</div>
			</div>
			<hr>
            <button class="btn btn-block button button--primary" id="btnStepOog">Continuar</button>
          </div>
          <div class="tab-pane fade" id="stepSeals" role="tabpanel">
            <h4>Precintos</h4>
            <div class="col-md-12">
				<div class="row">
			  		<div class="col-md-6">
		              <label for="formUnitSealCustoms">Precinto Aduana</label>
		              <input type="text" class="form-control required" id='formUnitSealCustoms' name="formUnitSealCustoms" required="required" maxlength="15"></input>
			      	</div>
				</div>
			</div>
			<hr>
            <button class="btn btn-block button button--primary" id="btnStepSeals">Continuar</button>
          </div>
          <div class="tab-pane fade" id="stepConfirm" role="tabpanel">
            <button class="btn btn-block button button--success" id="btnStepConfirm">Preavisar contenedor</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->

<div class="modal fade" id="modalPermission" tabindex="-1" role="dialog" 
	aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<div class="row">
	  		<div class="col-md-12">
	  			<table class="table table-hover">
	  				<thead class="thead-dark">
				  		<tr>
						    <th>Permiso</th>
				  		</tr>
				  	</thead>
				 	<tbody id="permissionsBody">
					</tbody>
				</table>
	      	</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block button button--primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
