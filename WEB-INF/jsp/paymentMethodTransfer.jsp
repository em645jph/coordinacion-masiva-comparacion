<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/paymentTransfer.js" />" charset="UTF-8"></script>
<script src="<c:url value="/resources/assets/js/jquery.maskMoney.js" />" type="text/javascript"></script>
<style>
#btnPay{
	display: none;
}
</style>
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Pago mediante Transferencia Bancaria</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-4 offset-md-4">
				<div class="card-box">
					<h4 class="text-uppercase text-center">Ingres� los datos para generar la transferencia</h4>
					<br>
						<table class="table table-dark">
							<tr>
								<td>DRAFT: <strong id="invNbr">${invoice.invNbr}</strong></td>
								<td>MONTO: $<strong id="invAmount"><fmt:formatNumber value = "${invoice.invOwed}" type = "number" maxFractionDigits = "2"/></strong></td>
							</tr>
						</table>
						<c:choose>
					    	<c:when test = "${interbankingIsAvailable}">
					    		<script>
									window.location.href = url_application + "/Invoices/drafts";
								</script>
					    	</c:when>
					    </c:choose>
					<hr>
					<div id="transfers">
						<table class="table-light" id="transfertable">
					        <tbody id="transferBody">
								<tr>
								    <th>Bnco.Destino</th>
									<th>Nro.Transf.</th>
									<th>Monto</th>
									<th>Fecha</th>
									<th></th>
						        </tr>
					    	</tbody>
						</table>
						<table class="table-light">
							<tfoot>
						    	<tr>
						    		<td><hr></td>
						    		<td><hr></td>
						    		<td><hr></td>
									<td><hr></td>
									<td><hr></td>
								</tr>
								<tr>
									<td></td>
									<td>Total</td>
									<td id="footTotal">$00.00</td>
									</tr>
							</tfoot>
						</table>
						</div>
					<hr>
					<label for="docTo">Banco de Destino</label>
				    <div class="input-group-prepend">
					    <select class="form-control" id="selectBank" name="selectBank" required>
					    	<option value="-1">Seleccione una opci�n...</option>
					    	<option value="ICBC">ICBC</option>
					    	<option value="HSBC">HSBC</option>
						</select>
				    </div>
				    <label for="aliasTo">N�mero de Transferencia</label>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="referenceNbr" name="referenceNbr" maxlength="50" required>
					</span>
					<label for="aliasTo">Monto de Transferencia (EN PESOS)</label>
					<span class="input-group-prepend"> 
						<input type="text" class="form-control" id="amount" name="amount" required>
					</span>
					<small>Recuerde que no puede ser menor al valor pendiente de su factura.</small>
					<br>
					<label for="cbuTo">Fecha de transferencia</label>
					<br>
					<table>
						<tr>
							<td>
								<input type="text" id="datetransfer" name="datetransfer" placeholder="yy-mm-dd" autocomplete="off" 
									class="form-control datepicker">
							</td>
						</tr>
					</table>
					<br>
					<button type="button" class="btn btn-lg btn-block button button--success" id="btnAddTransfer" name="btnAddTransfer">Agregar</button>
					<hr>
					<button type="button" class="btn btn-lg btn-block button button--primary" id="btnPay" name="btnPay">Pagar</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1 offset-md-0">
			<br>
				<button type="button" class="btn btn-lg btn-block button button--success" id="btnBack" name="btnBack">Volver</button>
			</div>
		</div>
	</div>
