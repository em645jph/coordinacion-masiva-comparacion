<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="sealNbr2Modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Importante - Precinto Aduanas</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-sm-12" style="font-size: 0.9em;>
				<p align="justify">
					Estimado Cliente,<br><br>
					<ul style="list-style-type: circle; line-height:200%;" align="justify">
					<li>Complete con el precinto de aduana alfanum�rico completo. Ejemplo: ABT012345 o DJ12345.</li>
					<li>En caso de contar con m�s de un precinto, coloque �nicamente el precinto de <strong>PUERTA del contenedor.</strong></li>
					<li>En caso de contar con <strong> m�s de un precinto en PUERTA</strong> del contenedor, coloque los n�meros subsiguientes de la siguiente forma, ejemplo: ABT012345/012346</li>
					<li>No coloque espacios ni guiones ni caracteres especiales al completarlo.</li>
					<li>En caso de haber una diferencia entre el precinto declarado y el f�sico al ingreso, no podr� ingresar el contenedor a la Terminal hasta solucionar la misma.</li>
					</ul>
					<br>
					Gracias!
				</p>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelSealNbr2Modal" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>	