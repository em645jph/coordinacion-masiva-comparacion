<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="selectApptOrderItemModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Seleccionar Tipo</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<label for="txtContainerIsoTypeId" class="col-sm-9 col-form-label">Tipo de contenedor (*):</label>
						<div class="col-sm-9">
							<select id="cbApptOrderItem" class="form-control" name="cbApptOrderItem"  
								data-bind="options:coordinationViewModel.apptOrderItemCb, optionsText:'description' , optionsValue:'itemGkey', optionsCaption: '--'">
							</select>
						</div>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSelectApptOrderItem">Continuar</button>
				<button type="button" class="btn btn-dark" id="btnCancelSelectApptOrderItem" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	