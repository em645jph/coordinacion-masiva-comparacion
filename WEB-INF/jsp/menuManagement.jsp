<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script charset="UTF-8" src="<c:url value="/resources/assets/js/menuManagement.js" />"></script>
<script> var canAddItem =<c:out value="${canAddItem}"/></script>
<script> var canEditItem =<c:out value="${canEditItem}"/></script>
<script> var canDeleteItem =<c:out value="${canDeleteItem}"/></script>
<script> var canRecoverItem =<c:out value="${canRecoverItem}"/></script>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title m-t-0 m-b-20"></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
						<div class="card-box">
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchMenuId"> Id</label>
										<input style="text-transform: uppercase;" type="text" name="txtSearchMenuId" id="txtSearchMenuId" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchMenuLabel"> Nombre</label>
										<input type="text" name="txtSearchMenuLabel" id="txtSearchMenuLabel" class="form-control" maxlength="50"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchParentMenu">Men� Padre</label>										
										<select id="cbSearchParentMenu" class="form-control" name="cbSearchParentMenu"  
										        data-bind="options:menuViewModel.parentMenu, optionsText:'label', optionsValue:'gkey', optionsCaption: '--'">
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<label for="txtSearchFromDate">Fecha Inicio</label>
										<input type="text" name="txtSearchFromDate" id="txtSearchFromDate" class="form-control" placeholder="YY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="txtSearchToDate">Fecha Fin</label>
										<input type="text" name="txtSearchToDate" id="txtSearchToDate" class="form-control" placeholder="YY-MM-DD"/>
									</div>
									<div class="col-sm-4">
										<label for="cbSearchMenuStatus"> Estado</label>										
										<select width="40%" id="cbSearchMenuStatus" class="form-control" name="cbMenuStatus">
											<option value="" >--</option>
											<option value="ACT">Activo</option>
											<option value="OBS">Obsoleto</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12" align="center">
										<button type="button" class="button button--primary" id="btnSearch">Buscar</button>&nbsp;&nbsp;
										<button type="button" class="button button--success" id="btnClean">Limpiar</button>
									</div>
								</div>
							</div>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div id="actionButtons">
								${actionButtons}
							</div>
							<div class="col-sm-12">
								<br />
							</div>
                            <div class="table-responsive">
                                <table class="table table-hover datatable" id="tbCreatedMenu" width="100%">
                                	${dataTable}                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="addEditMenuModal.jsp" %>
            </div> <!-- end container -->