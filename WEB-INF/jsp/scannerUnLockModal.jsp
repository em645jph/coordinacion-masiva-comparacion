<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<!-- Modal -->
<div class="modal fade" id="scannerUnLockModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Desbloquear Contenedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" novalidate>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="unit">Contenedor:</label>
		      <input type="text" class="form-control" id="unit"  disabled data-bind="textInput: unitIdModal">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="permission">Permiso:</label>
		      <input type="text" class="form-control" id="permission" disabled data-bind="textInput: holdReferenceIdModal">
		    </div>
		  <fieldset class="form-group col-md-12">
		    <div class="row">
		      <div class="col-sm-12">
		      	<label for="Note">Resultado:</label></br>
		        <div class="form-check">
		          <input class="form-check-input" type="radio" name="gridRadios" id="radSaveMessage" value="OK" data-bind="checked: radioSelectedOptionModal">
		          <label class="form-check-label" for="gridRadios2">
		             OK.
		          </label>
		        </div>
		         <div class="form-check">
		          <input class="form-check-input" type="radio" name="gridRadios" id="radUnlock" value="ERROR" data-bind="checked: radioSelectedOptionModal">
		          <label class="form-check-label" for="gridRadios1">
		             CON NOVEDAD.
		          </label>
		        </div>
		      </div>
		    </div>
		  </fieldset>  
		    <div class="form-group col-md-12" data-bind="style: { display: radioSelectedOptionModal() == 'ERROR' ? 'block' : 'none' }">
		      <label for="Note">Observación: </label>
		      <textarea class="form-control rounded-0" rows="3" id="noteUnlock" maxlength="200" placeholder="Observaciones adicionales" data-bind="textInput: noteModal , attr: {class: radioSelectedOptionModal() == 'ERROR' && noteModal() == '' || noteModal() == null ?  'form-control rounded-0 is-invalid' : 'form-control rounded-0' }"></textarea>
		        <div class="invalid-feedback">
				      Este campo es requerido. 
				 </div>
		    </div>
		  </div>
		</form>
      </div>
      <div class="modal-footer"> 
        	<button type="submit" class="btn btn-primary"  data-bind="click: actionUnLock, enable: radioSelectedOptionModal() == 'OK' ? true : radioSelectedOptionModal() == 'ERROR' && noteModal() == '' || noteModal() == null ? false : true">Escaneado</button> 
        	<button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>