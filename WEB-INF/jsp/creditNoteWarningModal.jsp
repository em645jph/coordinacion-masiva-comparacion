<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="creditNoteWarningModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Informaci�n</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<p align="justify">
					Le solicitamos que al momento de facturar cualquier operaci�n en APM Terminals Buenos Aires, pueda revisar los conceptos presupuestados antes de abonarlos a fin de evitar la nota de cr�dito posterior en caso de haber errores.
					<br><br>Dada la situaci�n en donde se hubieran abonado conceptos incorrectos, deber� realizar su <a style="color:#fd7e14;" href="https://apps.apmterminals.com.ar/solicitudesonline" target="_blank">Solicitud Online</a> 
					en un plazo menor a <strong>15 d�as</strong> desde la fecha de emisi�n de la factura por la cual se est� consultando, caso contrario, la Terminal no podr� emitir el cr�dito correspondiente acorde a la Resoluci�n General 4540/2019
				</p>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelModal" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>	