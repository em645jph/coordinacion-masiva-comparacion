<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/assets/css/icons.css" />" />
<script src="<c:url value="/resources/assets/js/documents.js" />" charset="UTF-8"></script>	

<style>
#tbUnits,#btnGenerate {
	display: none;
}
</style>
	
<div class="wrapper">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<h2>Descarga de Entrega de Exportación</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card-box">
					<div class="container">
					  	<div class="col-md-6">
					  		<label for="">Contenedor</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Destino Final</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Razon Social</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Domicilio</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
						<div class="col-md-6">
					  		<label for="">CUIT</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Documentos Aduaneros</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Telefono</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Despachante Aduana</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Mercaderia</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					  	<div class="col-md-6">
					  		<label for="">Numero</label>
							<input type="text" id="" name="" class="form-control">
					  	</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
<!-- end container -->