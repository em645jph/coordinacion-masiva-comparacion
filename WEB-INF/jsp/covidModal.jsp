<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="covidModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Comunicado</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<p align="justify">
					<strong>Estimado Cliente</strong>, se extiende el retiro de importaci�n y almacenaje libre para todas las cargas arribadas a partir del d�a 20 de marzo del 2020 hasta el 12 de junio del corriente a�o inclusive.<br><br>
					Las cargas de exportaci�n que ingresen para su embarque desde el d�a 20 de marzo de 2020 hasta el d�a 12 de junio del corriente a�o estar�n dentro del periodo de almacenamiento gratuito.<br><br>
					Cualquier duda, consulta o reclamo respecto a tu presupuesto pod�s cursarlo a trav�s de nuestros medios de comunicaci�n:<br>
					        *   Gener� una Solicitud Online mediante nuestra web haciendo click <a style="color:#fd7e14;" href="https://apps.apmterminals.com.ar/solicitudesonline" target="_blank">ac�</a>.<br>
					        *   Comunicate los d�as h�biles de 9 a 18hs con nuestro Centro de Atenci�n Telef�nica: 0810-555-APMT (2768)
				</p>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelCovidModal" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>	