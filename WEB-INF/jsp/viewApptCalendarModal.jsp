<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="viewApptCalendarModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Coordinar</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<label for="txtUnitId"> Contenedor:</label> 
						<input type="text" name="txtUnitId" id="txtUnitId" class="form-control" disabled />
					</div>
					<div class="col-sm-6">
						<label for="txtLimitDate">Fecha l�mite:</label>
						<input type="text" name="txtLimitDate" id="txtLimitDate" class="form-control" disabled />
					</div>
				</div>
				<div class="row">
					<div id="calendar"></div>
				</div>
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-dark" id="btnCancelViewApptCalendar" data-dismiss="modal">Cancelar</button>
			</div>		
		</div>
	</div>
</div>	