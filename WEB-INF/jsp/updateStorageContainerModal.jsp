<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<!-- The Modal -->
<div class="modal fade" id="updateStorageContainerModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Confirmar datos</h4>
				<button type="button" class="close" data-dismiss="modal">�</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
						<label for="txtContainerNbr" class="col-sm-9 col-form-label">Contenedor:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="txtContainerNbr" readonly>
						</div>
				</div>
				<div class="form-group">
						<label for="txtContainerIsoTypeId" class="col-sm-9 col-form-label">Tipo de contenedor (*):</label>
						<div class="col-sm-9">
							<select id="cbContainerIsoTypeId" class="form-control" name="cbContainerIsoTypeId"  
								data-bind="options:coordinationViewModel.archTypeCb, optionsText:'id' , optionsValue:'id', optionsCaption: '--'">
							</select>
						</div>
				</div>
				<div class="form-group">
						<label for="cbContainerLineOp" class="col-sm-9 col-form-label">L�nea (*):</label>
						<div class="col-sm-9">
							<select id="cbContainerLineOp" class="form-control" name="cbContainerLineOp"  
									data-bind="options:coordinationViewModel.lineOpCb, optionsText:'description' , optionsValue:'id', optionsCaption: '--'">
							</select>
						</div>
				</div>		
			</div>
            <!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnUpdateStorageContainer">Continuar</button>
				<button type="button" class="btn btn-dark" id="btnCancelUpdateStorageContainer" data-dismiss="modal">Volver</button>
			</div>
		</div>
	</div>
</div>	