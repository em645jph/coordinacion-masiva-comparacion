<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"%>

<!-- JQGrid -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui.jqgrid.css" />">

<script type='text/javascript' src="<c:url value="/resources/js/grid.locale-es.js" />"></script>
<script type='text/javascript'
	src="<c:url value="/resources/js/jquery.jqGrid.min.js" />"></script>
<script type='text/javascript'
	src="<c:url value="/resources/js/jqgrid/grid.loader.js" />"></script>

<script type="text/javascript">

	
$(document).ready(function(){
	dibujarGrilla();
});
	
function dibujarGrilla(){
	
	$("#activityLogGrid").GridUnload();
	$('#activityLogGrid').html('');
	
	jQuery("#activityLogGrid").jqGrid({
		url: url_application + '/logs', 
		datatype: "json", 
		colNames:['<spring:message code="logs.grid.date" />',
		          '<spring:message code="logs.grid.action" />', 
		          '<spring:message code="logs.grid.parameters" />',
		          '<spring:message code="logs.grid.user" />'], 
		colModel:[ {name:'timestamp',index:'timestamp', sorttype: "date", datefmt: "d-M-Y", 
						searchrules: {date: true},
						search: true,
						formatoptions: {newformat:'d/m/Y H:i:s'}
						},  
		           {name:'action',index:'action'}, 
		           {name:'parameters',index:'parameters'}, 
		           {name:'user.login' ,index:'user.login'}
		         ], 
		rowNum:15, 
		rowList:[15,30,50,100],
		sortname:'timestamp',
		sortorder: "desc",
		height:'100%',
		autowidth: true,
		shrinkToFit: true,
		pager: '#pager', 
		viewrecords: true, 
		caption:'<spring:message code="logs.grid.title" />', 
		onSelectRow: function(id){
			params[0] = id;
			fn.apply(null, params);
			},
		jsonReader : {
		    root:	 "resultados",
		    page: 	 "pagina",
		    total: 	 "totalpaginas",
		    records: "totalregistros"
		 },
		 loadError: function (jqXHR, textStatus, errorThrown) {
			 //checkErrors(jqXHR);
			 },
		 gridComplete: function () {
			$('#activityLogGrid' ).setGridWidth($('#activityLogDiv').width(), true);
		 }
	}); 
	
	jQuery("#activityLogGrid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false,search:true}); 
	
    $(window).resize( function() {
    	$('#activityLogGrid' ).setGridWidth($('#activityLogDiv').width(), true); 
     });
	
    $('html,body').animate({
        scrollTop: $("#activityLogGrid").offset().top},
        'slow');

}

</script>

<div id="page-wrapper">
          <div class="row">
		<div id="contenedor" class="container-fluid">
			<div class="row">
				<div id="activityLogDiv" class="col-sm-12">
					<table id="activityLogGrid"></table> 
					<div id="pager"></div>
				</div>
			</div>
		</div>
	</div>
</div>




