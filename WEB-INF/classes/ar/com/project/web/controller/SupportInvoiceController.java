//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.AgpCoordinationAuditService;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.CustomerSupportService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.service.UnitService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/SupportInvoice" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class SupportInvoiceController extends CommonController {

  private static final Logger logger              = Logger.getLogger (
      SupportInvoiceController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  AppointmentService          apptService;
  @Autowired
  LineOpService               lineOpService;
  @Autowired
  ScheduleRuleService         ruleService;
  @Autowired
  SrvOrderService             soService;
  @Autowired
  PrivilegeService            privilegeService;
  @Autowired
  CoordinationService         coordinationService;
  @Autowired
  AuditService                auditService;
  @Autowired
  AgpCoordinationAuditService agpCoordAuditService;
  @Autowired
  UnitService                 unitService;
  @Autowired
  CustomerSupportService      supportService;
  private              List   privilegeInPageList = new ArrayList ( );

  public
  SupportInvoiceController ( ) {
  }

  @RequestMapping (
      value = { "/supportInvoice" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView supportUnit (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                           ) {
    ModelAndView model       = new ModelAndView ( "supportInvoice" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.canAddItem  = false;
    this.canEditItem = false;
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List categoryList = this.coordinationService.getCoordinationCategoryList ( );
      List lineOpList   = this.lineOpService.getAllLineOp ( );
      Map  resultMap    = new HashMap ( );
      resultMap.put ( "categoryList" , categoryList );
      resultMap.put ( "lineOpList" , lineOpList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var5.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findUnit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findUnit ( @RequestParam String prUnitId ) throws SQLException {
    List           unitList = null;
    ResponseEntity response = new ResponseEntity ( unitList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      value = { "/findInvoice" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findInvoice (
      @RequestParam String prDraftNbrStr ,
      @RequestParam String prStatus , @RequestParam String prCostCenterStr ,
      @RequestParam String prFinalNbrStr , @RequestParam String prInvoiceType ,
      @RequestParam String prCustomerIdTax , @RequestParam String prEntityId ,
      @RequestParam String prEventId , @RequestParam String prTariffId ,
      @RequestParam String prCreator , @RequestParam String prFromDateStr ,
      @RequestParam String prToDateStr
                                      ) throws SQLException {
    List invoiceList = this.supportService.findInvoiceByParams ( ( List ) null , prDraftNbrStr ,
                                                                 prStatus ,
                                                                 prCostCenterStr , prFinalNbrStr ,
                                                                 prInvoiceType , prCustomerIdTax ,
                                                                 prEntityId , prEventId ,
                                                                 prTariffId , prCreator ,
                                                                 prFromDateStr , prToDateStr
                                                               );
    ResponseEntity response = new ResponseEntity ( invoiceList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      value = { "/getCustomerInfo" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Map > getCustomerInfo ( @RequestParam String prCustomerGkeyStr )
  throws SQLException {
    List customerInfoList = this.supportService.getCustomerInformation (
        Long.parseLong ( prCustomerGkeyStr ) );
    Map row =
        customerInfoList != null && ! customerInfoList.isEmpty ( ) ? ( Map ) customerInfoList.get (
            0 )
                                                                   : new HashMap ( );
    return new ResponseEntity ( row , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getInvoicePayments" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Map > getInvoicePayments (
      @RequestParam String prInvoiceGkeyStr ,
      @RequestParam String prDraftNbrStr
                                            ) throws SQLException {
    List paymentList = this.supportService.getInvoicePaymentList (
        Long.parseLong ( prInvoiceGkeyStr ) ,
        ( Long ) null
                                                                 );
    List ePaymentList = this.supportService.getInvoiceEPaymentList (
        Long.parseLong ( prDraftNbrStr ) );
    Map  result       = new HashMap ( );
    result.put ( "paymentList" , paymentList );
    result.put ( "ePaymentList" , ePaymentList );
    return new ResponseEntity ( result , HttpStatus.OK );
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }
}
