//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.SrvOrderService;
import ar.com.project.core.service.UnitService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.StringUtils;
import ar.com.project.core.worker.SrvOrderTypeEnum;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/TipService" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class TipServiceController extends CommonController {

  private static final Logger logger = Logger.getLogger ( TipServiceController.class );
  User user;
  @Autowired
  UserService userService;
  @Autowired
  UnitService unitService;
  @Autowired
  SrvOrderService soService;
  @Autowired
  AuditService auditService;

  public
  TipServiceController ( ) {
  }

  @RequestMapping (
      value = { "/tipService" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView tipService (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                          ) throws BaseException {
    ModelAndView model = new ModelAndView ( "tipService" );
    return model;
  }

  @RequestMapping (
      value = { "/initLoad" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  public
  ResponseEntity < Object > initLoad ( ) throws SQLException, BaseException {
    String userLogin = this.getUserLogin ( );
    this.user = userLogin != null ? this.userService.getUser ( userLogin ) : null;
    String emailAdress = this.user != null ? this.user.getEmailAddress ( ) : null;
    Map    map         = new HashMap ( );
    map.put ( "tipServiceList" , EnumUtil.getTipSrvOrderTypeList ( ) );
    map.put ( "emailAddress" , emailAdress == null ? "" : emailAdress );
    map.put ( "isAnonimous" , this.isAnonimous ( ) );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/saveTipService" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > saveTipService (
      @RequestParam String prContainerNbr ,
      @RequestParam String prServiceType , @RequestParam String prEmail
                                           )
  throws BaseException, ParseException, SQLException, IOException {
    String error = "";
    prContainerNbr = prContainerNbr.trim ( ).toUpperCase ( );
    SrvOrderTypeEnum tipSoTypeEnum =
        prServiceType != null && ! prServiceType.isEmpty ( ) ? EnumUtil.resolveTipSrvOrderTypeEnum (
            prServiceType ) : null;
    if ( this.isNullOrEmpty ( prContainerNbr ) ) {
      error = "Ingresá el número de contenedor";
    }
    else if ( this.isNullOrEmpty ( prServiceType ) ) {
      error = "Seleccioná un servicio";
    }
    else if ( tipSoTypeEnum == null ) {
      error = "El servicio seleccionado no es un servicio en TIP";
    }
    else if ( this.isNullOrEmpty ( prEmail ) ) {
      error = "Ingresá el email";
    }

    if ( ! this.isNullOrEmpty ( error ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
    }
    else {
      Map unitMap = this.unitService.findImportUnitInTip ( prContainerNbr );
      if ( unitMap != null && ! unitMap.isEmpty ( ) ) {
        Long unitGkey = ( Long ) unitMap.get ( "gkey" );
        Date date     = DateUtils.setTime ( new Date ( ) , 7 , 0 , 0 );
        String result = ( String ) this.soService.createTipService ( unitGkey.toString ( ) ,
                                                                     tipSoTypeEnum.getKey ( ) ,
                                                                     StringUtils.formatDate (
                                                                         date ) , prEmail
                                                                   );
        if ( ! result.isEmpty ( ) ) {
          if ( result.equals ( "SERVICE_ORDER_ALREADY_EXISTS" ) ) {
            result = "El servicio que está solicitando ya ha sido solicitado con anterioridad"
                     + ".<br>Recibirá un correo confirmando cuando se haya realizado.";
          }

          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          List < String > data = new ArrayList ( );
          data.add ( "unitGkey:" + unitGkey );
          data.add ( "unitId:" + prContainerNbr );
          data.add ( "serviceType:" + prServiceType );
          data.add ( "prEmail:" + prEmail );
          String userLogin = this.getUserLogin ( );
          this.auditService.saveAuditTransaction ( "createTipService" , data.toString ( ) ,
                                                   userLogin == null ? "--" : userLogin
                                                 );
          return new ResponseEntity (
              this.buildSuccessResponse (
                  "Se ha generado su solicitud. Recibirá un correo confirmando la solicitud y lo "
                  + "mantendremos informado vía e-mail" ) ,
              HttpStatus.OK
          );
        }
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse (
                "El contenedor " + prContainerNbr + " no se encuentra cargado en camión" ) ,
            HttpStatus.OK
        );
      }
    }
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String str ) {
    return str == null || str.isEmpty ( );
  }

  private
  boolean isAnonimous ( ) {
    Object principal = SecurityContextHolder.getContext ( ).getAuthentication ( ).getPrincipal ( );
    Object loggedUser = principal instanceof org.springframework.security.core.userdetails.User
                        ? ( org.springframework.security.core.userdetails.User ) principal : null;
    return loggedUser == null;
  }

  private
  String getUserLogin ( ) {
    Object principal = SecurityContextHolder.getContext ( ).getAuthentication ( ).getPrincipal ( );
    Object loggedUser = principal instanceof org.springframework.security.core.userdetails.User
                        ? ( org.springframework.security.core.userdetails.User ) principal : null;
    return loggedUser != null
           ? ( ( org.springframework.security.core.userdetails.User ) loggedUser ).getUsername ( )
           : null;
  }
}
