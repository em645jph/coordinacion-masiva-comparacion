//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.DocumentalCutoff;
import ar.com.project.core.dto.ShipmentCommnucationEventLockSrvDTO;
import ar.com.project.core.dto.ShipmentCommnucationSrvDTO;
import ar.com.project.core.dto.ShipmentCommunicationFilterDTO;
import ar.com.project.core.dto.UnitShipmenCommunicationParameterDTO;
import ar.com.project.core.service.CustomerShipmentCommunicationService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.StringUtils;
import ar.com.project.core.worker.ShipmentCommunicationActionTypeEnum;
import ar.com.project.core.worker.ShipmentCommunicationLockTypesEnum;
import ar.com.project.core.worker.ShipmentCommunicationResultTypesEnum;
import com.curcico.jproject.core.exception.BaseException;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/ShipmentCommunicationManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class ShipmentCommunicationController extends CommonController {

  private static final Logger logger                    = Logger.getLogger (
      ShipmentCommunicationController.class );
  private static final String APPLICATION_PDF           = "application/x-download";
  private final        String PRIVILEGE_ID_APPROVE      = "SHIPMENT_COMMUNICATION_APPROVE";
  private final        String PRIVILEGE_ID_BLOCK        = "SHIPMENT_COMMUNICATION_BLOCK";
  private final        String PRIVILEGE_ID_UNBLOCK      = "SHIPMENT_COMMUNICATION_UNBLOCK";
  private final        String PRIVILEGE_ID_FINAL_REPORT = "SHIPMENT_COMMUNICATION_FINAL_REPORT";
  private final        String PRIVILEGE_ID_VIEW         = "SHIPMENT_COMMUNICATION_VIEW";
  @Autowired
  CustomerShipmentCommunicationService customerShipmentCommunicationService;
  @Autowired
  PrivilegeService                     privilegeService;

  public
  ShipmentCommunicationController ( ) {
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List vesselList                            =
          this.customerShipmentCommunicationService.getAllVisselShipmentCommunication ( );
      List optionFilterShipmentCommunicationList =
          EnumUtil.getOptionFilterShipmentCommunicationList ( );
      List statusShipmentCommunicationList       = EnumUtil.getShipmentCommunicationStatusList ( );
      List lockTypesShipmentCommunicationList    =
          EnumUtil.getShipmentCommunicationLockTypesList ( );
      List resultTypesShipmentCommunicationList  =
          EnumUtil.getShipmentCommunicationResultTypesList ( );
      List actionTypeShipmentCommunicationList   =
          EnumUtil.getShipmentCommunicationActionTypesList ( );
      Map  resultMap                             = new HashMap ( );
      resultMap.put (
          "optionFilterShipmentCommunicationList" , optionFilterShipmentCommunicationList );
      resultMap.put ( "vesselList" , vesselList );
      resultMap.put ( "statusShipmentCommunicationList" , statusShipmentCommunicationList );
      resultMap.put ( "lockTypesShipmentCommunicationList" , lockTypesShipmentCommunicationList );
      resultMap.put (
          "resultTypesShipmentCommunicationList" , resultTypesShipmentCommunicationList );
      resultMap.put ( "actionTypeShipmentCommunicationList" , actionTypeShipmentCommunicationList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var9 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la informaciï¿½n. Por favor refresque la pï¿½gina" ,
          HttpStatus.BAD_REQUEST
      );
      var9.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/shipmentCommunication" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView shipmentCommunication (
      HttpServletRequest request ,
      HttpServletResponse response , Object handler
                                     ) {
    List privilegeList = this.getUserPrivilegeList ( request );
    if ( ! this.privilegeService.userCanViewPage (
        privilegeList , "SHIPMENT_COMMUNICATION_VIEW" ) ) {
      return this.redirectTo404 ( );
    }
    else {
      ModelAndView model             = new ModelAndView ( "shipmentCommunication" );
      String       actionButtonsHtml = this.getActionButtonsHtml ( privilegeList );
      model.addObject ( "actionButtons" , actionButtonsHtml );
      return model;
    }
  }

  @RequestMapping (
      value = { "/findUnitsShipmentCommunication" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > findUnitsShipmentCommunication (
      HttpServletRequest request ,
      @RequestBody ShipmentCommunicationFilterDTO shipmentCommunicationParameter
                                                           ) {
    ResponseEntity < Object > responseEntity = null;
    new ArrayList ( );

    try {
      List privilegeList = this.getUserPrivilegeList ( request );
      List < ShipmentCommnucationSrvDTO > unitsShipmentCommunicationList =
          this.customerShipmentCommunicationService.findUnitShipmentCommunication (
          shipmentCommunicationParameter.getVesselId ( ) ,
          shipmentCommunicationParameter.getUnitId ( ) ,
          shipmentCommunicationParameter.getCustomersBoardingPermissionId ( ) ,
          shipmentCommunicationParameter.getShipmentCommunicationStatus ( ) ,
          EnumUtil.getUfvTransitStateFilter (
              false , false , true , true , true , true , true , false )
                                                                                                                                                   );
      boolean canApprove = this.privilegeService.userCanPerformOther (
          privilegeList ,
          "SHIPMENT_COMMUNICATION_APPROVE"
                                                                     );
      boolean canBlock = this.privilegeService.userCanPerformOther (
          privilegeList ,
          "SHIPMENT_COMMUNICATION_BLOCK"
                                                                   );
      boolean canUnblock = this.privilegeService.userCanPerformOther (
          privilegeList ,
          "SHIPMENT_COMMUNICATION_UNBLOCK"
                                                                     );
      this.customerShipmentCommunicationService.addUserAccessToList (
          unitsShipmentCommunicationList ,
          canApprove , canBlock , canUnblock
                                                                    );
      responseEntity = new ResponseEntity ( unitsShipmentCommunicationList , HttpStatus.OK );
    }
    catch ( SQLException var9 ) {
      responseEntity = new ResponseEntity (
          "No se pudo cargar correctamente la informaciï¿½n. Por favor refresque la pï¿½gina" ,
          HttpStatus.NOT_FOUND
      );
    }
    catch ( ClassCastException var10 ) {
      responseEntity = new ResponseEntity (
          "Retorno de valores de tipos no esperado por la App. Por favor comuniquese con el "
          + "administrador de sistemas" ,
          HttpStatus.NOT_ACCEPTABLE
      );
    }
    catch ( NullPointerException var11 ) {
      responseEntity = new ResponseEntity (
          "Algunos de los paremetros puede que sea incorrecto o al menos uno de los campos "
          + "enviados sea igual a nulo. Por favor refresque la pï¿½gina" ,
          HttpStatus.BAD_REQUEST
      );
    }

    return responseEntity;
  }

  @RequestMapping (
      value = { "/updateStatusUnitShipmentComunication" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > updateStatusUnitShipmentComunication (
      HttpServletRequest request ,
      @RequestBody UnitShipmenCommunicationParameterDTO unitShipmenCommunicationParameter
                                                                 )
  throws BaseException, SQLException {
    ResponseEntity < Object > responseEntity = null;
    String                    statusResponse = null;

    try {
      String action = unitShipmenCommunicationParameter.getAction ( );
      String reason = unitShipmenCommunicationParameter.getReason ( );
      String result = unitShipmenCommunicationParameter.getResult ( );
      String obs    = unitShipmenCommunicationParameter.getNotes ( );
      obs = ! StringUtils.isNullOrEmpty ( obs ) ? obs.toUpperCase ( ) : obs;
      ShipmentCommunicationLockTypesEnum   reasonEnum = EnumUtil.resolveScLockTypeByKey ( reason );
      ShipmentCommunicationResultTypesEnum resultEnum = EnumUtil.resolveScResultTypeByKey (
          result );
      String                               reasonDesc = reasonEnum.getDescription ( );
      String                               resultDesc = resultEnum.getDescription ( );
      boolean                              isApprove  =
          this.customerShipmentCommunicationService.isApprove (
          action );
      boolean                              isBlock    =
          this.customerShipmentCommunicationService.isBlock (
          action );
      boolean                              isUnblock  =
          this.customerShipmentCommunicationService.isUnblock (
          action );
      Long flagGkey = isUnblock ? unitShipmenCommunicationParameter.getHoldGkey ( )
                                : unitShipmenCommunicationParameter.getFlagGkey ( );
      String referenceId = isUnblock ? unitShipmenCommunicationParameter.getHoldReferenceId ( )
                                     : (
                               isBlock ? unitShipmenCommunicationParameter.getReferenceId ( ) + "_"
                                         + reason
                                       : unitShipmenCommunicationParameter.getReferenceId ( )
                           );
      String notes = obs;
      if ( isBlock ) {
        notes =
            reasonDesc + ( StringUtils.isNullOrEmpty ( resultDesc ) ? "" : "(" + resultDesc + ")" )
            + (
                StringUtils.isNullOrEmpty ( obs ) ? "" : ":" + obs
            );
      }

      notes = notes.length ( ) > 200 ? notes.substring ( 0 , 199 ) : notes;
      List privilegeList = this.getUserPrivilegeList ( request );
      String validationMsg = this.validateBeforePeformAction ( action ,
                                                               unitShipmenCommunicationParameter.getGkey ( ) ,
                                                               flagGkey , referenceId , reasonDesc ,
                                                               isApprove ,
                                                               isBlock , isUnblock , privilegeList
                                                             );
      if ( ! StringUtils.isNullOrEmpty ( validationMsg ) ) {
        return new ResponseEntity ( validationMsg , HttpStatus.OK );
      }

      statusResponse =
          this.customerShipmentCommunicationService.updateStatusUnitShipmentComunication (
          unitShipmenCommunicationParameter.getGkey ( ) ,
          unitShipmenCommunicationParameter.getId ( ) ,
          flagGkey , referenceId , notes , unitShipmenCommunicationParameter.getAction ( ) ,
          unitShipmenCommunicationParameter.getObVessel ( ) , this.getUserLogin ( )
                                                                                                      );
    }
    catch ( IOException var21 ) {
      new ResponseEntity ( statusResponse , HttpStatus.EXPECTATION_FAILED );
      var21.printStackTrace ( );
    }

    responseEntity = statusResponse.equalsIgnoreCase ( "SUCCESS" ) ? new ResponseEntity (
        statusResponse ,
        HttpStatus.OK
    ) : new ResponseEntity (
        "Error a intentar acceder a los servicios. Por favor refresque la página o contacte al "
        + "administrador de sistemas" ,
        HttpStatus.CONFLICT
    );
    return responseEntity;
  }

  @RequestMapping (
      value = { "/lisEventByPermisions" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > getLisEventByPermisions (
      @RequestBody UnitShipmenCommunicationParameterDTO unitShipmenCommunicationParameter
                                                    ) {
    ResponseEntity < Object > responseEntity = null;
    new ArrayList ( );

    try {
      List < ShipmentCommnucationEventLockSrvDTO > lisEventByPermisions =
          this.customerShipmentCommunicationService.getLisEventByPermisions (
          unitShipmenCommunicationParameter.getGkey ( ) ,
          unitShipmenCommunicationParameter.getReferenceId ( )
                                                                                                                                            );
      responseEntity = new ResponseEntity ( lisEventByPermisions , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      responseEntity = new ResponseEntity (
          "No se pudo cargar correctamente la informaciï¿½n. Por favor refresque la pï¿½gina o "
          + "contacte al administrador de sistemas"
          + var5.getMessage ( ) , HttpStatus.NOT_FOUND );
    }
    catch ( ClassCastException var6 ) {
      responseEntity = new ResponseEntity (
          "Retorno de valores de tipos no esperado por la App. Por favor comuniquese con el "
          + "administrador de sistemas"
          + var6.getMessage ( ) , HttpStatus.NOT_ACCEPTABLE );
    }
    catch ( NullPointerException var7 ) {
      responseEntity = new ResponseEntity (
          "Algunos de los paremetros puede que sea incorrecto o al menos uno de los campos "
          + "enviados sea igual a nulo. Por favor refresque la pï¿½gina" ,
          HttpStatus.BAD_REQUEST
      );
    }

    return responseEntity;
  }

  @RequestMapping (
      value = { "/knowCutOff" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > knowCutOff ( @RequestParam ( "visitId") String visitId ) {
    ResponseEntity < Object > responseEntity         = null;
    Boolean                   notAbleToPerformAction = false;

    try {
      DocumentalCutoff doCutoff = this.customerShipmentCommunicationService.findDocumentalCutoff (
          visitId );
      notAbleToPerformAction = doCutoff == null || (
          new Date ( System.currentTimeMillis ( ) )
      ).after (
          doCutoff.getCutoffDateDoc ( ) );
    }
    catch ( BaseException var5 ) {
      new ResponseEntity (
          "No se pudo cargar correctamente la informaciï¿½n. Por favor refresque la pï¿½gina o "
          + "contacte al administrador de sistemas"
          + var5.getMessage ( ) , HttpStatus.OK );
    }

    responseEntity = new ResponseEntity ( notAbleToPerformAction , HttpStatus.OK );
    return responseEntity;
  }

  @RequestMapping (
      value = { "/getHistoryEvents" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getHistoryEvents (
      @RequestParam String unitGkeyStr ,
      @RequestParam String referenceId
                                             ) {
    try {
      List eventList = this.customerShipmentCommunicationService.getHistoryEvents (
          Long.parseLong ( unitGkeyStr ) , referenceId );
      return new ResponseEntity ( eventList , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      return new ResponseEntity (
          "Error al cargar el listado." + var4.getMessage ( ) ,
          HttpStatus.BAD_REQUEST
      );
    }
  }

  @RequestMapping (
      value = { "/applyActionByPermission" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > applyActionByPermission (
      HttpServletRequest request ,
      @RequestBody Object applyActionObject
                                                    ) {
    Map    applyObjectMap = ( Map ) applyActionObject;
    String visitId        = ( String ) applyObjectMap.get ( "visitId" );
    String permission     = ( String ) applyObjectMap.get ( "permission" );
    String action         = ( String ) applyObjectMap.get ( "action" );
    String reason         = ( String ) applyObjectMap.get ( "reason" );
    String notes          = ( String ) applyObjectMap.get ( "notes" );
    List   privilegeList  = this.getUserPrivilegeList ( request );
    if ( ShipmentCommunicationActionTypeEnum.LOCK.getKey ( ).equals ( action )
         && ! this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_BLOCK"
                                                        ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Usuario no tiene privilegios para bloquear" ) ,
          HttpStatus.OK
      );
    }
    else if ( ShipmentCommunicationActionTypeEnum.UNLOCK.getKey ( ).equals ( action )
              && ! this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_UNBLOCK"
                                                             ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Usuario no tiene privilegios para desbloquear" ) ,
          HttpStatus.OK
      );
    }
    else {
      Boolean applyAction = applyObjectMap.get ( "applyAction" ) == null ? false
                                                                         :
                            ( Boolean ) applyObjectMap.get (
                                                                             "applyAction" );
      if ( StringUtils.isNullOrEmpty ( permission ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá el permiso de embarque" ) ,
            HttpStatus.OK
        );
      }
      else if ( StringUtils.isNullOrEmpty ( action ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Seleccioná la acción a realizar" ) ,
            HttpStatus.OK
        );
      }
      else if ( StringUtils.isNullOrEmpty ( reason ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Seleccioná el motivo" ) , HttpStatus.OK );
      }
      else if ( StringUtils.isNullOrEmpty ( notes ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá observaciones para la acción a realizar" ) ,
            HttpStatus.OK
        );
      }
      else {
        ShipmentCommunicationActionTypeEnum actionTypeEnum = EnumUtil.resolveScActionTypeByKey (
            action );
        ShipmentCommunicationLockTypesEnum lockTypeEnum = EnumUtil.resolveScLockTypeByKey (
            reason );
        if ( actionTypeEnum == null ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "La acción seleccionada no es correcta" ) , HttpStatus.OK );
        }
        else if ( lockTypeEnum != null && ! StringUtils.isNullOrEmpty (
            lockTypeEnum.getKey ( ) ) ) {
          String questionMsg = "";
          if ( ShipmentCommunicationActionTypeEnum.LOCK.getKey ( ).equals ( action ) ) {
            questionMsg =
                "El bloqueo se aplicará para todos los contenedores del permiso " + permission
                + " que no estén bloqueados por " + lockTypeEnum.getDescription ( );
          }
          else if ( ShipmentCommunicationActionTypeEnum.UNLOCK.getKey ( ).equals ( action ) ) {
            questionMsg =
                "El desbloqueo se aplicará para todos los contenedores del permiso " + permission
                + " que estén bloqueados por " + lockTypeEnum.getDescription ( );
          }

          if ( StringUtils.isNullOrEmpty ( questionMsg ) ) {
            return new ResponseEntity ( this.buildErrorResponse (
                "Se produjo un error en la aplicación. Favor reportar a IT" ) , HttpStatus.OK );
          }
          else if ( ! applyAction ) {
            return new ResponseEntity (
                this.buildQuestionResponse ( questionMsg , "applyAction" ) ,
                HttpStatus.OK
            );
          }
          else {
            try {
              String reasonDesc = lockTypeEnum.getDescription ( );
              List dtoList =
                  this.customerShipmentCommunicationService.findUnitShipmentCommunication (
                  visitId , ( String ) null , permission , ( String ) null ,
                  EnumUtil.getUfvTransitStateFilter ( false , false , true , true , true , true ,
                                                      true ,
                                                      false
                                                    )
                                                                                                     );
              List updatedUnitList =
                  this.customerShipmentCommunicationService.applyActionByPermission (
                  dtoList , permission , action , reason , reasonDesc , notes ,
                  this.getUserLogin ( )
                                                                                                       );
              return updatedUnitList.isEmpty ( ) ? new ResponseEntity (
                  this.buildWarningResponse ( "No se actualizó ningún contenedor" ) ,
                  HttpStatus.OK
              )
                                                 : new ResponseEntity ( this.buildSuccessResponse (
                                                     "Contenedores actualizados: "
                                                     + updatedUnitList.size ( ) + "<br><br>"
                                                     + updatedUnitList ) , HttpStatus.OK );
            }
            catch ( SQLException var17 ) {
              var17.printStackTrace ( );
              return new ResponseEntity (
                  this.buildErrorResponse ( var17.getMessage ( ) ) , HttpStatus.OK );
            }
            catch ( IOException var18 ) {
              var18.printStackTrace ( );
              return new ResponseEntity (
                  this.buildErrorResponse ( var18.getMessage ( ) ) , HttpStatus.OK );
            }
            catch ( BaseException var19 ) {
              var19.printStackTrace ( );
              return new ResponseEntity (
                  this.buildErrorResponse ( var19.getMessage ( ) ) , HttpStatus.OK );
            }
          }
        }
        else {
          return new ResponseEntity (
              this.buildErrorResponse ( "El motivo seleccionado no es correcto" ) , HttpStatus.OK );
        }
      }
    }
  }

  private
  String validateBeforePeformAction (
      String action , Long unitGkey , Long flagGkey ,
      String referenceId , String reasonDesc , boolean isApprove , boolean isBlock ,
      boolean isUnblock ,
      List privilegeList
                                    ) throws SQLException {
    if ( ! isApprove && ! isBlock && ! isUnblock ) {
      return "Acción " + action + " no soportada";
    }
    else if ( isApprove && ! this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_APPROVE"
                                                                       ) ) {
      return "Usuario no tiene privilegios para aprobar";
    }
    else if ( isBlock && ! this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_BLOCK"
                                                                     ) ) {
      return "Usuario no tiene privilegios para bloquear";
    }
    else if ( isUnblock && ! this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_UNBLOCK"
                                                                       ) ) {
      return "Usuario no tiene privilegios para desbloquear";
    }
    else {
      List list;
      if ( isApprove ) {
        list = this.customerShipmentCommunicationService.findOtherUnitBlockedByScanner (
            unitGkey ,
            referenceId
                                                                                       );
        if ( list != null && ! list.isEmpty ( ) ) {
          return "El permiso de embarque tiene " + referenceId
                 + " otros contenedores bloqueados por escaner. <br><br> Revisá los registros "
                 + list;
        }
      }

      if ( isBlock || isUnblock ) {
        list = this.customerShipmentCommunicationService.getLisEventByPermisions (
            unitGkey ,
            referenceId
                                                                                 );

        for ( int i = 0 ; i < list.size ( ) ; ++ i ) {
          ShipmentCommnucationEventLockSrvDTO dto = ( ShipmentCommnucationEventLockSrvDTO ) list.get (
              i );
          if ( isBlock && dto.getVetoGkey ( ) == null ) {
            return "Ya existe un bloqueo por " + reasonDesc + " para este permiso";
          }

          if ( isUnblock && dto.getGkey ( ).equals ( flagGkey ) && dto.getVetoGkey ( ) != null ) {
            return "El bloqueo no está activo. Refrescá la página";
          }
        }
      }

      return "";
    }
  }

  private
  String getActionButtonsHtml ( List privilegeList ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.privilegeService.userCanExportTable ( privilegeList ) ) {
      sb.append (
          "<button  class=\"btn btn-sm-custom btn-dark btn-bordered\" id=\"btnExportShipmentCommunication\" data-bind=\"click: actionExport\"><i class=\"ti-file-export\"></i>Exportar EXCEL</button>&nbsp;&nbsp;" );
    }

    if ( this.privilegeService.userCanPerformOther (
        privilegeList ,
        "SHIPMENT_COMMUNICATION_FINAL_REPORT"
                                                   ) ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button class=\"btn btn-sm-custom btn-primary btn-bordered \" id=\"btnCreateReportVessels\" data-bind=\"click: actionDownloadPDF\"> <i class=\"ti-document\"></i>Comunicacion de Embarque</button>&nbsp;&nbsp;" );
    }

    sb.append (
        "<button  class=\"btn btn-sm-custom btn-info btn-bordered\" id=\"btnActionByPermission\" data-bind=\"click: actionByPermission, style: { display: (searchVisitId() != null && searchVisitId() != '') || (searchPermission() != null && searchPermission() != '')? 'inline' : 'none'}\">ACCIÓN POR FAMILIA</button>" );
    return sb.toString ( );
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/shipmentCommunication/export/pdf" },
      produces = { "application/x-download" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  String exportToPDF (
      HttpServletRequest request , HttpServletResponse response ,
      @RequestBody String vesselId
                     ) throws JRException, SQLException {
    try {
      File fileShipmentComunicaction = this.customerShipmentCommunicationService.getShipmentCpmunicationExp (
          vesselId );
      return Base64.getEncoder ( ).encodeToString (
          Files.toByteArray ( fileShipmentComunicaction ) );
    }
    catch ( IOException var5 ) {
      return null;
    }
  }

  private
  List getUserPrivilegeList ( HttpServletRequest request ) {
    String servletPath = request.getServletPath ( );
    String login       = this.getUserLogin ( );
    return this.privilegeService.getMenuPagePrivileges ( login , servletPath );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }
}
