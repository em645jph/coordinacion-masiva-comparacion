//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.AccountabilityLinkedUser;
import ar.com.project.core.domain.ArgoDocument;
import ar.com.project.core.domain.LinkedUser;
import ar.com.project.core.domain.Role;
import ar.com.project.core.domain.User;
import ar.com.project.core.domain.UserSignInRequest;
import ar.com.project.core.dto.UserLinkDTO;
import ar.com.project.core.dto.UserPendingDTO;
import ar.com.project.core.security.PasswordManager;
import ar.com.project.core.service.AccountabilityLinkedUserService;
import ar.com.project.core.service.ArgoDocumentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.DatabaseConnectorService;
import ar.com.project.core.service.LinkedUserService;
import ar.com.project.core.service.QueriesService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.UserSignInRequestService;
import ar.com.project.core.utils.MessagesUtils;
import ar.com.project.core.utils.NumberUtil;
import ar.com.project.core.utils.StringUtils;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import com.curcico.jproject.core.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "parameters" , "rolCC" , "customer" })
@RequestMapping ( { "/User" })
public
class UserController extends CommonController {

  private static final Logger logger = Logger.getLogger ( UserController.class );
  @Autowired
  UserService                     userService;
  @Autowired
  UserSignInRequestService        userSignInRequestService;
  @Autowired
  ArgoDocumentController          argoDocument;
  @Autowired
  DatabaseConnectorService        databaseConnectorService;
  @Autowired
  QueriesService                  queriesService;
  @Autowired
  ArgoDocumentService             argoDocumentService;
  @Autowired
  LinkedUserService               linkedUserService;
  @Autowired
  RoleService                     roleService;
  @Autowired
  AccountabilityLinkedUserService accountabilityLinkedUserService;
  @Autowired
  AuditService                    auditService;

  public
  UserController ( ) {
  }

  @RequestMapping (
      value = { "/userProfile" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView userProfile ( ) throws BaseException {
    logger.info ( "userProfile" );
    ModelAndView m = new ModelAndView ( "userProfile" );
    User         u = this.userService.getUser ( this.getUserContext ( ).getUsername ( ) );
    if ( u != null ) {
      m.addObject ( "rolCC" , u.getRoles ( ).contains ( this.roleService.getRoleByGkey ( 1 ) ) );
    }

    m.addObject ( "user" , u );
    return m;
  }

  @RequestMapping (
      value = { "/userRegisterPendings" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "hasAnyRole('USER_REQ_MANAGER')")
  public
  ModelAndView userRegisterPendings ( ) throws BaseException {
    logger.info ( "userRegisterPendings" );
    return new ModelAndView ( "userRegisterPendings" );
  }

  @RequestMapping (
      value = { "/users" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView users ( ) throws BaseException {
    logger.info ( "users" );
    return new ModelAndView ( "users" );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUserBeforeRegister/{userId}" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > findUserBeforeRegister ( @PathVariable String userId )
  throws BaseException {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "login" , SearchOption.EQUAL , userId ) );
      User u = ( User ) this.userService.findEntityByFilters ( parametersFilters );
      return u != null ? new ResponseEntity ( "Usuario ya existe en sistema." , HttpStatus.OK )
                       : new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
    }
    catch ( BusinessException var4 ) {
      return new ResponseEntity ( var4.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      headers = { "content-type=multipart/*" },
      value = { "/registerUser" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > registerUser (
      MultipartHttpServletRequest request ,
      HttpServletResponse response
                                         ) throws BaseException, ParseException {
    try {
      String  beBillingUser        = request.getParameter ( "beBillingUser" );
      boolean userRequestedBilling = false;
      if ( beBillingUser != null && beBillingUser.equalsIgnoreCase ( "on" ) ) {
        userRequestedBilling = true;
      }

      String telephone;
      if ( userRequestedBilling ) {
        Map < String, MultipartFile > fileMap       = request.getFileMap ( );
        MultipartFile                 multipartFile = null;
        Iterator                      var8          = fileMap.entrySet ( ).iterator ( );

        while ( var8.hasNext ( ) ) {
          Map.Entry < String, MultipartFile > set = ( Map.Entry ) var8.next ( );
          multipartFile = ( MultipartFile ) set.getValue ( );
          telephone     = this.validateFileInput ( multipartFile );
          if ( telephone != null ) {
            return new ResponseEntity ( telephone , HttpStatus.BAD_REQUEST );
          }
        }
      }

      String documentNbr = request.getParameter ( "documentNbr" );
      String fullname    = StringUtils.parseCompleteCharacters (
          request.getParameter ( "fullname" ) );
      String address     = StringUtils.parseCompleteCharacters (
          request.getParameter ( "address" ) );
      String emailAddress = StringUtils.parseCompleteCharacters (
          request.getParameter ( "emailAddress" ) );
      telephone = StringUtils.parseCompleteCharacters ( request.getParameter ( "telephone" ) );
      String emailComex      = null;
      String emailManagement = null;
      String businessType    = null;
      String city            = null;
      User   usr             = this.userService.getUser ( documentNbr );
      if ( usr == null ) {
        usr = new User ( );
        usr.setDocumentNbr ( new BigInteger ( documentNbr ) );
        usr.setLogin ( documentNbr );
        usr.setFullName ( fullname );
        usr.setAddress ( address );
        usr.setEmailAddress ( emailAddress );
        usr.setPhoneNumber ( telephone );
        usr.setChange ( true );
        usr = this.userService.add ( usr , "webform" );
      }

      Integer nusuario = this.userService.findUserWebTicket ( usr.getLogin ( ) );
      if ( nusuario == 0 || nusuario == null ) {
        this.userService.createUserWebTicket ( usr );
        nusuario = this.userService.findUserWebTicket ( usr.getLogin ( ) );
        this.userService.assignRolUserWebTicket ( nusuario );
      }

      Role rol = this.roleService.getRoleByGkey ( 1 );
      if ( usr.getRoles ( ) == null ) {
        ArrayList < Role > roles = new ArrayList ( );
        usr.setRoles ( roles );
        this.userService.addRole ( usr , rol , "system" );
      }
      else if ( ! usr.getRoles ( ).contains ( rol ) ) {
        this.userService.addRole ( usr , rol , "system" );
      }

      if ( userRequestedBilling ) {
        emailComex      = StringUtils.parseCompleteCharacters (
            request.getParameter ( "emailComex" ) );
        emailManagement = StringUtils.parseCompleteCharacters (
            request.getParameter ( "emailManagement" ) );
        city            = StringUtils.parseCompleteCharacters ( request.getParameter ( "city" ) );
        businessType    = StringUtils.parseCompleteCharacters (
            request.getParameter ( "businessType" ) );
        UserSignInRequest usrtemp = new UserSignInRequest ( );
        usrtemp.setComexEmailAddress ( emailComex );
        usrtemp.setManagementEmailAddress ( emailManagement );
        usrtemp.setEmailAddress ( emailAddress );
        usrtemp.setUserGkey ( usr.getGkey ( ) );
        usrtemp.setBusinessType ( businessType );
        usrtemp.setStatus ( "PENDING" );
        usrtemp.setTelephone ( telephone );
        usrtemp.setAddress ( address );
        usrtemp.setCity ( city );
        usrtemp.setFullName ( fullname );
        usrtemp = this.userSignInRequestService.add ( usrtemp , "webform" );
        String                        genericPath      = "C:/Temp/SignInRequestDocuments/";
        SimpleDateFormat              simpleDateFormat = new SimpleDateFormat (
            "yyyy-MM-dd_HHmmss" );
        String                        date             = simpleDateFormat.format ( new Date ( ) );
        String                        foldername       = "REQUEST_" + String.valueOf (
            usr.getGkey ( ) ) + "_" + date;
        boolean                       folderCreated    = (
            new File ( genericPath + foldername )
        ).mkdirs ( );
        Map < String, MultipartFile > fileMap          = request.getFileMap ( );
        Iterator                      var25            = fileMap.entrySet ( ).iterator ( );

        while ( var25.hasNext ( ) ) {
          Map.Entry < String, MultipartFile > set           = ( Map.Entry ) var25.next ( );
          MultipartFile                       multipartFile = ( MultipartFile ) set.getValue ( );
          String                              msgValidate   = this.validateFileInput (
              multipartFile );
          if ( msgValidate != null ) {
            return new ResponseEntity ( msgValidate , HttpStatus.BAD_REQUEST );
          }

          if ( folderCreated ) {
            String documentName = MessagesUtils.getBundle ( ( String ) set.getKey ( ) );
            String filename     = genericPath + foldername + "/" + documentName + "." + "pdf";

            try {
              FileOutputStream outputStream = new FileOutputStream ( filename );
              byte[]           strToBytes   = multipartFile.getBytes ( );
              outputStream.write ( strToBytes );
              outputStream.close ( );
            }
            catch ( IOException var32 ) {
              return new ResponseEntity ( var32.getMessage ( ) , HttpStatus.UNAUTHORIZED );
            }

            ArgoDocument doc = new ArgoDocument ( );
            doc.setDocumentName ( documentName );
            doc.setRelatedEntity ( "REQUEST" );
            doc.setRelatedGkey ( usrtemp.getGkey ( ) );
            doc.setDocumentType ( "pdf" );
            doc.setDocumentFilePath ( filename );
            this.argoDocumentService.add (
                doc ,
                ( ( User ) this.userService.findById ( usrtemp.getUserGkey ( ) ) ).getLogin ( )
                                         );
          }
        }
      }
    }
    catch ( Exception var33 ) {
      return new ResponseEntity ( var33.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }

    return new ResponseEntity ( "Usuario creado con exito" , HttpStatus.OK );
  }

  private
  String validateFileInput ( MultipartFile multipartFile ) {
    if ( ! multipartFile.isEmpty ( ) && multipartFile.getSize ( ) != 0L ) {
      if ( multipartFile.getSize ( ) / 1024L > 1024L ) {
        return "Archivo debe ser inferior a 1MB";
      }
      else {
        return ! multipartFile.getContentType ( ).toLowerCase ( ).equals ( "application/pdf" )
               ? "Archivo solo puede ser de formato PDF" : null;
      }
    }
    else {
      return "Archivo incorrecto o dañado";
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/{userId}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getUserById ( @PathVariable Integer userId ) throws BaseException {
    try {
      return new ResponseEntity ( this.userService.findById ( userId ) , HttpStatus.OK );
    }
    catch ( BusinessException var3 ) {
      logger.error ( var3.getMessage ( ) , var3 );
      return new ResponseEntity ( var3.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/role/{haveRol}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  Boolean haveRole ( @PathVariable String haveRol ) {
    boolean haveThisRole = false;

    try {
      List < SimpleGrantedAuthority > authorities = ( List ) SecurityContextHolder.getContext ( )
                                                                                  .getAuthentication ( )
                                                                                  .getAuthorities ( );
      if ( authorities.contains ( new SimpleGrantedAuthority ( haveRol ) ) ) {
        haveThisRole = true;
      }
    }
    catch ( Exception var4 ) {
      logger.error ( var4.getMessage ( ) , var4 );
      haveThisRole = false;
    }

    return haveThisRole;
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/onlyOneRole/{haveRol}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  Boolean onlyOneRole ( @PathVariable String haveRol ) {
    boolean haveThisRole = false;

    try {
      List < SimpleGrantedAuthority > authorities = ( List ) SecurityContextHolder.getContext ( )
                                                                                  .getAuthentication ( )
                                                                                  .getAuthorities ( );
      if ( authorities.contains ( new SimpleGrantedAuthority ( haveRol ) )
           && authorities.size ( ) == 1 ) {
        haveThisRole = true;
      }
    }
    catch ( Exception var4 ) {
      logger.error ( var4.getMessage ( ) , var4 );
      haveThisRole = false;
    }

    return haveThisRole;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/modifyUser" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > modifyUser ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String userid = ( String ) jsonObject.get ( "userid" );
      User   u      = this.userService.getUser ( userid );
      if ( u != null ) {
        u = ( User ) this.userService.createOrUpdate ( u , userid );
        return new ResponseEntity ( "Contraseña cambiada con éxito." , HttpStatus.OK );
      }
      else {
        return new ResponseEntity ( "Error en los datos" , HttpStatus.OK );
      }
    }
    catch ( BusinessException var6 ) {
      logger.error ( var6.getMessage ( ) , var6 );
      return new ResponseEntity ( var6.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/modifyUserFull" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > modifyUserFull ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String newPassword = ( String ) jsonObject.get ( "newPassword" );
      if ( ! newPassword.equals ( "" ) && newPassword != null ) {
        new User ( );
        String userid   = ( String ) jsonObject.get ( "userid" );
        User   u        = this.userService.getUser ( userid );
        String fullname = ( String ) jsonObject.get ( "fullname" );
        if ( ! fullname.equals ( "" ) && fullname != null ) {
          String password = ( String ) jsonObject.get ( "oldPassword" );
          if ( ! password.equals ( "" ) && password != null ) {
            password = PasswordManager.getMD5 ( password );
            if ( u != null ) {
              if ( u.getPassword ( ).equals ( password ) ) {
                u.setPassword ( PasswordManager.getMD5 ( newPassword ) );
                if ( ! u.getFullName ( ).equals ( fullname ) ) {
                  u.setFullName ( fullname );
                }

                u = ( User ) this.userService.createOrUpdate ( u , userid );
                return new ResponseEntity ( "Contraseña cambiada con éxito." , HttpStatus.OK );
              }
              else {
                return new ResponseEntity ( "Contraseña anterior incorrecta" , HttpStatus.OK );
              }
            }
            else {
              return new ResponseEntity ( "Error en los datos" , HttpStatus.OK );
            }
          }
          else {
            return new ResponseEntity (
                "El campo de antigua contraseña no puede estar vacío" ,
                HttpStatus.OK
            );
          }
        }
        else {
          return new ResponseEntity ( "El campo de nombre no puede estar vacío" , HttpStatus.OK );
        }
      }
      else {
        return new ResponseEntity (
            "El campo de nueva contraseña no puede estar vacío" ,
            HttpStatus.OK
        );
      }
    }
    catch ( BusinessException var9 ) {
      logger.error ( var9.getMessage ( ) , var9 );
      return new ResponseEntity ( var9.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getMyParentsUsers" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getMyParentsUsers ( ) {
    try {
      List < UserLinkDTO >     usrsDTO           = new ArrayList ( );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < LinkedUser > results = ( List ) this.linkedUserService.findByFilters (
          parametersFilters );
      Iterator            var5    = results.iterator ( );

      while ( var5.hasNext ( ) ) {
        LinkedUser  linkedUser = ( LinkedUser ) var5.next ( );
        UserLinkDTO uDTO       = new UserLinkDTO ( );
        User        u          = ( User ) this.userService.findById (
            linkedUser.getRequesterUserGkey ( ) );
        uDTO.setDocumentNbr ( u.getDocumentNbr ( ) );
        uDTO.setFullname ( u.getFullName ( ) );
        uDTO.setUserGkey ( u.getGkey ( ) );
        uDTO.setStatus ( linkedUser.getStatus ( ) );
        usrsDTO.add ( uDTO );
      }

      return new ResponseEntity ( usrsDTO , HttpStatus.OK );
    }
    catch ( BaseException var8 ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getMyChildrenUsers" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getMyChildrenUsers ( ) {
    try {
      List < UserLinkDTO >     usrsDTO           = new ArrayList ( );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < LinkedUser > results = ( List ) this.linkedUserService.findByFilters (
          parametersFilters );
      Iterator            var5    = results.iterator ( );

      while ( var5.hasNext ( ) ) {
        LinkedUser  linkedUser = ( LinkedUser ) var5.next ( );
        UserLinkDTO uDTO       = new UserLinkDTO ( );
        User        u          = ( User ) this.userService.findById (
            linkedUser.getLinkedUserGkey ( ) );
        uDTO.setDocumentNbr ( u.getDocumentNbr ( ) );
        uDTO.setFullname ( u.getFullName ( ) );
        uDTO.setUserGkey ( u.getGkey ( ) );
        uDTO.setStatus ( linkedUser.getStatus ( ) );
        usrsDTO.add ( uDTO );
      }

      return new ResponseEntity ( usrsDTO , HttpStatus.OK );
    }
    catch ( BaseException var8 ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getUsersPending" }
  )
  @ResponseBody
  public
  List < UserPendingDTO > getUsersPending ( ) throws BaseException {
    ArrayList < UserPendingDTO > pendgs            = new ArrayList ( );
    List < ConditionSimple >     parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "status" , SearchOption.EQUAL , "PENDING" ) );
    List < UserSignInRequest > results = ( List ) this.userSignInRequestService.findByFilters (
        parametersFilters );
    Iterator var5 = results.iterator ( );

    while ( var5.hasNext ( ) ) {
      UserSignInRequest req = ( UserSignInRequest ) var5.next ( );
      parametersFilters.clear ( );
      parametersFilters.add (
          new ConditionSimple ( "gkey" , SearchOption.EQUAL , req.getUserGkey ( ) ) );
      User           user = ( User ) this.userService.findEntityByFilters ( parametersFilters );
      UserPendingDTO pend = new UserPendingDTO ( );
      pend.setUserGkey ( req.getUserGkey ( ) );
      pend.setReqGkey ( req.getGkey ( ) );
      pend.setDocumentNbr ( user.getLogin ( ) );
      pend.setFullname ( req.getFullName ( ) );
      pend.setEmailAddress ( req.getEmailAddress ( ) );
      pend.setEmailComex ( req.getComexEmailAddress ( ) );
      pend.setEmailManagement ( req.getManagementEmailAddress ( ) );
      pend.setBusinessType ( req.getBusinessType ( ) );
      pend.setCity ( req.getCity ( ) );
      pend.setAddress ( req.getAddress ( ) );
      pendgs.add ( pend );
    }

    return pendgs;
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.PUT },
      value = { "/applyRegister" }
  )
  @ResponseBody
  @Transactional (
      rollbackOn = { BusinessException.class }
  )
  public
  ResponseEntity < Object > applyRegister ( @RequestBody String datos )
  throws BaseException, ParseException, MalformedURLException, RemoteException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String  action       = ( String ) jsonObject.get ( "action" );
      Integer gkey         = ( Integer ) jsonObject.get ( "gkey" );
      String  iibb         = ( String ) jsonObject.get ( "iibb" );
      String  cabaResident = ( String ) jsonObject.get ( "cabaResident" );
      String  docType      = ( String ) jsonObject.get ( "docType" );
      String  accountNbr   = ( String ) jsonObject.get ( "accountNbr" );
      if ( action != null && ! action.isEmpty ( ) && gkey != null && gkey > 0 && iibb != null
           && ! iibb.isEmpty ( ) && cabaResident != null && ! cabaResident.isEmpty ( )
           && docType != null
           && ! docType.isEmpty ( ) && accountNbr != null && ! accountNbr.isEmpty ( ) ) {
        List < ConditionSimple > parametersFilters = new ArrayList ( );
        parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL , gkey ) );
        UserSignInRequest usr =
            ( UserSignInRequest ) this.userSignInRequestService.findEntityByFilters (
            parametersFilters );
        if ( usr == null ) {
          return new ResponseEntity ( getBundle ( "error.data.missing" ) , HttpStatus.BAD_REQUEST );
        }
        else {
          usr.setStatus ( action.toUpperCase ( ) );
          String msg = "Usuario rechazado con éxito";
          if ( usr.getStatus ( ).equalsIgnoreCase ( "APPROVE" ) ) {
            msg = "Usuario aprobado con éxito";
            usr.setApprovedBy ( this.getUserContext ( ).getUsername ( ) );
            usr.setApprovedDate ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
            parametersFilters.clear ( );
            parametersFilters.add (
                new ConditionSimple ( "gkey" , SearchOption.EQUAL , usr.getUserGkey ( ) ) );
            User u = ( User ) this.userService.findEntityByFilters ( parametersFilters );
            if ( u == null ) {
              return new ResponseEntity (
                  "No se pudo aprobar la solicitud, intente mas tarde." ,
                  HttpStatus.BAD_REQUEST
              );
            }

            u.setBillingUser ( true );
            Role rol = this.roleService.getRoleByGkey ( 2 );
            u.getRoles ( ).add ( rol );
            this.userService.update ( u , this.getUserContext ( ).getUsername ( ) );
          }

          usr = ( UserSignInRequest ) this.userSignInRequestService.createOrUpdate (
              usr ,
              this.getUserContext ( ).getUsername ( )
                                                                                   );
          return new ResponseEntity ( msg , HttpStatus.OK );
        }
      }
      else {
        return new ResponseEntity ( getBundle ( "error.data.missing" ) , HttpStatus.BAD_REQUEST );
      }
    }
    catch ( ServiceException | BusinessException var15 ) {
      logger.error ( var15.getMessage ( ) , var15 );
      return new ResponseEntity ( var15.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/resetPasswordByEmail" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > resetPasswordByEmail ( @RequestBody String datos )
  throws BaseException, SQLException, ServiceException, IOException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String login = ( String ) jsonObject.get ( "login" );
      if ( login.length ( ) >= 9 && login.length ( ) <= 11 && NumberUtil.isNumeric ( login ) ) {
        List userList = this.userService.getUserByLogin ( login );
        if ( userList != null && ! userList.isEmpty ( ) ) {
          if ( userList.size ( ) > 1 ) {
            return new ResponseEntity (
                "Se encontró más de un usuario con el CUIT/CUIL ingresado. Por favor contactar "
                + "con Atención al cliente" ,
                HttpStatus.BAD_REQUEST
            );
          }
          else {
            User user = this.userService.getUser ( login );
            this.userService.resetPassword ( user , true );
            return new ResponseEntity ( "Contraseña enviada por email" , HttpStatus.OK );
          }
        }
        else {
          return new ResponseEntity (
              "No existe un usuario registrado con ese CUIT/CUIL" ,
              HttpStatus.BAD_REQUEST
          );
        }
      }
      else {
        return new ResponseEntity (
            "El CUIT/CUIL no es valido. Por favor, completar con un CUIT/CUIL correcto." ,
            HttpStatus.BAD_REQUEST
        );
      }
    }
    catch ( ParseException var7 ) {
      return new ResponseEntity ( var7.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/sendCodeForUser" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > sendCodeForUser ( @RequestBody String datos )
  throws BaseException, ServiceException, IOException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      User user = new User ( );
      user.setEmailAddress ( ( String ) jsonObject.get ( "email" ) );

      try {
        user.setDocumentNbr ( new BigInteger ( ( String ) jsonObject.get ( "documentNbr" ) ) );
      }
      catch ( NullPointerException | NumberFormatException var7 ) {
        return new ResponseEntity ( var7.getMessage ( ) , HttpStatus.BAD_REQUEST );
      }

      user = this.userService.getUser ( user );
      if ( user == null ) {
        return new ResponseEntity (
            "No existe un usuario registrado con esos datos" ,
            HttpStatus.BAD_REQUEST
        );
      }
      else if ( ! user.isLocked ( ) ) {
        return new ResponseEntity (
            "El usuario no se encuentra bloqueado, Por favor intente iniciar sesion nuevamente." ,
            HttpStatus.BAD_REQUEST
        );
      }
      else {
        this.userService.generateCodeForReactivateUser ( user );
        return new ResponseEntity ( "Codigo enviado por correo electrónico" , HttpStatus.OK );
      }
    }
    catch ( ParseException var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/confirmCode" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > confirmCode ( @RequestBody String datos )
  throws BaseException, ServiceException, IOException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;
    User       user       = new User ( );

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String email = ( String ) jsonObject.get ( "email" );
      String char1 = ( String ) jsonObject.get ( "char1" );
      String char2 = ( String ) jsonObject.get ( "char2" );
      String char3 = ( String ) jsonObject.get ( "char3" );
      String char4 = ( String ) jsonObject.get ( "char4" );
      String char5 = ( String ) jsonObject.get ( "char5" );
      String char6 = ( String ) jsonObject.get ( "char6" );
      String code  = char1 + char2 + char3 + char4 + char5 + char6;
      user.setCodeRequested ( code.toUpperCase ( ) );
      user.setEmailAddress ( email.toLowerCase ( ) );
      user = this.userService.getUser ( user );
      if ( user == null ) {
        return new ResponseEntity ( "Codigo incorrecto" , HttpStatus.BAD_REQUEST );
      }
      else {
        this.userService.reactivateUser ( user );
        this.userService.resetPassword ( user , true );
        return new ResponseEntity (
            "Su cuenta se encuentra nuevamente activa. Hemos enviado a su casilla de correo "
            + "electrónico una clave temporal para que pueda ingresar a nuestra plataforma." ,
            HttpStatus.OK
        );
      }
    }
    catch ( Exception var13 ) {
      return new ResponseEntity ( var13.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/changePassword" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > changePassword ( @RequestBody String datos )
  throws BaseException, ServiceException, IOException, SQLException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String password        = ( String ) jsonObject.get ( "password" );
      String confirmPassword = ( String ) jsonObject.get ( "confirmpassword" );
      if ( ! password.equalsIgnoreCase ( confirmPassword ) ) {
        return new ResponseEntity ( "Las contraseñas no son iguales." , HttpStatus.BAD_REQUEST );
      }
      else {
        List < ConditionSimple > parametersFilters = new ArrayList ( );
        parametersFilters.add (
            new ConditionSimple (
                "login" , SearchOption.EQUAL , this.getUserContext ( ).getUsername ( ) ) );
        User user = ( User ) this.userService.findEntityByFilters ( parametersFilters );
        if ( user == null ) {
          return new ResponseEntity ( "Error en la petición del Usuario" , HttpStatus.BAD_REQUEST );
        }
        else {
          this.userService.changePassword ( password , user );
          return new ResponseEntity ( "Contraseña cambiada con éxito." , HttpStatus.OK );
        }
      }
    }
    catch ( ParseException var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getTaxGroupsCombo" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getTaxGroupsCombo ( ) throws BaseException {
    try {
      String taxs = this.userService.buildTaxGroupsCombo ( );
      return taxs == null ? new ResponseEntity (
          "Error al cargar el listado de Tax Groups" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( taxs , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getTaxIdTypeCombo" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getTaxIdTypeCombo ( ) throws BaseException {
    try {
      String taxIdTypes = this.userService.buildTaxIdType ( );
      return taxIdTypes == null ? new ResponseEntity (
          "Error al cargar el listado de Tax Id Type" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( taxIdTypes , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getLastAccountNbr" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getLastAccountNbr ( ) throws BaseException {
    try {
      String lastNbr = this.userService.getLastAccountNbr ( );
      return lastNbr == null ? new ResponseEntity (
          "Error al cargar ultimo Account Nbr" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( lastNbr , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      headers = { "content-type=multipart/*" },
      value = { "/updateUserData" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUserData (
      MultipartHttpServletRequest request ,
      HttpServletResponse response
                                           ) throws BaseException, ParseException {
    String contactEmail = request.getParameter ( "contactEmail" );
    if ( contactEmail == null ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }
    else {
      UserSignInRequest usrtemp = new UserSignInRequest ( );
      usrtemp.setManagementEmailAddress ( contactEmail );
      usrtemp = ( UserSignInRequest ) this.userSignInRequestService.createOrUpdate (
          usrtemp ,
          this.getUserContext ( ).getUsername ( )
                                                                                   );
      Map < String, MultipartFile > fileMap       = request.getFileMap ( );
      MultipartFile                 multipartFile = null;
      String                        genericPath   = "C:/Temp/SignInRequestDocuments/";
      String                        foldername    = "REQUEST_" + String.valueOf (
          usrtemp.getGkey ( ) );
      boolean                       folderCreated = (
          new File ( genericPath + foldername )
      ).mkdirs ( );
      Iterator                      var11         = fileMap.entrySet ( ).iterator ( );

      while ( var11.hasNext ( ) ) {
        Map.Entry < String, MultipartFile > set = ( Map.Entry ) var11.next ( );
        multipartFile = ( MultipartFile ) set.getValue ( );
        String msgValidate = this.validateFileInput ( multipartFile );
        if ( msgValidate != null ) {
          return new ResponseEntity ( msgValidate , HttpStatus.BAD_REQUEST );
        }

        if ( folderCreated ) {
          String filename = genericPath + foldername + "/" + multipartFile.getOriginalFilename ( );

          try {
            FileOutputStream outputStream = new FileOutputStream ( filename );
            byte[]           strToBytes   = multipartFile.getBytes ( );
            outputStream.write ( strToBytes );
            outputStream.close ( );
          }
          catch ( IOException var16 ) {
            return new ResponseEntity ( var16.getMessage ( ) , HttpStatus.UNAUTHORIZED );
          }

          ArgoDocument doc = new ArgoDocument ( );
          doc.setRelatedEntity ( "REQUEST" );
          doc.setRelatedGkey ( usrtemp.getGkey ( ) );
          doc.setDocumentType ( "pdf" );
          doc.setDocumentFilePath ( filename );
          this.argoDocumentService.add ( doc , this.getUserContext ( ).getUsername ( ) );
        }
      }

      return new ResponseEntity (
          "Se ha generado la solicitud de modificación de datos" ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getUsers" }
  )
  @ResponseBody
  public
  List < User > getUsers ( ) throws BaseException {
    List < User > results = this.userService.findAll ( );
    return results;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getRolesByUserGkey" }
  )
  @ResponseBody
  public
  String getRolesByUserGkey ( @RequestBody String datos ) throws ParseException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String userGkey = ( String ) jsonObject.get ( "userGkey" );
      String roles    = this.userService.getRolesByUserGkey ( userGkey );
      return roles;
    }
    catch ( ParseException var6 ) {
      return null;
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateUserData" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > updateUserData ( @RequestBody String datos )
  throws ParseException, BaseException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String fullname     = ( String ) jsonObject.get ( "fullname" );
      String contactEmail = ( String ) jsonObject.get ( "contactEmail" );
      String telephone    = ( String ) jsonObject.get ( "telephone" );
      String source       = ( String ) jsonObject.get ( "source" );
      User   user         = this.userService.getUser ( this.getUserContext ( ).getUsername ( ) );
      user.setFullName ( fullname );
      user.setEmailAddress ( contactEmail );
      user.setPhoneNumber ( telephone );
      this.userService.update ( user , user.getLogin ( ) );
      if ( ! StringUtils.isNullOrEmpty ( source ) ) {
        List < String > data = new ArrayList ( );
        data.add ( "email:" + contactEmail );
        this.auditService.saveAuditTransaction ( source , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
      }

      return new ResponseEntity ( "Datos actualizados correctamente." , HttpStatus.OK );
    }
    catch ( ParseException var10 ) {
      return new ResponseEntity (
          "No se pudo actualizar la información." , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getMyChildrenUsersToComposition" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getMyChildrenUsersToComposition ( ) {
    try {
      List < UserLinkDTO >     usrsDTO           = new ArrayList ( );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < AccountabilityLinkedUser > results = ( List ) this.accountabilityLinkedUserService.findByFilters (
          parametersFilters );
      Iterator var5 = results.iterator ( );

      while ( var5.hasNext ( ) ) {
        AccountabilityLinkedUser linkedUser = ( AccountabilityLinkedUser ) var5.next ( );
        UserLinkDTO              uDTO       = new UserLinkDTO ( );
        User                     u          = ( User ) this.userService.findById (
            linkedUser.getLinkedUserGkey ( ) );
        uDTO.setDocumentNbr ( u.getDocumentNbr ( ) );
        uDTO.setFullname ( u.getFullName ( ) );
        uDTO.setUserGkey ( u.getGkey ( ) );
        uDTO.setStatus ( linkedUser.getStatus ( ) );
        usrsDTO.add ( uDTO );
      }

      return new ResponseEntity ( usrsDTO , HttpStatus.OK );
    }
    catch ( BaseException var8 ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getMyParentsUsersToComposition" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getMyParentsUsersToComposition ( ) {
    try {
      List < UserLinkDTO >     usrsDTO           = new ArrayList ( );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < AccountabilityLinkedUser > results = ( List ) this.accountabilityLinkedUserService.findByFilters (
          parametersFilters );
      Iterator var5 = results.iterator ( );

      while ( var5.hasNext ( ) ) {
        AccountabilityLinkedUser linkedUser = ( AccountabilityLinkedUser ) var5.next ( );
        UserLinkDTO              uDTO       = new UserLinkDTO ( );
        User                     u          = ( User ) this.userService.findById (
            linkedUser.getRequesterUserGkey ( ) );
        uDTO.setDocumentNbr ( u.getDocumentNbr ( ) );
        uDTO.setFullname ( u.getFullName ( ) );
        uDTO.setUserGkey ( u.getGkey ( ) );
        uDTO.setStatus ( linkedUser.getStatus ( ) );
        usrsDTO.add ( uDTO );
      }

      return new ResponseEntity ( usrsDTO , HttpStatus.OK );
    }
    catch ( BaseException var8 ) {
      return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.BAD_REQUEST );
    }
  }
}
