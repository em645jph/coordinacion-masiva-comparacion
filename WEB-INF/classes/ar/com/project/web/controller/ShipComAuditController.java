//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.service.CustomerShipmentCommunicationService;
import ar.com.project.core.service.EventService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.utils.StringUtils;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/ShipComAudit" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class ShipComAuditController extends CommonController {

  private static final Logger logger = Logger.getLogger ( ShipComAuditController.class );
  private final        String MODULE_ID_SHIP_COM = "SHIPMENT_COMMUNICATION";
  private final        String MODULE_ID_SHIP_COM_SCANNER = "SHIPMENT_COMMUNICATION_SCANNER";
  @Autowired
  CustomerShipmentCommunicationService shipComService;
  @Autowired
  EventService                         eventService;

  public
  ShipComAuditController ( ) {
  }

  @RequestMapping (
      value = { "/shipComAudit" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView shipComAudit (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                            ) {
    ModelAndView model = new ModelAndView ( "shipComAudit" );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      String moduleId = "SHIPMENT_COMMUNICATION".concat ( "," )
                                                .concat ( "SHIPMENT_COMMUNICATION_SCANNER" );
      List eventTypeEnumList = this.eventService.findActiveEventTypeEnumByModule ( moduleId );
      Map  resultMap         = new HashMap ( );
      resultMap.put ( "eventTypeEnumList" , eventTypeEnumList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refrescá la página" ,
          HttpStatus.BAD_REQUEST
      );
      var5.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findHistoryEvents" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findHistoryEvents (
      @RequestParam String prReferenceId ,
      @RequestParam String prUnitId , @RequestParam String prVesselName ,
      @RequestParam String prVoyage , @RequestParam String prEventTypeId ,
      @RequestParam String prUserId , @RequestParam String prFromDateStr ,
      @RequestParam String prToDateStr
                                              ) throws SQLException {
    if ( StringUtils.isNullOrEmpty ( prReferenceId ) && StringUtils.isNullOrEmpty ( prUnitId )
         && StringUtils.isNullOrEmpty ( prVesselName ) && StringUtils.isNullOrEmpty ( prVoyage )
         && StringUtils.isNullOrEmpty ( prEventTypeId ) && StringUtils.isNullOrEmpty ( prUserId )
         && StringUtils.isNullOrEmpty ( prFromDateStr ) && StringUtils.isNullOrEmpty (
        prToDateStr ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá al menos un campo para realizar la búsqueda" ) ,
          HttpStatus.OK
      );
    }
    else {
      String dateFormat = "yyyy-MM-dd";
      return (
                 StringUtils.isNullOrEmpty ( prFromDateStr ) || DateUtils.isValidDate (
                     prFromDateStr ,
                     dateFormat
                                                                                      )
             ) && (
                 StringUtils.isNullOrEmpty ( prToDateStr ) || DateUtils.isValidDate (
                     prToDateStr , dateFormat )
             ) ? new ResponseEntity (
          this.shipComService.getShipComHistoryEvents ( StringUtils.trimText ( prReferenceId ) ,
                                                        StringUtils.trimText ( prUnitId ) ,
                                                        prVesselName , prVoyage , prEventTypeId ,
                                                        StringUtils.trimText ( prUserId ) ,
                                                        prFromDateStr , prToDateStr
                                                      ) , HttpStatus.OK )
               : new ResponseEntity (
                   this.buildErrorResponse (
                       "Ingresá la fecha en formato " + dateFormat + ", por ejemplo: 2022-01-01" ) ,
                   HttpStatus.OK
               );
    }
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
