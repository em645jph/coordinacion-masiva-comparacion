//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.dto.BillOfLadingDTO;
import ar.com.project.core.dto.BookingDTO;
import ar.com.project.core.dto.UnitPreadvisedDTO;
import ar.com.project.core.dto.WebServiceResponseDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.WebService;
import ar.com.project.core.utils.MessagesUtils;
import com.curcico.jproject.core.exception.BaseException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/Appointment" })
public
class AppointmentController extends CommonController {

  private static final Logger logger = Logger.getLogger ( AppointmentController.class );
  @Autowired
  UserService        userService;
  @Autowired
  AppointmentService appointmentService;
  @Autowired
  WebService         webService;
  @Autowired
  AuditService       auditService;

  public
  AppointmentController ( ) {
  }

  @RequestMapping (
      value = { "/preadviseManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView preadviseManagement ( ) throws BaseException {
    logger.info ( "preadviseManagement" );
    ModelAndView m = new ModelAndView ( "preadviseManagement" );
    return m;
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findBooking/{bkg}/{lineOp}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findBooking ( @PathVariable String bkg , @PathVariable String lineOp ) {
    try {
      new BookingDTO ( );
      BookingDTO booking = this.appointmentService.findBookingByLine ( bkg , lineOp );
      return booking == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                             : new ResponseEntity ( booking , HttpStatus.OK );
    }
    catch ( Exception var4 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findPermissionByUnitGkey/{gkey}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findPermissionByUnitGkey ( @PathVariable Integer gkey ) {
    try {
      List < String > permissions = this.appointmentService.getPermissionByUnitGkey ( gkey );
      return permissions == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                                 : new ResponseEntity ( permissions , HttpStatus.OK );
    }
    catch ( Exception var3 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/preadviseUnit" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > preadviseUnit ( @RequestBody String datos )
  throws BaseException, ParseException, IOException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      new JSONObject ( );
      JSONObject jsonObject      = ( JSONObject ) jsonParser.parse ( datos );
      String     type            = ( String ) jsonObject.get ( "type" );
      Integer    unitGkey        = Integer.valueOf ( ( String ) jsonObject.get ( "formUnitGkey" ) );
      String     unitId          = ( String ) jsonObject.get ( "formUnitNbr" );
      Long       bookingGkey     = ( Long ) jsonObject.get ( "formBookingGkey" );
      String     bookingNbr      = ( String ) jsonObject.get ( "formBookingNbr" );
      Long       bookingItemGkey = ( Long ) jsonObject.get ( "formBookingItemGkey" );
      String     lineOpId        = ( String ) jsonObject.get ( "formUnitLine" );
      String     isoTypeId       = ( String ) jsonObject.get ( "formUnitSize" );
      String     temperature     = ( String ) jsonObject.get ( "formUnitTemp" );
      String     sealNbr2        = ( String ) jsonObject.get ( "formUnitSealCustoms" );
      String     tareWeight      = ( String ) jsonObject.get ( "formUnitTare" );
      String     grossWeight     = ( String ) jsonObject.get ( "formUnitWeight" );
      String     oogFront        = ( String ) jsonObject.get ( "formUnitOogFront" );
      String     oogBack         = ( String ) jsonObject.get ( "formUnitOogBack" );
      String     oogLeft         = ( String ) jsonObject.get ( "formUnitOogLeft" );
      String     oogRight        = ( String ) jsonObject.get ( "formUnitOogRight" );
      String     oogTop          = ( String ) jsonObject.get ( "formUnitOogTop" );
      String     vgmEmail        = ( String ) jsonObject.get ( "formUnitVgmEmail" );
      String     vgmCondition    = ( String ) jsonObject.get ( "formVgmCondition" );
      String[]   permsAdd        = null;
      if ( ( ( String ) jsonObject.get ( "permsADD" ) ).contains ( ";" ) ) {
        permsAdd = ( ( String ) jsonObject.get ( "permsADD" ) ).split ( ";" );
      }

      String[] permsRemove = null;
      if ( ( ( String ) jsonObject.get ( "permsREMOVE" ) ).contains ( ";" ) ) {
        permsRemove = ( ( String ) jsonObject.get ( "permsREMOVE" ) ).split ( ";" );
      }

      WebServiceResponseDTO msg = this.webService.preadviseUnit ( type , unitGkey , unitId ,
                                                                  bookingGkey ,
                                                                  bookingNbr , bookingItemGkey ,
                                                                  lineOpId , isoTypeId ,
                                                                  temperature , sealNbr2 ,
                                                                  tareWeight ,
                                                                  grossWeight , oogFront , oogBack ,
                                                                  oogLeft , oogRight , oogTop ,
                                                                  vgmCondition , vgmEmail ,
                                                                  permsAdd , permsRemove
                                                                );
      if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
        List < String > data = new ArrayList ( );
        data.add ( "unitId:" + unitId );
        data.add ( "bookingNbr:" + bookingNbr );
        data.add ( "isoTypeId:" + isoTypeId );
        data.add ( "vgmCondition:" + vgmCondition );
        data.add ( "vgmEmail:" + vgmEmail );
        data.add ( "sealNbr2:" + sealNbr2 );
        data.add ( "grossWeight:" + grossWeight );
        this.auditService.saveAuditTransaction ( "preadviseUnit" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity ( MessagesUtils.getBundle ( msg.getStatus ( ) ) , HttpStatus.OK );
      }
      else {
        return new ResponseEntity ( msg.getMessage ( ) , HttpStatus.OK );
      }
    }
    catch ( ServiceException var27 ) {
      return new ResponseEntity (
          "No se puede realizar el preaviso/actualización." ,
          HttpStatus.BAD_REQUEST
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/cancelPreadviseUnit" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelPreadviseUnit ( @RequestBody String datos )
  throws BaseException, ParseException, IOException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      new JSONObject ( );
      JSONObject            jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      Long                  unitGkey   = ( Long ) jsonObject.get ( "formUnitGkey" );
      String                unitId     = ( String ) jsonObject.get ( "formUnitNbr" );
      String                bookingNbr = ( String ) jsonObject.get ( "formBookingNbr" );
      WebServiceResponseDTO msg        = this.webService.cancelPreadviseUnit (
          unitGkey , unitId , bookingNbr );
      if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
        List < String > data = new ArrayList ( );
        data.add ( "unitGkey:" + unitGkey );
        data.add ( "unitId:" + unitId );
        data.add ( "bookingNbr:" + bookingNbr );
        this.auditService.saveAuditTransaction ( "cancelPreadviseUnit" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity ( msg.getStatus ( ) , HttpStatus.OK );
      }
      else {
        return new ResponseEntity ( msg.getMessage ( ) , HttpStatus.OK );
      }
    }
    catch ( ServiceException var9 ) {
      return new ResponseEntity ( var9.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findPreadviseUnit/{bkgkey}/{bknbr}/{unitId}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findPreadviseUnit (
      @PathVariable Long bkgkey ,
      @PathVariable String bknbr , @PathVariable String unitId
                                              )
  throws BaseException, ParseException, IOException {
    try {
      UnitPreadvisedDTO preadviseData = this.webService.getPreadviseDataByUnitGkey ( bkgkey ,
                                                                                     bknbr ,
                                                                                     unitId
                                                                                   );
      return preadviseData == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                                   : new ResponseEntity ( preadviseData , HttpStatus.OK );
    }
    catch ( Exception var5 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findBillOfLading/{unitId}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findBillOfLading ( @PathVariable String unitId ) {
    try {
      new BillOfLadingDTO ( );
      BillOfLadingDTO bl = this.appointmentService.findBillOfLadingById ( unitId );
      return bl == null ? new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK )
                        : new ResponseEntity ( bl , HttpStatus.OK );
    }
    catch ( Exception var3 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/updateApptTruckingDetails" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateApptTruckingDetails (
      @RequestParam String prApptNbrStr ,
      @RequestParam String prTruckLicense , @RequestParam String prDriverGkeyStr ,
      @RequestParam String prDriverName , @RequestParam String prDriverLicense
                                                      )
  throws IOException, SQLException {
    String error = "";
    prTruckLicense  = prTruckLicense != null ? prTruckLicense.replaceAll ( " " , "" )
                                                             .toUpperCase ( ) : "";
    prDriverName    = prDriverName != null ? prDriverName.trim ( ).toUpperCase ( ) : "";
    prDriverLicense =
        prDriverLicense != null ? prDriverLicense.replaceAll ( " " , "" ).toUpperCase ( ) : "";
    if ( prApptNbrStr == null || prApptNbrStr.isEmpty ( ) ) {
      error = "No se pudo obtener la información del turno. Refrescá la página";
    }

    if ( prTruckLicense.isEmpty ( ) || prDriverName.isEmpty ( ) || prDriverLicense.isEmpty ( ) ) {
      error = "Ingresá todos los campos obligatorios (*)";
    }

    if ( ! this.isValidTruckLicense ( prTruckLicense ) ) {
      error = "Ingresá una patente válida";
    }

    if ( ! this.isValidDriverLicense ( prDriverLicense ) ) {
      error = "Ingresá un número de licencia válido";
    }

    if ( prDriverName.length ( ) <= 5 ) {
      error = "Ingresá el nombre completo del conductor";
    }

    if ( ! error.isEmpty ( ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
    }
    else {
      String result = this.appointmentService.updateApptTruckingDetails ( prApptNbrStr ,
                                                                          prTruckLicense ,
                                                                          prDriverGkeyStr ,
                                                                          prDriverName ,
                                                                          prDriverLicense
                                                                        );
      if ( result.isEmpty ( ) ) {
        List < String > data = new ArrayList ( );
        data.add ( "apptNbr:" + prApptNbrStr );
        data.add ( "truckLicense:" + prTruckLicense );
        data.add ( "driverGkey:" + prDriverGkeyStr );
        data.add ( "driverName:" + prDriverName );
        data.add ( "driverLicense:" + prDriverLicense );
        this.auditService.saveAuditTransaction ( "updateApptTruckingDetails" , data.toString ( ) ,
                                                 this.getUserLogin ( )
                                               );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Datos de transporte actualizados" ) ,
            HttpStatus.OK
        );
      }
      else {
        return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
      }
    }
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  boolean isValidTruckLicense ( String truckLicense ) {
    return truckLicense.matches ( "([A-Z]{3}[0-9]{3})" ) || truckLicense.matches (
        "([A-Z]{2}[0-9]{3}[A-Z]{2})" );
  }

  private
  boolean isValidDriverLicense ( String truckLicense ) {
    return truckLicense.matches ( "([0-9]{8})" );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }
}
