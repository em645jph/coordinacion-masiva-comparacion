//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.MenuService;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping ( { "/Menu" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class MenuController extends CommonController {

  private static final Logger logger = Logger.getLogger ( MenuController.class );
  User user;
  @Autowired
  MenuService menuService;

  public
  MenuController ( ) {
  }

  @RequestMapping (
      value = { "/loadMenuByLogin" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  String loadMenuByLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return this.menuService.getHtmlMenuByLogin ( login );
  }

  @RequestMapping (
      value = { "/getMenuListByLogin" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getMenuListByLogin ( ) throws SQLException {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return new ResponseEntity ( this.menuService.getMenuListByLogin ( login ) , HttpStatus.OK );
  }
}
