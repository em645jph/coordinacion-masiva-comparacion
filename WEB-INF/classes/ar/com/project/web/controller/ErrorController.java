//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import com.curcico.jproject.core.exception.BaseException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes
@RequestMapping ( { "/errores" })
public
class ErrorController extends CommonController {

  private static final Logger logger = Logger.getLogger ( ErrorController.class );

  public
  ErrorController ( ) {
  }

  @RequestMapping (
      value = { "/403" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView error403 ( ) throws BaseException {
    logger.info ( "403 access denied - usuario: " + this.getUserContext ( ).getUsername ( ) );
    Map < String, String > model = new HashMap ( );
    model.put ( "mensaje" , CommonController.getBundle ( "access.denied" ) );
    logger.info ( "retorno viewName: error403" );
    return new ModelAndView ( "error403" , model );
  }

  @RequestMapping (
      value = { "/404" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView error404 ( ) throws BaseException {
    logger.info ( "404 page not found" );
    Map < String, String > model = new HashMap ( );
    model.put ( "mensaje" , CommonController.getBundle ( "page.not.found" ) );
    logger.info ( "retorno viewName: error404" );
    return new ModelAndView ( "error404" , model );
  }
}
