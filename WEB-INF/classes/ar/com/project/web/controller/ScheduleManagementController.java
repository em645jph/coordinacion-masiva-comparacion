//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.ScheduleFilter;
import ar.com.project.core.domain.ScheduleFilterDef;
import ar.com.project.core.domain.ScheduleRule;
import ar.com.project.core.domain.User;
import ar.com.project.core.dto.ScheduleFilterDefDTO;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.ScheduleFilterDefService;
import ar.com.project.core.service.ScheduleFilterService;
import ar.com.project.core.service.ScheduleRuleService;
import ar.com.project.core.utils.DateUtils;
import ar.com.project.core.worker.SchFilterDefTimeUnitEnum;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/ScheduleManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class ScheduleManagementController extends CommonController {

  private static final Logger logger              = Logger.getLogger (
      ScheduleManagementController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  ScheduleFilterDefService sfdService;
  @Autowired
  ScheduleFilterService    sfpService;
  @Autowired
  ScheduleRuleService      ruleService;
  @Autowired
  PrivilegeService         privilegeService;
  private              List   privilegeInPageList = new ArrayList ( );

  public
  ScheduleManagementController ( ) {
  }

  @RequestMapping (
      value = { "/scheduleManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView scheduleManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                                  ) {
    ModelAndView model       = new ModelAndView ( "scheduleManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.privilegeInPageList = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    this.canAddItem          = this.privilegeService.userCanAddItem ( this.privilegeInPageList );
    this.canEditItem         = this.privilegeService.userCanEditItem ( this.privilegeInPageList );
    this.canDeleteItem       = this.privilegeService.userCanDeleteItem ( this.privilegeInPageList );
    this.canRecoverItem      = this.privilegeService.userCanRecoverItem (
        this.privilegeInPageList );
    String filterActionButtonsHtml  = this.getFilterActionButtonsHtml ( );
    String filterDataTableHtml      = this.getFilterDataTableHtml ( );
    String filterParamButtonsHtml   = this.getFilterParamModalButtonsHtml ( );
    String filterParamDataTableHtml = this.getFilterParamDataTableHtml ( );
    String ruleActionButtonsHtml    = this.getRuleActionButtonsHtml ( );
    String ruleDataTableHtml        = this.getRuleDataTableHtml ( );
    model.addObject ( "filterActionButtons" , filterActionButtonsHtml );
    model.addObject ( "filterDataTable" , filterDataTableHtml );
    model.addObject ( "filterParamButtons" , filterParamButtonsHtml );
    model.addObject ( "filterParamDataTable" , filterParamDataTableHtml );
    model.addObject ( "ruleActionButtons" , ruleActionButtonsHtml );
    model.addObject ( "ruleDataTable" , ruleDataTableHtml );
    return model;
  }

  @RequestMapping (
      value = { "/loadSfdCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadSfdCombos ( ) {
    return this.loadSfdCombos ( false );
  }

  @RequestMapping (
      value = { "/loadRuleCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadRuleCombos ( ) {
    Map  map          = new HashMap ( );
    List tranTypeList = this.ruleService.getTranTypeList ( );
    List actionList   = this.ruleService.getActionList ( );
    map.put ( "tranTypeList" , tranTypeList );
    map.put ( "actionList" , actionList );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/loadSfdModalCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > loadSfdModalCombos ( ) {
    return this.loadSfdCombos ( true );
  }

  @RequestMapping (
      value = { "/findFilterDefByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findFilterDefByParam (
      @RequestParam String prId ,
      @RequestParam String prDescription , @RequestParam String prTranTypeKey ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prActionKey
                                               ) {
    List filterDefList = this.sfdService.findScheduleFilterDefByParams ( prId , prDescription ,
                                                                         prTranTypeKey ,
                                                                         DateUtils.parseDateNoTime (
                                                                             prFromDateStr ) ,
                                                                         DateUtils.parseDateNoTime (
                                                                             prToDateStr ) ,
                                                                         prActionKey
                                                                       );
    ResponseEntity response = new ResponseEntity ( filterDefList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateSchFilterDef" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateSchFilterDef (
      @RequestParam String prGkeyStr ,
      @RequestParam String prId , @RequestParam String prDescription ,
      @RequestParam String prTranTypeKey , @RequestParam String prActionKey ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prTimeUnitKey , @RequestParam String prDayKeyStr ,
      @RequestParam String prStartTime , @RequestParam String prEndTime ,
      @RequestParam String prColor , @RequestParam String prRecurrentStr
                                                    )
  throws BaseException, ParseException, SQLException {
    boolean isUpdate = prGkeyStr != null && ! prGkeyStr.isEmpty ( );
    if ( isUpdate && ! this.canEditItem || ! isUpdate && ! this.canAddItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String  error       = "";
      boolean isDayOfWeek = SchFilterDefTimeUnitEnum.DAY_OF_WEEK.getKey ( ).equals (
          prTimeUnitKey );
      if ( this.isNullOrEmpty ( prId ) ) {
        error = "Por favor ingrese un Id";
      }
      else if ( this.isNullOrEmpty ( prTranTypeKey ) ) {
        error = "Por favor selecione un tipo de transacción";
      }
      else if ( this.isNullOrEmpty ( prActionKey ) ) {
        error = "Por favor selecione una acción para el filtro";
      }
      else if ( this.isNullOrEmpty ( prFromDateStr ) ) {
        error = "Por favor seleccione una fecha para el campo \"Válido desde\"";
      }
      else if ( this.isNullOrEmpty ( prTimeUnitKey ) ) {
        error = "Por favor selecione una unidad de tiempo";
      }
      else if ( isDayOfWeek && this.isNullOrEmpty ( prDayKeyStr ) ) {
        error = "Por favor selecione día de la semana";
      }
      else if ( this.isNullOrEmpty ( prStartTime ) || this.isNullOrEmpty ( prEndTime ) ) {
        error = "Por favor ingrese la hora de inicio y fin";
      }

      if ( ! this.isNullOrEmpty ( error ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
      }
      else {
        prDayKeyStr = isDayOfWeek ? prDayKeyStr : "";
        String userLogin = this.getUserLogin ( );
        String result = this.sfdService.addOrUpdateSchFilterDef ( prGkeyStr , prId , prDescription ,
                                                                  prTranTypeKey , prActionKey ,
                                                                  prFromDateStr , prToDateStr ,
                                                                  prTimeUnitKey , prDayKeyStr ,
                                                                  prStartTime , prEndTime ,
                                                                  prColor , prRecurrentStr ,
                                                                  userLogin , isUpdate
                                                                );
        return ! result.isEmpty ( ) ? new ResponseEntity (
            this.buildErrorResponse ( result ) ,
            HttpStatus.OK
        )
                                    : new ResponseEntity (
                                        this.buildSuccessResponse ( "Filtro guardado con éxito" ) ,
                                        HttpStatus.OK
                                    );
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getFilterDef" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getFilterDef ( @RequestParam Integer prFilterDefGkey )
  throws BaseException, ParseException, SQLException {
    ScheduleFilterDefDTO sfdDto = this.sfdService.getScheduleFilterDefDtoByGkey ( prFilterDefGkey );
    return sfdDto != null ? new ResponseEntity ( sfdDto , HttpStatus.OK ) : new ResponseEntity (
        this.buildErrorResponse (
            "No se pudo cargar la información del filtro. Por favor refresque la página" ) ,
        HttpStatus.BAD_REQUEST
    );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getFilterParam" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getFilterParam ( @RequestParam Integer prFilterDefGkey )
  throws BaseException, ParseException, SQLException {
    List filterParamList = this.sfpService.findScheduleFilterByParams ( ( String ) null ,
                                                                        ( String ) null ,
                                                                        prFilterDefGkey
                                                                      );
    List paramTypeList = this.sfpService.getParamTypeList ( );
    Map  map           = new HashMap ( );
    map.put ( "filterParamList" , filterParamList );
    map.put ( "paramTypeList" , paramTypeList );
    map.put ( "filterDefGkey" , prFilterDefGkey );
    return new ResponseEntity ( map , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateSchFilterParam" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateSchFilterParam (
      @RequestParam String prFilterDefGkeyStr ,
      @RequestParam String prTypeKey , @RequestParam String prValue
                                                      )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem && ! this.canEditItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String error = "";
      if ( this.isNullOrEmpty ( prFilterDefGkeyStr ) ) {
        error = "No se pudo obtener el filtro relacionado. Por favor refresque la página";
      }

      if ( this.isNullOrEmpty ( prTypeKey ) ) {
        error = "Por favor seleccione un tipo de parámetro";
      }
      else if ( this.isNullOrEmpty ( prValue ) ) {
        error = "Por favor ingrese un valor para el parámetro";
      }

      if ( ! this.isNullOrEmpty ( error ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
      }
      else {
        String  userLogin     = this.getUserLogin ( );
        Integer filterDefGkey = Integer.valueOf ( prFilterDefGkeyStr );
        String result = this.sfpService.addScheduleFilter ( prTypeKey , prValue , filterDefGkey ,
                                                            userLogin
                                                          );
        if ( ! result.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          Map resultMap = this.buildSuccessResponse ( "Parámetro guardado con éxito" );
          List filterParamList = this.sfpService.findScheduleFilterByParams ( ( String ) null ,
                                                                              ( String ) null ,
                                                                              filterDefGkey
                                                                            );
          resultMap.put ( "filterParamList" , filterParamList );
          return new ResponseEntity ( resultMap , HttpStatus.OK );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteFilterDef" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteFilterDef ( @RequestBody ScheduleFilterDef filterDef )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      this.sfdService.deleteScheduleFilterDef ( filterDef , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Filtro eliminado con éxito" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteFilterParam" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteFilterParam ( @RequestBody ScheduleFilter filterParam )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      this.sfpService.deleteScheduleFilter ( filterParam , this.getUserLogin ( ) );
      Map resultMap = this.buildSuccessResponse ( "Parámetro eliminado con éxito" );
      List filterParamList = this.sfpService.findScheduleFilterByParams ( ( String ) null ,
                                                                          ( String ) null ,
                                                                          filterParam.getFilterDefGkey ( )
                                                                        );
      resultMap.put ( "filterParamList" , filterParamList );
      return new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getFilterCalendarDates" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getFilterCalendarDates ( )
  throws BaseException, ParseException, SQLException {
    return new ResponseEntity ( this.sfdService.getDefaultDateMap ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getFilterCalendarEvent" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getFilterCalendarEvent ( @RequestBody Object filterDefObject )
  throws BaseException, ParseException {
    List calendarEventList = this.sfdService.buildFilterCalendarFromViewModel (
        ( List ) filterDefObject );
    return new ResponseEntity ( calendarEventList , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/findRuleByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findRuleByParam (
      @RequestParam String prId ,
      @RequestParam String prDescription , @RequestParam String prTranTypeKey ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prActionKey
                                          ) {
    List ruleList = this.ruleService.findScheduleRuleByParams ( prId , prDescription ,
                                                                prTranTypeKey ,
                                                                DateUtils.parseDateNoTime (
                                                                    prFromDateStr ) ,
                                                                DateUtils.parseDateNoTime (
                                                                    prToDateStr ) ,
                                                                prActionKey
                                                              );
    ResponseEntity response = new ResponseEntity ( ruleList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateScheduleRule" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateScheduleRule (
      @RequestParam String prGkeyStr ,
      @RequestParam String prId , @RequestParam String prDescription ,
      @RequestParam String prTranTypeKey , @RequestParam String prActionKey ,
      @RequestParam String prDaysStr , @RequestParam String prTimeStr
                                                    )
  throws BaseException, ParseException, SQLException {
    if ( ! this.canAddItem && ! this.canEditItem ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
    else {
      String error = "";
      if ( this.isNullOrEmpty ( prTranTypeKey ) ) {
        error = "Por favor seleccione un tipo de transacción";
      }
      else if ( this.isNullOrEmpty ( prActionKey ) ) {
        error = "Por favor seleccione una acción";
      }
      else if ( this.isNullOrEmpty ( prDaysStr ) ) {
        error = "Por favor ingrese un valor para la cantidad de días";
      }
      else if ( ! NumberUtils.isNumber ( prDaysStr ) ) {
        error = "Por favor ingrese un valor válido para la cantidad de días";
      }
      else if ( this.isNullOrEmpty ( prTimeStr ) ) {
        error = "Por favor ingrese la hora";
      }

      if ( ! this.isNullOrEmpty ( error ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( error ) , HttpStatus.OK );
      }
      else {
        String  userLogin = this.getUserLogin ( );
        boolean isUpdate  = prGkeyStr != null && ! prGkeyStr.isEmpty ( );
        Integer days      = Integer.valueOf ( prDaysStr );
        String result = this.ruleService.addOrUpdateScheduleRule ( prGkeyStr , prId ,
                                                                   prDescription ,
                                                                   prTranTypeKey , prActionKey ,
                                                                   days , prTimeStr , userLogin ,
                                                                   isUpdate
                                                                 );
        if ( ! result.isEmpty ( ) ) {
          return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
        }
        else {
          Map resultMap = this.buildSuccessResponse ( "Regla guardada con éxito" );
          return new ResponseEntity ( resultMap , HttpStatus.OK );
        }
      }
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getRule" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getRule ( @RequestParam Integer prRuleGkey )
  throws BaseException, ParseException, SQLException {
    ScheduleRule rule = this.ruleService.getScheduleRuleByGkey ( prRuleGkey );
    return rule != null ? new ResponseEntity ( rule , HttpStatus.OK ) : new ResponseEntity (
        this.buildErrorResponse (
            "No se pudo cargar la información de la regla. Por favor refresque la página" ) ,
        HttpStatus.BAD_REQUEST
    );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteRule" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteRule ( @RequestBody ScheduleRule scheduleRule )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      this.ruleService.deleteScheduleRule ( scheduleRule , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Regla eliminada con éxito" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getFilterActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnAddFilterDef\">Agregar</button>&nbsp;&nbsp;" );
    }

    if ( sb.length ( ) > 0 ) {
      sb.append ( "\n" );
    }

    sb.append (
        "<button type=\"button\" class=\"btn btn-info\" "
        + "id=\"btnViewFilterCalendar\">Graficar</button>" );
    return sb.toString ( );
  }

  private
  String getRuleActionButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnAddRule\">Agregar</button>&nbsp;&nbsp;" );
    }

    return sb.toString ( );
  }

  private
  String getFilterParamModalButtonsHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    if ( this.canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnSaveSfp\">Agregar</button>" );
    }

    if ( sb.length ( ) > 0 ) {
      sb.append ( "\n" );
    }

    sb.append (
        "<button type=\"button\" class=\"btn btn-dark\" id=\"btnCleanSfp\">Limpiar</button>" );
    return sb.toString ( );
  }

  private
  String getFilterDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Descripción</th>\n" );
    sb.append ( "    <th>Transacción</th>\n" );
    sb.append ( "    <th>Acción</th>\n" );
    sb.append ( "    <th>Válido desde</th>\n" );
    sb.append ( "    <th>Válido hasta</th>\n" );
    sb.append ( "    <th>Hora Inicio</th>\n" );
    sb.append ( "    <th>Hora Fin</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    sb.append ( "    <th>Parámetros</th>\n" );
    sb.append ( "    <th></th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: scheduleViewModel.filterList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: tranTypeDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: actionDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: startDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: endDateStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: startTime\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: endTime\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changedStr\"></label></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-default btn-sm\" data-bind=\"click: $parent"
        + ".editFilterParam\"> <i class=\"ti-view-list\"></i> </button></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent.editFilterDef\"> <i class=\"ti-pencil-alt\"></i> </button></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent.deleteFilterDef\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getFilterParamDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Tipo</th>\n" );
    sb.append ( "    <th>Valor</th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: scheduleViewModel.filterParamList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: type\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: value\"></label></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent.deleteFilterParam\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getRuleDataTableHtml ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Descripción</th>\n" );
    sb.append ( "    <th>Transacción</th>\n" );
    sb.append ( "    <th>Acción</th>\n" );
    sb.append ( "    <th>Días</th>\n" );
    sb.append ( "    <th>Hora</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    sb.append ( "    <th></th>\n" );
    if ( this.canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: scheduleViewModel.ruleList\">\n" );
    sb.append ( "<tr>\n" );
    sb.append ( "    <td><label data-bind=\"text: id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: tranTypeDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: actionDesc\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: days\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: time\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changedStr\"></label></td>\n" );
    sb.append (
        "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent.editRule\"> <i class=\"ti-pencil-alt\"></i> </button></td>\n" );
    if ( this.canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent.deleteRule\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  ResponseEntity < Object > loadSfdCombos ( boolean modal ) {
    Map  map          = new HashMap ( );
    List tranTypeList = this.sfdService.getTranTypeList ( );
    List actionList   = this.sfdService.getActionList ( );
    List timeUnitList = this.sfdService.getTimeUnitList ( );
    List dayList      = this.sfdService.getDayList ( );
    map.put ( "tranTypeList" , tranTypeList );
    map.put ( "actionList" , actionList );
    if ( modal ) {
      map.put ( "timeUnitList" , timeUnitList );
      map.put ( "dayList" , dayList );
    }

    return new ResponseEntity ( map , HttpStatus.OK );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }

  private
  boolean isNullOrEmpty ( String str ) {
    return str == null || str.isEmpty ( );
  }
}
