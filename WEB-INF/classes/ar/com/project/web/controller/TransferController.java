//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.domain.Transfer;
import ar.com.project.core.domain.User;
import ar.com.project.core.dto.WebServiceResponseDTO;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.InvoiceService;
import ar.com.project.core.service.TransferService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.WebConnectorService;
import ar.com.project.core.utils.MessagesUtils;
import ar.com.project.core.worker.Constants;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/Transfer" })
public
class TransferController extends CommonController {

  private static final Logger logger = Logger.getLogger ( TransferController.class );
  @Autowired
  TransferService     transferService;
  @Autowired
  UserService         userService;
  @Autowired
  InvoiceService      invoiceService;
  @Autowired
  WebConnectorService webConnectorService;
  @Autowired
  AuditService        auditService;

  public
  TransferController ( ) {
  }

  @RequestMapping (
      value = { "/generateTransferPay" },
      method = { RequestMethod.POST }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > generateTransferPay ( @RequestBody String datos ) throws IOException {
    JSONParser jsonParser = new JSONParser ( );
    JSONObject jsonObject = null;

    try {
      WebServiceResponseDTO    resp              = null;
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add (
          new ConditionSimple (
              "login" , SearchOption.EQUAL , this.getUserContext ( ).getUsername ( ) ) );
      User u = ( User ) this.userService.findEntityByFilters ( parametersFilters );
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      Integer draftnbr = Integer.valueOf ( ( String ) (
          ( JSONArray ) jsonObject.get ( "T1" )
      ).get ( 5 ) );

      for ( int i = 1 ; i <= jsonObject.size ( ) ; ++ i ) {
        JSONArray j         = ( JSONArray ) jsonObject.get ( "T" + i );
        String    amountStr = j.get ( 2 ).toString ( );
        amountStr = amountStr != null && ! amountStr.isEmpty ( ) ? amountStr.replaceAll ( "," , "" )
                                                                            .replace ( "$" , "" )
                                                                 : "0.0";
        Double   amount = Double.parseDouble ( amountStr );
        Transfer transf = new Transfer ( );
        transf.setBankName ( ( String ) j.get ( 0 ) );
        transf.setReferenceNbr ( ( String ) j.get ( 1 ) );
        transf.setAmount ( amount );
        transf.setPaymentDate ( ( String ) j.get ( 3 ) + " 00:00:00" );
        transf.setDraftGkey ( Integer.valueOf ( j.get ( 5 ).toString ( ) ) );
        transf.setUserEmail ( u.getEmailAddress ( ) );
        transf.setUserId ( u );
        transf.setStatus ( "PENDIENTE" );
        transf.setLifeCycleState ( "ACT" );

        try {
          String ws = this.webConnectorService.buildXmlToCall (
              this.transferService.buildTransferXml ( transf , draftnbr ) );
          resp = this.webConnectorService.executeWebService ( ws , Constants.ARGO_SERVICE_URL_N4B );
          if ( resp == null || ! resp.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
            return new ResponseEntity (
                MessagesUtils.getBundle ( "FAILED_TO_SEND_PAYMENT" ) ,
                HttpStatus.BAD_REQUEST
            );
          }

          List < String > data = new ArrayList ( );
          data.add ( "draftGkey:" + transf.getDraftGkey ( ) );
          data.add ( "paymentDate:" + transf.getPaymentDate ( ) );
          data.add ( "useremail:" + transf.getUserEmail ( ) );
          data.add ( "amount:" + transf.getAmount ( ) );
          this.auditService.saveAuditTransaction ( "generateTransferPay" , data.toString ( ) ,
                                                   this.getUserContext ( ).getUsername ( )
                                                 );
        }
        catch ( ServiceException var15 ) {
          return new ResponseEntity (
              MessagesUtils.getBundle ( "FAILED_TO_SEND_PAYMENT" ) ,
              HttpStatus.BAD_REQUEST
          );
        }
      }

      return new ResponseEntity (
          MessagesUtils.getBundle ( "TRANSFER_SENDED_OK" ) , HttpStatus.OK );
    }
    catch ( ParseException var16 ) {
      return new ResponseEntity ( var16.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
    catch ( BaseException var17 ) {
      return new ResponseEntity ( var17.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }
}
