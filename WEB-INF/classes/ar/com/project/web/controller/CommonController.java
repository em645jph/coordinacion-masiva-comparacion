//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import com.curcico.jproject.core.daos.ConditionEntry;
import com.curcico.jproject.core.exception.BaseException;
import com.curcico.jproject.core.utils.ConditionsUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller
public abstract
class CommonController {

  private static ResourceBundle resourceBundle;
  protected      Logger         logger    = Logger.getLogger ( this.getClass ( ) );
  protected      String         logPrefix = "";

  public
  CommonController ( ) {
  }

  protected static
  String getBundle ( String key ) {
    try {
      return getResourceBundle ( ).getString ( key );
    }
    catch ( MissingResourceException var2 ) {
      if ( key != null && ! key.isEmpty ( ) ) {
        return key.replace ( '.' , ' ' );
      }
      else {
        throw var2;
      }
    }
  }

  protected static
  ResourceBundle getResourceBundle ( ) {
    if ( resourceBundle == null ) {
      resourceBundle = ResourceBundle.getBundle ( "messages" );
    }

    return resourceBundle;
  }

  protected static
  String getParametersToURL ( Map < String, Object > map ) {
    String result = "'";
    if ( map != null ) {
      result = result + "?";
      boolean isFirst = false;

      Map.Entry e;
      for (
          Iterator var4 = map.entrySet ( ).iterator ( ) ; var4.hasNext ( ) ;
          result = result + ( String ) e.getKey ( ) + "=" + e.getValue ( )
      ) {
        e = ( Map.Entry ) var4.next ( );
        if ( ! isFirst ) {
          result = result + "&";
        }
        else {
          isFirst = true;
        }
      }
    }

    return result + "'";
  }

  protected
  User getUserContext ( ) {
    User user = ( User ) SecurityContextHolder.getContext ( ).getAuthentication ( )
                                              .getPrincipal ( );
    return user;
  }

  protected
  List < ConditionEntry > transformFilters ( Class < ? > clase , String filters )
  throws BaseException {
    return ConditionsUtils.transformFilters ( clase , filters );
  }

  protected
  List < ConditionEntry > transformFiltersComplex ( Class < ? > clase , String filters )
  throws Exception {
    return ConditionsUtils.transformFiltersComplex ( clase , filters );
  }

  protected
  List < Object > transformJsonToList ( Class < ? > clase , String field , String data )
  throws Exception {
    return ConditionsUtils.transformJsonToList ( clase , field , data );
  }

  public
  String getLogPrefix ( ) {
    return this.logPrefix;
  }

  public
  void setLogPrefix ( String logPrefix ) {
    this.logPrefix = logPrefix;
  }

  public
  void clearLogPrefix ( ) {
    this.logPrefix = "";
  }

  public
  void logUser ( String msg ) {
    this.logger.info ( this.logPrefix + msg );
  }

  public
  ModelAndView redirectTo404 ( ) {
    return new ModelAndView ( "error404" );
  }
}
