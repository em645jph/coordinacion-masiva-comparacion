//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.ParameterService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.utils.StringUtils;
import com.curcico.jproject.core.exception.BaseException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Scope ( "session")
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter" ,
    "email" , "PhoneNumber"
}
)
public
class LoginController extends CommonController {

  private static final Logger logger = Logger.getLogger ( LoginController.class );
  User user;
  @Autowired
  UserService      userSrv;
  @Autowired
  ParameterService parameterService;
  @Autowired
  AuditService     auditService;

  public
  LoginController ( ) {
  }

  @RequestMapping (
      value = { "/index" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  String showInicio (
      @ModelAttribute ( "userid") String userid ,
      @ModelAttribute ( "loginName") String loginName ,
      @ModelAttribute ( "username") String username ,
      @ModelAttribute ( "fullname") String fullname , ModelMap modelMap
                    ) throws BaseException {
    logger.info ( "showInicio()" );
    String message = "";
    org.springframework.security.core.userdetails.User u =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String firstNameLetter    = "";
    String emailAdress        = null;
    String showInicioResponse = null;
    String phoneNumber        = null;
    if ( ! loginName.equals ( u.getUsername ( ) ) ) {
      try {
        username        = "";
        this.user       = this.userSrv.getUser ( u.getUsername ( ) );
        username        = this.user.toString ( );
        userid          = this.user.getLogin ( );
        loginName       = this.user.getLogin ( );
        fullname        = this.user.getFullName ( );
        firstNameLetter = fullname != null && ! fullname.isEmpty ( ) ? fullname.substring ( 0 , 1 )
                                                                     : "";
        emailAdress     = this.user.getEmailAddress ( );
        phoneNumber     = this.user.getPhoneNumber ( );
      }
      catch ( Exception var13 ) {
        logger.error ( "Error al buscar el usuario" , var13 );
      }
    }

    List < SimpleGrantedAuthority > authorities = ( List ) SecurityContextHolder.getContext ( )
                                                                                .getAuthentication ( )
                                                                                .getAuthorities ( );
    modelMap.addAttribute ( "userid" , userid );
    modelMap.addAttribute ( "username" , username );
    modelMap.addAttribute ( "message" , message );
    modelMap.addAttribute ( "authorities" , authorities );
    modelMap.addAttribute ( "fullname" , StringUtils.isNullOrEmpty ( fullname ) ? "0" : fullname );
    modelMap.addAttribute ( "firstNameLetter" , firstNameLetter );
    modelMap.addAttribute (
        "email" , StringUtils.isNullOrEmpty ( emailAdress ) ? "0" : emailAdress );
    modelMap.addAttribute (
        "PhoneNumber" ,
        StringUtils.isNullOrEmpty ( phoneNumber ) ? "0" : phoneNumber
                          );
    return this.user != null && this.user.isChange ( ) ? "changePassword" : "index";
  }

  public
  String redirectTo ( ModelMap map , String urlPage ) {
    return urlPage;
  }

  @RequestMapping (
      value = { "/login" },
      method = { RequestMethod.GET }
  )
  public
  String login (
      ModelMap model , @RequestParam ( value = "error", required = false) String error ,
      @RequestParam ( value = "logout", required = false) String logout
               ) {
    logger.info ( "login()" );
    if ( error != null ) {
      if ( error.equals ( "SESSION" ) ) {
        model.addAttribute ( "error" , getBundle ( "access.session" ) );
      }
      else {
        model.addAttribute ( "error" , getBundle ( "access.error" ) );
      }
    }

    if ( logout != null ) {
      model.addAttribute ( "msg" , getBundle ( "access.logged" ) );
    }

    return "login";
  }

  @RequestMapping (
      value = { "/" },
      method = { RequestMethod.GET }
  )
  public
  String showHome (
      @ModelAttribute ( "userid") String userid ,
      @ModelAttribute ( "fullname") String fullname
                  ) {
    try {
      if ( SecurityContextHolder.getContext ( ).getAuthentication ( )
                                .getPrincipal ( ) instanceof String ) {
        return "login";
      }

      org.springframework.security.core.userdetails.User u =
          ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
          .getContext ( )
          .getAuthentication ( ).getPrincipal ( );
      if ( u != null && u.isEnabled ( ) ) {
        return "index";
      }
    }
    catch ( Exception var4 ) {
      logger.error ( var4.getMessage ( ) , var4 );
    }

    return "login";
  }

  @RequestMapping (
      value = { "/logout" },
      method = { RequestMethod.GET }
  )
  public
  String logout (
      @ModelAttribute ( "userid") String userid ,
      @ModelAttribute ( "fullname") String fullname , ModelMap model
                ) throws BaseException {
    return "login";
  }

  @RequestMapping (
      value = { "/help" },
      method = { RequestMethod.GET }
  )
  public
  String showHelp ( @ModelAttribute ( "userid") String userid ) {
    logger.info ( "showHelp()" );
    return "help";
  }

  @RequestMapping (
      value = { "/resetPassword" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView resetPassword ( ) throws BaseException {
    logger.info ( "resetPassword" );
    return new ModelAndView ( "resetPassword" );
  }

  @RequestMapping (
      value = { "/reactivateAccount" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView reactivateAccount ( ) throws BaseException {
    logger.info ( "reactivateAccount" );
    return new ModelAndView ( "reactivateAccount" );
  }

  @RequestMapping (
      value = { "/confirmCode" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView confirmCode ( ) throws BaseException {
    logger.info ( "confirmCode" );
    return new ModelAndView ( "confirmCode" );
  }

  @RequestMapping (
      value = { "/userRegister" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView userRegister ( ) throws BaseException {
    logger.info ( "userRegister" );
    return new ModelAndView ( "userRegister" );
  }

  @RequestMapping (
      value = { "/changePassword" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView changePassword ( ) throws BaseException {
    logger.info ( "changePassword" );
    return new ModelAndView ( "changePassword" );
  }

  @RequestMapping (
      value = { "/updateMail" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView updateMail ( ) throws BaseException {
    logger.info ( "Start updateMail" );
    return new ModelAndView ( "updateMail" );
  }

  @ModelAttribute ( "username")
  public
  String getUsername ( ) {
    return "";
  }

  @ModelAttribute ( "userid")
  public
  String getUserid ( ) {
    return "";
  }

  @ModelAttribute ( "loginName")
  public
  String getLoginName ( ) {
    return "";
  }

  @ModelAttribute ( "parameters")
  public
  String getParameters ( ) {
    return "";
  }

  @ModelAttribute ( "fullname")
  public
  String getFullname ( ) {
    return "";
  }
}
