//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


package com.example.restservice.controllers;

import ar.com.project.core.dto.ScannerTwoGateSrvDTO;
import ar.com.project.core.dto.UnitShipmenCommunicationParameterDTO;
import ar.com.project.core.service.CustomerScannerService;
import ar.com.project.core.service.EventService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import com.curcico.jproject.core.exception.BaseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
@RequestMapping ( { "/scannerGateTwoManagement" })
public
class ScannerGateTwoManagementController extends CommonController {

  private static final Logger logger              = Logger.getLogger (
      DocumentalCutoffController.class );
  private final        String PRIVILEGE_ID_VIEW   = "SCANNER_GATE_TWO_VIEW";
  private final        String PRIVILEGE_ID_UNLOCK = "SCANNER_UNLOCK";
  @Autowired
  RoleService            roleService;
  @Autowired
  UserService            userService;
  @Autowired
  CustomerScannerService customerScannerService;
  @Autowired
  EventService           eventService;
  @Autowired
  PrivilegeService       privilegeService;

  public
  ScannerGateTwoManagementController ( ) {
  }

  @RequestMapping (
      value = { "/scannerGateTwo" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView scannerGateTwo ( HttpServletRequest request ) throws BaseException {
    logger.info ( "scannerGateTwo" );
    List privilegeList = this.getUserPrivilegeList ( request );
    if ( ! this.privilegeService.userCanViewPage ( privilegeList , "SCANNER_GATE_TWO_VIEW" ) ) {
      return this.redirectTo404 ( );
    }
    else {
      ModelAndView m           = new ModelAndView ( "scannerGateTwo" );
      String       servletPath = request.getServletPath ( );
      User loggedUser = ( User ) SecurityContextHolder.getContext ( ).getAuthentication ( )
                                                      .getPrincipal ( );
      String login = loggedUser.getUsername ( );
      return m;
    }
  }

  @RequestMapping (
      value = { "/getAllContainerBlockByScanner" },
      method = { RequestMethod.GET }
  )
  public
  ResponseEntity < Object > getAllContainerBlockByScannerList ( ) {
    new ArrayList ( );

    ResponseEntity responseEntity;
    try {
      List < ScannerTwoGateSrvDTO > containerBlockByScannerList =
          this.customerScannerService.getAllContainerBlockByScannerList ( );
      responseEntity = new ResponseEntity ( containerBlockByScannerList , HttpStatus.OK );
    }
    catch ( SQLException var4 ) {
      responseEntity = new ResponseEntity (
          "Error a intentar acceder a los servicios. Por favor refresque la página o contacte al "
          + "administrador de sistemas." ,
          HttpStatus.OK
      );
    }

    return responseEntity;
  }

  @RequestMapping (
      value = { "/SaveNoveltyByScanner" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  public
  ResponseEntity < Object > unLockContainerByScanner (
      @RequestBody UnitShipmenCommunicationParameterDTO unitShipmenCommunicationParameter
                                                     ) {
    ResponseEntity responseEntity;
    try {
      String response = this.customerScannerService.auditUnitWithNovelty (
          unitShipmenCommunicationParameter.getGkey ( ) ,
          unitShipmenCommunicationParameter.getId ( ) ,
          unitShipmenCommunicationParameter.getReferenceId ( ) ,
          unitShipmenCommunicationParameter.getObVessel ( ) ,
          unitShipmenCommunicationParameter.getAction ( ) ,
          unitShipmenCommunicationParameter.getNotes ( ) , this.getUserLogin ( )
                                                                         );
      responseEntity = new ResponseEntity ( response , HttpStatus.OK );
    }
    catch ( BaseException var4 ) {
      responseEntity = new ResponseEntity ( var4.getMessage ( ) , HttpStatus.OK );
    }

    return responseEntity;
  }

  @RequestMapping (
      value = { "/customsUnblockScanner" },
      method = { RequestMethod.POST }
  )
  @ResponseBody
  @PreAuthorize ( "isAuthenticated()")
  public
  ResponseEntity < Object > customsUnblockScanner (
      HttpServletRequest request ,
      @RequestBody UnitShipmenCommunicationParameterDTO unitShipmenCommunicationParameter
                                                  ) {
    List   privilegeList = this.getUserPrivilegeList ( request );
    String userId        = this.getUserLogin ( );

    ResponseEntity responseEntity;
    try {
      String updateResult = this.customerScannerService.updateStatusUnitShipmentComunication (
          unitShipmenCommunicationParameter , privilegeList , userId , "SCANNER_UNLOCK" );
      responseEntity = new ResponseEntity ( updateResult , HttpStatus.OK );
    }
    catch ( IOException var7 ) {
      var7.printStackTrace ( );
      responseEntity = new ResponseEntity ( var7.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
    }
    catch ( BaseException var8 ) {
      responseEntity = new ResponseEntity ( var8.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
      var8.printStackTrace ( );
    }
    catch ( SQLException var9 ) {
      var9.printStackTrace ( );
      responseEntity = new ResponseEntity ( var9.getMessage ( ) , HttpStatus.EXPECTATION_FAILED );
    }

    return responseEntity;
  }

  private
  String getUserLogin ( ) {
    return this.getUserContext ( ).getUsername ( );
  }

  private
  List getUserPrivilegeList ( HttpServletRequest request ) {
    String servletPath = request.getServletPath ( );
    String login       = this.getUserLogin ( );
    return this.privilegeService.getMenuPagePrivileges ( login , servletPath );
  }
}
