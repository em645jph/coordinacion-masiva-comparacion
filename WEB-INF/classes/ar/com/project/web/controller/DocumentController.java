//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.ArgoDocument;
import ar.com.project.core.dto.UnitDocumentDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.ArgoDocumentService;
import ar.com.project.core.service.BillOfLadingService;
import ar.com.project.core.service.BookingService;
import ar.com.project.core.service.DocumentService;
import ar.com.project.core.service.UnitService;
import ar.com.project.core.worker.UnitCategoryEnum;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.io.Files;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "userid" , "parameters" , "firstname" , "lastname" })
@RequestMapping ( { "/Document" })
public
class DocumentController extends CommonController {

  private static final Logger logger          = Logger.getLogger ( DocumentController.class );
  private static final String APPLICATION_PDF = "application/x-download";
  @Autowired
  ArgoDocumentService argoDocumentService;
  @Autowired
  DocumentService     documentService;
  @Autowired
  BookingService      bookingService;
  @Autowired
  BillOfLadingService billOfLadingService;
  @Autowired
  UnitService         unitService;
  @Autowired
  ServletContext      context;
  @Autowired
  AppointmentService  appointmentService;

  public
  DocumentController ( ) {
  }

  @RequestMapping (
      value = { "/documentsView" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView documentsView ( ) throws BaseException {
    logger.info ( "documentsView" );
    return new ModelAndView ( "documents" );
  }

  @RequestMapping (
      value = { "/otherDocsView" },
      method = { RequestMethod.GET }
  )
  public
  ModelAndView otherDocsView ( ) throws BaseException {
    logger.info ( "otherdocs" );
    return new ModelAndView ( "otherdocs" );
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getGatepassByType" },
      produces = { "application/x-download" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  Object getGatepassByType (
      HttpServletRequest request , HttpServletResponse response ,
      @RequestBody String datos
                           ) throws JRException, SQLException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String  category    = ( String ) jsonObject.get ( "formCategory" );
      String  units       = "";
      boolean isStrgeAppt = "STRGE".equals ( category ) || "APPT".equals ( category );
      JSONArray jsonArray = isStrgeAppt ? ( JSONArray ) jsonObject.get ( "formAppt" )
                                        : ( JSONArray ) jsonObject.get ( "formUnits" );
      String arrayValue;
      if ( jsonArray != null ) {
        for ( int i = 0 ; i < jsonArray.size ( ) ; ++ i ) {
          units      = units + ( units.isEmpty ( ) ? "" : "," );
          arrayValue = jsonArray.get ( i ).toString ( );
          units      = units + ( isStrgeAppt ? arrayValue : "'" + arrayValue + "'" );
        }
      }

      File gatepass = this.documentService.getGatepassByType ( category , units );
      arrayValue = Base64.getEncoder ( ).encodeToString ( Files.toByteArray ( gatepass ) );
      return arrayValue;
    }
    catch ( IOException var12 ) {
      System.out.println ( "Error:- " + var12.getMessage ( ) );
      return "";
    }
  }

  @JsonIgnore
  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/getDocumentEntregaExp" },
      produces = { "application/x-download" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  String getDocumentEntregaExp (
      HttpServletRequest request , HttpServletResponse response ,
      @RequestBody String datos
                               ) throws JRException, SQLException, ParseException {
    try {
      File gatepass = this.documentService.getDocumentEntregaExp ( datos );
      return Base64.getEncoder ( ).encodeToString ( Files.toByteArray ( gatepass ) );
    }
    catch ( IOException var5 ) {
      return null;
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/findUnits/{entity}/{value}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnits ( @PathVariable String entity , @PathVariable String value ) {
    try {
      List units;
      label59:
      {
        units = null;
        value = value.replaceAll ( "\n" , "" ).replaceAll ( "\t" , "" ).replaceAll ( "\r" , "" )
                     .replaceAll ( " " , "" ).toUpperCase ( );
        switch ( entity ) {
          case "BK":
            units = this.bookingService.findUnitsByBooking ( value );
            break label59;
          case "BL":
            units = this.billOfLadingService.findUnitsByBl ( value );
            break label59;
          case "APPT":
            units = this.unitService.findPumApptToDoc ( value );
            break label59;
          case "UNIT":
            units = this.unitService.findUnitsToDocs ( value );
            break label59;
          case "STRGE":
            units = this.unitService.findStorageUnitToDoc ( value );
            break label59;
        }

        return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
      }

      if ( units == null ) {
        return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
      }
      else {
        Map resultMap = new HashMap ( );
        resultMap.put ( "unitList" , units );
        String tempMsg = "";

        for ( int i = 0 ; i < units.size ( ) ; ++ i ) {
          UnitDocumentDTO unitDocDto = ( UnitDocumentDTO ) units.get ( i );
          if ( UnitCategoryEnum.EXPRT.getKey ( ).equals ( unitDocDto.getCategory ( ) )
               && ! unitDocDto.getHasTruckingDetails ( ) ) {
            tempMsg = "Completá los datos del transportista para poder generar el pase de puerta"
                      + ".<br><br> Hacé click en el ícono <i class=\"ti-pencil-alt\" "
                      + "style=\"color:#f01313;\"></i> para proceder.";
            resultMap.put ( "tempMsg" , tempMsg );
            break;
          }
        }

        return new ResponseEntity ( resultMap , HttpStatus.OK );
      }
    }
    catch ( Exception var9 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getDocumentsByRegisterId/{registerGkey}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getDocumentsByRegisterId ( @PathVariable String registerGkey ) {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add (
          new ConditionSimple (
              "relatedGkey" , SearchOption.EQUAL , Integer.valueOf ( registerGkey ) ) );
      Collection < ArgoDocument > docs = this.argoDocumentService.findByFilters (
          parametersFilters );
      if ( docs != null && docs.size ( ) != 0 ) {
        StringBuilder sb = new StringBuilder ( );
        sb.append ( "<div class=\"list-group\">" );
        Iterator var6 = docs.iterator ( );

        while ( var6.hasNext ( ) ) {
          ArgoDocument argoDocument = ( ArgoDocument ) var6.next ( );
          sb.append (
              "<button onclick=\"loadDocumentFrame(this.id)\" id=\"" + argoDocument.getGkey ( )
              + "\" url=\"" + argoDocument.getDocumentFilePath ( )
              + "\" class=\"list-group-item list-group-item-action active text-center\">"
              + argoDocument.getDocumentName ( ) + "</button><br>" );
        }

        sb.append ( "</div>" );
        return new ResponseEntity ( sb , HttpStatus.OK );
      }
      else {
        return new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
      }
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( "Error al cargar el listado" , HttpStatus.CONFLICT );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getServerDocumentByKey/{dockey}" }
  )
  public
  ResponseEntity < InputStreamResource > getServerDocumentByKey ( @PathVariable Integer dockey ) {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL , dockey ) );
      ArgoDocument doc = ( ArgoDocument ) this.argoDocumentService.findEntityByFilters (
          parametersFilters );
      File        file        = new File ( doc.getDocumentFilePath ( ) );
      HttpHeaders respHeaders = new HttpHeaders ( );
      MediaType   mediaType   = MediaType.parseMediaType ( "application/pdf" );
      respHeaders.setContentType ( mediaType );
      respHeaders.setContentLength ( file.length ( ) );
      InputStreamResource isr = new InputStreamResource ( new FileInputStream ( file ) );
      return new ResponseEntity ( isr , respHeaders , HttpStatus.OK );
    }
    catch ( Exception var8 ) {
      String message = "Error en el download del file: " + var8.getMessage ( );
      logger.error ( message , var8 );
      return new ResponseEntity ( HttpStatus.INTERNAL_SERVER_ERROR );
    }
  }
}
