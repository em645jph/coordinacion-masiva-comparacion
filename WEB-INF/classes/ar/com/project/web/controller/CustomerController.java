//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.AccountabilityLinkedUser;
import ar.com.project.core.domain.LinkedUser;
import ar.com.project.core.domain.User;
import ar.com.project.core.domain.UserSignInRequest;
import ar.com.project.core.dto.BalanceDTO;
import ar.com.project.core.dto.BasicCustomerDTO;
import ar.com.project.core.dto.WebServiceResponseDTO;
import ar.com.project.core.mail.SimpleEmailManager;
import ar.com.project.core.service.AccountabilityLinkedUserService;
import ar.com.project.core.service.ArgoDocumentService;
import ar.com.project.core.service.BalanceService;
import ar.com.project.core.service.DatabaseConnectorService;
import ar.com.project.core.service.LinkedUserService;
import ar.com.project.core.service.QueriesService;
import ar.com.project.core.service.RoleService;
import ar.com.project.core.service.UserService;
import ar.com.project.core.service.UserSignInRequestService;
import ar.com.project.core.service.WebService;
import com.curcico.jproject.core.daos.ConditionSimple;
import com.curcico.jproject.core.daos.SearchOption;
import com.curcico.jproject.core.exception.BaseException;
import com.curcico.jproject.core.exception.BusinessException;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes ( { "parameters" , "rolCC" })
@RequestMapping ( { "/Customer" })
public
class CustomerController extends CommonController {

  private static final Logger logger = Logger.getLogger ( CustomerController.class );
  @Autowired
  UserService                     userService;
  @Autowired
  UserSignInRequestService        userSignInRequestService;
  @Autowired
  ArgoDocumentController          argoDocument;
  @Autowired
  DatabaseConnectorService        databaseConnectorService;
  @Autowired
  QueriesService                  queriesService;
  @Autowired
  ArgoDocumentService             argoDocumentService;
  @Autowired
  RoleService                     roleService;
  @Autowired
  SimpleEmailManager              emailManager;
  @Autowired
  LinkedUserService               linkedUserService;
  @Autowired
  WebService                      webService;
  @Autowired
  BalanceService                  balanceService;
  @Autowired
  AccountabilityLinkedUserService accountabilityLinkedUserService;

  public
  CustomerController ( ) {
  }

  public static
  String getFormByCode ( int code ) {
    switch ( code ) {
      case 1:
        return buildFormForDigitalPort ( ) + buildFormForN4Billing ( );
      case 2:
        return buildFormForDigitalPort ( );
      case 3:
        return buildFormForN4Billing ( );
      case 4:
        return buildFormForComexEmail ( );
      case 5:
        return buildFormComex ( );
      default:
        return "";
    }
  }

  private static
  String buildFormComex ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<div id=\"divFormComex\">" );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"fullname\">Razon Social/Nombre</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" id=\"fullname\" "
        + "name=\"fullname\" required=\"\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"email\">Correo electr&oacute;nico de Contacto</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"email\" required id=\"emailContact\" "
        + "name=\"emailContact\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"telephone\">Tel&eacute;fono</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" required id=\"telephone\" "
        + "name=\"telephone\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "</div>" );
    return sb.toString ( );
  }

  private static
  String buildFormForDigitalPort ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<div id=\"divFormBasic\">" );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"fullname\">Razon Social/Nombre</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" id=\"fullname\" "
        + "name=\"fullname\" maxlength=\"80\" required=\"\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"address\">Ubicaci&oacute;n f&iacute;sica</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" required id=\"address\" "
        + "name=\"address\"  maxlength=\"60\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"email\">Correo electr&oacute;nico de Contacto</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"email\" required id=\"emailAddress\" "
        + "name=\"emailAddress\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"telephone\">Tel&eacute;fono</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" required id=\"telephone\" "
        + "name=\"telephone\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "</div>" );
    return sb.toString ( );
  }

  private static
  String buildFormForN4Billing ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<div id=\"divFormBilling\">" );
    sb.append ( "   \t<div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append (
        "            <label for=\"commercialEmailAddress\">Correo Responsable ComEx.</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"email\" required name=\"emailComex\" "
        + "id=\"emailComex\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"emailContact\">Correo de Facturación</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"email\" required "
        + "name=\"emailManagement\" id=\"emailManagement\"  maxlength=\"80\" placeholder=\".."
        + ".\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"city\">Ciudad</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" required name=\"city\" "
        + "id=\"city\"  maxlength=\"30\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"businessType\">Tipo de Actividad</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"text\" required name=\"businessType\" "
        + "id=\"businessType\"  maxlength=\"50\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "    " );
    sb.append ( "    <div class=\"form-group m-b-20\">" );
    sb.append ( "        <div class=\"col-12\">" );
    sb.append ( "            <label for=\"membCardFile\">Carta Membretada</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"file\" head=\"Carta Membretada\" "
        + "required id=\"membCardFile\" name=\"membCardFile\" placeholder=\"...\">" );
    sb.append (
        "            <small>Para descargar un ejemplo de Carta Membretada, haga clic <a "
        + "style=\"color:#fd7e14;\" href=\"https://www.apmterminals"
        + ".com/en/buenos-aires/practical-information/-/media/americas/Buenos-Aires/practical"
        + "-information/carta-membretada-puerto-digital\" target=\"_blank\" "
        + "download=\"\">aquí</a></small>" );
    sb.append ( "            <label for=\"constanciaFile\">Constancia CUIT/CUIL</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"file\" head=\"Constancia CUIT/CUIL\" "
        + "required id=\"constanciaFile\" name=\"constanciaFile\" placeholder=\"...\">" );
    sb.append ( "            <label for=\"iibbFile\">Ingresos Brutos</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"file\" head=\"Ingresos Brutos\" "
        + "required id=\"iibbFile\" name=\"iibbFile\" placeholder=\"...\">" );
    sb.append ( "        </div>" );
    sb.append ( "    </div>" );
    sb.append ( "</div>" );
    return sb.toString ( );
  }

  private static
  String buildFormForComexEmail ( ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<div class=\"form-group m-b-20\" id=\"divFormComexEmail\">" );
    sb.append ( "    <div class=\"col-12\">" );
    sb.append ( "        <label for=\"commercialEmailAddress\">Correo Responsable ComEx.</label>" );
    sb.append (
        "        <input class=\"form-control\" type=\"email\" required name=\"emailComex\" "
        + "id=\"emailComex\" placeholder=\"...\">" );
    sb.append ( "    </div>" );
    sb.append ( "    <div class=\"col-12\">" );
    sb.append ( "            <label for=\"membCardFile\">Carta Membretada</label>" );
    sb.append (
        "            <input class=\"form-control\" type=\"file\" head=\"Carta Membretada\" "
        + "required id=\"membCardFile\" name=\"membCardFile\" placeholder=\"...\">" );
    sb.append ( "    </div>" );
    sb.append ( "</div>" );
    return sb.toString ( );
  }

  @RequestMapping (
      value = { "/userRegisterPendings" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView userRegisterPendings ( @ModelAttribute ( "userid") String userid )
  throws BaseException {
    logger.info ( "userRegisterPendings" );
    return new ModelAndView ( "userRegisterPendings" );
  }

  @RequestMapping (
      value = { "/myReportsView" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView myReportsView ( @ModelAttribute ( "userid") String userid ) throws BaseException {
    logger.info ( "myReportsView" );
    return new ModelAndView ( "myReportsView" );
  }

  @RequestMapping (
      value = { "/allCustomerReportsView" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView allCustomerReportsView ( @ModelAttribute ( "userid") String userid )
  throws BaseException {
    logger.info ( "allCustomerReportsView" );
    return new ModelAndView ( "allCustomerReportsView" );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUserBeforeRegister/{userId}" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > findUserBeforeRegister ( @PathVariable String userId )
  throws BaseException {
    try {
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "login" , SearchOption.EQUAL , userId ) );
      User u = ( User ) this.userService.findEntityByFilters ( parametersFilters );
      return u != null ? new ResponseEntity ( "Usuario ya existe en sistema." , HttpStatus.OK )
                       : new ResponseEntity ( ( MultiValueMap ) null , HttpStatus.OK );
    }
    catch ( BusinessException var4 ) {
      return new ResponseEntity ( var4.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/loadUserInformation" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  String loadUserInformation ( ) {
    org.springframework.security.core.userdetails.User u =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    return this.userService.getBasicInformationByUserId ( u.getUsername ( ) );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getTaxGroupsCombo" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getTaxGroupsCombo ( ) throws BaseException {
    try {
      String taxs = this.userService.buildTaxGroupsCombo ( );
      return taxs == null ? new ResponseEntity (
          "Error al cargar el listado de Tax Groups" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( taxs , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getTaxIdTypeCombo" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getTaxIdTypeCombo ( ) throws BaseException {
    try {
      String taxIdTypes = this.userService.buildTaxIdType ( );
      return taxIdTypes == null ? new ResponseEntity (
          "Error al cargar el listado de Tax Id Type" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( taxIdTypes , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getLastAccountNbr" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getLastAccountNbr ( ) throws BaseException {
    try {
      String lastNbr = this.userService.getLastAccountNbr ( );
      return lastNbr == null ? new ResponseEntity (
          "Error al cargar ultimo Account Nbr" ,
          HttpStatus.BAD_REQUEST
      ) : new ResponseEntity ( lastNbr , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/manageUserRequest" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > manageUserRequest ( @RequestBody String datos ) {
    String resp = "No se pudo ejecutar la acción solicitada";

    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      Integer                  requestGkey       = Integer.valueOf (
          ( String ) jsonObject.get ( "requestGkey" ) );
      String                   reason            = ( String ) jsonObject.get ( "reason" );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL , requestGkey ) );
      UserSignInRequest req =
          ( UserSignInRequest ) this.userSignInRequestService.findEntityByFilters (
          parametersFilters );
      switch ( action ) {
        case "REJECT":
          resp = this.rejectUserRequest ( req , reason ) ? "Solicitud rechazada correctamente"
                                                         : "No se pudo rechazar la solicitud";
          this.emailManager.sendMail ( req.getManagementEmailAddress ( ) , "Rechazo de solicitud" ,
                                       "Solicitud rechazada por motivo: " + reason
                                     );
      }
    }
    catch ( Exception var11 ) {
      return new ResponseEntity ( var11.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }

    return new ResponseEntity ( resp , HttpStatus.OK );
  }

  private
  boolean rejectUserRequest ( UserSignInRequest req , String reason )
  throws BaseException, ServiceException, IOException {
    if ( req == null ) {
      return false;
    }
    else {
      req.setStatus ( "REJECT" );
      req.setChanged ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
      req.setChanger ( this.getUserContext ( ).getUsername ( ) );
      req.setNotes ( reason != null && ! reason.isEmpty ( ) ? reason : "Sin especificar motivo" );
      req = ( UserSignInRequest ) this.userSignInRequestService.createOrUpdate (
          req ,
          this.getUserContext ( ).getUsername ( )
                                                                               );
      return true;
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addLinkUser" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > addLinkUser ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String  cuitLink     = ( String ) jsonObject.get ( "cuitLink" );
      String  type         = ( String ) jsonObject.get ( "type" );
      boolean isAggregated = false;
      if ( type.equalsIgnoreCase ( "BILLING" ) ) {
        isAggregated = this.addUserBilling ( cuitLink );
      }
      else if ( type.equalsIgnoreCase ( "COMPOST" ) ) {
        isAggregated = this.addUserComposition ( cuitLink );
      }

      return isAggregated ? new ResponseEntity ( "Usuario agregado correctamente." , HttpStatus.OK )
                          : new ResponseEntity (
                              "Usuario no pudo ser agregado." , HttpStatus.CONFLICT );
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( var7.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  boolean addUserBilling ( String cuitLink ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "login" , SearchOption.EQUAL , cuitLink ) );
    User usr = ( User ) this.userService.findEntityByFilters ( parametersFilters );
    if ( usr == null ) {
      return false;
    }
    else {
      LinkedUser link = new LinkedUser ( );
      link.setLinkedUserGkey ( usr.getGkey ( ) );
      link.setRequesterUserGkey (
          this.userService.getUser ( this.getUserContext ( ).getUsername ( ) ).getGkey ( ) );
      link.setStatus ( "APPROVE" );
      link.setCreated ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
      link.setCreator ( this.getUserContext ( ).getUsername ( ) );
      link.setLifeCycleState ( "ACT" );
      link = ( LinkedUser ) this.linkedUserService.createOrUpdate (
          link ,
          this.getUserContext ( ).getUsername ( )
                                                                  );
      return true;
    }
  }

  private
  boolean addUserComposition ( String cuitLink ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add ( new ConditionSimple ( "login" , SearchOption.EQUAL , cuitLink ) );
    User usr = ( User ) this.userService.findEntityByFilters ( parametersFilters );
    if ( usr == null ) {
      return false;
    }
    else {
      AccountabilityLinkedUser link = new AccountabilityLinkedUser ( );
      link.setLinkedUserGkey ( usr.getGkey ( ) );
      link.setRequesterUserGkey (
          this.userService.getUser ( this.getUserContext ( ).getUsername ( ) ).getGkey ( ) );
      link.setStatus ( "APPROVE" );
      link.setType ( "ESTADO_CUENTA" );
      link.setCreated ( new Timestamp ( Calendar.getInstance ( ).getTimeInMillis ( ) ) );
      link.setCreator ( this.getUserContext ( ).getUsername ( ) );
      link.setLifeCycleState ( "ACT" );
      link = ( AccountabilityLinkedUser ) this.accountabilityLinkedUserService.createOrUpdate (
          link ,
          this.getUserContext ( ).getUsername ( )
                                                                                              );
      return true;
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/findLinkUser" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > findLinkUser ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      String cuitLink = ( String ) jsonObject.get ( "cuitLink" );
      if ( cuitLink.equalsIgnoreCase ( this.getUserContext ( ).getUsername ( ) ) ) {
        return new ResponseEntity (
            "Usted no puede agregarse a si mismo" , HttpStatus.BAD_REQUEST );
      }
      else {
        List < ConditionSimple > parametersFilters = new ArrayList ( );
        parametersFilters.add ( new ConditionSimple ( "login" , SearchOption.EQUAL , cuitLink ) );
        User usr = ( User ) this.userService.findEntityByFilters ( parametersFilters );
        if ( usr == null ) {
          return new ResponseEntity ( "CUIT/CUIL no existe" , HttpStatus.BAD_REQUEST );
        }
        else {
          parametersFilters.clear ( );
          parametersFilters.add (
              new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL , usr.getGkey ( ) ) );
          parametersFilters.add ( new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL ,
                                                        this.userService.getUser (
                                                            this.getUserContext ( )
                                                                .getUsername ( ) ).getGkey ( )
          ) );
          LinkedUser link = ( LinkedUser ) this.linkedUserService.findEntityByFilters (
              parametersFilters );
          return link != null ? new ResponseEntity (
              "Ya se encuentra agregado a la lista de cuentas vinculadas" , HttpStatus.BAD_REQUEST )
                              : new ResponseEntity ( usr , HttpStatus.OK );
        }
      }
    }
    catch ( Exception var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/removeLinkUser" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > removeLinkUser ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      Long    gkey      = ( Long ) jsonObject.get ( "gkey" );
      String  type      = ( String ) jsonObject.get ( "type" );
      boolean isDeleted = false;
      if ( type.equalsIgnoreCase ( "BILLING" ) ) {
        isDeleted = this.removeBillingLinkedUser ( gkey );
      }
      else if ( type.equalsIgnoreCase ( "COMPOST" ) ) {
        isDeleted = this.removeCompositionLinkedUser ( gkey );
      }

      return isDeleted ? new ResponseEntity (
          "Usuario desvinculado correctamente." , HttpStatus.OK )
                       : new ResponseEntity (
                           "No se pudo eliminar la vinculación" , HttpStatus.CONFLICT );
    }
    catch ( Exception var7 ) {
      return new ResponseEntity ( var7.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  boolean removeBillingLinkedUser ( Long gkey ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add (
        new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL , gkey.intValue ( ) ) );
    parametersFilters.add ( new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL ,
                                                  this.userService.getUser (
                                                          this.getUserContext ( ).getUsername ( ) )
                                                                  .getGkey ( )
    ) );
    LinkedUser link = ( LinkedUser ) this.linkedUserService.findEntityByFilters (
        parametersFilters );
    if ( link == null ) {
      return false;
    }
    else {
      this.linkedUserService.delete ( link , this.getUserContext ( ).getUsername ( ) );
      return true;
    }
  }

  private
  boolean removeCompositionLinkedUser ( Long gkey ) throws BaseException {
    List < ConditionSimple > parametersFilters = new ArrayList ( );
    parametersFilters.add (
        new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL , gkey.intValue ( ) ) );
    parametersFilters.add ( new ConditionSimple ( "requesterUserGkey" , SearchOption.EQUAL ,
                                                  this.userService.getUser (
                                                          this.getUserContext ( ).getUsername ( ) )
                                                                  .getGkey ( )
    ) );
    AccountabilityLinkedUser link =
        ( AccountabilityLinkedUser ) this.accountabilityLinkedUserService.findEntityByFilters (
        parametersFilters );
    if ( link == null ) {
      return false;
    }
    else {
      this.accountabilityLinkedUserService.delete (
          link , this.getUserContext ( ).getUsername ( ) );
      return true;
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getBalanceAccount" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getBalanceAccount ( ) throws BaseException {
    try {
      ArrayList < String >     linkedCuits       = new ArrayList ( );
      List < ConditionSimple > parametersFilters = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "linkedUserGkey" , SearchOption.EQUAL ,
                                                    this.userService.getUser (
                                                            this.getUserContext ( ).getUsername ( ) )
                                                                    .getGkey ( )
      ) );
      List < LinkedUser > results = ( List ) this.linkedUserService.findByFilters (
          parametersFilters );

      for ( int i = 0 ; i < results.size ( ) ; ++ i ) {
        parametersFilters.clear ( );
        parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL ,
                                                      (
                                                          ( LinkedUser ) results.get ( i )
                                                      ).getRequesterUserGkey ( )
        ) );
        linkedCuits.add (
            ( ( User ) this.userService.findEntityByFilters ( parametersFilters ) ).getLogin ( ) );
      }

      List < BalanceDTO > balances = this.userService.getCustomerBalanceByTaxIds ( linkedCuits );
      return new ResponseEntity ( balances , HttpStatus.OK );
    }
    catch ( Exception var5 ) {
      return new ResponseEntity ( var5.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/approveRequest" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > approveRequest ( @RequestBody String datos )
  throws BaseException, ParseException {
    try {
      JSONParser jsonParser = new JSONParser ( );
      JSONObject jsonObject = null;
      jsonObject = ( JSONObject ) jsonParser.parse ( datos );
      Integer                  reqGkey                = Integer.valueOf (
          ( String ) jsonObject.get ( "gkey" ) );
      String                   fullName               = ( String ) jsonObject.get ( "full_name" );
      String                   billingAccountNbr      = this.userService.getLastAccountNbr ( );
      String                   billingCreditStatus    = "CASH";
      boolean                  billingIsCabaResident  = ( String ) jsonObject.get (
          "billingIsCabaResident" ) == "1";
      String                   billingTaxGroupId      = ( String ) jsonObject.get (
          "billingTaxGroupId" );
      String                   billingTaxIdType       = ( String ) jsonObject.get (
          "billingTaxIdType" );
      String                   businessType           = ( String ) jsonObject.get (
          "businessType" );
      String                   city                   = ( String ) jsonObject.get ( "city" );
      String                   comexEmailAddress      = ( String ) jsonObject.get (
          "comexEmailAddress" );
      String                   managementEmailAddress = ( String ) jsonObject.get (
          "managementEmailAddress" );
      String                   notes                  = ( String ) jsonObject.get ( "notes" );
      List < ConditionSimple > parametersFilters      = new ArrayList ( );
      parametersFilters.add ( new ConditionSimple ( "gkey" , SearchOption.EQUAL , reqGkey ) );
      UserSignInRequest req =
          ( UserSignInRequest ) this.userSignInRequestService.findEntityByFilters (
          parametersFilters );
      if ( req == null ) {
        return new ResponseEntity ( "Error al encontrar la solicitud" , HttpStatus.BAD_REQUEST );
      }
      else {
        req.setFullName ( fullName );
        req.setBillingAccountNbr ( new BigInteger ( billingAccountNbr ) );
        req.setBillingCreditStatus ( billingCreditStatus );
        req.setBillingId ( this.getNextCustomerRandomId ( ) );
        req.setBillingIsCabaResident ( billingIsCabaResident );
        req.setBillingTaxGroupId ( billingTaxGroupId );
        req.setBillingTaxIdType ( billingTaxIdType );
        req.setBusinessType ( businessType == null ? "" : businessType );
        req.setCity ( city == null ? "" : city );
        req.setComexEmailAddress ( comexEmailAddress == null ? "" : comexEmailAddress );
        req.setManagementEmailAddress (
            managementEmailAddress == null ? "" : managementEmailAddress );
        req.setNotes ( notes == null ? "" : notes );
        parametersFilters.clear ( );
        parametersFilters.add (
            new ConditionSimple ( "gkey" , SearchOption.EQUAL , req.getUserGkey ( ) ) );
        User    usr            = ( User ) this.userService.findEntityByFilters (
            parametersFilters );
        boolean existsCustomer = this.userService.findCustomerExists ( usr.getLogin ( ) );
        if ( existsCustomer ) {
          requestType = "UPDATE_CUSTOMER";
        }

        WebServiceResponseDTO msg = null;
        switch ( requestType ) {
          case "UPDATE_CUSTOMER":
            System.out.println ( "req = " + req.toString ( ) );
            msg = this.webService.updateCustomer ( req , usr );
            break;
          case "CREATE_CUSTOMER":
            msg = this.webService.createCustomer ( req , usr );
        }

        if ( ( msg == null || ! msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) && ! msg
            .getMessage ( )
            .contains ( "EMPTY_RESPONSE" ) ) {
          return new ResponseEntity ( "No se pudo crear el Customer" , HttpStatus.BAD_REQUEST );
        }
        else {
          usr.setFullName ( req.getFullName ( ) );
          usr.setAddress ( req.getAddress ( ) );
          usr.setManagementEmailAddress ( req.getManagementEmailAddress ( ) );
          usr.setPhoneNumber ( req.getTelephone ( ) );
          usr.setEmailAddress ( req.getEmailAddress ( ) );
          usr.setComexEmailAddress ( req.getComexEmailAddress ( ) );
          usr.setCity ( req.getCity ( ) );
          usr.setBusinessType ( req.getBusinessType ( ) );
          usr.setBillingUser ( true );
          usr = this.userService.update ( usr , this.getUserContext ( ).getUsername ( ) );
          req.setStatus ( "APPROVE" );
          req = ( UserSignInRequest ) this.userSignInRequestService.createOrUpdate (
              req ,
              this.getUserContext ( ).getUsername ( )
                                                                                   );
          this.emailManager.sendMail ( usr.getManagementEmailAddress ( ) ,
                                       "Aprobacion de Solicitud" ,
                                       "Estimado Cliente su solicitud ha sido aprobada. Ya puede operar en Puerto Digital."
                                     );
          return new ResponseEntity ( "Aprobación correcta." , HttpStatus.OK );
        }
      }
    }
    catch ( Exception var23 ) {
      return new ResponseEntity ( var23.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }

  private
  String getNextCustomerRandomId ( ) {
    String generatedString = "";

    boolean existsCode;
    do {
      generatedString = RandomStringUtils.randomAlphanumeric ( 5 );
      existsCode      = this.userService.findCustomerCodeExists ( generatedString );
    }
    while ( ! existsCode );

    return generatedString.toUpperCase ( );
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getAllCustomers" }
  )
  @ResponseBody
  public
  ResponseEntity < Object > getAllCustomers ( ) throws BaseException {
    try {
      List < BasicCustomerDTO > customers = this.balanceService.getAllCustomersExistsBalance ( );
      return new ResponseEntity ( customers , HttpStatus.OK );
    }
    catch ( Exception var2 ) {
      return new ResponseEntity ( var2.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }
}
