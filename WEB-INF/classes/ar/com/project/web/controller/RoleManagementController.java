//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.Role;
import ar.com.project.core.domain.User;
import ar.com.project.core.dto.RoleDTO;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.RoleService;
import com.curcico.jproject.core.exception.BaseException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/RoleManagement" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class RoleManagementController extends CommonController {

  private static final Logger logger              = Logger.getLogger (
      RoleManagementController.class );
  boolean canAddItem     = false;
  boolean canEditItem    = false;
  boolean canDeleteItem  = false;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  RoleService      roleService;
  @Autowired
  PrivilegeService privilegeService;
  private              List   privilegeInPageList = new ArrayList ( );

  public
  RoleManagementController ( ) {
  }

  @RequestMapping (
      value = { "/roleManagement" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView roleManagement (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                              ) {
    ModelAndView model       = new ModelAndView ( "roleManagement" );
    String       servletPath = request.getServletPath ( );
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    this.privilegeInPageList = this.privilegeService.getMenuPagePrivileges ( login , servletPath );
    this.canAddItem          = this.privilegeService.userCanAddItem ( this.privilegeInPageList );
    this.canEditItem         = this.privilegeService.userCanEditItem ( this.privilegeInPageList );
    this.canDeleteItem       = this.privilegeService.userCanDeleteItem ( this.privilegeInPageList );
    this.canRecoverItem      = this.privilegeService.userCanRecoverItem (
        this.privilegeInPageList );
    String actionButtonsHtml = this.getActionButtonsHtml ( this.canAddItem , this.canDeleteItem ,
                                                           this.canRecoverItem
                                                         );
    String dataTableHtml = this.getDataTableHtml ( this.canEditItem , this.canDeleteItem ,
                                                   this.canRecoverItem
                                                 );
    String rolePrivilegeDataTableHtml =
        ! this.canAddItem && ! this.canEditItem ? this.getPrivilegeDataTableHtml ( true ) : "";
    boolean viewOnly               = ! this.canAddItem && ! this.canEditItem;
    String  privilegeDataTableHtml = this.getPrivilegeDataTableHtml ( viewOnly );
    String  modalButtons           = this.getModalButtonsHtml (
        this.canAddItem , this.canDeleteItem );
    model.addObject ( "actionButtons" , actionButtonsHtml );
    model.addObject ( "dataTable" , dataTableHtml );
    model.addObject ( "rolePrivilegeDataTable" , rolePrivilegeDataTableHtml );
    model.addObject ( "privilegeDataTable" , privilegeDataTableHtml );
    model.addObject ( "modalButtons" , modalButtons );
    return model;
  }

  @RequestMapping (
      value = { "/getPrivilegeList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getPrivilegeList ( ) {
    ResponseEntity response;
    try {
      List privilegeList = this.privilegeService.getActivePrivilegeList ( );
      response = new ResponseEntity ( privilegeList , HttpStatus.OK );
    }
    catch ( BaseException var3 ) {
      var3.printStackTrace ( );
      response = new ResponseEntity (
          "No se pudo cargar la lista de privilegios. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
    }

    return response;
  }

  @RequestMapping (
      value = { "/getPrivilegeDtoList" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > getPrivilegeDtoList ( ) {
    List           privilegeList = this.privilegeService.findActivePriviligeDtoList ( );
    ResponseEntity response      = new ResponseEntity ( privilegeList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      value = { "/findRoleByParam" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > findRoleByParam (
      @RequestParam String prId ,
      @RequestParam String prDescription , @RequestParam String prPrivilegeGkeyStr ,
      @RequestParam String prFromDateStr , @RequestParam String prToDateStr ,
      @RequestParam String prLifeCycleState
                                          ) {
    logger.debug ( "step 1" );
    List roleList = this.roleService.findRoleByParams ( prId , prDescription , prPrivilegeGkeyStr ,
                                                        prFromDateStr , prToDateStr ,
                                                        prLifeCycleState
                                                      );
    ResponseEntity response = new ResponseEntity ( roleList , HttpStatus.OK );
    return response;
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/addOrUpdateRole" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addOrUpdateRole (
      @RequestParam String prGkey ,
      @RequestParam String prId , @RequestParam String prDescription , String prPrivilegeGkeyStr
                                            )
  throws BaseException, ParseException, SQLException {
    boolean isUpdating = prGkey != null && ! prGkey.isEmpty ( );
    if ( ( ! isUpdating || this.canEditItem ) && ( isUpdating || this.canAddItem ) ) {
      String userLogin = this.getUserLogin ( );
      String result =
          isUpdating ? this.roleService.updateRole ( prGkey , prId , prDescription , userLogin )
                     : this.roleService.addRole ( prId , prDescription , userLogin );
      if ( ! result.isEmpty ( ) ) {
        return new ResponseEntity ( this.buildErrorResponse ( result ) , HttpStatus.OK );
      }
      else {
        if ( prPrivilegeGkeyStr != null ) {
          this.roleService.updateRolePrivilegeList ( prGkey , prId ,
                                                     ( List ) (
                                                         prPrivilegeGkeyStr.isEmpty ( )
                                                         ? new ArrayList ( )
                                                         : Arrays.asList (
                                                             prPrivilegeGkeyStr.split ( "," ) )
                                                     ) , userLogin
                                                   );
        }

        return new ResponseEntity (
            this.buildSuccessResponse ( "Rol guardado con éxito" ) ,
            HttpStatus.OK
        );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/getRole" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getRole ( @RequestParam Integer prRoleGkey )
  throws BaseException, ParseException, SQLException {
    RoleDTO roleDto = this.roleService.getRoleDtoByGkey (
        prRoleGkey ,
        this.canAddItem || this.canEditItem
                                                        );
    return roleDto != null ? new ResponseEntity ( roleDto , HttpStatus.OK ) : new ResponseEntity (
        this.buildErrorResponse (
            "No se pudo cargar la información del rol. Por favor refresque la página" ) ,
        HttpStatus.BAD_REQUEST
    );
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteRole" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteRole ( @RequestBody Role role )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      this.roleService.deleteRole ( role , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Rol eliminado con éxito" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverRole" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > recoverRole ( @RequestBody Role role )
  throws BaseException, ParseException {
    if ( this.canRecoverItem ) {
      this.roleService.recoverRole ( role , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Rol activado con éxito" ) , HttpStatus.OK );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/deleteSelectedRole" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > deleteSelectedRole ( @RequestBody Long[] prRoleGkeyArray )
  throws BaseException, ParseException {
    if ( this.canDeleteItem ) {
      this.roleService.deleteRoleByGkey ( prRoleGkeyArray , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Roles eliminados con éxito" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      method = { RequestMethod.POST },
      value = { "/recoverSelectedRole" },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > recoverSelectedRole ( @RequestBody Long[] prRoleGkeyArray )
  throws BaseException, ParseException {
    if ( this.canRecoverItem ) {
      this.roleService.deleteRoleByGkey ( prRoleGkeyArray , this.getUserLogin ( ) );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Roles activados con éxito" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No tiene permisos para realizar esta acción" ) ,
          HttpStatus.OK
      );
    }
  }

  private
  String getUserLogin ( ) {
    org.springframework.security.core.userdetails.User loggedUser =
        ( org.springframework.security.core.userdetails.User ) SecurityContextHolder
        .getContext ( )
        .getAuthentication ( ).getPrincipal ( );
    String login = loggedUser.getUsername ( );
    return login;
  }

  private
  String getActionButtonsHtml (
      boolean canAddItem , boolean canDeleteItem ,
      boolean canRecoverItem
                              ) {
    StringBuilder sb = new StringBuilder ( );
    if ( canAddItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" "
          + "id=\"btnAddRole\">Agregar</button>&nbsp;&nbsp;" );
    }

    if ( canDeleteItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-danger\" "
          + "id=\"btnDeleteSelectedRole\">Eliminar</button>&nbsp;&nbsp;" );
    }

    if ( canRecoverItem ) {
      if ( sb.length ( ) > 0 ) {
        sb.append ( "\n" );
      }

      sb.append (
          "<button type=\"button\" class=\"btn btn-warning\" "
          + "id=\"btnRecoverSelectedRole\">Activar</button>" );
    }

    return sb.toString ( );
  }

  private
  String getModalButtonsHtml ( boolean canAddItem , boolean canDeleteItem ) {
    StringBuilder sb = new StringBuilder ( );
    if ( canAddItem || this.canEditItem ) {
      sb.append (
          "<button type=\"button\" class=\"btn btn-primary\" id=\"btnSaveRole\">Guardar</button>" );
    }

    if ( sb.length ( ) > 0 ) {
      sb.append ( "\n" );
    }

    sb.append (
        "<button type=\"button\" class=\"btn btn-dark\" id=\"btnCancelSaveRole\" "
        + "data-dismiss=\"modal\">Cancelar</button>" );
    return sb.toString ( );
  }

  private
  String getDataTableHtml (
      boolean canEditItem , boolean canDeleteItem ,
      boolean canRecoverItem
                          ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAll\"></th>\n" );
    }

    sb.append ( "    <th>Id</th>\n" );
    sb.append ( "    <th>Descripción</th>\n" );
    sb.append ( "    <th>Creado por</th>\n" );
    sb.append ( "    <th>Fecha Creación</th>\n" );
    sb.append ( "    <th>Act. por</th>\n" );
    sb.append ( "    <th>Fecha Act.</th>\n" );
    sb.append ( "    <th>Estado</th>\t\n" );
    sb.append ( "    <th></th>\n" );
    if ( canDeleteItem ) {
      sb.append ( "    <th></th>\n" );
    }

    if ( canRecoverItem ) {
      sb.append ( "    <th></th>\n" );
    }

    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: roleViewModel.createdRole\">\n" );
    sb.append ( "<tr>\n" );
    if ( canDeleteItem || canRecoverItem ) {
      sb.append (
          "    <td><input id=\"ckCheckOne\" class=\"selectable\" type=\"checkbox\" "
          + "data-bind=\"attr: {gkey:gkey}\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: id\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: description\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: creator\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: createdStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changer\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: changedStr\"></label></td>\n" );
    sb.append ( "    <td><label data-bind=\"text: lifeCycleState\"></label></td>\n" );
    if ( canEditItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-info btn-sm\" data-bind=\"click: $parent.editRole, enable: isActive\"> <i class=\"ti-pencil-alt\"></i> </button></td>\n" );
    }
    else {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-default btn-sm\" data-bind=\"click: $parent.editRole\"> <i class=\"ti-view-list\"></i> </button></td>\n" );
    }

    if ( canDeleteItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-danger btn-sm\" data-bind=\"click: $parent.deleteRole, enable: isActive\"> <i class=\"ti-close\"></i> </button></td>\n" );
    }

    if ( canRecoverItem ) {
      sb.append (
          "    <td><button class=\"btn btn-icon btn-warning btn-sm\" data-bind=\"click: $parent.recoverRole, enable: !isActive\"> <i class=\"ti-reload\"></i> </button></td>\n" );
    }

    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  String getPrivilegeDataTableHtml ( boolean viewOnly ) {
    StringBuilder sb = new StringBuilder ( );
    sb.append ( "<thead>\n" );
    sb.append ( "<tr>\n" );
    if ( ! viewOnly ) {
      sb.append ( "    <th> <input type=\"checkbox\" id=\"ckCheckAllPrivilege\"></th>\n" );
    }

    sb.append ( "    <th>Nombre</th>\n" );
    sb.append ( "</tr>\n" );
    sb.append ( "</thead>\n" );
    sb.append ( "<tbody data-bind=\"foreach: roleViewModel.rolePrivilege\">\n" );
    sb.append ( "<tr>\n" );
    if ( ! viewOnly ) {
      sb.append (
          "    <td><input id=\"ckCheckOnePrivilege\" class=\"selectablePrivilege\" type=\"checkbox\" data-bind=\"attr: {gkey:gkey}, checked: isIncludedOnRole\"></td>\n" );
    }

    sb.append ( "    <td><label data-bind=\"text: label\"></label></td>\n" );
    sb.append ( "</tr>\n" );
    return sb.toString ( );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    return map;
  }
}
