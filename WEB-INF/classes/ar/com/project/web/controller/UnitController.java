//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.dto.UnitImportFreeDebtDTO;
import ar.com.project.core.service.DatabaseConnectorService;
import ar.com.project.core.service.QueriesService;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes ( { "parameters" })
@RequestMapping ( { "/Unit" })
public
class UnitController extends CommonController {

  private static final Logger logger = Logger.getLogger ( UnitController.class );
  @Autowired
  QueriesService           queriesService;
  @Autowired
  DatabaseConnectorService databaseConnectorService;

  public
  UnitController ( ) {
  }

  @RequestMapping (
      method = { RequestMethod.GET },
      value = { "/findUnitsByDraft/{invNbr}" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findUnitsByDraft ( @PathVariable Integer invNbr )
  throws ParseException, SQLException {
    try {
      String                         query      = this.queriesService.findUnitsByDraft ( invNbr );
      Connection                     connection =
          this.databaseConnectorService.createN4DBConnection ( );
      Statement                      st         = connection.createStatement ( );
      ResultSet                      rs         = st.executeQuery ( query );
      List < UnitImportFreeDebtDTO > units      = new ArrayList ( );

      while ( rs.next ( ) ) {
        UnitImportFreeDebtDTO unit = new UnitImportFreeDebtDTO ( );
        unit.setUnitId ( rs.getString ( "unitId" ) );
        unit.setLdDate ( rs.getTimestamp ( "ldFecha" ) );
        unit.setLdStatus ( rs.getString ( "ldStatus" ) );
        unit.setApptTime ( rs.getTimestamp ( "apptTime" ) );
        units.add ( unit );
        System.out.println ( "Agregamos al listado el contenedor " + unit.getUnitId ( ) );
      }

      return new ResponseEntity ( units , HttpStatus.OK );
    }
    catch ( ServiceException var8 ) {
      return new ResponseEntity ( var8.getMessage ( ) , HttpStatus.BAD_REQUEST );
    }
  }
}
