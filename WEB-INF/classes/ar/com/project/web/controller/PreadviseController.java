//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.restservice.controllers;

import ar.com.project.core.domain.User;
import ar.com.project.core.dto.BookingDTO;
import ar.com.project.core.dto.DocumentApptDTO;
import ar.com.project.core.dto.WebServiceResponseDTO;
import ar.com.project.core.service.AppointmentService;
import ar.com.project.core.service.AuditService;
import ar.com.project.core.service.CoordinationService;
import ar.com.project.core.service.LineOpService;
import ar.com.project.core.service.PreadviseService;
import ar.com.project.core.service.PrivilegeService;
import ar.com.project.core.service.UnitService;
import ar.com.project.core.service.WebService;
import ar.com.project.core.utils.EnumUtil;
import ar.com.project.core.utils.StringUtils;
import ar.com.project.core.worker.UfvTransitStateEnum;
import ar.com.project.core.worker.UnitCategoryEnum;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping ( { "/Preadvise" })
@SessionAttributes ( {
    "userid" , "loginName" , "username" , "parameters" , "fullname" , "firstNameLetter"
}
)
public
class PreadviseController extends CommonController {

  private static final Logger logger                = Logger.getLogger (
      PreadviseController.class );
  private final        String PROPERTY_GROUP_SEAL   = "UNIT_SEAL";
  private final        String PROPERTY_GROUP_OOG    = "UNIT_OOG";
  private final        String PROPERTY_GROUP_VGM    = "UNIT_VGM_CONDITION";
  private final        String PROPERTY_GROUP_WEIGHT = "UNIT_WEIGHT";
  boolean canAddItem     = true;
  boolean canEditItem    = true;
  boolean canDeleteItem  = true;
  boolean canRecoverItem = false;
  User    user;
  @Autowired
  LineOpService       lineOpService;
  @Autowired
  PrivilegeService    privilegeService;
  @Autowired
  CoordinationService coordinationService;
  @Autowired
  AuditService        auditService;
  @Autowired
  UnitService         unitService;
  @Autowired
  AppointmentService  appointmenttService;
  @Autowired
  PreadviseService    preadviseService;
  @Autowired
  WebService          webService;

  public
  PreadviseController ( ) {
  }

  @RequestMapping (
      value = { "/preadvise" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  public
  ModelAndView preadvise (
      HttpServletRequest request , HttpServletResponse response ,
      Object handler
                         ) {
    ModelAndView model = new ModelAndView ( "preadvise" );
    return model;
  }

  @RequestMapping (
      value = { "/loadInitCombos" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < List > loadInitCombos ( ) {
    ResponseEntity response;
    try {
      List yesNoList  = EnumUtil.getSimpleYesNoSpanishEnumList ( );
      List lineOpList = this.lineOpService.getAllLineOp ( );
      Map  resultMap  = new HashMap ( );
      resultMap.put ( "lineOpList" , lineOpList );
      resultMap.put ( "yesNoList" , yesNoList );
      response = new ResponseEntity ( resultMap , HttpStatus.OK );
    }
    catch ( SQLException var5 ) {
      response = new ResponseEntity (
          "No se pudo cargar correctamente la información. Por favor refresque la página" ,
          HttpStatus.BAD_REQUEST
      );
      var5.printStackTrace ( );
    }

    return response;
  }

  @RequestMapping (
      value = { "/findBooking" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > findBooking (
      @RequestParam String prBookingNbr ,
      @RequestParam String prLineOpId
                                        )
  throws SQLException, ServiceException, IOException, InterruptedException {
    String unitExportCategory = UnitCategoryEnum.EXPRT.getKey ( );
    List documentApptList = this.coordinationService.findDocument ( unitExportCategory ,
                                                                    prBookingNbr ,
                                                                    prLineOpId
                                                                  );
    if ( documentApptList != null && ! documentApptList.isEmpty ( ) ) {
      if ( documentApptList.size ( ) > 1 && ( prLineOpId == null || prLineOpId.isEmpty ( ) ) ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "Se encontró más de un resultado para el booking ingresado. Por favor seleccioná "
                + "la Línea Operadora" ) ,
            HttpStatus.OK
        );
      }
      else {
        DocumentApptDTO documentApptDto = ( DocumentApptDTO ) documentApptList.get ( 0 );
        prLineOpId = prLineOpId != null && ! prLineOpId.isEmpty ( ) ? prLineOpId
                                                                    :
                     documentApptDto.getLineOpId ( );
        BookingDTO bookingDTO = this.appointmenttService.findBookingByLine (
            prBookingNbr ,
            prLineOpId
                                                                           );
        return new ResponseEntity ( bookingDTO , HttpStatus.OK );
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse ( "No se encontró el booking ingresado" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/getBookingPreadvisedUnits" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getBookingPreadvisedUnits (
      @RequestParam String prBookingNbr ,
      @RequestParam String prLineOpId
                                                      )
  throws SQLException, ServiceException, IOException, InterruptedException {
    BookingDTO bookingDTO = this.appointmenttService.findBookingByLine (
        prBookingNbr , prLineOpId );
    return new ResponseEntity ( bookingDTO.getUnits ( ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/validateBkgQty" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > validateBkgQty (
      @RequestParam String prBookingNbr ,
      @RequestParam String prLineOpId
                                           )
  throws SQLException, ServiceException, IOException, InterruptedException {
    BookingDTO bookingDTO = this.appointmenttService.findBookingByLine (
        prBookingNbr , prLineOpId );
    int        qty        = bookingDTO.getQuantity ( );
    int        unitQty    = bookingDTO.getUnits ( ).size ( );
    return unitQty >= qty ? new ResponseEntity (
        this.buildErrorResponse (
            "El booking alcanzó la cantidad máxima de contenedores preavisados (" + qty + ")" ) ,
        HttpStatus.OK
    ) : new ResponseEntity ( this.buildSuccessResponse ( "" ) , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/getUnitCustomsPerms" },
      method = { RequestMethod.GET }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > getUnitCustomsPerms ( @RequestParam String prUnitGkey )
  throws SQLException, ServiceException, IOException, InterruptedException {
    Object result = this.getUnitCustomPermsIntern ( prUnitGkey );
    return result instanceof String ? new ResponseEntity (
        this.buildErrorResponse ( ( String ) result ) ,
        HttpStatus.OK
    ) : new ResponseEntity ( ( List ) result , HttpStatus.OK );
  }

  @RequestMapping (
      value = { "/preadviseUnit" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > preadviseUnit ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map     unitMap         = ( Map ) unitObject;
    Integer bookingGkey     = ( Integer ) unitMap.get ( "bookingGkey" );
    Integer bookingItemGkey = ( Integer ) unitMap.get ( "bookingItemGkey" );
    String  unitId          = ( String ) unitMap.get ( "unitId" );
    String  bookingNbr      = ( String ) unitMap.get ( "bookingNbr" );
    String  isoTypeId       = ( String ) unitMap.get ( "isoTypeId" );
    String  sealNbr2        = ( String ) unitMap.get ( "unitSealNbr2" );
    String  tareWtStr       = ( String ) unitMap.get ( "unitTareWt" );
    String  contentsWtStr   = ( String ) unitMap.get ( "unitContentWt" );
    String  vgmEmail        = ( String ) unitMap.get ( "unitEmail" );
    String  vgmCondition    = ( String ) unitMap.get ( "unitVgmCondition" );
    String  referenceId     = ( String ) unitMap.get ( "unitReferenceId" );
    boolean isOog           = "1".equals ( unitMap.get ( "unitIsOog" ) );
    if ( bookingGkey != null && bookingItemGkey != null ) {
      if ( StringUtils.isNullOrEmpty ( unitId ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá el número de contenedor" ) ,
            HttpStatus.OK
        );
      }
      else if ( ! this.isValidUnitIdFormat ( unitId.toUpperCase ( ) ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá un número de contenedor correcto" ) ,
            HttpStatus.OK
        );
      }
      else if ( StringUtils.isNullOrEmpty ( isoTypeId ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Seleccioná el tamaño del contenedor" ) ,
            HttpStatus.OK
        );
      }
      else if ( StringUtils.isNullOrEmpty ( tareWtStr ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá el peso de la Tara" ) ,
            HttpStatus.OK
        );
      }
      else if ( StringUtils.isNullOrEmpty ( contentsWtStr ) ) {
        return new ResponseEntity (
            this.buildErrorResponse ( "Ingresá el peso de la Mercadería" ) ,
            HttpStatus.OK
        );
      }
      else {
        String seal2ValidationError = this.preadviseService.validateUnitSeal2 ( sealNbr2 );
        if ( ! StringUtils.isNullOrEmpty ( seal2ValidationError ) ) {
          return new ResponseEntity (
              this.buildErrorResponse ( seal2ValidationError ) , HttpStatus.OK );
        }
        else if ( ! NumberUtils.isNumber ( tareWtStr ) ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "Ingresá un peso válido para la Tara" ) ,
              HttpStatus.OK
          );
        }
        else if ( ! NumberUtils.isNumber ( contentsWtStr ) ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "Ingresá un peso válido para la Mercadería" ) ,
              HttpStatus.OK
          );
        }
        else {
          Double tareWt     = Double.parseDouble ( tareWtStr );
          Double contentsWt = Double.parseDouble ( contentsWtStr );
          Double grossWt    = tareWt + contentsWt;
          Double safeWt     = this.preadviseService.getUnitSafeWeight ( unitId , isoTypeId );
          if ( safeWt != null && grossWt > safeWt ) {
            return new ResponseEntity (
                this.buildErrorResponse (
                    "El peso total del contenedor excede el peso máximo seguro (Safe Weight)" ) ,
                HttpStatus.OK
            );
          }
          else {
            unitMap.put ( "unitGrossWt" , grossWt );
            if ( StringUtils.isNullOrEmpty ( vgmCondition ) ) {
              return new ResponseEntity (
                  this.buildErrorResponse ( "Seleccioná el tipo de VGM" ) ,
                  HttpStatus.OK
              );
            }
            else if ( StringUtils.isNullOrEmpty ( vgmEmail ) ) {
              return new ResponseEntity (
                  this.buildErrorResponse ( "Ingresá un email de notificación" ) ,
                  HttpStatus.OK
              );
            }
            else {
              String referenceIdValidationError = this.preadviseService.validateUnitReferenceId (
                  referenceId );
              if ( ! StringUtils.isNullOrEmpty ( referenceIdValidationError ) ) {
                return new ResponseEntity (
                    this.buildErrorResponse ( referenceIdValidationError ) ,
                    HttpStatus.OK
                );
              }
              else {
                if ( isOog ) {
                  String oogTopStr   = ( String ) unitMap.get ( "oogTop" );
                  String oogFrontStr = ( String ) unitMap.get ( "oogFront" );
                  String oogBackStr  = ( String ) unitMap.get ( "oogBack" );
                  String oogLeftStr  = ( String ) unitMap.get ( "oogLeft" );
                  String oogRightStr = ( String ) unitMap.get ( "oogRight" );
                  if ( StringUtils.isNullOrEmpty ( oogTopStr ) && StringUtils.isNullOrEmpty (
                      oogFrontStr )
                       && StringUtils.isNullOrEmpty ( oogBackStr ) && StringUtils.isNullOrEmpty (
                      oogLeftStr ) && StringUtils.isNullOrEmpty ( oogRightStr ) ) {
                    return new ResponseEntity (
                        this.buildErrorResponse ( "Ingresá la información de Fuera de Medida" ) ,
                        HttpStatus.OK
                    );
                  }
                }

                WebServiceResponseDTO msg = this.preadviseService.preadviseUnit ( unitMap );
                if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
                  List < String > data = new ArrayList ( );
                  data.add ( "unitId:" + unitId );
                  data.add ( "bookingNbr:" + bookingNbr );
                  data.add ( "isoTypeId:" + isoTypeId );
                  data.add ( "vgmCondition:" + vgmCondition );
                  data.add ( "vgmEmail:" + vgmEmail );
                  data.add ( "sealNbr2:" + sealNbr2 );
                  data.add ( "grossWeight:" + unitMap.get ( "unitGrossWt" ) );
                  data.add ( "tareWeight:" + tareWtStr );
                  data.add ( "isOog:" + isOog );
                  this.auditService.saveAuditTransaction ( "preadviseUnit" , data.toString ( ) ,
                                                           this.getUserContext ( ).getUsername ( )
                                                         );
                  this.addUnitCustomsPermIntern (
                      ( Integer ) null , unitId , bookingNbr , referenceId );
                  return new ResponseEntity (
                      this.buildSuccessResponse ( "Contenedor preavisado!" ) ,
                      HttpStatus.OK
                  );
                }
                else {
                  return new ResponseEntity (
                      this.buildErrorResponse ( msg.getReadableMsg ( ) ) ,
                      HttpStatus.OK
                  );
                }
              }
            }
          }
        }
      }
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse (
              "Error al obtener información de booking o equipamento. Intentá nuevamente" ) ,
          HttpStatus.OK
      );
    }
  }

  @RequestMapping (
      value = { "/updateUnitSeal2" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUnitSeal2 ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map    unitMap              = ( Map ) unitObject;
    String sealNbr2             = ( String ) unitMap.get ( "seal2" );
    String updateSealNbr2       = ( String ) unitMap.get ( "updateUnitSealNbr2" );
    String seal2ValidationError = this.preadviseService.validateUnitSeal2 ( updateSealNbr2 );
    if ( ! StringUtils.isNullOrEmpty ( seal2ValidationError ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( seal2ValidationError ) , HttpStatus.OK );
    }
    else if ( updateSealNbr2.equalsIgnoreCase ( sealNbr2 ) ) {
      return new ResponseEntity (
          this.buildSuccessResponse ( "No se detectó ningún cambio" ) ,
          HttpStatus.OK
      );
    }
    else {
      unitMap.put ( "eventNote" , "Preaviso - Precinto Aduana" );
      unitMap.put ( "propertyGroupName" , "UNIT_SEAL" );
      unitMap.put ( "unitSealNbr2" , updateSealNbr2.toUpperCase ( ) );
      unitMap.put ( "auditDetails" , "sealNbr2:" + updateSealNbr2 );
      return this.updateUnitPreadvise ( unitMap );
    }
  }

  @RequestMapping (
      value = { "/updateUnitOog" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUnitOog ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map     unitMap        = ( Map ) unitObject;
    boolean updateIsOog    = "1".equals ( unitMap.get ( "updateUnitIsOog" ) );
    boolean isOog          = ( Boolean ) unitMap.get ( "oog" );
    String  oogTopStr      = updateIsOog ? ( String ) unitMap.get ( "updateOogTop" ) : "";
    String  oogFrontStr    = updateIsOog ? ( String ) unitMap.get ( "updateOogFront" ) : "";
    String  oogBackStr     = updateIsOog ? ( String ) unitMap.get ( "updateOogBack" ) : "";
    String  oogLeftStr     = updateIsOog ? ( String ) unitMap.get ( "updateOogLeft" ) : "";
    String  oogRightStr    = updateIsOog ? ( String ) unitMap.get ( "updateOogRight" ) : "";
    Double  updateOogTop   = this.getDoubleValue ( oogTopStr );
    Double  updateOogFront = this.getDoubleValue ( oogFrontStr );
    Double  updateOogBack  = this.getDoubleValue ( oogBackStr );
    Double  updateOogLeft  = this.getDoubleValue ( oogLeftStr );
    Double  updateOogRight = this.getDoubleValue ( oogRightStr );
    Double  oogTop         = this.getDoubleValue ( unitMap.get ( "oogTop" ) );
    Double  oogFront       = this.getDoubleValue ( unitMap.get ( "oogFront" ) );
    Double  oogBack        = this.getDoubleValue ( unitMap.get ( "oogBack" ) );
    Double  oogLeft        = this.getDoubleValue ( unitMap.get ( "oogLeft" ) );
    Double  oogRight       = this.getDoubleValue ( unitMap.get ( "oogRight" ) );
    if ( updateIsOog == isOog && ObjectUtils.nullSafeEquals ( updateOogFront , oogFront )
         && ObjectUtils.nullSafeEquals ( updateOogBack , oogBack ) && ObjectUtils.nullSafeEquals (
        updateOogLeft , oogLeft ) && ObjectUtils.nullSafeEquals ( updateOogRight , oogRight )
         && ObjectUtils.nullSafeEquals ( updateOogTop , oogTop ) ) {
      return new ResponseEntity (
          this.buildSuccessResponse ( "No se detectó ningún cambio" ) ,
          HttpStatus.OK
      );
    }
    else if ( updateIsOog && StringUtils.isNullOrEmpty ( oogTopStr ) && StringUtils.isNullOrEmpty (
        oogFrontStr ) && StringUtils.isNullOrEmpty ( oogBackStr ) && StringUtils.isNullOrEmpty (
        oogLeftStr ) && StringUtils.isNullOrEmpty ( oogRightStr ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá la información de Fuera de Medida" ) , HttpStatus.OK );
    }
    else {
      unitMap.put ( "unitIsOog" , "1" );
      unitMap.put ( "oogTop" , oogTopStr );
      unitMap.put ( "oogFront" , oogFrontStr );
      unitMap.put ( "oogBack" , oogBackStr );
      unitMap.put ( "oogLeft" , oogLeftStr );
      unitMap.put ( "oogRight" , oogRightStr );
      unitMap.put ( "eventNote" , "Preaviso - Fuera de medida" );
      unitMap.put ( "propertyGroupName" , "UNIT_OOG" );
      if ( updateIsOog ) {
        unitMap.put (
            "auditDetails" ,
            "isOog:" + updateIsOog + ", oogTop:" + oogTopStr + ", oogFront:" + oogFrontStr
            + ", oogBack:" + oogBackStr + ", oogLeft:" + oogLeftStr + ", oogRight:"
            + oogRightStr
                    );
      }
      else {
        unitMap.put ( "auditDetails" , "isOog:" + updateIsOog );
      }

      return this.updateUnitPreadvise ( unitMap );
    }
  }

  @RequestMapping (
      value = { "/updateUnitVgm" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUnitVgm ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map    unitMap            = ( Map ) unitObject;
    Map    vgmConditionMap    = ( Map ) unitMap.get ( "vgmConditionEnum" );
    String vgmCondition       = vgmConditionMap != null ? ( String ) vgmConditionMap.get ( "key" )
                                                        : "";
    String vgmEmail           = ( String ) unitMap.get ( "vgmEmail" );
    String updateVgmCondition = ( String ) unitMap.get ( "updateVgmCondition" );
    String updateVgmEmail     = ( String ) unitMap.get ( "updateVgmEmail" );
    if ( StringUtils.isNullOrEmpty ( updateVgmCondition ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Seleccioná el tipo de VGM" ) ,
          HttpStatus.OK
      );
    }
    else if ( StringUtils.isNullOrEmpty ( updateVgmEmail ) ) {
      return new ResponseEntity (
          this.buildSuccessResponse ( "Ingresá un email de notificación" ) ,
          HttpStatus.OK
      );
    }
    else if ( updateVgmCondition.equalsIgnoreCase ( vgmCondition )
              && updateVgmEmail.equalsIgnoreCase (
        vgmEmail ) ) {
      return new ResponseEntity (
          this.buildSuccessResponse ( "No se detectó ningún cambio" ) ,
          HttpStatus.OK
      );
    }
    else {
      unitMap.put ( "eventNote" , "Preaviso - Precinto VGM" );
      unitMap.put ( "propertyGroupName" , "UNIT_VGM_CONDITION" );
      unitMap.put ( "unitVgmCondition" , updateVgmCondition );
      unitMap.put ( "unitEmail" , updateVgmEmail );
      unitMap.put (
          "auditDetails" ,
          "vgmCondition:" + updateVgmCondition + ", vgmEmail:" + updateVgmEmail
                  );
      return this.updateUnitPreadvise ( unitMap );
    }
  }

  @RequestMapping (
      value = { "/updateUnitWeight" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUnitWeight ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map    unitMap            = ( Map ) unitObject;
    Double tareKg             = this.getDoubleValue ( unitMap.get ( "tareKg" ) );
    Double weight             = this.getDoubleValue ( unitMap.get ( "weight" ) );
    String updateTareWtStr    = ( String ) unitMap.get ( "updateUnitTareWt" );
    String updateContentWtStr = ( String ) unitMap.get ( "updateUnitContentWt" );
    if ( ! NumberUtils.isNumber ( updateTareWtStr ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá un peso válido para la Tara" ) ,
          HttpStatus.OK
      );
    }
    else if ( ! NumberUtils.isNumber ( updateContentWtStr ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá un peso válido para la Mercadería" ) , HttpStatus.OK );
    }
    else {
      Double contentKg = weight - tareKg;
      Double tareWt    = Double.parseDouble ( updateTareWtStr );
      Double contentWt = Double.parseDouble ( updateContentWtStr );
      if ( tareWt == tareKg && contentWt == contentKg ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "No se detectó ningún cambio" ) ,
            HttpStatus.OK
        );
      }
      else {
        Double grossWt = tareWt + contentWt;
        String unitId  = ( String ) unitMap.get ( "unitId" );
        Double safeWt  = this.preadviseService.getUnitSafeWeight ( unitId , "" );
        if ( safeWt != null && grossWt > safeWt ) {
          return new ResponseEntity (
              this.buildErrorResponse (
                  "El peso total del contenedor excede el peso máximo seguro (Safe Weight)" ) ,
              HttpStatus.OK
          );
        }
        else {
          unitMap.put ( "unitTareWt" , updateTareWtStr );
          unitMap.put ( "unitGrossWt" , grossWt );
          unitMap.put ( "eventNote" , "Preaviso - Precinto Peso" );
          unitMap.put ( "propertyGroupName" , "UNIT_WEIGHT" );
          unitMap.put (
              "auditDetails" ,
              "tareWt:" + tareWt + ", contentWt:" + contentWt + ", grossWt:" + grossWt
                      );
          return this.updateUnitPreadvise ( unitMap );
        }
      }
    }
  }

  @RequestMapping (
      value = { "/updateUnitId" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > updateUnitId ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map    unitMap      = ( Map ) unitObject;
    String unitId       = ( String ) unitMap.get ( "unitId" );
    String updateUnitId = ( String ) unitMap.get ( "updateUnitId" );
    if ( StringUtils.isNullOrEmpty ( updateUnitId ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá el número de contenedor" ) ,
          HttpStatus.OK
      );
    }
    else if ( ! this.isValidUnitIdFormat ( unitId.toUpperCase ( ) ) ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "Ingresá un número de contenedor correcto" ) ,
          HttpStatus.OK
      );
    }
    else {
      unitId       = StringUtils.trimText ( unitId );
      updateUnitId = StringUtils.trimText ( updateUnitId );
      if ( updateUnitId.equalsIgnoreCase ( unitId ) ) {
        return new ResponseEntity (
            this.buildSuccessResponse ( "No se detectó ningún cambio" ) ,
            HttpStatus.OK
        );
      }
      else {
        unitMap.put ( "eventNote" , "Preaviso - Renumber" );
        unitMap.put ( "newUnitId" , updateUnitId );
        unitMap.put ( "auditDetails" , "prevUnitId:" + unitId + ", newUnitId:" + updateUnitId );
        return this.renumberPreadvisedUnit ( unitMap );
      }
    }
  }

  @RequestMapping (
      value = { "/cancelPreadvisedUnit" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelPreadvisedUnit ( @RequestBody Object unitObject )
  throws SQLException, IOException {
    Map     unitMap    = ( Map ) unitObject;
    String  unitId     = ( String ) unitMap.get ( "unitId" );
    Integer unitGkey   = ( Integer ) unitMap.get ( "gkey" );
    String  bookingNbr = ( String ) unitMap.get ( "bkNrb" );

    try {
      WebServiceResponseDTO msg = this.preadviseService.cancelPreadvisedUnit (
          unitGkey.longValue ( ) ,
          unitId , bookingNbr
                                                                             );
      if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
        List < String > data = new ArrayList ( );
        data.add ( "unitGkey:" + unitGkey );
        data.add ( "unitId:" + unitId );
        data.add ( "bookingNbr:" + bookingNbr );
        this.auditService.saveAuditTransaction ( "cancelPreadvisedUnit" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Contenedor " + unitId + " desvinculado con éxito!" ) ,
            HttpStatus.OK
        );
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse ( msg.getMessage ( ) ) , HttpStatus.OK );
      }
    }
    catch ( ServiceException var8 ) {
      return new ResponseEntity (
          this.buildErrorResponse ( "EXCEPTION:" + var8.getMessage ( ) ) ,
          HttpStatus.BAD_REQUEST
      );
    }
  }

  @RequestMapping (
      value = { "/addUnitCustomsPerm" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > addUnitCustomsPerm ( @RequestBody Object unitObject )
  throws NumberFormatException, SQLException {
    Map     unitMap         = ( Map ) unitObject;
    Integer unitGkey        = ( Integer ) unitMap.get ( "gkey" );
    Map     locationMap     = ( Map ) unitMap.get ( "location" );
    String  locationKey     = ( String ) locationMap.get ( "location" );
    String  locationDesc    = ( String ) locationMap.get ( "description" );
    String  unitId          = ( String ) unitMap.get ( "unitId" );
    String  referenceId     = ( String ) unitMap.get ( "referenceId" );
    String  validationError = this.preadviseService.validateUnitReferenceId ( referenceId );
    if ( ! StringUtils.isNullOrEmpty ( validationError ) ) {
      return new ResponseEntity ( this.buildErrorResponse ( validationError ) , HttpStatus.OK );
    }
    else if ( StringUtils.isNullOrEmpty ( locationKey ) ) {
      return new ResponseEntity (
          this.buildErrorResponse (
              "No se pudo determinar la locación del contenedor. Por favor refrescá la página" ) ,
          HttpStatus.OK
      );
    }
    else if ( ! UfvTransitStateEnum.INBOUND.getKey ( ).equals ( locationKey ) ) {
      return new ResponseEntity ( this.buildErrorResponse (
          "El contenedor se encuentra " + locationDesc + " (" + locationKey
          + "). No se puede agregar el permiso de embarque." ) , HttpStatus.OK );
    }
    else {
      Object result = this.getUnitCustomPermsIntern ( unitGkey.toString ( ) );
      if ( result instanceof String ) {
        return new ResponseEntity ( this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
      }
      else {
        List customsPermList = ( List ) result;

        for ( int i = 0 ; i < customsPermList.size ( ) ; ++ i ) {
          Map    row             = ( Map ) customsPermList.get ( i );
          String unitReferenceId = ( String ) row.get ( "reference_id" );
          if ( unitReferenceId.equalsIgnoreCase ( referenceId ) ) {
            return new ResponseEntity (
                this.buildErrorResponse (
                    "Permiso " + referenceId + " ya existe en el contenedor " + unitId ) ,
                HttpStatus.OK
            );
          }
        }

        try {
          String bookingNbr = ( String ) unitMap.get ( "bkNrb" );
          return this.addUnitCustomsPermIntern ( unitGkey , unitId , bookingNbr , referenceId );
        }
        catch ( IOException var15 ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "EXCEPTION:" + var15.getMessage ( ) ) ,
              HttpStatus.BAD_REQUEST
          );
        }
      }
    }
  }

  @RequestMapping (
      value = { "/cancelUnitCustomsPerm" },
      method = { RequestMethod.POST },
      consumes = { "application/json" }
  )
  @PreAuthorize ( "isAuthenticated()")
  @ResponseBody
  public
  ResponseEntity < Object > cancelUnitCustomsPerm ( @RequestBody Object unitPermissionObject )
  throws NumberFormatException, SQLException {
    Map     unitMap     = ( Map ) unitPermissionObject;
    Integer unitGkey    = ( Integer ) unitMap.get ( "gkey" );
    String  unitId      = ( String ) unitMap.get ( "id" );
    String  referenceId = ( String ) unitMap.get ( "reference_id" );
    Object  result      = this.getUnitCustomPermsIntern ( unitGkey.toString ( ) );
    if ( result instanceof String ) {
      return new ResponseEntity ( this.buildErrorResponse ( ( String ) result ) , HttpStatus.OK );
    }
    else {
      List customsPermissionList = ( List ) result;
      if ( customsPermissionList.size ( ) < 2 ) {
        return new ResponseEntity (
            this.buildErrorResponse (
                "El contenedor no puede quedar sin permiso de embarque. <br>Agregá el correcto y "
                + "luego eliminá el incorrecto." ) ,
            HttpStatus.OK
        );
      }
      else {
        try {
          WebServiceResponseDTO msg = this.preadviseService.cancelUnitCustomsPerm (
              unitId ,
              referenceId
                                                                                  );
          if ( msg.getStatus ( ).equals ( "SUCCESS" ) ) {
            String          bookingNbr = ( String ) unitMap.get ( "bookingNbr" );
            List < String > data       = new ArrayList ( );
            data.add ( "unitGkey:" + unitGkey );
            data.add ( "unitId:" + unitId );
            data.add ( "bookingNbr:" + bookingNbr );
            data.add ( "referenceId:" + referenceId );
            this.auditService.saveAuditTransaction ( "cancelUnitCustomsPerm" , data.toString ( ) ,
                                                     this.getUserContext ( ).getUsername ( )
                                                   );
            return new ResponseEntity (
                this.buildSuccessResponse ( "Permiso " + referenceId + " cancelado con éxito!" ) ,
                HttpStatus.OK
            );
          }
          else {
            return new ResponseEntity (
                this.buildErrorResponse (
                    "No se pudo eliminar el permiso " + referenceId + ". Intentá nuevamente." ) ,
                HttpStatus.OK
            );
          }
        }
        catch ( IOException var11 ) {
          return new ResponseEntity (
              this.buildErrorResponse ( "EXCEPTION:" + var11.getMessage ( ) ) ,
              HttpStatus.BAD_REQUEST
          );
        }
      }
    }
  }

  private
  ResponseEntity < Object > updateUnitPreadvise ( Map unitMap ) throws SQLException, IOException {
    Integer unitGkey = ( Integer ) unitMap.get ( "gkey" );
    Boolean acceptUpdateCharges = unitMap.get ( "acceptUpdateCharges" ) == null ? false
                                                                                :
                                  ( Boolean ) unitMap.get (
                                                                                    "acceptUpdateCharges" );
    boolean hasDraftHandleExp = this.preadviseService.hasDraftHandleExpEvent (
        unitGkey.longValue ( ) );
    boolean hasPaidHandleExp  = this.preadviseService.hasPaidHandleExpEvent (
        unitGkey.longValue ( ) );
    boolean hasAdministraEvent = this.preadviseService.hasAdminstraPreadviseEvent (
        unitGkey.longValue ( ) );
    if ( ( hasDraftHandleExp || hasPaidHandleExp ) && ! hasAdministraEvent
         && ! acceptUpdateCharges ) {
      return new ResponseEntity (
          this.buildQuestionResponse (
              "Esta acción genera cargos administrativos." ,
              "acceptUpdateCharges"
                                     ) , HttpStatus.OK );
    }
    else {
      WebServiceResponseDTO msg = this.preadviseService.updatePreadvisedUnit ( unitMap );
      if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
        String          unitId       = ( String ) unitMap.get ( "unitId" );
        String          bookingNbr   = ( String ) unitMap.get ( "bkNrb" );
        String          lineOpId     = ( String ) unitMap.get ( "lineOp" );
        String          auditDetails = ( String ) unitMap.get ( "auditDetails" );
        List < String > data         = new ArrayList ( );
        data.add ( "unitGkey:" + unitGkey );
        data.add ( "unitId:" + unitId );
        data.add ( "bookingNbr:" + bookingNbr );
        data.add ( auditDetails );
        if ( acceptUpdateCharges ) {
          data.add ( "acceptCharges:" + acceptUpdateCharges );
          data.add ( "hasAdministraEvent:" + hasAdministraEvent );
          data.add ( "hasDraftHandleExp:" + hasDraftHandleExp );
          data.add ( "hasPaidHandleExp:" + hasPaidHandleExp );
          if ( ! hasAdministraEvent ) {
            String eventNote = ( String ) unitMap.get ( "eventNote" );
            this.preadviseService.recordAdministraToUnit ( unitGkey.longValue ( ) , unitId ,
                                                           UnitCategoryEnum.EXPRT.getKey ( ) ,
                                                           lineOpId , eventNote ,
                                                           this.getUserContext ( ).getUsername ( )
                                                         );
          }
        }

        this.auditService.saveAuditTransaction ( "updatePreadvisedUnit" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Preaviso actualizado correctamente" ) ,
            HttpStatus.OK
        );
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse ( msg.getMessage ( ) ) , HttpStatus.OK );
      }
    }
  }

  private
  ResponseEntity < Object > renumberPreadvisedUnit ( Map unitMap )
  throws SQLException, IOException {
    Integer unitGkey = ( Integer ) unitMap.get ( "gkey" );
    Boolean acceptRenumberCharges = unitMap.get ( "acceptRenumberCharges" ) == null ? false
                                                                                    : ( Boolean ) unitMap.get (
                                                                                        "acceptRenumberCharges" );
    if ( ! acceptRenumberCharges ) {
      return new ResponseEntity (
          this.buildQuestionResponse (
              "Esta acción genera cargos administrativos." ,
              "acceptRenumberCharges"
                                     ) , HttpStatus.OK );
    }
    else {
      WebServiceResponseDTO msg = this.preadviseService.renumberPreadvisedUnit ( unitMap );
      if ( msg.getStatus ( ).equalsIgnoreCase ( "SUCCESS" ) ) {
        String          unitId       = ( String ) unitMap.get ( "unitId" );
        String          bookingNbr   = ( String ) unitMap.get ( "bkNrb" );
        String          lineOpId     = ( String ) unitMap.get ( "lineOp" );
        String          auditDetails = ( String ) unitMap.get ( "auditDetails" );
        List < String > data         = new ArrayList ( );
        data.add ( "unitId:" + unitId );
        data.add ( "bookingNbr:" + bookingNbr );
        data.add ( "acceptCharges:" + acceptRenumberCharges );
        data.add ( auditDetails );
        String eventNote = ( String ) unitMap.get ( "eventNote" );
        this.preadviseService.recordAdministraToUnit ( unitGkey.longValue ( ) , unitId ,
                                                       UnitCategoryEnum.EXPRT.getKey ( ) ,
                                                       lineOpId , eventNote ,
                                                       this.getUserContext ( ).getUsername ( )
                                                     );
        this.auditService.saveAuditTransaction ( "renumberPreadvisedUnit" , data.toString ( ) ,
                                                 this.getUserContext ( ).getUsername ( )
                                               );
        return new ResponseEntity (
            this.buildSuccessResponse ( "Preaviso renombrado correctamente" ) ,
            HttpStatus.OK
        );
      }
      else {
        return new ResponseEntity (
            this.buildErrorResponse ( msg.getMessage ( ) ) , HttpStatus.OK );
      }
    }
  }

  private
  Object getUnitCustomPermsIntern ( String unitGkeyStr )
  throws NumberFormatException, SQLException {
    if ( StringUtils.isNullOrEmpty ( unitGkeyStr ) ) {
      return "No se pudo obtener información del contenedor. Refrescá la página";
    }
    else {
      return ! NumberUtils.isNumber ( unitGkeyStr )
             ? "Información errónea del contenedor. Refrescá la página"
             : this.preadviseService.getUnitCustomsPerm ( Long.parseLong ( unitGkeyStr ) );
    }
  }

  private
  ResponseEntity < Object > addUnitCustomsPermIntern (
      Integer unitGkey , String unitId ,
      String bookingNbr , String referenceId
                                                     ) throws IOException {
    WebServiceResponseDTO msg = this.preadviseService.createUnitCustomsPerm (
        unitId , referenceId );
    if ( msg.getStatus ( ).equals ( "SUCCESS" ) ) {
      List < String > data = new ArrayList ( );
      if ( unitGkey != null ) {
        data.add ( "unitGkey:" + unitGkey );
      }

      data.add ( "unitId:" + unitId );
      data.add ( "bookingNbr:" + bookingNbr );
      data.add ( "referenceId:" + referenceId );
      this.auditService.saveAuditTransaction ( "addUnitCustomsPerm" , data.toString ( ) ,
                                               this.getUserContext ( ).getUsername ( )
                                             );
      return new ResponseEntity (
          this.buildSuccessResponse ( "Permiso " + referenceId + " agregado con éxito!" ) ,
          HttpStatus.OK
      );
    }
    else {
      return new ResponseEntity (
          this.buildErrorResponse (
              "No se pudo agregar el permiso " + referenceId + ". Intentá nuevamente." ) ,
          HttpStatus.OK
      );
    }
  }

  private
  boolean isValidUnitIdFormat ( String untiId ) {
    return untiId.matches ( "^[A-Z]{4}\\d{7}" );
  }

  private
  Map buildSuccessResponse ( String msg ) {
    return this.buildResponse ( "OK" , msg );
  }

  private
  Map buildErrorResponse ( String msg ) {
    return this.buildResponse ( "ERROR" , msg );
  }

  private
  Map buildQuestionResponse ( String msg , String questionType ) {
    Map map = this.buildResponse ( "QUESTION" , msg );
    if ( questionType != null && ! questionType.isEmpty ( ) ) {
      map.put ( "questionType" , questionType );
    }

    return map;
  }

  private
  Map buildWarningResponse ( String msg ) {
    return this.buildResponse ( "WARNING" , msg );
  }

  private
  Map buildResponse ( String status , String msg ) {
    this.logUser ( msg );
    Map map = new HashMap ( );
    map.put ( "status" , status );
    map.put ( "msg" , msg );
    this.clearLogPrefix ( );
    return map;
  }

  private
  void setUnitLogPrefix ( Map unitMap ) {
    Integer unitGkey     = ( Integer ) unitMap.get ( "unitGkey" );
    String  unitId       = ( String ) unitMap.get ( "unitId" );
    String  unitCategory = ( String ) unitMap.get ( "unitCategory" );
    this.setLogPrefix ( "Unit[" + unitGkey + "/" + unitId + "/" + unitCategory + "] " );
  }

  private
  Double getDoubleValue ( Object weight ) {
    if ( weight instanceof Integer ) {
      return ( ( Integer ) weight ).doubleValue ( );
    }
    else if ( weight instanceof Double ) {
      return ( Double ) weight;
    }
    else {
      return weight instanceof String && weight != null && NumberUtils.isNumber (
          ( String ) weight )
             ? Double.valueOf ( ( String ) weight ) : null;
    }
  }
}
