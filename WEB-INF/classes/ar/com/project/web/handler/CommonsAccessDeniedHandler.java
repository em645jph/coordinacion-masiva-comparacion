//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.handler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

public
class CommonsAccessDeniedHandler implements AccessDeniedHandler {

  private static final Logger logger = Logger.getLogger ( CommonsAccessDeniedHandler.class );
  private              String errorPage;

  public
  CommonsAccessDeniedHandler ( ) {
  }

  public
  CommonsAccessDeniedHandler ( String errorPage ) {
    this.errorPage = errorPage;
  }

  public
  void handle (
      HttpServletRequest request , HttpServletResponse response ,
      AccessDeniedException accessDeniedException
              ) throws IOException, ServletException {
    logger.warn ( accessDeniedException );
    response.sendRedirect ( request.getContextPath ( ) + this.errorPage );
  }

  public
  String getErrorPage ( ) {
    return this.errorPage;
  }

  public
  void setErrorPage ( String errorPage ) {
    this.errorPage = errorPage;
  }
}
