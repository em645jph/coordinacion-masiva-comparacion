//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ar.com.project.web.utils;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public
class JQGridWrapper < T > extends CollectionWrapper < T > {

  Integer                              pagina;
  Integer                              totalpaginas;
  Integer                              totalregistros;
  Integer                              tamanioPagina;
  Set < Map.Entry < String, String > > values;

  public
  JQGridWrapper (
      Collection < T > resultados , Integer pagina , Integer totalregistros ,
      Integer tamanioPagina
                ) {
    super ( resultados );
    this.resultados     = resultados;
    this.pagina         = pagina;
    this.totalregistros = totalregistros;
    this.tamanioPagina  = tamanioPagina;
    this.totalpaginas   =
        Math.round ( ( float ) ( totalregistros / tamanioPagina ) ) + (
            totalregistros % tamanioPagina == 0
            ? 0 : 1
        );
  }

  public
  Collection < T > getResultados ( ) {
    return this.resultados;
  }

  public
  void setResultados ( Collection < T > resultados ) {
    this.resultados = resultados;
  }

  public
  String getMessage ( ) {
    return this.message;
  }

  public
  void setMessage ( String message ) {
    this.message = message;
  }

  public
  Integer getPagina ( ) {
    return this.pagina;
  }

  public
  void setPagina ( Integer pagina ) {
    this.pagina = pagina;
  }

  public
  Integer getTotalpaginas ( ) {
    return this.totalpaginas;
  }

  public
  void setTotalpaginas ( Integer totalpaginas ) {
    this.totalpaginas = totalpaginas;
  }

  public
  Integer getTotalregistros ( ) {
    return this.totalregistros;
  }

  public
  void setTotalregistros ( Integer totalregistros ) {
    this.totalregistros = totalregistros;
  }

  public
  Integer getTamanioPagina ( ) {
    return this.tamanioPagina;
  }

  public
  void setTamanioPagina ( Integer tamanioPagina ) {
    this.tamanioPagina = tamanioPagina;
  }

  public
  Set < Map.Entry < String, String > > getValues ( ) {
    return this.values;
  }

  public
  void setValues ( Set < Map.Entry < String, String > > values ) {
    this.values = values;
  }

  public static
  enum Status {
    ERROR,
    OK;

    private
    Status ( ) {
    }
  }
}
