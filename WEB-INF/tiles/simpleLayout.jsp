<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<meta content="Sistema de gesti�n de Restaurants" name="description">

		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />

		<title><tiles:insertAttribute name="title" ignore="true" /></title>

		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.10.2.custom.css" />" />
		<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">

		<style type="text/css">
			
			#page-wrap{
				margin: auto;
				height: auto;
				min-height:100%;
			}
			 
			#page-wrap:after{
				height:20px;
				display:block;
				clear:both;
			}
			 
			#footer{
				height: 20px;
				margin: -20px auto 0;
				background: #313b53;
			}
						

			.btn-menu_lk{
				min-width: 100%; 
				text-align: left;
				width: 100%;
				margin-bottom: 5px;
			}
		</style>	
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-62424448-1', 'auto');
		  ga('send', 'pageview');
		
		</script>

		<script type="text/javascript">
			var url_logout 		= '${pageContext.servletContext.contextPath}/j_spring_security_logout';
			var url_application = '${pageContext.servletContext.contextPath}';
		</script>	

		<!-- JQuery -->
		<script src="<c:url value="/resources/js/jquery-1.10.2.js" />" ></script>
		<script src="<c:url value="/resources/js/jquery-ui-1.10.2.custom.min.js" />"></script>
	
		<!-- Versi�n compilada y comprimida del JavaScript de Bootstrap -->
		<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
		
		<script src="<c:url value="/resources/i18n/i18n_utils.js" />"></script>
	 
	    <!-- librer�as opcionales que activan el soporte de HTML5 para IE8 -->
	    <!--[if lt IE 9]>
	      <script src="<c:url value="/resources/js/html5shiv.js" />"></script>
	      <script src="<c:url value="/resources/js/respond.min.js" />"></script>
	    <![endif]-->		
	</head>
	<body>
		<div id="general_modal" class="modal fade"></div>
		<div id="page-wrap" class="container-fluid wrapper">
			<div class="row header">
				<tiles:insertAttribute name="header" />
			</div>		
			<div class="row fill" >
				<div class="col-md-12" style="background-color: rgb(226, 226, 226); margin-top: 10px;">
					<tiles:insertAttribute name="body" />
				</div>	
			</div>
		</div>
		<div id="footer" class="container-fluid">
			<div class="row">
   				<div class="col-xs-4 col-xs-offset-8">
   					<a href="http://www.none.com.ar" >
	   					<img src="<c:url value="/resources/images/logo-general.png" />" 
	   					alt="None" class="pull-right img-responsive"
	   					style="max-height: 20px;">
   					</a>
   				</div>
			</div>
		</div>
	</body>
</html>
