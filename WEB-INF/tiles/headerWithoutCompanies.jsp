
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"   uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>

<sec:authentication var="principal" property="principal" />

<% 
	java.util.Date date = new java.util.Date();
	long time = date.getTime();
	request.setAttribute("time", time);
%>

<style>

	html, body {
	    height: 100%;
	    background: #e2e2e2
	}
	
	.header {
	    background: #313b53;
		color: #e2e2e2;
		min-height: 100px;
	}
	
	.popup {
	    background-color: #FFFFFF;
	    border-radius: 10px;
	    box-shadow: 0 0 25px 5px #999999;
	    color: #111111;
	    min-width: 450px;
	    padding: 25px;
	}

.button.b-close, .button.bClose {
    border-radius: 7px;
    box-shadow: none;
    font: bold 131% sans-serif;
    padding: 0 6px 2px;
    position: absolute;
    right: -7px;
    top: -7px;
}
	
</style>

<script type='text/javascript'
	src="<c:url value="/resources/js/general.utils.js" />"></script>
	
<div class="col-md-9">
	<a href="${pageContext.servletContext.contextPath}/index">
		<img id="headerLogoEmpresa" class="img-responsive" ></img>
	</a>

</div>
<div class="col-md-3">
	<br>
	<div class="input-group input-group-xs">
  		<span class="input-group-addon">
  			<span class="glyphicon glyphicon-user"></span>
  		</span>
  		<input type="text" class="form-control" value="${username}" readonly="readonly">
  		<span class="input-group-addon label-danger">
				<a class="usuario" href='${pageContext.servletContext.contextPath}/j_spring_security_logout' >
		 		<span class="glyphicon glyphicon-log-out"></span>
			</a>
  		</span>
	</div>
</div>
